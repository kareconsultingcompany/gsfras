﻿using DataBase.DeviceMan;
using DataModel.DeviceModel;
using DataModel.SDK;
using FaceKit.EventInterface;
using GSFRAS.UserInfo;
using GSFRAS.View.Dialog.DialogRegisterFR;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GSFRAS.View
{
    public partial class FormFRUnitReg : Form, FaceEventWrapper
    {
        private readonly int SN = 1;
        FaceKit.FaceKit mKit;
        private int selectedRow = -1;
        private Hashtable mDeviceList = new Hashtable();
        private ArrayList mHardware;
        private delegate void UpdateData();

        public FormFRUnitReg()
        {
            Constant.LogHelper.SetReportClass(this.GetType());
            mKit = FaceKit.FaceKit.getInstance();
            mKit.setListener(this);
            InitializeComponent();
            showDevice();
            updateFRList();
        }

        #region Custom Method
        private void updateFRList()
        {
            cmbFRUnit.SelectedIndex = -1;
            cmbFRUnit.Items.Clear();
            mDeviceList = mKit.getDevices();
            ICollection key = mDeviceList.Keys;

            foreach (String k in key)
            {
                DeviceData aDevice = (DeviceData)mDeviceList[k];
                cmbFRUnit.Items.Add(mDeviceList[k]);
                cmbFRUnit.Update();


            }
            try
            {
                this.BeginInvoke((Action)delegate()
                {
                    cmbFRUnit.Update();
                });
            }
            catch (Exception)
            {
                cmbFRUnit.Update();
            }
        }

        private void showDevice()
        {
            mHardware = HardwareMan.getHardwareData();
            gvDevice.DataSource = HardwareMan.intialDataSource(mHardware);
        }

        private ArrayList getSelected()
        {
            ArrayList al = new ArrayList();
            foreach (DataGridViewRow dr in gvDevice.Rows)
            {
                if (dr.Cells[0].Value != null && (Boolean)(dr.Cells[0].Value) == true)
                {

                    String va = dr.Cells[SN].Value.ToString();
                    Constant.LogHelper.Info("Selected " + va, GUserInfo.getInstance().getCureentUser().ID.ToString());
                    al.Add(va);
                }
            }
            return al;

        }


        private void editRecord(object sender, EventArgs e)
        {
            try
            {
                DialogFRDevice dialog = new DialogFRDevice(DialogFRDevice.MODE_EDIT);
                dialog.setData((HardwareInfo)mHardware[selectedRow]);
                dialog.ShowDialog();
                showDevice();
            }
            catch (Exception)
            {
                MessageBox.Show("Device data problem.");
            }

        }
        private void deleteRecord(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("Are you sure to delete the selected record?", "Important Message", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
            if (dialogResult == DialogResult.OK)
            {
                HardwareInfo mInfo = (HardwareInfo)mHardware[selectedRow];
                Constant.LogHelper.Info("Delete device info " + mInfo.DeviceSVN, GUserInfo.getInstance().getCureentUser().ID.ToString());

                if (HardwareMan.deletehardware(mInfo.DeviceSVN))
                {
                    MessageBox.Show("Records uploaded successfully");
                    showDevice();
                }
                else
                {
                    MessageBox.Show("Data upload unsuccessfully. Please try again!", "Critical Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
        }

        private DeviceData getSelectData()
        {
            DeviceData aDevice = (DeviceData)cmbFRUnit.SelectedItem;
            return aDevice;
        }

        #endregion

        #region AuotoGenerate
        public void OnEventConnect()
        {
            try
            {
                Invoke(new UpdateData(updateFRList));
            }
            catch (Exception ex)
            {
                Constant.LogHelper.Warn("Device connect problem", ex, GUserInfo.getInstance().getCureentUser().ID.ToString());

            }
        }

        public void OnEventDisconnect()
        {
            try
            {
                Invoke(new UpdateData(updateFRList));
            }
            catch (Exception ex)
            {
                Constant.LogHelper.Warn("Device disconnect problem", ex, GUserInfo.getInstance().getCureentUser().ID.ToString());

            }
        }

        public void OnTrapAccess(string strDevSn, int lOpCode, int lUserData, int lExtendParam, int lUserID, string strRecTime, int lRecType, int lScore, int lStatus, int lPhotoType, int lPhotoLen, int lReason, string strBase64PhotoData)
        {
        }

        public void OnEventErrorNotify(string strDevSn, int lOpCode, int lUserData, int lExtendParam, int lErrorCode)
        {
        }

        public void OnError(string error, int errorCod)
        {
        }

        public void OnUploadStart(string message)
        {
        }

        public void OnUploadStarting(int count)
        {
        }

        public void OnUplading(int val, string message)
        {
        }

        public void OnUpladEnd()
        {
        }
        public void OnInsertDeviceProblem(int id, String name) { }


        private void btnSync_Click(object sender, EventArgs e)
        {
            updateFRList();
        }

        private void btnRegister_Click(object sender, EventArgs e)
        {
            if (cmbFRUnit.SelectedIndex > -1)
            {
                Constant.LogHelper.Info("Open device register form", GUserInfo.getInstance().getCureentUser().ID.ToString());
                DeviceData aData = getSelectData();
                if (aData.hinfo == null)
                {
                    DialogFRDevice dialog = new DialogFRDevice(DialogFRDevice.MODE_CREATE);
                    dialog.setData(aData);
                    dialog.ShowDialog();
                }
                else
                {
                    DialogFRDevice dialog = new DialogFRDevice(DialogFRDevice.MODE_EDIT);
                    dialog.setData(aData);
                    dialog.ShowDialog();
                }
                showDevice();
                mKit.refreshDevice();
                updateFRList();

            }
            else
            {
                MessageBox.Show("Please select item.");
            }

        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            ArrayList al = getSelected();
            if (al.Count == 0)
            {
                MessageBox.Show("Please select item");
            }
            else
            {
                foreach (String s in al)
                {
                    HardwareMan.deletehardware(s);
                }
                MessageBox.Show("Updated database");
                mKit.refreshDevice();
                showDevice();
            }

        }

        private void FormFRUnitReg_FormClosing(object sender, FormClosingEventArgs e)
        {
            mKit.setListener(null);
        }
        private void btnCloae_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void gvDevice_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                ContextMenu m = new ContextMenu();
                m.MenuItems.Add(new MenuItem("Edit", new EventHandler(editRecord)));
                m.MenuItems.Add(new MenuItem("Delete", new EventHandler(deleteRecord)));
                selectedRow = gvDevice.HitTest(e.X, e.Y).RowIndex;
                m.Show(gvDevice, new Point(e.X, e.Y));

            }
        }
        #endregion








    }
}

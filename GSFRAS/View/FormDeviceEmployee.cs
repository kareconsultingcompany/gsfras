﻿using DataBase.DeviceMan;
using DataBase.User;
using DataModel.SDK;
using FaceKit.EventInterface;
using GSFRAS.UserInfo;
using GSFRAS.View.Dialog.Waiting;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GSFRAS.View
{
    public partial class FormDeviceEmployee : Form, FaceEventWrapper
    {
        private readonly int EMPOLYEE_ID = 2;
        private readonly int USERNAME = 4;
        private readonly int EMPLOYEE = 3;
        private readonly int CARD_NO = 5;
        private readonly int VARIFY_MODE = 6;
        private delegate void UpdateData();
        private Boolean notClose = false;
        private FaceKit.FaceKit kit;

        //Defination in this form
        private int selectedRow = -1;
        private Boolean isCh = true;
        private Hashtable mDeviceList = new Hashtable();

        DialogWaiting waiting;
        Boolean isOpen = false;
        private ArrayList tmpUsers = new ArrayList();

        public FormDeviceEmployee()
        {
            Constant.LogHelper.SetReportClass(this.GetType());
            InitializeComponent();
            kit = FaceKit.FaceKit.getInstance();
            kit.setListener(this);
            setDataSet("");
            updateFRList();

        }
        #region SDKInterface
        public void OnEventConnect()
        {
            try
            {
                Invoke(new UpdateData(updateFRList));
            }
            catch (Exception ex)
            {
                Constant.LogHelper.Warn("Device connect problem", ex, GUserInfo.getInstance().getCureentUser().ID.ToString());

            }
        }

        public void OnEventDisconnect()
        {
            try
            {
                Invoke(new UpdateData(updateFRList));
            }
            catch (Exception ex)
            {
                Constant.LogHelper.Warn("Device disconnect problem", ex, GUserInfo.getInstance().getCureentUser().ID.ToString());

            }
        }

        public void OnTrapAccess(string strDevSn, int lOpCode, int lUserData, int lExtendParam, int lUserID, string strRecTime, int lRecType, int lScore, int lStatus, int lPhotoType, int lPhotoLen, int lReason, string strBase64PhotoData)
        {

        }

        public void OnEventErrorNotify(string strDevSn, int lOpCode, int lUserData, int lExtendParam, int lErrorCode)
        {
            Constant.LogHelper.Error("SDK error, error code:" + lErrorCode, GUserInfo.getInstance().getCureentUser().ID.ToString());
        }

        public void OnError(string error, int errorCod)
        {
            Constant.LogHelper.Error("SDK error, error:" + error, GUserInfo.getInstance().getCureentUser().ID.ToString());
        }
        public void OnUploadStart(String message)
        {
            waiting.Message = "In progress, please wait...";
            waiting.ProgressValue = 0;
        }

        int totalRecord = 0;
        public void OnUploadStarting(int count)
        {
            totalRecord = count;
        }
        public void OnUplading(int val, String message)
        {
            Constant.LogHelper.Info("Downloading " + message, GUserInfo.getInstance().getCureentUser().ID.ToString());
            waiting.Message = "Updating user: " + message + "; " + val + "/" + totalRecord;
            waiting.ProgressValue = (int)((val / (double)totalRecord) * 100);
        }
        public void OnUpladEnd()
        {
            waiting.Message = "Integrating data.";
        }


        public void OnInsertDeviceProblem(int id, String name) {

            tmpUser u = new tmpUser(id,name);
            tmpUsers.Add(u);
        }


        #endregion

        private void updateFRList()
        {
            cmbFRUnit.Items.Clear();
            mDeviceList = kit.getDevices();
            ICollection key = mDeviceList.Keys;

            foreach (String k in key)
            {
                DeviceData aDevice = (DeviceData)mDeviceList[k];
                cmbFRUnit.Items.Add(mDeviceList[k]);
                // cmbFRUnit.Update();


            }
            try
            {
                this.BeginInvoke((Action)delegate()
               {
                   cmbFRUnit.Update();
               });
            }
            catch (Exception)
            {
                cmbFRUnit.Update();
            }
        }




        private void setDataSet(String key)
        {
            DataTable dt = DataBase.DeviceMan.DeviceMan.getEmpolyeeInDevice(key);
            gvDevice.DataSource = dt;
            int count = dt.Rows.Count;
            labCount.Text = count.ToString();
        }

        private void setDataSet(int mode, bool isUserID, bool isUserName, String key)
        {
            DataTable dt = DataBase.DeviceMan.DeviceMan.getEmpolyeeInDevice(DataBase.DeviceMan.DeviceMan.getEmployeeDeviceDataByFilter(mode, isUserID, isUserName, key));
            gvDevice.DataSource = dt;
            int count = dt.Rows.Count;
            labCount.Text = count.ToString();
        }
        private void btnSearch_Click(object sender, EventArgs e)
        {
            Boolean isUserID = cbUserId.Checked;
            Boolean isUserName = cbUserName.Checked;
            if (isUserID == true || isUserName == true)
            {
                if (txtSearch.Text != "")
                {
                    if (radExclude.Checked)
                    {
                        setDataSet(DeviceMan.MODE_EXCLUDE, isUserID, isUserName, txtSearch.Text);
                    }
                    else if (radInclude.Checked)
                    {
                        setDataSet(DeviceMan.MODE_INCLUDE, isUserID, isUserName, txtSearch.Text);
                    }
                }
                else
                {
                    MessageBox.Show("Please input the key words");
                }
            }
            else
            {
                MessageBox.Show("Please select search item.");
            }

            //setDataSet(txtSearch.Text);
        }

        private ArrayList getSelectedDataID()
        {
            ArrayList al = new ArrayList();
            foreach (DataGridViewRow dr in gvDevice.Rows)
            {
                if (dr.Cells[0].Value != null && (Boolean)(dr.Cells[0].Value) == true)
                {

                    String va = dr.Cells[EMPLOYEE].Value.ToString();
                    /*String va1 = dr.Cells[1].Value.ToString();
                    String va2 = dr.Cells[2].Value.ToString(); ;
                    string va3 = dr.Cells[3].Value.ToString(); ;
                    string va4 = dr.Cells[4].Value.ToString(); ;
                    string va5 = dr.Cells[5].Value.ToString(); ;
                    string va6 = dr.Cells[6].Value.ToString(); ;*/
                  
                    Constant.LogHelper.Info("Selected " + va, GUserInfo.getInstance().getCureentUser().ID.ToString());

                    al.Add(Convert.ToInt32(va));
                }
            }
            return al;
        }

        private ArrayList getSelectedDataByRow()
        {
            ArrayList al = new ArrayList();
            String va = gvDevice.Rows[selectedRow].Cells[EMPOLYEE_ID].Value.ToString();
            al.Add(Convert.ToInt32(DataBase.User.UserMan.generateID(va)));
            return al;
        }

        private void selectFromCSV(ArrayList al)
        {
            int selected = 0;
            foreach (DataGridViewRow dr in gvDevice.Rows)
            {
                foreach (DataModel.DeviceModel.UserDevice ud in al)
                {
                    int idt = 0;
                    String name = "";
                    try
                    {
                        name = dr.Cells[USERNAME].Value.ToString().Trim();
                        idt = Convert.ToInt32(DataBase.User.UserMan.generateID(dr.Cells[EMPOLYEE_ID].Value.ToString()));

                    }
                    catch (Exception ex)
                    {
                        idt = 0;
                        Constant.LogHelper.Warn(ex.Message,GUserInfo.getInstance().getCureentUser().ID.ToString());
                    }
                    if (ud.EmployeeID == idt/* || ud.UserName.Trim() == name.Trim()*/)
                    {
                        dr.Cells[0].Value = true;
                        selected++;
                    }
                }

            }
            labUpdateMessage.Text = selected + " Selected";
        }

        private void selectionAll(Boolean isCheck)
        {
            foreach (DataGridViewRow dr in gvDevice.Rows)
            {

                dr.Cells[0].Value = isCheck;

            }
            if (isCheck)
            {
                btnSelectAll.Text = "Deselect All";
            }
            else
            {
                btnSelectAll.Text = "Select All";
            }
        }
        private ArrayList getSelectedData()
        {
            return DeviceMan.getSelectedEmployeeData(getSelectedDataID());
        }

        private String getCurrentDevice()
        {
            String sn = "";
            DeviceData aDevice = (DeviceData)cmbFRUnit.SelectedItem;
            if (aDevice != null)
            {
                sn = aDevice.deviceSn;
            }
            return sn;
        }
        private void btnImport_Click(object sender, EventArgs e)
        {

            OpenFileDialog fo = new OpenFileDialog();
            fo.Filter = "csv file|*.csv";
            fo.ShowDialog();
            if (fo.FileName != "")
            {
                Constant.LogHelper.Info("Import csv", GUserInfo.getInstance().getCureentUser().ID.ToString());

                String[] fileName = fo.FileName.Split('\\');
                txtFilePath.Text = fo.FileName;
                ArrayList al = DeviceMan.getCSV(fo.FileName, fileName[fileName.Length - 1]);
                selectFromCSV(al);
            }
            // DeviceMan.getCSV();
        }



        private void showSaveDialog()
        {
            Constant.LogHelper.Info("Export csv", GUserInfo.getInstance().getCureentUser().ID.ToString());

            SaveFileDialog saveFileDialog1 = new SaveFileDialog();

            saveFileDialog1.InitialDirectory = @"C:\";

            saveFileDialog1.Title = "Save CSV Files";

            saveFileDialog1.Filter = "CSV (*.csv)|*.*";

            saveFileDialog1.FilterIndex = 2;

            saveFileDialog1.RestoreDirectory = true;

            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                String path = saveFileDialog1.FileName;
                if (!path.Contains(".csv")) { path += ".csv"; }
                ArrayList al = getSelectedData();
                DeviceMan.exportCSV(al, path);
                labUpdateMessage.Text = "Export " + al.Count + " records";
            }
        }



        private void editRecord(object sender, EventArgs e)
        {
            try
            {
                DataModel.DeviceModel.UserDevice user = DeviceMan.getUser(getSelectedDataByRow()[0].ToString());
                DialogEmployeeEdit EmployeeEdit = new DialogEmployeeEdit();
                EmployeeEdit.setUser(ref user);
                EmployeeEdit.ShowDialog();
                gvDevice.Rows[selectedRow].Cells[EMPLOYEE].Value = user.EmployeeID;
                gvDevice.Rows[selectedRow].Cells[EMPOLYEE_ID].Value = UserMan.eidToHKID(user.EmployeeID.ToString());
                gvDevice.Rows[selectedRow].Cells[CARD_NO].Value = user.CardNo;
                gvDevice.Rows[selectedRow].Cells[VARIFY_MODE].Value = user.VerfyMode;
                gvDevice.Rows[selectedRow].Cells[USERNAME].Value = user.UserName;


                // setDataSet("");
            }
            catch (Exception)
            {
                MessageBox.Show("Employee ID problem.");
            }

        }
        private void deleteRecord(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("Are you sure to delete the selected record?", "Important Message", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
            if (dialogResult == DialogResult.OK)
            {
                Constant.LogHelper.Info("Delete user", GUserInfo.getInstance().getCureentUser().ID.ToString());

                if (DeviceMan.deleteRecord(getSelectedDataByRow()))
                {
                    MessageBox.Show("Records uploaded successfully");
                    setDataSet("");
                }
                else
                {
                    MessageBox.Show("Data upload unsuccessfully. Please try again!", "Critical Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
        }


        private void buttonControl(Boolean isEnable)
        {
            foreach (Control c in Controls)
            {
                Button b = c as Button;
                if (b != null)
                {
                    b.Enabled = isEnable;
                }
            }
            notClose = !isEnable;
        }



        /*********************auto generate************************/

        private void btnDelete_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("Are you sure to delete the selected record?", "Important Message", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
            if (dialogResult == DialogResult.OK)
            {
                if (DeviceMan.deleteRecord(getSelectedDataID()))
                {
                    MessageBox.Show("Selected record deleted successfully", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    setDataSet("");
                }
                else
                {
                    MessageBox.Show("Selected record deleted unsuccessfully. Please try again!", "Critical Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
        }
        private void gvDevice_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                ContextMenu m = new ContextMenu();
                m.MenuItems.Add(new MenuItem("Edit", new EventHandler(editRecord)));
                m.MenuItems.Add(new MenuItem("Delete", new EventHandler(deleteRecord)));
                selectedRow = gvDevice.HitTest(e.X, e.Y).RowIndex;
                m.Show(gvDevice, new Point(e.X, e.Y));

            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            setDataSet("");
        }


        private void btnExport_Click(object sender, EventArgs e)
        {
            showSaveDialog();
        }
        private void btnSelectAll_Click(object sender, EventArgs e)
        {
            selectionAll(isCh);
            isCh = !isCh;

        }
        private void btnDownloadFrUnit_Click(object sender, EventArgs e)
        {
            if (!isOpen)
            {
                if (cmbFRUnit.SelectedIndex >= 0)
                {
                    Constant.LogHelper.Info("Start to upload fr unit data", GUserInfo.getInstance().getCureentUser().ID.ToString());

                    buttonControl(false);
                    isOpen = true;
                    waiting = new DialogWaiting();
                    waiting.Show();

                    ArrayList allUser = kit.getAllUserWithFeature(getCurrentDevice());
                    this.OnUpladEnd();
                    if (DeviceMan.updateData(allUser))
                    {
                        waiting.Close();
                        isOpen = false;
                        MessageBox.Show("Employee record updated successful!", "Successfully!");
                        setDataSet("");
                        labUpdateMessage.Text = allUser.Count + " New Employee records uploaded";

                    }
                    else
                    {
                        waiting.Close();
                        isOpen = false;
                        MessageBox.Show("Data Error!! Please try again!");
                    }
                    buttonControl(true);

                }
                else
                {
                    MessageBox.Show("Please select a FR unit and start synchronization...", "Critical Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
        }



        private void btnUploadFBUnit_Click_1(object sender, EventArgs e)
        {
            if (!isOpen)
            {
                if (cmbFRUnit.SelectedIndex >= 0)
                {
                    buttonControl(false);
                    isOpen = true;
                    waiting = new DialogWaiting();
                    waiting.Show();

                    Constant.LogHelper.Info("download data to fr unit", GUserInfo.getInstance().getCureentUser().ID.ToString());
                    ArrayList al = getSelectedData();
                    if (kit.AddUsersWithFeature(al, getCurrentDevice()))
                    {
                        waiting.Close();
                        isOpen = false;
                        labUpdateMessage.Text = "Updated " + al.Count + " records";
                        if (tmpUsers.Count > 0)
                        {
                            String notok = "Please check following user:\n\r";
                            foreach (tmpUser u in tmpUsers)
                            {
                                notok += u.username + " (" + u.userid + ");";
                            }
                            MessageBox.Show(notok);
                        }
                        else {
                            MessageBox.Show("Employee record updated", "Successfully!");
                        }
                        tmpUsers.Clear();
                    }
                }
                else
                {
                    MessageBox.Show("Please select a FR unit and start synchronization...", "Critical Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }

                buttonControl(true);
            }

        }
        private void btnSync_Click(object sender, EventArgs e)
        {
            updateFRList();

        }


        private void FormDeviceEmployee_Load(object sender, EventArgs e)
        {
            labUpdateMessage.Text = "0";
        }

        private void btnDelete_Click_1(object sender, EventArgs e)
        {

            DialogResult dialogResult = MessageBox.Show("Are you sure to delete the selected record?", "Important Message", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
            if (dialogResult == DialogResult.OK)
            {
                Constant.LogHelper.Info("Delete user", GUserInfo.getInstance().getCureentUser().ID.ToString());

                if (DeviceMan.deleteRecord(getSelectedDataID()))
                {
                    MessageBox.Show("Selected record deleted successfully", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    setDataSet("");
                }
                else
                {
                    MessageBox.Show("Selected record deleted unsuccessfully. Please try again!", "Critical Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
        }

        private void btnDuplicate_Click(object sender, EventArgs e)
        {
            DataTable dt = DataBase.DeviceMan.DeviceMan.getEmpolyeeInDevice(DataBase.DeviceMan.DeviceMan.getDuplicateData());
            gvDevice.DataSource = dt;
            int count = dt.Rows.Count;
            labCount.Text = count.ToString();
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            setDataSet("");
        }

        private void FormDeviceEmployee_FormClosed(object sender, FormClosedEventArgs e)
        {
            kit.setListener(null);

        }
        private void DeviceRecord_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = notClose;
        }



        private void btnCreate_Click(object sender, EventArgs e)
        {
            DialogEmployeeEdit dia = new DialogEmployeeEdit();
            dia.ShowDialog();
            Refresh();

        }

        public class tmpUser
        {
            public int userid;
            public String username;
            public tmpUser(int userid, String username) {
                this.userid = userid;
                this.username = username;
            }

        }
    }
}

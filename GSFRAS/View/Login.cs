﻿using DataBase.User;
using GSFRAS.UserInfo;
using System;
using System.Windows.Forms;

namespace GSFRAS.View
{
    public partial class Login : Form
    {
        public Login()
        {
            Constant.LogHelper.SetReportClass(this.GetType());
            Constant.LogHelper.Info("Login page start", "");
            InitializeComponent();
            labVersion.Text = Constant.Constants.VERSION;
            labProduct.Text = Constant.Constants.PRODUCT_ID;
        }


        #region UserMethod
        private void openMain()
        {
            Main main = new Main();
            main.ShowDialog();
        }
        #endregion

        #region AutoGenerate
        private void btnLogin_Click_1(object sender, EventArgs e)
        {
            try
            {
                String username = txtUserName.Text;
                String pw = txtPassword.Text;
                if (username == "" || pw == "")
                {
                    MessageBox.Show("Please input the user name or password.");
                    return;
                }

                DataModel.User.UserInfo user = UserMan.checkUser(username, pw);
                if (user != null)
                {
                    GUserInfo.getInstance().setCurrentUser(user);
                    Constant.LogHelper.Info("Login success", GUserInfo.getInstance().getCureentUser().ID.ToString());
                    openMain();
                    txtUserName.Text = "";
                    txtPassword.Text = "";
                }
                else
                {
                    MessageBox.Show("Invalid user name or password.");
                    Constant.LogHelper.Info("Login fail, login user name is " + username + " and password is " + pw, "");

                }
            }
            catch (Exception ex) { Constant.LogHelper.Error("System problem", ex, ""); }
        }

        private void btnExit_Click_1(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void txtPassword_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Enter)
            {
                btnLogin.PerformClick();
            }
        }

        #endregion
    }
}

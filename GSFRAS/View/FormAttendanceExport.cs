﻿using DataBase.AttendanceMan;
using DataBase.User;
using DataModel.User;
using GSFRAS.UserInfo;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GSFRAS.View
{
    public partial class FormAttendanceExport : Form
    {

       // private Boolean isCh = true;
       // private readonly int ATTENDANCE_ID = 1;
        private ArrayList export;

        public FormAttendanceExport()
        {
            Constant.LogHelper.SetReportClass(this.GetType());
            InitializeComponent();
            getAttendance(null, null, null, null, 0);
        }


        private void getAttendance(double? startTime, double? endTime, float? employeeID, double? deviceID, int isSuccess)
        {
                export = AttendanceMan.getAttendanceData(startTime, endTime, employeeID, deviceID, isSuccess);

          //  if(radAll.Checked){
                gvDevice.DataSource = AttendanceMan.getAttendanceInDatabase(export);
          //  }
          /*  else if(radFI.Checked){

                export=AttendanceMan.attendanceList(AttendanceMan.TYPE_FI,export,Constant.Constants.ConvetTimestamp((double)startTime,Constant.Constants.TIME_ZONE),Constant.Constants.ConvetTimestamp((double)endTime,Constant.Constants.TIME_ZONE));
                gvDevice.DataSource = AttendanceMan.getAttendaceByFilter(export);
            }
            else if(radFILO.Checked){
                export = AttendanceMan.attendanceList(AttendanceMan.TYPE_FIFO, export, Constant.Constants.ConvetTimestamp((double)startTime, Constant.Constants.TIME_ZONE), Constant.Constants.ConvetTimestamp((double)endTime, Constant.Constants.TIME_ZONE));
                gvDevice.DataSource = AttendanceMan.getAttendaceByFilter(export);
            }*/
          
        }

        private void exportData(ArrayList al)
        {

            SaveFileDialog saveFileDialog1 = new SaveFileDialog();

            saveFileDialog1.InitialDirectory = @"C:\";

            saveFileDialog1.Title = "Save CSV Files";

            saveFileDialog1.Filter = "CSV (*.csv)|*.*";

            saveFileDialog1.FilterIndex = 2;

            saveFileDialog1.RestoreDirectory = true;

            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                String path = saveFileDialog1.FileName;
                if (!path.Contains(".csv")) { path += ".csv"; }
                AttendanceMan.exportCSV(al,path);
                txtFilePath.Text = path;
                MessageBox.Show("Saved");
                //DeviceMan.exportCSV(al, path);
                //labUpdateMessage.Text = "Export " + al.Count + " records";
            }
        }

       


       /* private void searchByAllKey()
        {
            if (dtpStart.Value > dtpEnd.Value)
            {
                MessageBox.Show("Please select the validity start date and end date.");
                return;
            }
            float? empolyID = null;
            if (txtEmploy.Text == "") { empolyID = null; }
            else
            {
                try
                {
                    empolyID = float.Parse(txtEmploy.Text);
                }
                catch (Exception) { MessageBox.Show("Invalidate input in Employee text box"); return; }

            }

            float? deviceSN = null;
            if (txtDeviceSN.Text == "") { deviceSN = null; }
            else
            {
                try
                {
                    deviceSN = float.Parse(txtDeviceSN.Text);
                }
                catch (Exception)
                {
                    MessageBox.Show("Imvalidate input in Device SN text box.");
                }
            }

            double? startTime = Constant.Constants.ConvertDateToTimestamp(dtpStart.Value, Constant.Constants.TIME_ZONE);
            double? endTime = Constant.Constants.ConvertDateToTimestamp(dtpEnd.Value, Constant.Constants.TIME_ZONE);
            getAttendance(startTime, endTime, empolyID, deviceSN, 0);

        }*/

        private void searchByKey()
        {
            double? startTime = Constant.Constants.ConvertDateToTimestamp(Constant.Constants.ConvertStandardDate(dtpStart.Value.ToString("yyyy-MM-dd") + " 00:00:00"), Constant.Constants.TIME_ZONE);
            double? endTime = Constant.Constants.ConvertDateToTimestamp(Constant.Constants.ConvertStandardDate(dtpEnd.Value.ToString("yyyy-MM-dd") + " 23:59:59"), Constant.Constants.TIME_ZONE);
            if (startTime > endTime)
            {

                MessageBox.Show("Start date cannot be greater than end date");
            }
            else if (startTime == endTime)
            {
                float? empolyID = null;
                if (txtEmploy.Text == "") { empolyID = null; }
                else
                {
                    try
                    {
                        empolyID = float.Parse(UserMan.generateID(txtEmploy.Text));
                    }
                    catch (Exception) { MessageBox.Show("Invalidate input in Employee text box"); return; }

                }

                float? deviceSN = null;
                if (txtDeviceSN.Text == "") { deviceSN = null; }
                else
                {
                    try
                    {
                        deviceSN = float.Parse(txtDeviceSN.Text, CultureInfo.InvariantCulture.NumberFormat);
                    }
                    catch (Exception)
                    {
                        MessageBox.Show("Imvalidate input in Device SN text box.");
                    }
                }
               // DateTime ch = dtpStart.Value.AddDays(-1);
               // double? startTimeC = Constant.Constants.ConvertDateToTimestamp(ch,Constant.Constants.TIME_ZONE);
               // double? endTime = Constant.Constants.ConvertDateToTimestamp(dtpEnd.Value,Constant.Constants.TIME_ZONE);
                getAttendance(startTime, endTime, empolyID, deviceSN, 0);
            }
            else
            {
                float? empolyID = null;
                if (txtEmploy.Text == "") { empolyID = null; }
                else
                {
                    try
                    {
                        empolyID = float.Parse(txtEmploy.Text, CultureInfo.InvariantCulture.NumberFormat);
                    }
                    catch (Exception) { MessageBox.Show("Invalidate input in Employee text box"); return; }

                }

                double? deviceSN = null;
                if (txtDeviceSN.Text == "") { deviceSN = null; }
                else
                {
                    try
                    {
                        deviceSN = double.Parse(txtDeviceSN.Text);
                    }
                    catch (Exception)
                    {
                        MessageBox.Show("Invalidate input in Device SN text box.");
                    }
                }

                //double? startTime = Constant.Constants.ConvertDateToTimestamp(dtpStart.Value, Constant.Constants.TIME_ZONE);
                //double? endTime = Constant.Constants.ConvertDateToTimestamp(dtpEnd.Value, Constant.Constants.TIME_ZONE);
                getAttendance(startTime, endTime, empolyID, deviceSN, 0);
            }


        }



    /*    private ArrayList getSelectedDataID()
        {
            ArrayList al = new ArrayList();
            int i = 0;
            foreach (DataGridViewRow dr in gvDevice.Rows)
            {
                //if (dr.Cells[0].Value != null && (Boolean)(dr.Cells[0].Value) == true)
               // {
                    //String va = dr.Cells[ATTENDANCE_ID].Value.ToString();
                    al.Add(i);
                //}
                i++;
            }
            return al;
        }
        */
        /***********************Auto generate******************************/
        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            ArrayList csv = new ArrayList();
          //  ArrayList selected = getSelectedDataID();
            //ArrayList datas = AttendanceMan.getAttendanceByID(selected);
            if (radAll.Checked)
            {
                Constant.LogHelper.Info("Export all format", GUserInfo.getInstance().getCureentUser().ID.ToString());
                foreach (DataModel.User.AttendanceInfo info in export)
                {
                    ExportAtendance ex = new ExportAtendance();
                    ex.Code =UserMan.eidToHKID( info.EmployeeID.ToString());
                    try
                    {
                        ex.Name = DataBase.DeviceMan.DeviceMan.getUser(info.EmployeeID.ToString()).UserName;
                    }
                    catch(Exception){}
                    ex.TransactionDate = info.getDateTime.ToString(Constant.Constants.DATETIME_FORMAT);
                    ex.ReaderID1 =info.DeviceID.ToString();
                    csv.Add(ex);
                }

            }
            else if (radFI.Checked)
            {
                /*foreach (int index in selected) {
                    csv.Add(export[index]);
                }  */
                Constant.LogHelper.Info("Export fi", GUserInfo.getInstance().getCureentUser().ID.ToString());

                csv=AttendanceMan.attendanceList(AttendanceMan.TYPE_FI, export, dtpStart.Value, dtpEnd.Value);
            }
            else if(radFILO.Checked){
                Constant.LogHelper.Info("Export filo", GUserInfo.getInstance().getCureentUser().ID.ToString());

                csv = AttendanceMan.attendanceList(AttendanceMan.TYPE_FIFO, export, dtpStart.Value, dtpEnd.Value);
            }
            else
            {
                MessageBox.Show("Please select export type.");
                return;
            }

            exportData(csv);

        }

      

        private void btnSearch_Click(object sender, EventArgs e)
        {
           // searchByAllKey();
            searchByKey();

        }

        private void txtEmploy_TextChanged(object sender, EventArgs e)
        {
            searchByKey();

        }

        private void txtDeviceSN_TextChanged(object sender, EventArgs e)
        {
            searchByKey();
        }

        private void btnShowAll_Click(object sender, EventArgs e)
        {
            getAttendance(null, null, null, null, 0);
        }

        private void dtpStart_ValueChanged(object sender, EventArgs e)
        {
            searchByKey();
        }


    }
}

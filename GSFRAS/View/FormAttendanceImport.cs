﻿using DataBase.AttendanceMan;
using DataModel.SDK;
using FaceKit.EventInterface;
using GSFRAS.UserInfo;
using GSFRAS.View.Dialog.Waiting;
using System;
using System.Collections;
using System.Data;
using System.Windows.Forms;

namespace GSFRAS.View
{
    public partial class FormAttendanceImport : Form, FaceEventWrapper
    {

        private FaceKit.FaceKit kit;
        private delegate void UpdateData();
        private Hashtable mDeviceList = new Hashtable();
        private Boolean isOpen = false;
        private DialogWaiting waiting;
        private Boolean notClose = false;

        public FormAttendanceImport()
        {
            Constant.LogHelper.SetReportClass(this.GetType());
            InitializeComponent();
            kit = FaceKit.FaceKit.getInstance();
            kit.setListener(this);
            updateFRList();
        }

        #region SDKInterface
        public void OnEventConnect()
        {
            try
            {
                Invoke(new UpdateData(updateFRList));
            }
            catch (Exception) { }
        }

        public void OnEventDisconnect()
        {
            try
            {
                Invoke(new UpdateData(updateFRList));
            }
            catch (Exception) { }
        }

        public void OnTrapAccess(string strDevSn, int lOpCode, int lUserData, int lExtendParam, int lUserID, string strRecTime, int lRecType, int lScore, int lStatus, int lPhotoType, int lPhotoLen, int lReason, string strBase64PhotoData)
        {
        }

        public void OnEventErrorNotify(string strDevSn, int lOpCode, int lUserData, int lExtendParam, int lErrorCode)
        {
        }

        public void OnError(string error, int errorCod)
        {
        }

        public void OnUploadStart(String message)
        {
            waiting.Message = "In progress, please wait...";
            waiting.ProgressValue = 0;
        }

        int totalRecord = 0;
        public void OnUploadStarting(int count)
        {
            totalRecord = count;
        }
        public void OnUplading(int val, String message)
        {
           // Constant.LogHelper.Info("Downloading " + message, GUserInfo.getInstance().getCureentUser().ID.ToString());
            waiting.Message = val + "/" + totalRecord;
            waiting.ProgressValue = (int)((val / (double)totalRecord) * 100);
        }
        public void OnUpladEnd()
        {
            waiting.Message = "Integrating data.";
        }
        public void OnInsertDeviceProblem(int id, String name) { }

        #endregion
        private void updateFRList()
        {
            cmbFRUnit.Items.Clear();
            mDeviceList = kit.getDevices();
            ICollection key = mDeviceList.Keys;

            foreach (String k in key)
            {
                DeviceData aDevice = (DeviceData)mDeviceList[k];
                cmbFRUnit.Items.Add(mDeviceList[k]);
                cmbFRUnit.Update();


            }
            try
            {
                this.BeginInvoke((Action)delegate()
                {
                    cmbFRUnit.Update();
                });
            }
            catch (Exception)
            {
                cmbFRUnit.Update();
            }
        }

        private String getCurrentDevice()
        {
            String sn = "";
            DeviceData aDevice = (DeviceData)cmbFRUnit.SelectedItem;
            if (aDevice != null)
            {
                sn = aDevice.deviceSn;
            }
            return sn;
        }

        #region AutoGenerate
        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void attendance_import_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = notClose;
        }

        private void btnSync_Click(object sender, EventArgs e)
        {
            updateFRList();
        }

        private void btnDownloadFrUnit_Click(object sender, EventArgs e)
        {
            if (!isOpen)
            {
                if (cmbFRUnit.SelectedIndex >= 0)
                {
                    

                    Constant.LogHelper.Info("Import attendance " + dtpStart.Value.ToString("yyyy-MM-dd") + " 00:00:00" + " - " + dtpEnd.Value.ToString("yyyy-MM-dd") + " 23:59:59", GUserInfo.getInstance().getCureentUser().ID.ToString());
                    double start = Constant.Constants.ConvertDateToTimestamp(dtpStart.Value,Constant.Constants.TIME_ZONE);
                    double end = Constant.Constants.ConvertDateToTimestamp(dtpEnd.Value, Constant.Constants.TIME_ZONE);
                    if (start <= end)
                    {
                        buttonControl(false);
                        isOpen = true;
                        waiting = new DialogWaiting();
                        waiting.Show();
                        String startDate = dtpStart.Value.ToString("yyyy-MM-dd") + " 00:00:00";
                        String endDate = dtpEnd.Value.ToString("yyyy-MM-dd") + " 23:59:59";
                        ArrayList access = kit.getUserAccess(getCurrentDevice(), startDate, endDate, true);
                        //MessageBox.Show("");

                        if (AttendanceMan.createAttendance(access))
                        {
                            waiting.Close();
                            isOpen = false;
                            MessageBox.Show("Successful");
                            DataTable su = AttendanceMan.getAttendanceAfterInsert(access, 0);
                            gvDevice.DataSource = su;
                            txtUpdate.Text = su.Rows.Count.ToString();


                        }
                        else {
                            isOpen = false;
                        }
                        /*    if (DeviceMan.updateData(allUser))
                            {
                                MessageBox.Show("Successful");
                                setDataSet("");
                                labUpdateMessage.Text = allUser.Count + "users added";

                            }
                            else
                            {
                                MessageBox.Show("Data Error!!");
                            }*/
                    }
                    else
                    {
                        isOpen = false;

                        MessageBox.Show("Start date must be greater than or equal end date. Please check it.");
                    }
                }
                else
                {
                    isOpen = false;
                    MessageBox.Show("Please synchronize the application and select the item");
                }
                buttonControl(true);

            }
        }


        #endregion
        private void buttonControl(Boolean isEnable)
        {
            foreach (Control c in Controls)
            {
                Button b = c as Button;
                if (b != null)
                {
                    b.Enabled = isEnable;
                }
            }
            notClose = !isEnable;
        }

        private void FormAttendanceImport_FormClosed(object sender, FormClosedEventArgs e)
        {
            kit.setListener(null);
        }
    }
}

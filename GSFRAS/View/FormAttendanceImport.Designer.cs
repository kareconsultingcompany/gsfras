﻿namespace GSFRAS.View
{
    partial class FormAttendanceImport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.label1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.cmbFRUnit = new System.Windows.Forms.ComboBox();
            this.btnSync = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnDownloadFrUnit = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.gvDevice = new System.Windows.Forms.DataGridView();
            this.TransactionDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DeviceID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EmployeeID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ARID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txtUpdate = new System.Windows.Forms.Label();
            this.labProcess = new System.Windows.Forms.Label();
            this.labTotalSuccess = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.labTotalRecord = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.dtpEnd = new System.Windows.Forms.DateTimePicker();
            this.dtpStart = new System.Windows.Forms.DateTimePicker();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gvDevice)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.Control;
            this.label1.Location = new System.Drawing.Point(7, 10);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(198, 20);
            this.label1.TabIndex = 18;
            this.label1.Text = "Import Attendance Record";
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label4.Location = new System.Drawing.Point(0, 0);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(704, 543);
            this.label4.TabIndex = 17;
            this.label4.Text = "label4";
            // 
            // cmbFRUnit
            // 
            this.cmbFRUnit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmbFRUnit.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbFRUnit.FormattingEnabled = true;
            this.cmbFRUnit.Location = new System.Drawing.Point(5, 17);
            this.cmbFRUnit.Margin = new System.Windows.Forms.Padding(2);
            this.cmbFRUnit.Name = "cmbFRUnit";
            this.cmbFRUnit.Size = new System.Drawing.Size(355, 25);
            this.cmbFRUnit.TabIndex = 9;
            // 
            // btnSync
            // 
            this.btnSync.BackColor = System.Drawing.SystemColors.WindowFrame;
            this.btnSync.FlatAppearance.BorderSize = 0;
            this.btnSync.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSync.ForeColor = System.Drawing.SystemColors.Control;
            this.btnSync.Location = new System.Drawing.Point(5, 46);
            this.btnSync.Name = "btnSync";
            this.btnSync.Size = new System.Drawing.Size(96, 25);
            this.btnSync.TabIndex = 11;
            this.btnSync.Text = "Refresh FR Unit";
            this.btnSync.UseVisualStyleBackColor = false;
            this.btnSync.Click += new System.EventHandler(this.btnSync_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.cmbFRUnit);
            this.groupBox1.Controls.Add(this.btnSync);
            this.groupBox1.Controls.Add(this.btnDownloadFrUnit);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.groupBox1.Location = new System.Drawing.Point(36, 92);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox1.Size = new System.Drawing.Size(364, 80);
            this.groupBox1.TabIndex = 26;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Select a FR Unit";
            // 
            // btnDownloadFrUnit
            // 
            this.btnDownloadFrUnit.BackColor = System.Drawing.SystemColors.WindowFrame;
            this.btnDownloadFrUnit.FlatAppearance.BorderSize = 0;
            this.btnDownloadFrUnit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDownloadFrUnit.ForeColor = System.Drawing.SystemColors.Control;
            this.btnDownloadFrUnit.Location = new System.Drawing.Point(106, 46);
            this.btnDownloadFrUnit.Margin = new System.Windows.Forms.Padding(2);
            this.btnDownloadFrUnit.Name = "btnDownloadFrUnit";
            this.btnDownloadFrUnit.Size = new System.Drawing.Size(254, 25);
            this.btnDownloadFrUnit.TabIndex = 12;
            this.btnDownloadFrUnit.Text = "Start upload from FR Unit";
            this.btnDownloadFrUnit.UseVisualStyleBackColor = false;
            this.btnDownloadFrUnit.Click += new System.EventHandler(this.btnDownloadFrUnit_Click);
            // 
            // label5
            // 
            this.label5.BackColor = System.Drawing.Color.DarkGray;
            this.label5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label5.Location = new System.Drawing.Point(13, 34);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(12, 502);
            this.label5.TabIndex = 30;
            // 
            // gvDevice
            // 
            this.gvDevice.AllowUserToAddRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.gvDevice.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.gvDevice.BackgroundColor = System.Drawing.SystemColors.WindowFrame;
            this.gvDevice.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gvDevice.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.TransactionDate,
            this.DeviceID,
            this.EmployeeID,
            this.ARID});
            this.gvDevice.GridColor = System.Drawing.SystemColors.WindowFrame;
            this.gvDevice.Location = new System.Drawing.Point(36, 176);
            this.gvDevice.Name = "gvDevice";
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.WindowFrame;
            this.gvDevice.RowsDefaultCellStyle = dataGridViewCellStyle2;
            this.gvDevice.RowTemplate.DefaultCellStyle.BackColor = System.Drawing.Color.Gray;
            this.gvDevice.Size = new System.Drawing.Size(658, 332);
            this.gvDevice.TabIndex = 31;
            // 
            // TransactionDate
            // 
            this.TransactionDate.DataPropertyName = "TransactionDate";
            this.TransactionDate.HeaderText = "Transaction Date";
            this.TransactionDate.Name = "TransactionDate";
            this.TransactionDate.Width = 180;
            // 
            // DeviceID
            // 
            this.DeviceID.DataPropertyName = "DeviceID";
            this.DeviceID.HeaderText = "Device ID";
            this.DeviceID.Name = "DeviceID";
            this.DeviceID.Width = 280;
            // 
            // EmployeeID
            // 
            this.EmployeeID.DataPropertyName = "EmployeeID";
            this.EmployeeID.HeaderText = "Employee ID";
            this.EmployeeID.Name = "EmployeeID";
            this.EmployeeID.Width = 150;
            // 
            // ARID
            // 
            this.ARID.DataPropertyName = "ARID";
            this.ARID.HeaderText = "ID";
            this.ARID.Name = "ARID";
            this.ARID.ReadOnly = true;
            this.ARID.Visible = false;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.txtUpdate);
            this.groupBox2.Controls.Add(this.labProcess);
            this.groupBox2.Controls.Add(this.labTotalSuccess);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.labTotalRecord);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.groupBox2.Location = new System.Drawing.Point(404, 92);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox2.Size = new System.Drawing.Size(290, 80);
            this.groupBox2.TabIndex = 27;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Information";
            // 
            // txtUpdate
            // 
            this.txtUpdate.AutoSize = true;
            this.txtUpdate.Location = new System.Drawing.Point(174, 22);
            this.txtUpdate.Name = "txtUpdate";
            this.txtUpdate.Size = new System.Drawing.Size(0, 15);
            this.txtUpdate.TabIndex = 38;
            // 
            // labProcess
            // 
            this.labProcess.AutoSize = true;
            this.labProcess.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labProcess.ForeColor = System.Drawing.SystemColors.Control;
            this.labProcess.Location = new System.Drawing.Point(4, 57);
            this.labProcess.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labProcess.Name = "labProcess";
            this.labProcess.Size = new System.Drawing.Size(112, 15);
            this.labProcess.TabIndex = 37;
            this.labProcess.Text = "Process completed";
            this.labProcess.Visible = false;
            // 
            // labTotalSuccess
            // 
            this.labTotalSuccess.AutoSize = true;
            this.labTotalSuccess.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labTotalSuccess.ForeColor = System.Drawing.SystemColors.Control;
            this.labTotalSuccess.Location = new System.Drawing.Point(161, 37);
            this.labTotalSuccess.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labTotalSuccess.Name = "labTotalSuccess";
            this.labTotalSuccess.Size = new System.Drawing.Size(0, 15);
            this.labTotalSuccess.TabIndex = 36;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.SystemColors.Control;
            this.label7.Location = new System.Drawing.Point(4, 37);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(121, 15);
            this.label7.TabIndex = 35;
            this.label7.Text = "Upload successfully :";
            // 
            // labTotalRecord
            // 
            this.labTotalRecord.AutoSize = true;
            this.labTotalRecord.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labTotalRecord.ForeColor = System.Drawing.SystemColors.Control;
            this.labTotalRecord.Location = new System.Drawing.Point(161, 20);
            this.labTotalRecord.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labTotalRecord.Name = "labTotalRecord";
            this.labTotalRecord.Size = new System.Drawing.Size(0, 15);
            this.labTotalRecord.TabIndex = 34;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.Control;
            this.label2.Location = new System.Drawing.Point(4, 20);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(168, 15);
            this.label2.TabIndex = 33;
            this.label2.Text = "Total Record to be uploaded :";
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(36, 514);
            this.progressBar1.Margin = new System.Windows.Forms.Padding(2);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(658, 19);
            this.progressBar1.TabIndex = 33;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label11);
            this.groupBox3.Controls.Add(this.label12);
            this.groupBox3.Controls.Add(this.dtpEnd);
            this.groupBox3.Controls.Add(this.dtpStart);
            this.groupBox3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.groupBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.ForeColor = System.Drawing.SystemColors.Control;
            this.groupBox3.Location = new System.Drawing.Point(36, 41);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox3.Size = new System.Drawing.Size(658, 49);
            this.groupBox3.TabIndex = 38;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Date";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.SystemColors.Control;
            this.label11.Location = new System.Drawing.Point(239, 20);
            this.label11.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(25, 17);
            this.label11.TabIndex = 39;
            this.label11.Text = "To";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.SystemColors.Control;
            this.label12.Location = new System.Drawing.Point(19, 20);
            this.label12.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(40, 17);
            this.label12.TabIndex = 38;
            this.label12.Text = "From";
            // 
            // dtpEnd
            // 
            this.dtpEnd.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpEnd.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpEnd.Location = new System.Drawing.Point(283, 18);
            this.dtpEnd.Margin = new System.Windows.Forms.Padding(2);
            this.dtpEnd.Name = "dtpEnd";
            this.dtpEnd.Size = new System.Drawing.Size(166, 23);
            this.dtpEnd.TabIndex = 7;
            // 
            // dtpStart
            // 
            this.dtpStart.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpStart.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpStart.Location = new System.Drawing.Point(59, 18);
            this.dtpStart.Margin = new System.Windows.Forms.Padding(2);
            this.dtpStart.Name = "dtpStart";
            this.dtpStart.Size = new System.Drawing.Size(166, 23);
            this.dtpStart.TabIndex = 6;
            // 
            // FormAttendanceImport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.ClientSize = new System.Drawing.Size(704, 543);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.gvDevice);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label4);
            this.ForeColor = System.Drawing.SystemColors.Control;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "FormAttendanceImport";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "attendance_import";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.attendance_import_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FormAttendanceImport_FormClosed);
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gvDevice)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cmbFRUnit;
        private System.Windows.Forms.Button btnSync;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnDownloadFrUnit;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DataGridView gvDevice;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label labTotalRecord;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label labTotalSuccess;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.Label labProcess;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.DateTimePicker dtpEnd;
        private System.Windows.Forms.DateTimePicker dtpStart;
        private System.Windows.Forms.DataGridViewTextBoxColumn TransactionDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn DeviceID;
        private System.Windows.Forms.DataGridViewTextBoxColumn EmployeeID;
        private System.Windows.Forms.DataGridViewTextBoxColumn ARID;
        private System.Windows.Forms.Label txtUpdate;
    }
}
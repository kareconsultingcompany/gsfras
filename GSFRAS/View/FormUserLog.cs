﻿using DataBase.Log;
using DataBase.User;
using GSFRAS.UserInfo;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GSFRAS.View
{
    public partial class FormUserLog : Form
    {
        public FormUserLog()
        {
            Constant.LogHelper.SetReportClass(this.GetType());
            Constant.LogHelper.SetReportClass(this.GetType());
            InitializeComponent();
            cmbUser.Items.AddRange(UserMan.getAllUser().ToArray());
        }

        private void search() {
            Constant.LogHelper.Info("Search log:" + dtpStart.Value.ToString(Constant.Constants.DATE_FORMAT) + " - " + dtpEnd.Value.AddDays(1).ToString(Constant.Constants.DATE_FORMAT), GUserInfo.getInstance().getCureentUser().ID.ToString());
            double startTime = Constant.Constants.ConvertDateToTimestamp(Constant.Constants.ConvertStandardDate(dtpStart.Value.ToString(Constant.Constants.DATE_FORMAT)), Constant.Constants.TIME_ZONE);
            double endTime = Constant.Constants.ConvertDateToTimestamp(Constant.Constants.ConvertStandardDate(dtpEnd.Value.ToString(Constant.Constants.DATE_FORMAT)), Constant.Constants.TIME_ZONE);
            if(startTime> endTime){
                MessageBox.Show("Start date cannot be greater than end date");
            }
            else if (startTime == endTime) {
                DateTime sd = Constant.Constants.ConvertStandardDate(dtpStart.Value.ToString(Constant.Constants.DATE_FORMAT));
                DateTime ed = Constant.Constants.ConvertStandardDate(dtpEnd.Value.ToString(Constant.Constants.DATE_FORMAT)).AddDays(1);
                String uid="";
                if(cmbUser.SelectedIndex<0){
                    MessageBox.Show("Please select the user");
                } 
                else {
                    int index = cmbUser.SelectedIndex;
                    DataModel.User.UserInfo user = (DataModel.User.UserInfo)cmbUser.Items[index];
                    uid = user.ID.ToString();
                    ArrayList al = LogMan.getLog(uid, sd, ed,DataModel.Type.LogType.INFO);
                    gvLog.DataSource = LogMan.getLogSources(al);
                }
                        
            }
            else {

                DateTime sd = Constant.Constants.ConvertStandardDate(dtpStart.Value.ToString(Constant.Constants.DATE_FORMAT));
                DateTime ed = Constant.Constants.ConvertStandardDate(dtpEnd.Value.ToString(Constant.Constants.DATE_FORMAT)).AddDays(1);
                String uid = "";
                if (cmbUser.SelectedIndex < 0)
                {
                    MessageBox.Show("Please select the user");

                } 
                else
                {
                    int index = cmbUser.SelectedIndex;
                    DataModel.User.UserInfo user = (DataModel.User.UserInfo)cmbUser.Items[index];
                    uid = user.ID.ToString();
                    ArrayList al = LogMan.getLog(uid, sd, ed, DataModel.Type.LogType.INFO);
                    gvLog.DataSource = LogMan.getLogSources(al);
                }


            }

        
        }

        /**********************Auto generate****************************/
        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            cmbUser.SelectedIndex = -1;
            dtpStart.Value = DateTime.Now;
            dtpEnd.Value = DateTime.Now;
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            search();

        }


    }
}

﻿using DataBase.User;
using DataModel.User;
using GSFRAS.UserInfo;
using GSFRAS.View.Dialog.DialogEmployee;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GSFRAS.View
{
    public partial class FormEmployee : Form
    {
        private readonly int EMPOLYEE_ID = 1;
        private int selectedRow = -1;
        private Boolean isCh = true;

        private ArrayList currentList = new ArrayList();
        public FormEmployee()
        {
            Constant.LogHelper.SetReportClass(this.GetType());
            InitializeComponent();
            initalData();
        }

        private void initalData() {
            currentList = EmployeeMan.getAllEmployee();
            gvDevice.DataSource = EmployeeMan.InitalData(currentList);
            labCount.Text = currentList.Count.ToString();

        }

        private void searchByKey() {
            String key = txtSearch.Text;
            if (key == "")
            {
                initalData();
            }
            else
            {
                currentList = EmployeeMan.searchEmployee(key);
                gvDevice.DataSource = EmployeeMan.InitalData(currentList);
                labCount.Text = currentList.Count.ToString();
            }
            txtSearch.Text = "";
        }

        private void searchDulpicate() {
            currentList = EmployeeMan.dulpicateEmployee();
            gvDevice.DataSource = EmployeeMan.InitalData(currentList);
            labCount.Text = currentList.Count.ToString();

        }

        private ArrayList getSelectedDataByRow()
        {
            ArrayList al = new ArrayList();
            String va = gvDevice.Rows[selectedRow].Cells[EMPOLYEE_ID].Value.ToString();
            al.Add(Convert.ToInt32(DataBase.User.UserMan.generateID(va)));
            return al;
        }


        private void editRecord(object sender, EventArgs e)
        {
            try
            {
                //DataModel.DeviceModel.UserDevice user = DeviceMan.getUser(getSelectedDataByRow()[0].ToString());
                EmployeeDevice ed = (EmployeeDevice)currentList[selectedRow];
                DialogEmployeeInfo info = new DialogEmployeeInfo();
                info.setInfo(ed);
                info.ShowDialog();
               // initalData();
               /* DialogEmployeeEdit EmployeeEdit = new DialogEmployeeEdit();
                EmployeeEdit.setUser(user);
                EmployeeEdit.ShowDialog();
                setDataSet("");*/
            }
            catch (Exception)
            {
                MessageBox.Show("Employee ID problem.");
            }

        }
        private void deleteRecord(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("Are you sure to delete the selected record?", "Important Message", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
            if (dialogResult == DialogResult.OK)
            {
               // Constant.LogHelper.Info("Delete user", GUserInfo.getInstance().getCureentUser().ID.ToString());
                ArrayList sel = getSelectedDataByRow();
                foreach (int va in sel){

                    EmployeeMan.deleteEmployee(va);
                }

                MessageBox.Show("Records delete successfully");
                initalData();
               
            }
        }

        private void selectionAll(Boolean isCheck)
        {
            foreach (DataGridViewRow dr in gvDevice.Rows)
            {

                dr.Cells[0].Value = isCheck;

            }
            if (isCheck)
            {
                btnSelectAll.Text = "Deselect All";
            }
            else
            {
                btnSelectAll.Text = "Select All";
            }
        }


        private ArrayList getSelectedDataID()
        {
            ArrayList al = new ArrayList();
            foreach (DataGridViewRow dr in gvDevice.Rows)
            {
                if (dr.Cells[0].Value != null && (Boolean)(dr.Cells[0].Value) == true)
                {

                    String va = dr.Cells[EMPOLYEE_ID].Value.ToString();
                    al.Add(Convert.ToInt32(va));
                }
            }
            return al;
        }



        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("Are you sure to delete the selected record?", "Important Message", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
            if (dialogResult == DialogResult.OK)
            {
                //Constant.LogHelper.Info("Delete user", GUserInfo.getInstance().getCureentUser().ID.ToString());
                ArrayList sel=getSelectedDataID();
                foreach(int va in sel){
                    EmployeeMan.deleteEmployee(va);
                    Constant.LogHelper.Info("Delete employee "+va, GUserInfo.getInstance().getCureentUser().ID.ToString());
                }

                MessageBox.Show("Selected record deleted successfully", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
                initalData();
              
            }
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            initalData();
        }

        private void btnCreate_Click(object sender, EventArgs e)
        {
            DialogEmployeeInfo dia = new DialogEmployeeInfo();
            dia.ShowDialog();
        }

        private void gvDevice_MouseUp(object sender, MouseEventArgs e)
        {

            if (e.Button == MouseButtons.Right)
            {
                ContextMenu m = new ContextMenu();
                m.MenuItems.Add(new MenuItem("Edit", new EventHandler(editRecord)));
                m.MenuItems.Add(new MenuItem("Delete", new EventHandler(deleteRecord)));
                selectedRow = gvDevice.HitTest(e.X, e.Y).RowIndex;
                m.Show(gvDevice, new Point(e.X, e.Y));

            }
        }

        private void btnSelectAll_Click(object sender, EventArgs e)
        {
            selectionAll(isCh);
            isCh = !isCh;
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            searchByKey();

        }

        private void btnDC_Click(object sender, EventArgs e)
        {
            searchDulpicate();
        }

        private void btnHavingDR_Click(object sender, EventArgs e)
        {
            currentList = EmployeeMan.missEmployee(true);
            gvDevice.DataSource = EmployeeMan.InitalData(currentList);
            labCount.Text = currentList.Count.ToString();
        }

        private void btnHavingER_Click(object sender, EventArgs e)
        {
            currentList = EmployeeMan.missEmployee(false); 
            gvDevice.DataSource = EmployeeMan.InitalData(currentList);
            labCount.Text = currentList.Count.ToString();
        }
    }
}

﻿using DataBase.User;
using GSFRAS.UserInfo;
using GSFRAS.View.Dialog.DialogUser;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GSFRAS.View
{
    public partial class FromUser : Form
    {

        private ArrayList userData;
        private int selectedRow = -1;

        public FromUser()
        {
            Constant.LogHelper.SetReportClass(this.GetType());
            InitializeComponent();
            search();
        }

        private ArrayList getSelectedData()
        {
            ArrayList al = new ArrayList();
            int i = 0;
            foreach (DataGridViewRow dr in gvUser.Rows)
            {
                if (dr.Cells[0].Value != null && (Boolean)(dr.Cells[0].Value) == true)
                {
                    if (userData != null && userData.Count > 0) { }
                    al.Add(userData[i]);
                }
                i++;
            }
            return al;
        }

        private void search()
        {
            String key = txtUsername.Text;
            if (key == "")
            {
                Constant.LogHelper.Info("Get all users data", GUserInfo.getInstance().getCureentUser().ID.ToString());

                userData = UserMan.getUserAllData();

            }
            else
            {
                Constant.LogHelper.Info("Search user: " + key, GUserInfo.getInstance().getCureentUser().ID.ToString());

                userData = UserMan.getUserAllData(key);
            }
            gvUser.DataSource = UserMan.IntialSources(userData);
        }

        private void createUser()
        {
            Constant.LogHelper.Info("Add user dialog open", GUserInfo.getInstance().getCureentUser().ID.ToString());
            DialogUserBox box = new DialogUserBox(DialogUserBox.MODE_CREATE);
            box.ShowDialog();
            txtUsername.Text = "";
            search();
        }

        private void unselectAll()
        {

            foreach (DataGridViewRow dr in gvUser.Rows)
            {
                if (dr.Cells[0].Value != null && (Boolean)(dr.Cells[0].Value) == true)
                {
                    dr.Cells[0].Value = false;
                }
            }
        }

        private void editRecord(object sender, EventArgs e)
        {
            try
            {

                DataModel.User.UserInfo info = (DataModel.User.UserInfo)userData[selectedRow];
                Constant.LogHelper.Info("Edit user " + info.ID, GUserInfo.getInstance().getCureentUser().ID.ToString());

                DialogUserBox aDialog = new DialogUserBox(DialogUserBox.MODE_EDIT);
                aDialog.setUser(info);
                aDialog.ShowDialog();
                search();
            }
            catch (Exception)
            {
                MessageBox.Show("User ID problem.");
            }

        }
        private void deleteRecord(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("Are you sure to delete the selected record?", "Important Message", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
            if (dialogResult == DialogResult.OK)
            {
                Constant.LogHelper.Info("Delete user", GUserInfo.getInstance().getCureentUser().ID.ToString());
                DataModel.User.UserInfo inf = (DataModel.User.UserInfo)userData[selectedRow];
                ArrayList deleteList = new ArrayList();
                deleteList.Add(inf);
                ArrayList al = UserMan.deleteUser(deleteList);
                foreach (DataModel.User.UserInfo info in al)
                {
                    Constant.LogHelper.Info("Delete user " + info.ID, GUserInfo.getInstance().getCureentUser().ID.ToString());

                }
                String message = "";
                if (al.Count == 0)
                {
                    MessageBox.Show("Delete successful");
                }
                else
                {

                    foreach (DataModel.User.UserInfo info in al)
                    {
                        message += info.Username + "\n";
                        Constant.LogHelper.Info("Fail to delete user " + info.ID, GUserInfo.getInstance().getCureentUser().ID.ToString());
                    }
                    MessageBox.Show("Following user is locked:\n" + message);

                }

                search();

            }
        }

        /*********************Auto generate*************************/
        private void btnCreate_Click(object sender, EventArgs e)
        {
            createUser();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            ArrayList selected = getSelectedData();
            ArrayList al = UserMan.deleteUser(selected);
            foreach (DataModel.User.UserInfo info in al)
            {
                Constant.LogHelper.Info("Delete user " + info.ID, GUserInfo.getInstance().getCureentUser().ID.ToString());

            }
            String message = "";
            if (al.Count == 0)
            {
                MessageBox.Show("Delete successful");
            }
            else
            {

                foreach (DataModel.User.UserInfo info in al)
                {
                    message += info.Username + "\n";
                    Constant.LogHelper.Info("Fail to delete user " + info.ID, GUserInfo.getInstance().getCureentUser().ID.ToString());
                }
                MessageBox.Show("Following user is locked:\n" + message);

            }

            search();

        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            txtUsername.Text = "";
            search();
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            search();

        }

        private void txtUsername_TextChanged(object sender, EventArgs e)
        {
            search();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void gvUser_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                ContextMenu m = new ContextMenu();
                m.MenuItems.Add(new MenuItem("Edit", new EventHandler(editRecord)));
                m.MenuItems.Add(new MenuItem("Delete", new EventHandler(deleteRecord)));
                selectedRow = gvUser.HitTest(e.X, e.Y).RowIndex;
                m.Show(gvUser, new Point(e.X, e.Y));

            }
        }


    }
}

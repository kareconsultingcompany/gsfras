﻿using DataBase.DeviceMan;
using DataBase.User;
using DataModel.DeviceModel;
using GSFRAS.UserInfo;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GSFRAS.View
{
    public partial class DialogEmployeeEdit : Form
    {
        UserDevice mUser;
        public DialogEmployeeEdit()
        {
            Constant.LogHelper.SetReportClass(this.GetType());
            InitializeComponent();
          
        }

       public void setUser(ref UserDevice user) {
           this.mUser = user;
           txtEmployeeID.Text = UserMan.eidToHKID(mUser.EmployeeID.ToString());
           txtCardNum.Text = mUser.CardNo.ToString();
           txtUsername.Text = mUser.UserName;
           pbImage.Image = Constant.Constants.Base64toImage(mUser.Base64PhotoData);
           cmbRegister.SelectedIndex = (int)mUser.VerfyMode;
        }

       
        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (mUser != null)
                {
                    int emploteeID = mUser.EmployeeID;
                    mUser.EmployeeID = int.Parse(UserMan.generateID(txtEmployeeID.Text));
                    mUser.VerfyMode = cmbRegister.SelectedIndex;
                    mUser.UserName = txtUsername.Text;
                    if (txtCardNum.Text != "")
                    {
                        mUser.CardNo = txtCardNum.Text.PadLeft(10, '0');
                    }
                    if (DeviceMan.updateUser(mUser, emploteeID))
                    {
                        Constant.LogHelper.Info("Update roster roster id is : " + emploteeID, GUserInfo.getInstance().getCureentUser().ID.ToString());
                        MessageBox.Show("Updated");
                        this.Close();
                    }
                    else
                    {
                        MessageBox.Show("Employee ID duplicate, please try again.");
                    }
                }
                else {
                    String eid=UserMan.generateID(txtEmployeeID.Text);
                    int emploteeID = int.Parse(eid);
                    mUser = new UserDevice();
                    mUser.EmployeeID = emploteeID;
                    mUser.VerfyMode = cmbRegister.SelectedIndex;
                    mUser.UserName = txtUsername.Text;
                    mUser.RegStatus = 0;
                    mUser.PhotoLen = 0;
                    mUser.Base64PhotoData = "";
                    mUser.FeatureBase64 = "";
                    mUser.FeatureLen = 0;
                    mUser.UserType = 0;
                    if (txtCardNum.Text != "")
                    {
                        mUser.CardNo = txtCardNum.Text.PadLeft(10, '0');
                    }
                    else {
                        mUser.CardNo = "";
                    }
                    if (DeviceMan.updateUser(mUser, emploteeID))
                    {
                        Constant.LogHelper.Info("Create roster roster id is : " + emploteeID, GUserInfo.getInstance().getCureentUser().ID.ToString());
                        MessageBox.Show("Create Successfully");
                        this.Close();
                    }
                    else {
                        MessageBox.Show("Data invalidate.");
                        mUser = null;
                    }
                
                }
            }
            catch (Exception) { MessageBox.Show("Employee ID duplicate, please try again."); }
        }
    }
}

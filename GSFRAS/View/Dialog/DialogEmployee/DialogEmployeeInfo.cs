﻿using DataBase.DeviceMan;
using DataBase.User;
using DataModel.DeviceModel;
using DataModel.SDK;
using DataModel.User;
using FaceKit.EventInterface;
using GSFRAS.UserInfo;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GSFRAS.View.Dialog.DialogEmployee
{
    public partial class DialogEmployeeInfo : Form, FaceEventWrapper
    {

        private static readonly int MODE_CREATE = 0;
        private static readonly int MODE_CREATE_DEVICE = 1;
        private static readonly int MODE_MODIFY = 2;
        private static readonly int MODE_MODIFY_DEVICE = 3;

        private int mode = 1;
        EmployeeDevice infos;
        UserDevice uinfo = null;
        EmployeeInfo info = null;

        private FaceKit.FaceKit kit;
        private Hashtable mDeviceList = new Hashtable();
        private delegate void UpdateData();

        public DialogEmployeeInfo()
        {
            InitializeComponent();
            kit = FaceKit.FaceKit.getInstance();
            kit.setListener(this);
            updateFRList();

        }
        private void updateFRList()
        {
            cmbFRUnit.Items.Clear();
            mDeviceList = kit.getDevices();
            ICollection key = mDeviceList.Keys;

            foreach (String k in key)
            {
                DeviceData aDevice = (DeviceData)mDeviceList[k];
                cmbFRUnit.Items.Add(mDeviceList[k]);
                cmbFRUnit.Update();


            }
            try
            {
                this.BeginInvoke((Action)delegate()
                {
                    cmbFRUnit.Update();
                });
            }
            catch (Exception)
            {
                cmbFRUnit.Update();
            }
        }
        public void setInfo(EmployeeDevice aInfo)
        {
            txtHKID.Enabled = false;

            this.infos = aInfo;
            uinfo = infos.mDevice;
            info = infos.mInfo;
            if (uinfo == null)
            {
                mode = MODE_MODIFY_DEVICE;
            }
            else if (info == null)
            {
                mode = MODE_CREATE;
            }
            else
            {
                mode = MODE_MODIFY;
            }
            setData();
        }

        public void setData()
        {
            if (info != null)
            {
                txtHKID.Enabled = false;
                txtUsername.Text = info.Name;
                txtHKID.Text = info.HKID;
                //DateTime dob = dtpDOB.Value;
                dtpDOB.Value = Constant.Constants.ConvetTimestamp((double)info.DOB, Constant.Constants.TIME_ZONE);
                cbStatus.SelectedValue = info.RelationshipStatus;
                txtTel.Text = info.Tel.ToString();
                txtMobile.Text = info.Mobile.ToString();
                txtEmail.Text = info.Email;
                txtType.Text = info.Type;
                txtBasicSalary.Text = info.BasicSalary.ToString();
                txtMPF.Text = info.MPF.ToString();
                cbDepartment.SelectedValue = info.Department;
                cbAcademic.SelectedValue = info.Academic;
                //DateTime joinDate = dtpJoinDate.Value;
                dtpJoinDate.Value = Constant.Constants.ConvetTimestamp((double)info.JoinDate, Constant.Constants.TIME_ZONE);
                //DateTime quitDate = dtpQuitDate.Value;
                dtpQuitDate.Value = Constant.Constants.ConvetTimestamp((double)info.QuitDate, Constant.Constants.TIME_ZONE);
                txtAnnualLeave.Text = info.AnnualLeave.ToString();
                txtAcyualAnnualLeave.Text = info.ActualAnnualLeave.ToString();
                cbPosition.SelectedValue = info.Position;
                txtOT.Text = info.OT.ToString();
                txtAddress.Text = info.Address;
                txtRemark.Text = info.Remark;
            }
            else if (uinfo != null)
            {
                txtHKID.Enabled = false;
                txtUsername.Text = uinfo.UserName;
                txtHKID.Text = UserMan.eidToHKID(uinfo.EmployeeID.ToString());

            }
        }

        private void saveRecord()
        {
            String name = txtUsername.Text;
            String hkid = txtHKID.Text;
            if (name == "" || hkid == "")
            {
                MessageBox.Show("User name and HKID cannot empty.");
                return;
            }
            DateTime dob = dtpDOB.Value;
            int status = -1;
            if (cbStatus.Items.Count > 0)
            {
                status = (int)cbStatus.SelectedValue;

            }
            int telphone = 0;
            if (txtTel.Text != "")
            {
                telphone = int.Parse(txtTel.Text);
            }
            int mobile = 0;
            if (txtMobile.Text != "")
            {
                mobile = int.Parse(txtMobile.Text);
            }
            String email = txtEmail.Text;
            String type = txtType.Text;
            int basicSalary = 0;
            if (txtBasicSalary.Text != "")
            {
                basicSalary = int.Parse(txtBasicSalary.Text);
            }
            int mpf = 0;
            if (txtMPF.Text != "")
            {
                mpf = int.Parse(txtMPF.Text);
            }
            int department = -1;
            if (cbDepartment.Items.Count > 0)
            {
                department = (int)cbDepartment.SelectedValue; ;
            }
            int academic = -1;
            if (cbAcademic.Items.Count > 0)
            {
                academic = (int)cbAcademic.SelectedValue;
            }
            DateTime joinDate = dtpJoinDate.Value;
            DateTime quitDate = dtpQuitDate.Value;
            int annualLeave = 0;
            if (txtAnnualLeave.Text != "")
            {
                annualLeave = int.Parse(txtAnnualLeave.Text);
            }
            int actualAnnualLeave = 0;
            if (txtAcyualAnnualLeave.Text != "")
            {
                actualAnnualLeave = int.Parse(txtAcyualAnnualLeave.Text);
            }
            int position = -1;
            if (cbPosition.Items.Count > 0)
            {
                position = (int)cbPosition.SelectedValue;

            }
            int ot = 0;
            if (txtOT.Text != "")
            {
                ot = int.Parse(txtOT.Text);
            }
            String address = txtAddress.Text;
            String remark = txtRemark.Text;
            if (mode == MODE_CREATE)
            {
                createEmployee(name, hkid, dob, status, telphone, mobile, email, type, basicSalary, mpf, department, academic, joinDate, quitDate, annualLeave, actualAnnualLeave, position, ot, address, remark);
                MessageBox.Show("Created");
                Constant.LogHelper.Info("Create employee, hkid:" + hkid + ",user name:" + name, GUserInfo.getInstance().getCureentUser().ID.ToString());
            }
            else if (mode == MODE_CREATE_DEVICE)
            {
                createDevice(hkid, name);
                createEmployee(name, hkid, dob, status, telphone, mobile, email, type, basicSalary, mpf, department, academic, joinDate, quitDate, annualLeave, actualAnnualLeave, position, ot, address, remark);
                MessageBox.Show("Created");
                Constant.LogHelper.Info("Create employee, hkid:" + hkid + ",user name:" + name + " and create roster", GUserInfo.getInstance().getCureentUser().ID.ToString());

            }
            else if (mode == MODE_MODIFY)
            {
                modfiyEmployee(name, hkid, dob, status, telphone, mobile, email, type, basicSalary, mpf, department, academic, joinDate, quitDate, annualLeave, actualAnnualLeave, position, ot, address, remark);
                MessageBox.Show("Updated");
                Constant.LogHelper.Info("Update employee, hkid:" + hkid + ",user name:" + name, GUserInfo.getInstance().getCureentUser().ID.ToString());

            }
            else if (mode == MODE_MODIFY_DEVICE)
            {
                createDevice(hkid, name);
                modfiyEmployee(name, hkid, dob, status, telphone, mobile, email, type, basicSalary, mpf, department, academic, joinDate, quitDate, annualLeave, actualAnnualLeave, position, ot, address, remark);
                MessageBox.Show("Updated");
                Constant.LogHelper.Info("update employee, hkid:" + hkid + ",user name:" + name + ", and create roster.", GUserInfo.getInstance().getCureentUser().ID.ToString());

            }

        }

        private bool createEmployee(String name, String hkid, DateTime dob, int status, int telphone, int mobile, String email, String type, int basicSalary, int mpf, int department, int academic, DateTime joinDate, DateTime quitDate, int annualLeave, int actualAnnualLeave, int position, int ot, String address, String remark)
        {
            EmployeeInfo info = new EmployeeInfo(int.Parse(UserMan.generateID(hkid)), name, hkid, Constant.Constants.ConvertDateToTimestamp(dob, Constant.Constants.TIME_ZONE),
                    status, address, telphone, mobile, email, type, position, department, academic, Constant.Constants.ConvertDateToTimestamp(joinDate, Constant.Constants.TIME_ZONE),
                    Constant.Constants.ConvertDateToTimestamp(quitDate, Constant.Constants.TIME_ZONE), basicSalary, ot, annualLeave, actualAnnualLeave, mpf, remark, GUserInfo.getInstance().getCureentUser().ID, Constant.Constants.ConvertDateToTimestamp(DateTime.Now, Constant.Constants.TIME_ZONE));
            return EmployeeMan.createEmployee(info);
        }
        private void createDevice(String hkid, String name)
        {
            UserDevice ud = new UserDevice(int.Parse(UserMan.generateID(hkid)), null, name, 1, 1, 0, "", 0, "", 0);
            if (DeviceMan.updateUser(ud, ud.EmployeeID))
            {

            }
        }
        private void modfiyEmployee(String name, String hkid, DateTime dob, int status, int telphone, int mobile, String email, String type, int basicSalary, int mpf, int department, int academic, DateTime joinDate, DateTime quitDate, int annualLeave, int actualAnnualLeave, int position, int ot, String address, String remark)
        {
            if (info != null)
            {
                info.Academic = academic;
                info.ActualAnnualLeave = actualAnnualLeave;
                info.Address = address;
                info.AnnualLeave = annualLeave;
                info.BasicSalary = basicSalary;
                info.Department = department;
                info.DOB = Constant.Constants.ConvertDateToTimestamp(dob, Constant.Constants.TIME_ZONE);
                info.Email = email;
                info.JoinDate = Constant.Constants.ConvertDateToTimestamp(joinDate, Constant.Constants.TIME_ZONE);
                info.lastUser = GUserInfo.getInstance().getCureentUser().ID;
                info.Mobile = mobile;
                info.MPF = mpf;
                info.Name = name;
                info.OT = ot;
                info.Position = position;
                info.QuitDate = Constant.Constants.ConvertDateToTimestamp(quitDate, Constant.Constants.TIME_ZONE);
                info.RelationshipStatus = status;
                info.Remark = remark;
                info.Tel = telphone;
                info.Type = type;
                if (EmployeeMan.updateEmployee(info))
                {
                }

            }

        }


        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void DialogEmployeeInfo_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'gSFRASDataSet2.Position' table. You can move, or remove it, as needed.
            this.positionTableAdapter.Fill(this.gSFRASDataSet2.Position);
            // TODO: This line of code loads data into the 'gSFRASDataSet1.Academic' table. You can move, or remove it, as needed.
            this.academicTableAdapter.Fill(this.gSFRASDataSet1.Academic);
            // TODO: This line of code loads data into the 'gSFRASDataSet.Department' table. You can move, or remove it, as needed.
            this.departmentTableAdapter.Fill(this.gSFRASDataSet.Department);
            // TODO: This line of code loads data into the 'gSFRASDataSetStatus.RelationshipStatus' table. You can move, or remove it, as needed.
            GSFRAS.GSFRASDataSetStatus.RelationshipStatusDataTable dt = this.gSFRASDataSetStatus.RelationshipStatus;
            this.relationshipStatusTableAdapter.Fill(dt);


        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            saveRecord();
        }





        public void OnEventConnect()
        {
            try
            {
                Invoke(new UpdateData(updateFRList));
            }
            catch (Exception ex)
            {
                Constant.LogHelper.Warn("Device connect problem", ex, GUserInfo.getInstance().getCureentUser().ID.ToString());

            }
        }

        public void OnEventDisconnect()
        {
            try
            {
                Invoke(new UpdateData(updateFRList));
            }
            catch (Exception ex)
            {
                Constant.LogHelper.Warn("Device disconnect problem", ex, GUserInfo.getInstance().getCureentUser().ID.ToString());

            }
        }

        public void OnTrapAccess(string strDevSn, int lOpCode, int lUserData, int lExtendParam, int lUserID, string strRecTime, int lRecType, int lScore, int lStatus, int lPhotoType, int lPhotoLen, int lReason, string strBase64PhotoData)
        {
        }

        public void OnEventErrorNotify(string strDevSn, int lOpCode, int lUserData, int lExtendParam, int lErrorCode)
        {
        }

        public void OnError(string error, int errorCod)
        {
        }

        public void OnUploadStart(string message)
        {
        }

        public void OnUploadStarting(int count)
        {
        }

        public void OnUplading(int val, string message)
        {
        }

        public void OnUpladEnd()
        {
        }

        public void OnInsertDeviceProblem(int id, String name)
        {
        }


        private void DialogEmployeeInfo_FormClosed(object sender, FormClosedEventArgs e)
        {

        }

        private void btnSyn_Click(object sender, EventArgs e)
        {
            if (cmbFRUnit.SelectedIndex >= 0)
            {
                String sn = "";
                DeviceData aDevice = (DeviceData)cmbFRUnit.SelectedItem;
                if (aDevice != null)
                {
                    sn = aDevice.deviceSn;
                }
                String hkid = txtHKID.Text.Trim();

                int userType = 0;
                int userId = int.Parse(UserMan.generateID(hkid));
                String cardNo = "0";
                String userName = txtUsername.Text;
                int verifyMode = 1;
                int status = 1;
                int photLen = 0;
                String base64Photo = "";
                if (uinfo != null)
                {
                    cardNo = uinfo.CardNo.ToString();
                    verifyMode = (int)uinfo.VerfyMode;
                    status = (int)uinfo.RegStatus;
                    photLen = (int)uinfo.PhotoLen;
                    base64Photo = uinfo.Base64PhotoData;
                    ArrayList al = new ArrayList();
                    al.Add(uinfo);
                    kit.AddUsersWithFeature(al, sn);//(sn, (int)uinfo.UserType, uinfo.EmployeeID, uinfo.CardNo, uinfo.UserName, (int)uinfo.VerfyMode, (int)uinfo.RegStatus, uinfo., base64Photo);
                    MessageBox.Show("Sync to device successfully");

                }
                else
                {
                    kit.addUser(sn, userType, userId, cardNo, userName, verifyMode, status, photLen, base64Photo);
                    MessageBox.Show("Sync to device successfully");
                }
            }
            else
            {
                MessageBox.Show("Please select the device.");
            }
        }

        private void btnDevice_Click(object sender, EventArgs e)
        {
            DialogEmployeeEdit dia = new DialogEmployeeEdit();
            if (uinfo == null)
            {
                String name = txtUsername.Text;
                String hkid = txtHKID.Text.ToUpper();
                if (name == "" || hkid == "")
                {
                    MessageBox.Show("User name and HKID cannot be empty.");
                    return;
                }
                uinfo = new UserDevice(int.Parse(UserMan.generateID(hkid)), "", name, 1, 1, 0, "", 0, "", 0);
            }
            dia.setUser(ref uinfo);
            dia.ShowDialog();
            mode = MODE_MODIFY;
        }

        private void txtTel_KeyDown(object sender, KeyEventArgs e)
        {
            //Allow navigation keyboard arrows
            switch (e.KeyCode)
            {
                case Keys.Up:
                case Keys.Down:
                case Keys.Left:
                case Keys.Right:
                case Keys.PageUp:
                case Keys.PageDown:
                case Keys.Delete:
                    e.SuppressKeyPress = false;
                    return;
                default:
                    break;
            }

            //Block non-number characters
            char currentKey = (char)e.KeyCode;
            bool modifier = e.Control || e.Alt || e.Shift;
            bool nonNumber = char.IsLetter(currentKey) ||
                             char.IsSymbol(currentKey) ||
                             char.IsWhiteSpace(currentKey) ||
                             char.IsPunctuation(currentKey);

            if (!modifier && nonNumber)
                e.SuppressKeyPress = true;

            //Handle pasted Text
            if (e.Control && e.KeyCode == Keys.V)
            {
                //Preview paste data (removing non-number characters)
                string pasteText = Clipboard.GetText();
                string strippedText = "";
                for (int i = 0; i < pasteText.Length; i++)
                {
                    if (char.IsDigit(pasteText[i]))
                        strippedText += pasteText[i].ToString();
                }

                if (strippedText != pasteText)
                {
                    //There were non-numbers in the pasted text
                    e.SuppressKeyPress = true;

                    //OPTIONAL: Manually insert text stripped of non-numbers
                    TextBox me = (TextBox)sender;
                    int start = me.SelectionStart;
                    string newTxt = me.Text;
                    newTxt = newTxt.Remove(me.SelectionStart, me.SelectionLength); //remove highlighted text
                    newTxt = newTxt.Insert(me.SelectionStart, strippedText); //paste
                    me.Text = newTxt;
                    me.SelectionStart = start + strippedText.Length;
                }
                else
                    e.SuppressKeyPress = false;
            }
        }


    }
}

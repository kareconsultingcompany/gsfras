﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GSFRAS.View.Dialog.DialogFunction
{
    public partial class DialogStatus : Form
    {
        public DialogStatus()
        {
            InitializeComponent();
        }

        private void DialogStatus_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'gSFRASDataSetStatus.RelationshipStatus' table. You can move, or remove it, as needed.
            this.relationshipStatusTableAdapter.Fill(this.gSFRASDataSetStatus.RelationshipStatus);

        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            this.relationshipStatusTableAdapter.Update(this.gSFRASDataSetStatus.RelationshipStatus);
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow item in this.dataGridView1.SelectedRows)
            {
                dataGridView1.Rows.RemoveAt(item.Index);
            }
            this.relationshipStatusTableAdapter.Update(this.gSFRASDataSetStatus.RelationshipStatus);

        }
    }
}

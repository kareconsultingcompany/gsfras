﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GSFRAS.View.Dialog.DialogFunction
{
    public partial class DialogDepartment : Form
    {
        public DialogDepartment()
        {
            InitializeComponent();
        }

        private void DialogDepartment_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'gSFRASDataSet.Department' table. You can move, or remove it, as needed.
            this.departmentTableAdapter.Fill(this.gSFRASDataSet.Department);

        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            this.departmentTableAdapter.Update(this.gSFRASDataSet.Department);
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow item in this.dataGridView1.SelectedRows)
            {
                dataGridView1.Rows.RemoveAt(item.Index);
            }
            this.departmentTableAdapter.Update(this.gSFRASDataSet.Department);

        }
    }
}

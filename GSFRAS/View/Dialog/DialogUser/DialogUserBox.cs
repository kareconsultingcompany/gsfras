﻿using DataBase.User;
using GSFRAS.UserInfo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GSFRAS.View.Dialog.DialogUser
{
    public partial class DialogUserBox : Form
    {
        public static readonly int MODE_EDIT = 0;
        public static readonly int MODE_CREATE = 1;

        private int type;
        private DataModel.User.UserInfo info;

        public DialogUserBox(int type)
        {
            Constant.LogHelper.SetReportClass(this.GetType());
            this.type = type;
            InitializeComponent();
            if (GUserInfo.getInstance().getCureentUser().SysRight != 1) {
                cbAdminRight.Enabled = false;
                cbReadLog.Enabled = false;
                cbUserLock.Enabled = false;
            }
            
        }

        public void setUser(DataModel.User.UserInfo info)
        {
            this.info = info;
            if (type == DialogUserBox.MODE_EDIT)
            {
                txtLoginName.Text = info.Name;
                txtDisplayName.Text = info.Username;
                if (info.SysRight == 1)
                {
                    cbAdminRight.Checked = true;
                }
                if (info.UserLock == 1)
                {
                    cbUserLock.Checked = true;
                }
                if (info.Reclog == 1)
                {
                    cbReadLog.Checked = true;
                }
            }
        }

        private void actionUser()
        {
            String pw = txtPw.Text;
            String cpw = txtCpw.Text;

            String loginName = txtLoginName.Text;
            String displayName = txtDisplayName.Text;
            int sysRight = 0;
            int reLog = 0;
            int userLock = 0;
            if (cbAdminRight.Checked == true) {
                sysRight = 1;
            }
            if(cbReadLog.Checked==true){
                reLog = 1;
            }
            if(cbUserLock.Checked==true){
                userLock = 1;
            }
            if(pw==""){
                MessageBox.Show("Password cannot be empty.");
                return;
            }
            if (pw == cpw)
            {
                if (type == DialogUserBox.MODE_CREATE)
                {
                    if (UserMan.chechUserName(loginName)) {
                        MessageBox.Show("The login name have been used.");                    
                        return;
                    }

                    DataModel.User.UserInfo ainfo = new DataModel.User.UserInfo();
                    ainfo.Lang = "en";
                    ainfo.LastUser = GUserInfo.getInstance().getCureentUser().ID;
                    ainfo.Name = loginName;
                    ainfo.Password = pw;
                    ainfo.Reclog = reLog;
                    ainfo.SysRight = sysRight;
                    ainfo.Txlog = 1;
                    ainfo.UserLevel = 1;
                    ainfo.UserLock = userLock;
                    ainfo.Username = displayName;
                    ainfo.CreateDate=Constant.Constants.ConvertDateToTimestamp(DateTime.Now,Constant.Constants.TIME_ZONE);

                    if (UserMan.createUser(ainfo))
                    {
                        MessageBox.Show("Create successsful");
                        this.Close();
                    }
                    else {
                        MessageBox.Show("Data problem.");                    
                    }


                }
                else if (type == DialogUserBox.MODE_EDIT)
                {
                    info.LastUser = GUserInfo.getInstance().getCureentUser().ID;
                    info.Password = pw;
                    info.Reclog = reLog;
                    info.SysRight = sysRight;
                    info.Txlog = 1;
                    info.UserLevel = 1;
                    info.UserLock = userLock;
                    info.Username = displayName;

                    if (UserMan.updateUser(info))
                    {
                        MessageBox.Show("Update successful");
                        GUserInfo.getInstance().setCurrentUser(info);
                        this.Close();
                    }
                    else {
                        MessageBox.Show("Update fail, please check the user data.");
                    }
                }
            }
            else {
                MessageBox.Show("Password is not same. Please check it");
            }
        }

        /****************Auto generate**********************/
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            actionUser();
        }
    }
}

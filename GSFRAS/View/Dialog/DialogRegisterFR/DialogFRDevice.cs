﻿using DataBase.DeviceMan;
using DataModel.DeviceModel;
using DataModel.SDK;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GSFRAS.View.Dialog.DialogRegisterFR
{
    public partial class DialogFRDevice : Form
    {

        public static readonly int MODE_CREATE = 0;
        public static readonly int MODE_EDIT = 1;
        private HardwareInfo mInfo;
        private int type = 0;
        public DialogFRDevice(int type)
        {
            this.type = type;
            InitializeComponent();
            if (type == MODE_CREATE)
            {
                txtDeviceName.Enabled = true;
            }
        }

        public void setData(HardwareInfo aInfo)
        {
            this.mInfo = aInfo;
            txtDeviceSn.Text = mInfo.DeviceSVN;
            txtIP.Text = mInfo.IpAddress;
            txtLocation.Text = mInfo.Location;
            txtPW.Text = mInfo.AccessPassword;
            txtUsername.Text = mInfo.AccessUserName;
            txtDeviceName.Text = mInfo.Name;
        }

        public void setData(DeviceData aData)
        {
            if (type == MODE_EDIT)
            {
                setData(aData.hinfo);
            }
            txtDeviceSn.Text = aData.deviceSn;
            txtIP.Text = aData.deviceIP;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {

            if (type == MODE_CREATE)
            {
                mInfo = new HardwareInfo();
                mInfo.AccessPassword = txtPW.Text;
                mInfo.AccessUserName = txtUsername.Text;
                mInfo.CreateDate = Constant.Constants.ConvertDateToTimestamp(DateTime.Now, Constant.Constants.TIME_ZONE);
                mInfo.DeviceSVN = txtDeviceSn.Text;
                mInfo.IpAddress = txtIP.Text;
                mInfo.LastSync = Constant.Constants.ConvertDateToTimestamp(DateTime.Now, Constant.Constants.TIME_ZONE);
                mInfo.Location = txtLocation.Text;
                mInfo.Name = txtDeviceName.Text;
                if (HardwareMan.createInfo(mInfo))
                {
                    MessageBox.Show("Create successful");
                }
                else
                {
                    MessageBox.Show("Data problem");
                }
            }
            else if (type == MODE_EDIT)
            {
                mInfo.AccessPassword = txtPW.Text;
                mInfo.AccessUserName = txtUsername.Text;
                mInfo.CreateDate = Constant.Constants.ConvertDateToTimestamp(DateTime.Now, Constant.Constants.TIME_ZONE);
                mInfo.DeviceSVN = txtDeviceSn.Text;
                mInfo.IpAddress = txtIP.Text;
                mInfo.LastSync = Constant.Constants.ConvertDateToTimestamp(DateTime.Now, Constant.Constants.TIME_ZONE);
                mInfo.Location = txtLocation.Text;
                mInfo.Name = txtDeviceName.Text;
                if (HardwareMan.updateInfo(mInfo))
                {
                    MessageBox.Show("Update successful");

                }
                else
                {
                    MessageBox.Show("Data problem");
                }

            }

            this.Close();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GSFRAS.UserInfo
{
    public class GUserInfo
    {
        private static GUserInfo mInstance;
        private DataModel.User.UserInfo currentUser;
        private DateTime dt;

        private GUserInfo() { }

        public static GUserInfo getInstance()
        {
            if (mInstance == null)
            {
                mInstance = new GUserInfo();
            }
            return mInstance;
        }

        public DataModel.User.UserInfo getCureentUser()
        {
            if (currentUser == null)
            {
                throw new NullReferenceException();
            }
            return currentUser;
        }

        public void setCurrentUser(DataModel.User.UserInfo aUser)
        {
            dt = DateTime.Now;
            this.currentUser = aUser;
        }

        public DateTime getSeesionTime() {
            return dt;
        }


    }
}

﻿namespace GSFRAS
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.employeeMaintenanceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.employeeRecordToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.staffCardPrintingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.rosterUploadAndDownloadToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.attendanceRecordToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.reportingPrinting2ndPhaseDevelopmentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.importDailyAttendanceFromFRUnitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exportDailyAttendanceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.systemToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dailyAttendanceReport2ndPhaseDevelopmentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dailyLateReport2ndPhaseDevelopmentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.systemUserProfileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.userManagementToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.systemLogToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.faceRecognitionUnitRegistrationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.selectionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.academicToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.positionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.departmentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statusToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.loginAnotherUserToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.labVersion = new System.Windows.Forms.Label();
            this.labProduct = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lbl_Username = new System.Windows.Forms.Label();
            this.lbl_Current_Date = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.Control;
            this.label1.Location = new System.Drawing.Point(14, 67);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(278, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Face Recognition Attendance System";
            // 
            // label2
            // 
            this.label2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label2.Location = new System.Drawing.Point(16, 93);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(286, 1);
            this.label2.TabIndex = 5;
            this.label2.Text = "label2";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.employeeMaintenanceToolStripMenuItem,
            this.toolStripMenuItem1,
            this.attendanceRecordToolStripMenuItem,
            this.systemToolStripMenuItem,
            this.exitToolStripMenuItem,
            this.exitToolStripMenuItem1});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(4, 2, 0, 2);
            this.menuStrip1.Size = new System.Drawing.Size(1076, 24);
            this.menuStrip1.TabIndex = 6;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // employeeMaintenanceToolStripMenuItem
            // 
            this.employeeMaintenanceToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.employeeRecordToolStripMenuItem,
            this.staffCardPrintingToolStripMenuItem});
            this.employeeMaintenanceToolStripMenuItem.Name = "employeeMaintenanceToolStripMenuItem";
            this.employeeMaintenanceToolStripMenuItem.Size = new System.Drawing.Size(145, 20);
            this.employeeMaintenanceToolStripMenuItem.Text = "Employee Management";
            // 
            // employeeRecordToolStripMenuItem
            // 
            this.employeeRecordToolStripMenuItem.Image = global::GSFRAS.Properties.Resources.account_manage;
            this.employeeRecordToolStripMenuItem.Name = "employeeRecordToolStripMenuItem";
            this.employeeRecordToolStripMenuItem.Size = new System.Drawing.Size(310, 22);
            this.employeeRecordToolStripMenuItem.Text = "Employee Record Management";
            this.employeeRecordToolStripMenuItem.Click += new System.EventHandler(this.employeeRecordToolStripMenuItem_Click);
            // 
            // staffCardPrintingToolStripMenuItem
            // 
            this.staffCardPrintingToolStripMenuItem.Image = global::GSFRAS.Properties.Resources.reports;
            this.staffCardPrintingToolStripMenuItem.Name = "staffCardPrintingToolStripMenuItem";
            this.staffCardPrintingToolStripMenuItem.Size = new System.Drawing.Size(310, 22);
            this.staffCardPrintingToolStripMenuItem.Text = "Staff Card Printing (2nd phase Development)";
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.rosterUploadAndDownloadToolStripMenuItem});
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(120, 20);
            this.toolStripMenuItem1.Text = "Roster Managment";
            // 
            // rosterUploadAndDownloadToolStripMenuItem
            // 
            this.rosterUploadAndDownloadToolStripMenuItem.Image = global::GSFRAS.Properties.Resources.roster;
            this.rosterUploadAndDownloadToolStripMenuItem.Name = "rosterUploadAndDownloadToolStripMenuItem";
            this.rosterUploadAndDownloadToolStripMenuItem.Size = new System.Drawing.Size(215, 22);
            this.rosterUploadAndDownloadToolStripMenuItem.Text = "Roster Record Managment";
            this.rosterUploadAndDownloadToolStripMenuItem.Click += new System.EventHandler(this.rosterUploadAndDownloadToolStripMenuItem_Click);
            // 
            // attendanceRecordToolStripMenuItem
            // 
            this.attendanceRecordToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.reportingPrinting2ndPhaseDevelopmentToolStripMenuItem,
            this.importDailyAttendanceFromFRUnitToolStripMenuItem,
            this.exportDailyAttendanceToolStripMenuItem});
            this.attendanceRecordToolStripMenuItem.Name = "attendanceRecordToolStripMenuItem";
            this.attendanceRecordToolStripMenuItem.Size = new System.Drawing.Size(194, 20);
            this.attendanceRecordToolStripMenuItem.Text = "Attendance Record Management";
            // 
            // reportingPrinting2ndPhaseDevelopmentToolStripMenuItem
            // 
            this.reportingPrinting2ndPhaseDevelopmentToolStripMenuItem.Image = global::GSFRAS.Properties.Resources.reports;
            this.reportingPrinting2ndPhaseDevelopmentToolStripMenuItem.Name = "reportingPrinting2ndPhaseDevelopmentToolStripMenuItem";
            this.reportingPrinting2ndPhaseDevelopmentToolStripMenuItem.Size = new System.Drawing.Size(315, 22);
            this.reportingPrinting2ndPhaseDevelopmentToolStripMenuItem.Text = "Reporting Printing(2nd Phase Development)";
            // 
            // importDailyAttendanceFromFRUnitToolStripMenuItem
            // 
            this.importDailyAttendanceFromFRUnitToolStripMenuItem.Image = global::GSFRAS.Properties.Resources.file_type_csv;
            this.importDailyAttendanceFromFRUnitToolStripMenuItem.Name = "importDailyAttendanceFromFRUnitToolStripMenuItem";
            this.importDailyAttendanceFromFRUnitToolStripMenuItem.Size = new System.Drawing.Size(315, 22);
            this.importDailyAttendanceFromFRUnitToolStripMenuItem.Text = "Import Daily Attendance Record From FR Unit";
            this.importDailyAttendanceFromFRUnitToolStripMenuItem.Click += new System.EventHandler(this.importDailyAttendanceFromFRUnitToolStripMenuItem_Click);
            // 
            // exportDailyAttendanceToolStripMenuItem
            // 
            this.exportDailyAttendanceToolStripMenuItem.Image = global::GSFRAS.Properties.Resources.file_type_csv;
            this.exportDailyAttendanceToolStripMenuItem.Name = "exportDailyAttendanceToolStripMenuItem";
            this.exportDailyAttendanceToolStripMenuItem.Size = new System.Drawing.Size(315, 22);
            this.exportDailyAttendanceToolStripMenuItem.Text = "Export Daily Attendance Record";
            this.exportDailyAttendanceToolStripMenuItem.Click += new System.EventHandler(this.exportDailyAttendanceToolStripMenuItem_Click);
            // 
            // systemToolStripMenuItem
            // 
            this.systemToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.dailyAttendanceReport2ndPhaseDevelopmentToolStripMenuItem,
            this.dailyLateReport2ndPhaseDevelopmentToolStripMenuItem});
            this.systemToolStripMenuItem.Name = "systemToolStripMenuItem";
            this.systemToolStripMenuItem.Size = new System.Drawing.Size(71, 20);
            this.systemToolStripMenuItem.Text = "Reporting";
            // 
            // dailyAttendanceReport2ndPhaseDevelopmentToolStripMenuItem
            // 
            this.dailyAttendanceReport2ndPhaseDevelopmentToolStripMenuItem.Image = global::GSFRAS.Properties.Resources.reports;
            this.dailyAttendanceReport2ndPhaseDevelopmentToolStripMenuItem.Name = "dailyAttendanceReport2ndPhaseDevelopmentToolStripMenuItem";
            this.dailyAttendanceReport2ndPhaseDevelopmentToolStripMenuItem.Size = new System.Drawing.Size(338, 22);
            this.dailyAttendanceReport2ndPhaseDevelopmentToolStripMenuItem.Text = "Daily Attendance Report(2nd Phase Development)";
            // 
            // dailyLateReport2ndPhaseDevelopmentToolStripMenuItem
            // 
            this.dailyLateReport2ndPhaseDevelopmentToolStripMenuItem.Image = global::GSFRAS.Properties.Resources.reports;
            this.dailyLateReport2ndPhaseDevelopmentToolStripMenuItem.Name = "dailyLateReport2ndPhaseDevelopmentToolStripMenuItem";
            this.dailyLateReport2ndPhaseDevelopmentToolStripMenuItem.Size = new System.Drawing.Size(338, 22);
            this.dailyLateReport2ndPhaseDevelopmentToolStripMenuItem.Text = "Daily Late Report(2nd Phase Development)";
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.systemUserProfileToolStripMenuItem,
            this.userManagementToolStripMenuItem,
            this.systemLogToolStripMenuItem,
            this.faceRecognitionUnitRegistrationToolStripMenuItem,
            this.selectionToolStripMenuItem,
            this.toolStripSeparator1,
            this.loginAnotherUserToolStripMenuItem});
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(57, 20);
            this.exitToolStripMenuItem.Text = "System";
            // 
            // systemUserProfileToolStripMenuItem
            // 
            this.systemUserProfileToolStripMenuItem.Image = global::GSFRAS.Properties.Resources.management_icon;
            this.systemUserProfileToolStripMenuItem.Name = "systemUserProfileToolStripMenuItem";
            this.systemUserProfileToolStripMenuItem.Size = new System.Drawing.Size(256, 22);
            this.systemUserProfileToolStripMenuItem.Text = "User Profile";
            this.systemUserProfileToolStripMenuItem.Click += new System.EventHandler(this.systemUserProfileToolStripMenuItem_Click);
            // 
            // userManagementToolStripMenuItem
            // 
            this.userManagementToolStripMenuItem.Image = global::GSFRAS.Properties.Resources.account;
            this.userManagementToolStripMenuItem.Name = "userManagementToolStripMenuItem";
            this.userManagementToolStripMenuItem.Size = new System.Drawing.Size(256, 22);
            this.userManagementToolStripMenuItem.Text = "User Management";
            this.userManagementToolStripMenuItem.Click += new System.EventHandler(this.userManagementToolStripMenuItem_Click);
            // 
            // systemLogToolStripMenuItem
            // 
            this.systemLogToolStripMenuItem.Image = global::GSFRAS.Properties.Resources.file_type_log;
            this.systemLogToolStripMenuItem.Name = "systemLogToolStripMenuItem";
            this.systemLogToolStripMenuItem.Size = new System.Drawing.Size(256, 22);
            this.systemLogToolStripMenuItem.Text = "System Log";
            this.systemLogToolStripMenuItem.Click += new System.EventHandler(this.systemLogToolStripMenuItem_Click);
            // 
            // faceRecognitionUnitRegistrationToolStripMenuItem
            // 
            this.faceRecognitionUnitRegistrationToolStripMenuItem.Image = global::GSFRAS.Properties.Resources.face_recognition_scan_128;
            this.faceRecognitionUnitRegistrationToolStripMenuItem.Name = "faceRecognitionUnitRegistrationToolStripMenuItem";
            this.faceRecognitionUnitRegistrationToolStripMenuItem.Size = new System.Drawing.Size(256, 22);
            this.faceRecognitionUnitRegistrationToolStripMenuItem.Text = "Face Recognition Unit Registration";
            this.faceRecognitionUnitRegistrationToolStripMenuItem.Click += new System.EventHandler(this.faceRecognitionUnitRegistrationToolStripMenuItem_Click);
            // 
            // selectionToolStripMenuItem
            // 
            this.selectionToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.academicToolStripMenuItem,
            this.positionToolStripMenuItem,
            this.departmentToolStripMenuItem,
            this.statusToolStripMenuItem});
            this.selectionToolStripMenuItem.Name = "selectionToolStripMenuItem";
            this.selectionToolStripMenuItem.Size = new System.Drawing.Size(256, 22);
            this.selectionToolStripMenuItem.Text = "Selection";
            // 
            // academicToolStripMenuItem
            // 
            this.academicToolStripMenuItem.Name = "academicToolStripMenuItem";
            this.academicToolStripMenuItem.Size = new System.Drawing.Size(137, 22);
            this.academicToolStripMenuItem.Text = "Academic";
            this.academicToolStripMenuItem.Click += new System.EventHandler(this.academicToolStripMenuItem_Click);
            // 
            // positionToolStripMenuItem
            // 
            this.positionToolStripMenuItem.Name = "positionToolStripMenuItem";
            this.positionToolStripMenuItem.Size = new System.Drawing.Size(137, 22);
            this.positionToolStripMenuItem.Text = "Position";
            this.positionToolStripMenuItem.Click += new System.EventHandler(this.positionToolStripMenuItem_Click);
            // 
            // departmentToolStripMenuItem
            // 
            this.departmentToolStripMenuItem.Name = "departmentToolStripMenuItem";
            this.departmentToolStripMenuItem.Size = new System.Drawing.Size(137, 22);
            this.departmentToolStripMenuItem.Text = "Department";
            this.departmentToolStripMenuItem.Click += new System.EventHandler(this.departmentToolStripMenuItem_Click);
            // 
            // statusToolStripMenuItem
            // 
            this.statusToolStripMenuItem.Name = "statusToolStripMenuItem";
            this.statusToolStripMenuItem.Size = new System.Drawing.Size(137, 22);
            this.statusToolStripMenuItem.Text = "Status";
            this.statusToolStripMenuItem.Click += new System.EventHandler(this.statusToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(253, 6);
            // 
            // loginAnotherUserToolStripMenuItem
            // 
            this.loginAnotherUserToolStripMenuItem.Image = global::GSFRAS.Properties.Resources.system_login_icon;
            this.loginAnotherUserToolStripMenuItem.Name = "loginAnotherUserToolStripMenuItem";
            this.loginAnotherUserToolStripMenuItem.Size = new System.Drawing.Size(256, 22);
            this.loginAnotherUserToolStripMenuItem.Text = "Login another user";
            this.loginAnotherUserToolStripMenuItem.Click += new System.EventHandler(this.loginAnotherUserToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem1
            // 
            this.exitToolStripMenuItem1.Name = "exitToolStripMenuItem1";
            this.exitToolStripMenuItem1.Size = new System.Drawing.Size(37, 20);
            this.exitToolStripMenuItem1.Text = "Exit";
            this.exitToolStripMenuItem1.Click += new System.EventHandler(this.exitToolStripMenuItem1_Click);
            // 
            // labVersion
            // 
            this.labVersion.AutoSize = true;
            this.labVersion.BackColor = System.Drawing.Color.Transparent;
            this.labVersion.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labVersion.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.labVersion.Location = new System.Drawing.Point(15, 104);
            this.labVersion.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labVersion.Name = "labVersion";
            this.labVersion.Size = new System.Drawing.Size(60, 13);
            this.labVersion.TabIndex = 14;
            this.labVersion.Text = "Version 1.0";
            // 
            // labProduct
            // 
            this.labProduct.AutoSize = true;
            this.labProduct.BackColor = System.Drawing.Color.Transparent;
            this.labProduct.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labProduct.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.labProduct.Location = new System.Drawing.Point(15, 118);
            this.labProduct.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labProduct.Name = "labProduct";
            this.labProduct.Size = new System.Drawing.Size(122, 9);
            this.labProduct.TabIndex = 15;
            this.labProduct.Text = "Product ID: KARE-FR28-1668-001";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.SystemColors.Control;
            this.label3.Location = new System.Drawing.Point(38, 198);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(151, 20);
            this.label3.TabIndex = 16;
            this.label3.Text = "General Information";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.SystemColors.Control;
            this.label4.Location = new System.Drawing.Point(86, 236);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(128, 17);
            this.label4.TabIndex = 17;
            this.label4.Text = "Session login user:";
            // 
            // lbl_Username
            // 
            this.lbl_Username.AutoSize = true;
            this.lbl_Username.BackColor = System.Drawing.Color.Transparent;
            this.lbl_Username.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_Username.ForeColor = System.Drawing.SystemColors.Control;
            this.lbl_Username.Location = new System.Drawing.Point(213, 236);
            this.lbl_Username.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbl_Username.Name = "lbl_Username";
            this.lbl_Username.Size = new System.Drawing.Size(73, 17);
            this.lbl_Username.TabIndex = 18;
            this.lbl_Username.Text = "Username";
            // 
            // lbl_Current_Date
            // 
            this.lbl_Current_Date.AutoSize = true;
            this.lbl_Current_Date.BackColor = System.Drawing.Color.Transparent;
            this.lbl_Current_Date.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_Current_Date.ForeColor = System.Drawing.SystemColors.Control;
            this.lbl_Current_Date.Location = new System.Drawing.Point(213, 258);
            this.lbl_Current_Date.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbl_Current_Date.Name = "lbl_Current_Date";
            this.lbl_Current_Date.Size = new System.Drawing.Size(52, 17);
            this.lbl_Current_Date.TabIndex = 19;
            this.lbl_Current_Date.Text = "lblDate";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.SystemColors.Control;
            this.label7.Location = new System.Drawing.Point(86, 258);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(126, 17);
            this.label7.TabIndex = 20;
            this.label7.Text = "Session login time:";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox1.Image = global::GSFRAS.Properties.Resources.business_user;
            this.pictureBox1.Location = new System.Drawing.Point(44, 234);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(38, 41);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 21;
            this.pictureBox1.TabStop = false;
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(38)))), ((int)(((byte)(38)))));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1076, 474);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.lbl_Current_Date);
            this.Controls.Add(this.lbl_Username);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.labProduct);
            this.Controls.Add(this.labVersion);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "Main";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FRAS - System Screen";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Main_FormClosed);
            this.Load += new System.EventHandler(this.Main_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem employeeMaintenanceToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem employeeRecordToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem attendanceRecordToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem systemToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem dailyAttendanceReport2ndPhaseDevelopmentToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dailyLateReport2ndPhaseDevelopmentToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem systemUserProfileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem systemLogToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem faceRecognitionUnitRegistrationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem rosterUploadAndDownloadToolStripMenuItem;
        private System.Windows.Forms.Label labVersion;
        private System.Windows.Forms.Label labProduct;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lbl_Username;
        private System.Windows.Forms.Label lbl_Current_Date;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ToolStripMenuItem reportingPrinting2ndPhaseDevelopmentToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem importDailyAttendanceFromFRUnitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exportDailyAttendanceToolStripMenuItem;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.ToolStripMenuItem loginAnotherUserToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem staffCardPrintingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem userManagementToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem selectionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem academicToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem positionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem departmentToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem statusToolStripMenuItem;
    }
}


﻿using GSFRAS.UserInfo;
using GSFRAS.View;
using GSFRAS.View.Dialog.DialogFunction;
using GSFRAS.View.Dialog.DialogUser;
using System;
using System.Windows.Forms;

namespace GSFRAS
{
    public partial class Main : Form
    {
        public Main()
        {
            Constant.LogHelper.SetReportClass(this.GetType());
            InitializeComponent();

            lbl_Username.Text = GUserInfo.getInstance().getCureentUser().Username;
            lbl_Current_Date.Text = GUserInfo.getInstance().getSeesionTime().ToString("yyyy-MM-dd HH:mm:ss");
            labVersion.Text = Constant.Constants.VERSION;
            labProduct.Text = Constant.Constants.PRODUCT_ID;
            FaceKit.FaceKit kit = FaceKit.FaceKit.getInstance();
            kit.serverStart(Constant.Constants.SERVER_PORT);

        }


        private void Main_Load(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Maximized;

        }

        #region CustomMethod
        private void showDevice()
        {
            Constant.LogHelper.Info("Open device and employee form", GUserInfo.getInstance().getCureentUser().ID.ToString());
            FormDeviceEmployee device = new FormDeviceEmployee();
            device.ShowDialog();

        }

        private void showAttendanceImport()
        {
            Constant.LogHelper.Info("Open import attendance form", GUserInfo.getInstance().getCureentUser().ID.ToString());
            FormAttendanceImport attendanceImport = new FormAttendanceImport();
            attendanceImport.ShowDialog();
        }

        private void showAttendanceExport()
        {
            Constant.LogHelper.Info("Open export arrendance form", GUserInfo.getInstance().getCureentUser().ID.ToString());
            FormAttendanceExport attendanceExport = new FormAttendanceExport();
            attendanceExport.ShowDialog();
        }

        private void showUserLog()
        {
            if (GUserInfo.getInstance().getCureentUser().Reclog == 1)
            {
                Constant.LogHelper.Info("Open log form", GUserInfo.getInstance().getCureentUser().ID.ToString());
                FormUserLog userlog = new FormUserLog();
                userlog.ShowDialog();
            }
            else
            {
                Constant.LogHelper.Info("No permission to open log form", GUserInfo.getInstance().getCureentUser().ID.ToString());
                MessageBox.Show("You do not have permisson.");
            }
        }

        private void showFRUnitReg()
        {
            Constant.LogHelper.Info("Open FR unit form", GUserInfo.getInstance().getCureentUser().ID.ToString());
            FormFRUnitReg FRUnitReg = new FormFRUnitReg();
            FRUnitReg.ShowDialog();
        }


        private void showUserForm()
        {
            if (GUserInfo.getInstance().getCureentUser().SysRight == 1)
            {
                FromUser auser = new FromUser();
                auser.ShowDialog();
                Constant.LogHelper.Info("Open user management form", GUserInfo.getInstance().getCureentUser().ID.ToString());

            }
            else
            {
                Constant.LogHelper.Info("No perission to open the user management form.", GUserInfo.getInstance().getCureentUser().ID.ToString());
                MessageBox.Show("You are not have permission to management the user.");
            }
        }

        private void showUserProfile()
        {
            Constant.LogHelper.Info("Open user management form to manage own info.", GUserInfo.getInstance().getCureentUser().ID.ToString());
            DialogUserBox user = new DialogUserBox(DialogUserBox.MODE_EDIT);
            user.setUser(GUserInfo.getInstance().getCureentUser());
            user.ShowDialog();
        }

        private void showEmployee(){
            Constant.LogHelper.Info("Open employee form", GUserInfo.getInstance().getCureentUser().ID.ToString());
            FormEmployee fe = new FormEmployee();
            fe.ShowDialog();
        }
        #endregion

        #region AutoGenerate
        private void exitToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void rosterUploadAndDownloadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            showDevice();
        }

        private void importDailyAttendanceFromFRUnitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            showAttendanceImport();
        }

        private void exportDailyAttendanceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            showAttendanceExport();
        }

        private void systemUserProfileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            showUserProfile();
        }

        private void userManagementToolStripMenuItem_Click(object sender, EventArgs e)
        {
            showUserForm();
        }

        private void systemLogToolStripMenuItem_Click(object sender, EventArgs e)
        {
            showUserLog();
        }

        private void loginAnotherUserToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void faceRecognitionUnitRegistrationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            showFRUnitReg();
        }
        private void employeeRecordToolStripMenuItem_Click(object sender, EventArgs e)
        {
            showEmployee();
        }
        #endregion

        private void academicToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Constant.LogHelper.Info("Open academic form", GUserInfo.getInstance().getCureentUser().ID.ToString());
            DialogAcademic da = new DialogAcademic();
            da.ShowDialog();
        }

        private void positionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Constant.LogHelper.Info("Open position form", GUserInfo.getInstance().getCureentUser().ID.ToString());
            DialogPosition dp = new DialogPosition();
            dp.ShowDialog();
        }

        private void departmentToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Constant.LogHelper.Info("Open department form", GUserInfo.getInstance().getCureentUser().ID.ToString());
            DialogDepartment dd = new DialogDepartment();
            dd.ShowDialog();
        }

        private void statusToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Constant.LogHelper.Info("Open relationship status form", GUserInfo.getInstance().getCureentUser().ID.ToString());
            DialogStatus ds = new DialogStatus();
            ds.ShowDialog();
        }

        private void Main_FormClosed(object sender, FormClosedEventArgs e)
        {
            FaceKit.FaceKit.getInstance().serverClose();
        }




    }
}

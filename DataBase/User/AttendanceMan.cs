﻿using DataBase.User;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataBase.AttendanceMan
{
    public class AttendanceMan
    {

        public static readonly int TYPE_FIFO = 0;
        public static readonly int TYPE_FI = 1;

        private static DataTable InitalAttendance()
        {
            DataTable dt = new DataTable("Attendance");
            DataColumn dcID = new DataColumn("ARID");
            dt.Columns.Add(dcID);
            DataColumn dcTD = new DataColumn("TransactionDate");
            dt.Columns.Add(dcTD);
            DataColumn dcDeviceID = new DataColumn("DeviceID");
            dt.Columns.Add(dcDeviceID);
            DataColumn employeeID = new DataColumn("EmployeeID");
            dt.Columns.Add(employeeID);
            return dt;
        }

        private static DataTable InitalStart()
        {
            //"Code,Name,Transaction Date,In Time,Out Time,ReaderID1,ReaderID2"
            DataTable dt = new DataTable("Attendance");
            //DataColumn dcID = new DataColumn("ARID");
            //dt.Columns.Add(dcID);
            DataColumn dcCode = new DataColumn("Code");
            dt.Columns.Add(dcCode);
            DataColumn dcName = new DataColumn("Name");
            dt.Columns.Add(dcName);
            DataColumn dcTD = new DataColumn("TransactionDate");
            dt.Columns.Add(dcTD);
            DataColumn dcIn = new DataColumn("InTime");
            dt.Columns.Add(dcIn);
            DataColumn dcOut = new DataColumn("OutTime");
            dt.Columns.Add(dcOut);
            DataColumn reader01 = new DataColumn("Reader01");
            dt.Columns.Add(reader01);
            DataColumn reader02 = new DataColumn("Reader02");
            dt.Columns.Add(reader02);
            return dt;

        }


        public static DataTable getAttendanceInDatabase(ArrayList al/*double? startTime, double? endTime, float? employeeID, float? deviceID, int isSuccess*/)
        {
            DataTable dt = InitalAttendance();
            DataRow dr;
           // ArrayList al = getAttendanceData(startTime, endTime, employeeID, deviceID, isSuccess);
            foreach (DataModel.User.AttendanceInfo d in al)
            {
                dr = dt.NewRow();
                dr["ARID"] = d.ARID;
                dr["TransactionDate"] = d.getDateTime;
                dr["DeviceID"] = d.DeviceID;
                dr["EmployeeID"] = UserMan.eidToHKID(d.EmployeeID.ToString());
                dt.Rows.Add(dr);

            }
            return dt;
        }

        public static DataTable getAttendaceByFilter(ArrayList al) {
            DataTable dt = InitalStart();
            DataRow dr;
            // ArrayList al = getAttendanceData(startTime, endTime, employeeID, deviceID, isSuccess);
            foreach (DataModel.User.ExportAtendance d in al)
            {
                dr = dt.NewRow();
                dr["Code"] = UserMan.eidToHKID(d.Code);
                dr["Name"] = d.Name;
                dr["TransactionDate"] = d.TransactionDate;
                dr["InTime"] = d.InTime;
                dr["OutTime"] = d.OutTime;
                dr["Reader01"] = d.ReaderID1;
                dr["Reader02"] = d.ReaderID2;
                dt.Rows.Add(dr);

            }
            return dt;
        
        }

        //isSuccess is checked by not that value
        public static ArrayList getAttendanceData(double? startTime, double? endTime, float? employeeID, double? deviceID, int? isSuccess)
        {
            ArrayList al = new ArrayList();

            gsfrasDataContext mContext = new gsfrasDataContext();
            var result = from attendance in mContext.EmployeeAttendanceRecords where attendance.EmployeeID != isSuccess orderby attendance.ARID select attendance;

            if (startTime != null & endTime != null && employeeID != null && deviceID != null)
            {
                result = from attendance in mContext.EmployeeAttendanceRecords where attendance.TransactionDate >= startTime && attendance.TransactionDate <= endTime && attendance.EmployeeID == employeeID && attendance.DeviceID == deviceID && attendance.EmployeeID != isSuccess orderby attendance.ARID select attendance;
            }
            else if (startTime != null & endTime != null && employeeID == null && deviceID == null)
            {
                result = from attendance in mContext.EmployeeAttendanceRecords where attendance.TransactionDate >= startTime && attendance.TransactionDate <= endTime && attendance.EmployeeID != isSuccess orderby attendance.ARID select attendance;
            }
            else if (startTime != null & endTime != null && employeeID == null && deviceID != null)
            {
                result = from attendance in mContext.EmployeeAttendanceRecords where attendance.TransactionDate >= startTime && attendance.TransactionDate <= endTime && attendance.DeviceID == deviceID && attendance.EmployeeID != isSuccess orderby attendance.ARID select attendance;
            }
            else if (startTime != null & endTime != null && employeeID != null && deviceID == null)
            {
                result = from attendance in mContext.EmployeeAttendanceRecords where attendance.TransactionDate >= startTime && attendance.TransactionDate <= endTime && attendance.EmployeeID.ToString().Contains(employeeID.ToString()) && attendance.EmployeeID != isSuccess orderby attendance.ARID select attendance;
            }

            else if (startTime == null & endTime == null && employeeID != null && deviceID != null)
            {
                result = from attendance in mContext.EmployeeAttendanceRecords where attendance.EmployeeID.ToString().Contains(employeeID.ToString()) && attendance.DeviceID == deviceID && attendance.EmployeeID != isSuccess orderby attendance.ARID select attendance;
            }
            else if (startTime == null & endTime == null && employeeID == null && deviceID != null)
            {
                result = from attendance in mContext.EmployeeAttendanceRecords where attendance.DeviceID == deviceID && attendance.EmployeeID != isSuccess orderby attendance.ARID select attendance;
            }
            else if (startTime == null & endTime == null && employeeID != null && deviceID == null)
            {
                result = from attendance in mContext.EmployeeAttendanceRecords where attendance.EmployeeID.ToString().Contains(employeeID.ToString()) && attendance.EmployeeID != isSuccess orderby attendance.ARID select attendance;
            }

            foreach (var item in result)
            {
                al.Add(new DataModel.User.AttendanceInfo(item.ARID, item.TransactionDate, item.DeviceID, item.EmployeeID, item.RecType, item.Status, item.PhotoLen, item.Reason, item.Base64PhotoData));
            }
            return al;

        }




        public static ArrayList getAttendanceByID(ArrayList al)
        {
            ArrayList re = new ArrayList();
            gsfrasDataContext mContext = new gsfrasDataContext();
            String[] ids = new String[al.Count];
            for (int i = 0; i < al.Count; i++)
            {
                ids[i] = al[i].ToString();
            }
            var result = from attendance in mContext.EmployeeAttendanceRecords where ids.Contains(attendance.ARID.ToString()) orderby attendance.ARID select attendance;
            foreach (var item in result)
            {
                re.Add(new DataModel.User.AttendanceInfo(item.ARID, item.TransactionDate, item.DeviceID, item.EmployeeID, item.RecType, item.Status, item.PhotoLen, item.Reason, item.Base64PhotoData));
            }
            return re;
        }

        public static Boolean createAttendance(ArrayList al)
        {
            Boolean isAdded = true;
            try
            {

                gsfrasDataContext mContext = new gsfrasDataContext();
                foreach (DataModel.User.AttendanceInfo att in al)
                {
                    EmployeeAttendanceRecord employee = (from e in mContext.EmployeeAttendanceRecords where e.ARID == att.ARID select e).SingleOrDefault();
                    if (employee == null)
                    {

                        EmployeeAttendanceRecord ea = new EmployeeAttendanceRecord()
                        {
                            ARID = att.ARID,
                            Base64PhotoData = att.Base64PhotoData,
                            DeviceID = att.DeviceID,
                            EmployeeID = att.EmployeeID,
                            PhotoLen = att.PhotoLen,
                            Reason = att.Reason,
                            RecType = att.RecType,
                            Status = att.Status,
                            TransactionDate = att.TransactionDate
                        };
                        mContext.EmployeeAttendanceRecords.InsertOnSubmit(ea);
                        mContext.SubmitChanges();
                    }


                }

            }
            catch (Exception) { isAdded = false; }

            return isAdded;
        }

        public static DataTable getAttendanceAfterInsert(ArrayList al, int isSuccess)
        {
            DataTable dt = InitalAttendance();
            DataRow dr;
            foreach (DataModel.User.AttendanceInfo d in al)
            {
                if (d.EmployeeID != isSuccess)
                {
                    dr = dt.NewRow();
                    dr["ARID"] = d.ARID;
                    dr["TransactionDate"] = d.getDateTime;
                    dr["DeviceID"] = d.DeviceID;
                    dr["EmployeeID"] = UserMan.eidToHKID(d.EmployeeID.ToString());
                    dt.Rows.Add(dr);
                }
            }
            return dt;
        }



        public static void exportCSV(ArrayList al, String path)
        {

            Stopwatch sWatch = new Stopwatch();
            sWatch.Start();
            StringBuilder sBuilder = new StringBuilder();
            sBuilder.AppendLine("Code,Name,Transaction Date,In Time,Out Time,ReaderID1,ReaderID2");
            foreach (DataModel.User.ExportAtendance ud in al)
            {
                sBuilder.AppendLine(ud.Code.ToString() + "," + ud.Name + "," + ud.TransactionDate + "," + ud.InTime + "," + ud.OutTime + "," + ud.ReaderID1 + "," + ud.ReaderID2);
            }
            File.WriteAllText(path, sBuilder.ToString());
            sWatch.Stop();
        }

        public static ArrayList attendanceList(int type, ArrayList al, DateTime aStartDate, DateTime aEndDate)
        {
            ArrayList exportList = new ArrayList();
            String[] userIDs = new String[al.Count];
            for (int i = 0; i < al.Count; i++)
            {
                userIDs[i] = ((DataModel.User.AttendanceInfo)al[i]).EmployeeID.ToString();
            }
            String[] uids = userIDs.Distinct().ToArray();

            if (type == TYPE_FIFO)
            {
                for (int i = 0; i < uids.Length; i++)
                {
                    DateTime startDate = aStartDate;
                    DateTime endDate = aEndDate;

                    while (startDate < endDate)
                    {
                        DateTime currnent = startDate;
                        startDate = startDate.AddDays(1);
                        double sd = Constant.Constants.ConvertDateToTimestamp(currnent, Constant.Constants.TIME_ZONE);
                        double ed = Constant.Constants.ConvertDateToTimestamp(startDate, Constant.Constants.TIME_ZONE);
                        var result = from DataModel.User.AttendanceInfo attendance in al where attendance.TransactionDate > sd && attendance.TransactionDate < ed && attendance.EmployeeID == double.Parse(uids[i]) select attendance;
                        if (result.Count() > 0)
                        {
                            DataModel.User.ExportAtendance ep = new DataModel.User.ExportAtendance();
                            ep.Code = uids[i];
                            try
                            {
                                ep.Name = DeviceMan.DeviceMan.getUser(uids[i]).UserName;
                            }
                            catch(Exception){}
                            ep.TransactionDate = result.First().getDateTime.ToString(Constant.Constants.DATE_FORMAT);
                            ep.InTime = result.First().getDateTime.ToString(Constant.Constants.TIME_FORMAT);
                            ep.ReaderID1 = result.First().DeviceID.ToString();
                            if (result.Count() > 1)
                            {
                                ep.OutTime = result.Last().getDateTime.ToString(Constant.Constants.TIME_FORMAT);
                                ep.ReaderID2 = result.Last().DeviceID.ToString();
                            }
                            exportList.Add(ep);

                        }

                    }


                }
            }
            else if (type == TYPE_FI)
            {
                for (int i = 0; i < uids.Length; i++)
                {
                    DateTime startDate = aStartDate;
                    DateTime endDate = aEndDate;

                    while (startDate < endDate)
                    {
                        DateTime currnent = startDate;
                        startDate = startDate.AddDays(1);
                        double sd = Constant.Constants.ConvertDateToTimestamp(currnent, Constant.Constants.TIME_ZONE);
                        double ed = Constant.Constants.ConvertDateToTimestamp(startDate, Constant.Constants.TIME_ZONE);
                        var result = from DataModel.User.AttendanceInfo attendance in al where attendance.TransactionDate > sd && attendance.TransactionDate < ed /*&& attendance.EmployeeID == double.Parse(uids[i])*/ select attendance;
                        if (System.Linq.Enumerable.Count(result) > 0)
                        {
                            DataModel.User.ExportAtendance ep = new DataModel.User.ExportAtendance();
                            ep.Code = uids[i];
                            try
                            {
                                ep.Name = DeviceMan.DeviceMan.getUser(uids[i]).UserName;
                            }
                            catch(Exception){}
                            ep.TransactionDate = result.First().getDateTime.ToString(Constant.Constants.DATE_FORMAT);
                            ep.InTime = result.First().getDateTime.ToString(Constant.Constants.TIME_FORMAT);
                            ep.ReaderID1 = result.First().DeviceID.ToString();
                            /*if (result.Count() > 1)
                            {
                                ep.OutTime = result.Last().getDateTime.ToString(Constant.Constants.DATETIME_FORMAT);
                                ep.ReaderID2 = result.Last().DeviceID.ToString();
                            }*/
                            exportList.Add(ep);

                        }

                    }


                }
            }
            return exportList;


        }

    }
}

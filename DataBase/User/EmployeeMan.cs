﻿using DataModel.DeviceModel;
using DataModel.User;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataBase.User
{
    public class EmployeeMan
    {

        public static ArrayList getAllEmployee()
        {
            ArrayList al = new ArrayList();
            gsfrasDataContext mContext = new gsfrasDataContext();

            var a = from employee in mContext.Employees
                    join device in mContext.DeviceEmployees on employee.EmployeeID equals device.EmployeeID into ED
                    from ed in ED.DefaultIfEmpty()
                    select new
                    {
                        eAcademic = employee.Academic,
                        eActualAnnualLeave = employee.ActualAnnualLeave,
                        eAddress = employee.Address,
                        eAnnualLeave = employee.AnnualLeave,
                        eBasicSalary = employee.BasicSalary,
                        eCreateDate = employee.CreateDate,
                        eDepartment = employee.Department,
                        eDOB = employee.DOB,
                        eEmail = employee.Email,
                        eEmployeeID = (employee.EmployeeID == null) ? 0 : employee.EmployeeID,
                        eHKID = employee.HKID,
                        eJoinDate = employee.JoinDate,
                        eRemark = employee.Remark,
                        elastUser = employee.LastUser,
                        eMobile = employee.Mobile,
                        eMPF = employee.MPF,
                        eName = employee.Name,
                        eOT = employee.OT,
                        ePosition = employee.Postion,
                        eQuitDate = employee.QuitDate,
                        eRelationshipStatus = employee.RelationshipStatus,
                        eTel = employee.Tel,
                        eType = employee.Type,

                        dEmployeeID = (ed.EmployeeID == null) ? 0 : ed.EmployeeID,
                        dCardNo = ed.CardNo,
                        dUserName = ed.UserName,
                        dVerfyMode = ed.VerifyMode,
                        dRegStatus = ed.RegStatus,
                        dPhotoLen = ed.PhotoLen,
                        dBase64PhotoData = ed.Base64PhotoData,
                        dFeatureLen = ed.FeatureLen,
                        dFeatureBase64 = ed.FeatureBase64,
                        dUserType = ed.userType


                    };
            var b = from device in mContext.DeviceEmployees
                    join employee in mContext.Employees on device.EmployeeID equals employee.EmployeeID into ED
                    from ed in ED.DefaultIfEmpty()
                    select new
                    {
                        eAcademic = ed.Academic,
                        eActualAnnualLeave = ed.ActualAnnualLeave,
                        eAddress = ed.Address,
                        eAnnualLeave = ed.AnnualLeave,
                        eBasicSalary = ed.BasicSalary,
                        eCreateDate = ed.CreateDate,
                        eDepartment = ed.Department,
                        eDOB = ed.DOB,
                        eEmail = ed.Email,
                        eEmployeeID = (ed.EmployeeID == null) ? 0 : ed.EmployeeID,
                        eHKID = ed.HKID,
                        eJoinDate = ed.JoinDate,
                        eRemark = ed.Remark,
                        elastUser = ed.LastUser,
                        eMobile = ed.Mobile,
                        eMPF = ed.MPF,
                        eName = ed.Name,
                        eOT = ed.OT,
                        ePosition = ed.Postion,
                        eQuitDate = ed.QuitDate,
                        eRelationshipStatus = ed.RelationshipStatus,
                        eTel = ed.Tel,
                        eType = ed.Type,

                        dEmployeeID = (device.EmployeeID == null) ? 0 : device.EmployeeID,
                        dCardNo = device.CardNo,
                        dUserName = device.UserName,
                        dVerfyMode = device.VerifyMode,
                        dRegStatus = device.RegStatus,
                        dPhotoLen = device.PhotoLen,
                        dBase64PhotoData = device.Base64PhotoData,
                        dFeatureLen = device.FeatureLen,
                        dFeatureBase64 = device.FeatureBase64,
                        dUserType = device.userType


                    };

            var c = a.Concat(b).Distinct();

            foreach (var cc in c)
            {
                EmployeeDevice ed = new EmployeeDevice();
                UserDevice u = null;
                EmployeeInfo e = null;
                if (cc.dEmployeeID != 0)
                {
                    ed.EmployeeID = cc.dEmployeeID;

                    u = new UserDevice(cc.dEmployeeID, cc.dCardNo, cc.dUserName, cc.dVerfyMode, cc.dRegStatus, cc.dPhotoLen, cc.dBase64PhotoData, cc.dFeatureLen, cc.dFeatureBase64, cc.dUserType);
                }
                if (cc.eEmployeeID != 0)
                {
                    ed.EmployeeID = cc.eEmployeeID;

                    e = new EmployeeInfo(cc.eEmployeeID, cc.eName, cc.eHKID, cc.eDOB, cc.eRelationshipStatus, cc.eAddress, cc.eTel, cc.eMobile, cc.eEmail, cc.eType, cc.ePosition, cc.eDepartment, cc.eAcademic, cc.eJoinDate, cc.eQuitDate, cc.eBasicSalary, cc.eOT, cc.eAnnualLeave, cc.eActualAnnualLeave, cc.eMPF, cc.eRemark, cc.elastUser, cc.eCreateDate);
                }

                ed.mDevice = u;
                ed.mInfo = e;
                al.Add(ed);
                /* EmployeeInfo e = new EmployeeInfo();
                e.Academic = cc.eAcademic;
                e.ActualAnnualLeave = cc.eActualAnnualLeave;
                e.Address = cc.eAddress;
                e.AnnualLeave = cc.eAnnualLeave;
                e.BasicSalary = cc.eBasicSalary;
                e.CreateDate = cc.eCreateDate;
                e.Department = cc.eDepartment;
                e.DOB = cc.eDOB;
                e.Email = cc.eEmail;
                e.EmployeeID = cc.eEmployeeID;
                e.HKID = cc.eHKID;
                e.JoinDate = cc.eJoinDate;
                e.lastUser = cc.elastUser;
                e.Mobile = cc.eMobile;
                e.MPF = cc.eMPF;
                e.Name = cc.eName;
                e.OT = cc.eOT;
                e.Position = cc.ePosition;
                e.QuitDate = cc.eQuitDate;
                e.RelationshipStatus = cc.eRelationshipStatus;
                e.Remark = cc.eRemark;
                e.Tel = cc.eTel;
                e.Type = cc.eType;*/

            }

            return al;
        }


        public static Boolean updateEmployee(EmployeeInfo ei)
        {
            Boolean isUpdate = true;
            gsfrasDataContext mContext = new gsfrasDataContext();
            try
            {
                Employee user = mContext.Employees.Single(p => p.EmployeeID == ei.EmployeeID);
                user.Academic = ei.Academic;
                user.ActualAnnualLeave = ei.ActualAnnualLeave;
                user.Address = ei.Address;
                user.AnnualLeave = ei.AnnualLeave;
                user.BasicSalary = ei.BasicSalary;
                user.CreateDate = ei.CreateDate;
                user.Department = ei.Department;
                user.DOB = ei.DOB;
                user.Email = ei.Email;
                user.HKID = ei.HKID;
                user.JoinDate = ei.JoinDate;
                user.LastUser = ei.lastUser;
                user.Mobile = ei.Mobile;
                user.MPF = ei.MPF;
                user.Name = ei.Name;
                user.OT = ei.OT;
                user.Postion = ei.Position;
                user.QuitDate = ei.QuitDate;
                user.RelationshipStatus = ei.RelationshipStatus;
                user.Remark = ei.Remark;
                user.Tel = user.Tel;
                user.Type = ei.Type;
                mContext.SubmitChanges();
            }
            catch (Exception) { isUpdate = false; }
            return isUpdate;
        }

        public static Boolean createEmployee(EmployeeInfo ei)
        {
            Boolean isCreate = true;
            gsfrasDataContext mContext = new gsfrasDataContext();

            try
            {
                Employee user = new Employee();
                user.EmployeeID = ei.EmployeeID;
                user.Academic = ei.Academic;
                user.ActualAnnualLeave = ei.ActualAnnualLeave;
                user.Address = ei.Address;
                user.AnnualLeave = ei.AnnualLeave;
                user.BasicSalary = ei.BasicSalary;
                user.CreateDate = ei.CreateDate;
                user.Department = ei.Department;
                user.DOB = ei.DOB;
                user.Email = ei.Email;
                user.HKID = ei.HKID;
                user.JoinDate = ei.JoinDate;
                user.LastUser = ei.lastUser;
                user.Mobile = ei.Mobile;
                user.MPF = ei.MPF;
                user.Name = ei.Name;
                user.OT = ei.OT;
                user.Postion = ei.Position;
                user.QuitDate = ei.QuitDate;
                user.RelationshipStatus = ei.RelationshipStatus;
                user.Remark = ei.Remark;
                user.Tel = ei.Tel;
                user.Type = ei.Type;
                mContext.Employees.InsertOnSubmit(user);
                mContext.SubmitChanges();
            }
            catch (Exception) { isCreate = false; }

            return isCreate;
        }

        public static Boolean deleteEmployee(int employeeID)
        {
            Boolean isDelete = true;
            gsfrasDataContext mContext = new gsfrasDataContext();

            try
            {
                var DelItem = from p in mContext.Employees where p.EmployeeID == employeeID select p;

                mContext.Employees.DeleteAllOnSubmit(DelItem);
                mContext.SubmitChanges();
            }
            catch (Exception) { isDelete = false; }

            return isDelete;
        }

        private static DataTable InitalEmployee()
        {
            DataTable dt = new DataTable("Employee");
            DataColumn dcID = new DataColumn("EmployeeID");
            dt.Columns.Add(dcID);
            DataColumn dcHKID = new DataColumn("HKID");
            dt.Columns.Add(dcHKID);
            DataColumn dcName = new DataColumn("Name");
            dt.Columns.Add(dcName);
            //DataColumn dcDName = new DataColumn("DeviceName");
            DataColumn dcStatus = new DataColumn("Status");
            dt.Columns.Add(dcStatus);
            //dt.Columns.Add(dcDName);
            return dt;
        }

        public static DataTable InitalData(ArrayList al)
        {
            DataTable dt = InitalEmployee();
            DataRow dr;
            foreach (EmployeeDevice d in al)
            {
                dr = dt.NewRow();
                dr["EmployeeID"] = d.EmployeeID;
                dr["HKID"] = UserMan.eidToHKID(d.EmployeeID.ToString());
                dr["Name"] = "";
                if (d.mInfo != null)
                {
                    dr["Name"] = d.mInfo.Name;
                }
                // dr["DeviceName"]="";
                if (d.mDevice != null)
                {
                    dr["Name"] = d.mDevice.UserName;
                }
                if (d.mInfo == null || d.mDevice == null)
                {
                    dr["Status"] = "Pending";
                }
                else
                {
                    dr["Status"] = "Synced";
                }
                dt.Rows.Add(dr);
            }
            return dt;
        }


        public static ArrayList searchEmployee(String key)
        {
            ArrayList al = new ArrayList();
            gsfrasDataContext mContext = new gsfrasDataContext();

            var a = from employee in mContext.Employees
                    join device in mContext.DeviceEmployees on employee.EmployeeID equals device.EmployeeID into ED
                    from ed in ED.DefaultIfEmpty()
                    select new
                    {
                        eAcademic = employee.Academic,
                        eActualAnnualLeave = employee.ActualAnnualLeave,
                        eAddress = employee.Address,
                        eAnnualLeave = employee.AnnualLeave,
                        eBasicSalary = employee.BasicSalary,
                        eCreateDate = employee.CreateDate,
                        eDepartment = employee.Department,
                        eDOB = employee.DOB,
                        eEmail = employee.Email,
                        eEmployeeID = (employee.EmployeeID == null) ? 0 : employee.EmployeeID,
                        eHKID = employee.HKID,
                        eJoinDate = employee.JoinDate,
                        eRemark = employee.Remark,
                        elastUser = employee.LastUser,
                        eMobile = employee.Mobile,
                        eMPF = employee.MPF,
                        eName = employee.Name,
                        eOT = employee.OT,
                        ePosition = employee.Postion,
                        eQuitDate = employee.QuitDate,
                        eRelationshipStatus = employee.RelationshipStatus,
                        eTel = employee.Tel,
                        eType = employee.Type,

                        dEmployeeID = (ed.EmployeeID == null) ? 0 : ed.EmployeeID,
                        dCardNo = ed.CardNo,
                        dUserName = ed.UserName,
                        dVerfyMode = ed.VerifyMode,
                        dRegStatus = ed.RegStatus,
                        dPhotoLen = ed.PhotoLen,
                        dBase64PhotoData = ed.Base64PhotoData,
                        dFeatureLen = ed.FeatureLen,
                        dFeatureBase64 = ed.FeatureBase64,
                        dUserType = ed.userType


                    };
            var b = from device in mContext.DeviceEmployees
                    join employee in mContext.Employees on device.EmployeeID equals employee.EmployeeID into ED
                    from ed in ED.DefaultIfEmpty()
                    select new
                    {
                        eAcademic = ed.Academic,
                        eActualAnnualLeave = ed.ActualAnnualLeave,
                        eAddress = ed.Address,
                        eAnnualLeave = ed.AnnualLeave,
                        eBasicSalary = ed.BasicSalary,
                        eCreateDate = ed.CreateDate,
                        eDepartment = ed.Department,
                        eDOB = ed.DOB,
                        eEmail = ed.Email,
                        eEmployeeID = (ed.EmployeeID == null) ? 0 : ed.EmployeeID,
                        eHKID = ed.HKID,
                        eJoinDate = ed.JoinDate,
                        eRemark = ed.Remark,
                        elastUser = ed.LastUser,
                        eMobile = ed.Mobile,
                        eMPF = ed.MPF,
                        eName = ed.Name,
                        eOT = ed.OT,
                        ePosition = ed.Postion,
                        eQuitDate = ed.QuitDate,
                        eRelationshipStatus = ed.RelationshipStatus,
                        eTel = ed.Tel,
                        eType = ed.Type,

                        dEmployeeID = (device.EmployeeID == null) ? 0 : device.EmployeeID,
                        dCardNo = device.CardNo,
                        dUserName = device.UserName,
                        dVerfyMode = device.VerifyMode,
                        dRegStatus = device.RegStatus,
                        dPhotoLen = device.PhotoLen,
                        dBase64PhotoData = device.Base64PhotoData,
                        dFeatureLen = device.FeatureLen,
                        dFeatureBase64 = device.FeatureBase64,
                        dUserType = device.userType


                    };

            var c = a.Concat(b).Distinct();
            var d = from dd in c where dd.eName.Contains(key) || dd.eHKID.Contains(key) || dd.dUserName.Contains(key) || dd.dEmployeeID.ToString().Contains(UserMan.generateID(key)) select dd;
            foreach (var cc in d)
            {
                EmployeeDevice ed = new EmployeeDevice();
                UserDevice u = null;
                EmployeeInfo e = null;
                if (cc.dEmployeeID != 0)
                {
                    ed.EmployeeID = cc.dEmployeeID;

                    u = new UserDevice(cc.dEmployeeID, cc.dCardNo, cc.dUserName, cc.dVerfyMode, cc.dRegStatus, cc.dPhotoLen, cc.dBase64PhotoData, cc.dFeatureLen, cc.dFeatureBase64, cc.dUserType);
                }
                if (cc.eEmployeeID != 0)
                {
                    ed.EmployeeID = cc.eEmployeeID;

                    e = new EmployeeInfo(cc.eEmployeeID, cc.eName, cc.eHKID, cc.eDOB, cc.eRelationshipStatus, cc.eAddress, cc.eTel, cc.eMobile, cc.eEmail, cc.eType, cc.ePosition, cc.eDepartment, cc.eAcademic, cc.eJoinDate, cc.eQuitDate, cc.eBasicSalary, cc.eOT, cc.eAnnualLeave, cc.eActualAnnualLeave, cc.eMPF, cc.eRemark, cc.elastUser, cc.eCreateDate);
                }

                ed.mDevice = u;
                ed.mInfo = e;
                al.Add(ed);

            }

            return al;
        }


        public static ArrayList dulpicateEmployee()
        {
            ArrayList al = new ArrayList();
            gsfrasDataContext mContext = new gsfrasDataContext();

            var a = from employee in mContext.Employees
                    join device in mContext.DeviceEmployees on employee.EmployeeID equals device.EmployeeID into ED
                    from ed in ED.DefaultIfEmpty()
                    select new
                    {
                        eAcademic = employee.Academic,
                        eActualAnnualLeave = employee.ActualAnnualLeave,
                        eAddress = employee.Address,
                        eAnnualLeave = employee.AnnualLeave,
                        eBasicSalary = employee.BasicSalary,
                        eCreateDate = employee.CreateDate,
                        eDepartment = employee.Department,
                        eDOB = employee.DOB,
                        eEmail = employee.Email,
                        eEmployeeID = (employee.EmployeeID == null) ? 0 : employee.EmployeeID,
                        eHKID = employee.HKID,
                        eJoinDate = employee.JoinDate,
                        eRemark = employee.Remark,
                        elastUser = employee.LastUser,
                        eMobile = employee.Mobile,
                        eMPF = employee.MPF,
                        eName = employee.Name,
                        eOT = employee.OT,
                        ePosition = employee.Postion,
                        eQuitDate = employee.QuitDate,
                        eRelationshipStatus = employee.RelationshipStatus,
                        eTel = employee.Tel,
                        eType = employee.Type,

                        dEmployeeID = (ed.EmployeeID == null) ? 0 : ed.EmployeeID,
                        dCardNo = ed.CardNo,
                        dUserName = ed.UserName,
                        dVerfyMode = ed.VerifyMode,
                        dRegStatus = ed.RegStatus,
                        dPhotoLen = ed.PhotoLen,
                        dBase64PhotoData = ed.Base64PhotoData,
                        dFeatureLen = ed.FeatureLen,
                        dFeatureBase64 = ed.FeatureBase64,
                        dUserType = ed.userType


                    };
            var b = from device in mContext.DeviceEmployees
                    join employee in mContext.Employees on device.EmployeeID equals employee.EmployeeID into ED
                    from ed in ED.DefaultIfEmpty()
                    select new
                    {
                        eAcademic = ed.Academic,
                        eActualAnnualLeave = ed.ActualAnnualLeave,
                        eAddress = ed.Address,
                        eAnnualLeave = ed.AnnualLeave,
                        eBasicSalary = ed.BasicSalary,
                        eCreateDate = ed.CreateDate,
                        eDepartment = ed.Department,
                        eDOB = ed.DOB,
                        eEmail = ed.Email,
                        eEmployeeID = (ed.EmployeeID == null) ? 0 : ed.EmployeeID,
                        eHKID = ed.HKID,
                        eJoinDate = ed.JoinDate,
                        eRemark = ed.Remark,
                        elastUser = ed.LastUser,
                        eMobile = ed.Mobile,
                        eMPF = ed.MPF,
                        eName = ed.Name,
                        eOT = ed.OT,
                        ePosition = ed.Postion,
                        eQuitDate = ed.QuitDate,
                        eRelationshipStatus = ed.RelationshipStatus,
                        eTel = ed.Tel,
                        eType = ed.Type,

                        dEmployeeID = (device.EmployeeID == null) ? 0 : device.EmployeeID,
                        dCardNo = device.CardNo,
                        dUserName = device.UserName,
                        dVerfyMode = device.VerifyMode,
                        dRegStatus = device.RegStatus,
                        dPhotoLen = device.PhotoLen,
                        dBase64PhotoData = device.Base64PhotoData,
                        dFeatureLen = device.FeatureLen,
                        dFeatureBase64 = device.FeatureBase64,
                        dUserType = device.userType


                    };

            var c = a.Concat(b).Distinct();
            var d = from dd in c where dd.eName != null && dd.dUserName != null group dd by dd.eName into g where g.Count() > 1 select g;
            foreach (var cc in d)
            {
                foreach (var item in cc)
                {
                    EmployeeDevice ed = new EmployeeDevice();
                    UserDevice u = null;
                    EmployeeInfo e = null;
                    if (item.dEmployeeID != 0)
                    {
                        ed.EmployeeID = item.dEmployeeID;

                        u = new UserDevice(item.dEmployeeID, item.dCardNo, item.dUserName, item.dVerfyMode, item.dRegStatus, item.dPhotoLen, item.dBase64PhotoData, item.dFeatureLen, item.dFeatureBase64, item.dUserType);
                    }
                    if (item.eEmployeeID != 0)
                    {
                        ed.EmployeeID = item.eEmployeeID;

                        e = new EmployeeInfo(item.eEmployeeID, item.eName, item.eHKID, item.eDOB, item.eRelationshipStatus, item.eAddress, item.eTel, item.eMobile, item.eEmail, item.eType, item.ePosition, item.eDepartment, item.eAcademic, item.eJoinDate, item.eQuitDate, item.eBasicSalary, item.eOT, item.eAnnualLeave, item.eActualAnnualLeave, item.eMPF, item.eRemark, item.elastUser, item.eCreateDate);
                    }

                    ed.mDevice = u;
                    ed.mInfo = e;
                    al.Add(ed);
                }

            }

            return al;
        }

        public static ArrayList missEmployee(Boolean isLeft)
        {
            ArrayList al = new ArrayList();
            gsfrasDataContext mContext = new gsfrasDataContext();

            var a = from employee in mContext.Employees
                    join device in mContext.DeviceEmployees on employee.EmployeeID equals device.EmployeeID into ED
                    from ed in ED.DefaultIfEmpty()
                    select new
                    {
                        eAcademic = employee.Academic,
                        eActualAnnualLeave = employee.ActualAnnualLeave,
                        eAddress = employee.Address,
                        eAnnualLeave = employee.AnnualLeave,
                        eBasicSalary = employee.BasicSalary,
                        eCreateDate = employee.CreateDate,
                        eDepartment = employee.Department,
                        eDOB = employee.DOB,
                        eEmail = employee.Email,
                        eEmployeeID = (employee.EmployeeID == null) ? 0 : employee.EmployeeID,
                        eHKID = employee.HKID,
                        eJoinDate = employee.JoinDate,
                        eRemark = employee.Remark,
                        elastUser = employee.LastUser,
                        eMobile = employee.Mobile,
                        eMPF = employee.MPF,
                        eName = employee.Name,
                        eOT = employee.OT,
                        ePosition = employee.Postion,
                        eQuitDate = employee.QuitDate,
                        eRelationshipStatus = employee.RelationshipStatus,
                        eTel = employee.Tel,
                        eType = employee.Type,

                        dEmployeeID = (ed.EmployeeID == null) ? 0 : ed.EmployeeID,
                        dCardNo = ed.CardNo,
                        dUserName = ed.UserName,
                        dVerfyMode = ed.VerifyMode,
                        dRegStatus = ed.RegStatus,
                        dPhotoLen = ed.PhotoLen,
                        dBase64PhotoData = ed.Base64PhotoData,
                        dFeatureLen = ed.FeatureLen,
                        dFeatureBase64 = ed.FeatureBase64,
                        dUserType = ed.userType


                    };
            var b = from device in mContext.DeviceEmployees
                    join employee in mContext.Employees on device.EmployeeID equals employee.EmployeeID into ED
                    from ed in ED.DefaultIfEmpty()
                    select new
                    {
                        eAcademic = ed.Academic,
                        eActualAnnualLeave = ed.ActualAnnualLeave,
                        eAddress = ed.Address,
                        eAnnualLeave = ed.AnnualLeave,
                        eBasicSalary = ed.BasicSalary,
                        eCreateDate = ed.CreateDate,
                        eDepartment = ed.Department,
                        eDOB = ed.DOB,
                        eEmail = ed.Email,
                        eEmployeeID = (ed.EmployeeID == null) ? 0 : ed.EmployeeID,
                        eHKID = ed.HKID,
                        eJoinDate = ed.JoinDate,
                        eRemark = ed.Remark,
                        elastUser = ed.LastUser,
                        eMobile = ed.Mobile,
                        eMPF = ed.MPF,
                        eName = ed.Name,
                        eOT = ed.OT,
                        ePosition = ed.Postion,
                        eQuitDate = ed.QuitDate,
                        eRelationshipStatus = ed.RelationshipStatus,
                        eTel = ed.Tel,
                        eType = ed.Type,

                        dEmployeeID = (device.EmployeeID == null) ? 0 : device.EmployeeID,
                        dCardNo = device.CardNo,
                        dUserName = device.UserName,
                        dVerfyMode = device.VerifyMode,
                        dRegStatus = device.RegStatus,
                        dPhotoLen = device.PhotoLen,
                        dBase64PhotoData = device.Base64PhotoData,
                        dFeatureLen = device.FeatureLen,
                        dFeatureBase64 = device.FeatureBase64,
                        dUserType = device.userType


                    };

            var c = a.Concat(b).Distinct();
            if (isLeft)
            {
                var d = from dd in c where dd.eName == null select dd;
                foreach (var cc in d)
                {

                    EmployeeDevice ed = new EmployeeDevice();
                    UserDevice u = null;
                    EmployeeInfo e = null;
                    if (cc.dEmployeeID != 0)
                    {
                        ed.EmployeeID = cc.dEmployeeID;

                        u = new UserDevice(cc.dEmployeeID, cc.dCardNo, cc.dUserName, cc.dVerfyMode, cc.dRegStatus, cc.dPhotoLen, cc.dBase64PhotoData, cc.dFeatureLen, cc.dFeatureBase64, cc.dUserType);
                    }
                    if (cc.eEmployeeID != 0)
                    {
                        ed.EmployeeID = cc.eEmployeeID;

                        e = new EmployeeInfo(cc.eEmployeeID, cc.eName, cc.eHKID, cc.eDOB, cc.eRelationshipStatus, cc.eAddress, cc.eTel, cc.eMobile, cc.eEmail, cc.eType, cc.ePosition, cc.eDepartment, cc.eAcademic, cc.eJoinDate, cc.eQuitDate, cc.eBasicSalary, cc.eOT, cc.eAnnualLeave, cc.eActualAnnualLeave, cc.eMPF, cc.eRemark, cc.elastUser, cc.eCreateDate);
                    }
                    ed.mDevice = u;
                    ed.mInfo = e;
                    al.Add(ed);


                }
            }
            else
            {
                var d = from dd in c where dd.dUserName == null select dd;
                foreach (var cc in d)
                {

                    EmployeeDevice ed = new EmployeeDevice();
                    UserDevice u = null;
                    EmployeeInfo e = null;
                    if (cc.dEmployeeID != 0)
                    {
                        ed.EmployeeID = cc.dEmployeeID;

                        u = new UserDevice(cc.dEmployeeID, cc.dCardNo, cc.dUserName, cc.dVerfyMode, cc.dRegStatus, cc.dPhotoLen, cc.dBase64PhotoData, cc.dFeatureLen, cc.dFeatureBase64, cc.dUserType);
                    }
                    if (cc.eEmployeeID != 0)
                    {
                        ed.EmployeeID = cc.eEmployeeID;

                        e = new EmployeeInfo(cc.eEmployeeID, cc.eName, cc.eHKID, cc.eDOB, cc.eRelationshipStatus, cc.eAddress, cc.eTel, cc.eMobile, cc.eEmail, cc.eType, cc.ePosition, cc.eDepartment, cc.eAcademic, cc.eJoinDate, cc.eQuitDate, cc.eBasicSalary, cc.eOT, cc.eAnnualLeave, cc.eActualAnnualLeave, cc.eMPF, cc.eRemark, cc.elastUser, cc.eCreateDate);
                    }
                    ed.mDevice = u;
                    ed.mInfo = e;
                    al.Add(ed);
                }
            }
            return al;
        }

    }
}

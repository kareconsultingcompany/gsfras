﻿using DataModel.User;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataBase.User
{
    public class UserMan
    {
        public static UserInfo checkUser(String username, String pw)
        {
            gsfrasDataContext mContext = new gsfrasDataContext();
            var result = from profile in mContext.UserProfiles where profile.Name == username && profile.Password == pw select profile;
            int count = result.Count();
            UserInfo user = null;

            if (count == 1)
            {
                foreach (var item in result)
                {
                    user = new UserInfo(item.ID, item.Name, item.Password, item.UserLevel, item.Txlog, item.Reclog, item.SysRight, item.UserLock, item.LastUser, item.CreateDate, item.Lang, item.UserName);
                }
            }
            return user;
        }

        public static UserInfo getUser(String uid)
        {
            gsfrasDataContext mContext = new gsfrasDataContext();
            int aUid = int.Parse(uid);
            var result = from profile in mContext.UserProfiles where profile.ID == aUid select profile;
            UserInfo user = null;
            foreach (var item in result)
            {
                user = new UserInfo(item.ID, item.Name, item.Password, item.UserLevel, item.Txlog, item.Reclog, item.SysRight, item.UserLock, item.LastUser, item.CreateDate, item.Lang, item.UserName);
            }
            return user;
        }

        public static Boolean chechUserName(String name)
        {

            gsfrasDataContext mContext = new gsfrasDataContext();
            var result = from profile in mContext.UserProfiles where profile.Name == name select profile;
            int count = result.Count();
            if (count == 1)
            {
                return true;
            }
            return false;
        }

        public static Boolean createUser(UserInfo aUser)
        {
            Boolean isCreated = true;
            gsfrasDataContext mContext = new gsfrasDataContext();
            try
            {
                UserProfile user = new UserProfile();
                user.CreateDate = Constant.Constants.ConvertDateToTimestamp(DateTime.Now, Constant.Constants.TIME_ZONE);
                user.Lang = aUser.Lang;
                user.LastUser = aUser.LastUser;
                user.Name = aUser.Name;
                user.Password = aUser.Password;
                user.Reclog = aUser.Reclog;
                user.SysRight = aUser.SysRight;
                user.Txlog = aUser.Txlog;
                user.UserLevel = aUser.UserLevel;
                user.UserLock = aUser.UserLock;
                user.UserName = aUser.Username;
                mContext.UserProfiles.InsertOnSubmit(user);
                mContext.SubmitChanges();
            }
            catch (Exception)
            {
                isCreated = false;
            }
            return isCreated;
        }
        public static ArrayList deleteUser(ArrayList al)
        {
            ArrayList reList = new ArrayList();
            gsfrasDataContext mContext = new gsfrasDataContext();

            foreach (UserInfo info in al)
            {
                if (info.UserLock != 1)
                {
                    var DelItem = from p in mContext.UserProfiles where p.ID == info.ID select p;
                    mContext.UserProfiles.DeleteAllOnSubmit(DelItem);
                }
                else { reList.Add(info); }

            }

            mContext.SubmitChanges();

            return reList;
        }


        public static Boolean updateUser(UserInfo aUser)
        {
            Boolean isUpdated = true;
            gsfrasDataContext mContext = new gsfrasDataContext();
            try
            {
                UserProfile user = mContext.UserProfiles.Single(p => p.ID == aUser.ID);
                user.Lang = aUser.Lang;
                user.LastUser = aUser.LastUser;
                user.Name = aUser.Name;
                user.Password = aUser.Password;
                user.Reclog = aUser.Reclog;
                user.SysRight = aUser.SysRight;
                user.Txlog = aUser.Txlog;
                user.UserLevel = aUser.UserLevel;
                user.UserLock = aUser.UserLock;
                user.UserName = aUser.Username;
                mContext.SubmitChanges();
            }
            catch (Exception) { isUpdated = false; }
            return isUpdated;
        }

        public static ArrayList getAllUser()
        {
            ArrayList al = new ArrayList();
            gsfrasDataContext mContext = new gsfrasDataContext();
            var result = from profile in mContext.UserProfiles orderby profile.UserName select profile;
            foreach (var item in result)
            {
                al.Add(new UserInfo(item.ID, item.Name, item.Password, item.UserLevel, item.Txlog, item.Reclog, item.SysRight, item.UserLock, item.LastUser, item.CreateDate, item.Lang, item.UserName));
            }
            return al;
        }

        public static String generateID(String key)
        {
            String id = "";
            // key = key.Remove(key.Length );
            Char[] cs = key.ToCharArray();
            foreach (Char c in cs)
            {
                int i = (int)(c - '0');
                if (i >= 0 && i <= 9)
                {
                    id += c;
                }
                else
                {
                    int va = c;
                    id += va.ToString();
                }

            }
            return id;

        }

        public static String eidToHKID(String id)
        {
            String hkid = "";
            try
            {
                Char[] sid = id.ToCharArray();
                int cc = Convert.ToInt32(sid[0].ToString() + sid[1].ToString());
                hkid += (char)cc;
                for (int i = 2; i < sid.Length; i++)
                {
                    hkid += sid[i];
                }
            }
            catch (Exception)
            {
                hkid = id;
            }

            return hkid;
        }



        private static DataTable InitalUserTable()
        {
            DataTable dt = new DataTable("UserTable");

            DataColumn dcUserName = new DataColumn("UserName");
            dt.Columns.Add(dcUserName);
            DataColumn dcloginName = new DataColumn("loginName");
            dt.Columns.Add(dcloginName);
            DataColumn dcCreateDate = new DataColumn("CreateDate");
            dt.Columns.Add(dcCreateDate);
            DataColumn dcID = new DataColumn("ID");
            dt.Columns.Add(dcID);
            return dt;
        }


        public static ArrayList getUserAllData()
        {
            ArrayList al = new ArrayList();

            gsfrasDataContext mContext = new gsfrasDataContext();
            var result = from profile in mContext.UserProfiles orderby profile.UserName select profile;
            foreach (var item in result)
            {
                al.Add(new DataModel.User.UserInfo(item.ID, item.Name, item.Password, item.UserLevel, item.Txlog, item.Reclog, item.SysRight, item.UserLock, item.LastUser, item.CreateDate, item.Lang, item.UserName));
            }


            return al;
        }

        public static ArrayList getUserAllData(String key)
        {

            ArrayList al = new ArrayList();

            gsfrasDataContext mContext = new gsfrasDataContext();
            var result = from profile in mContext.UserProfiles where profile.UserName.Contains(key) orderby profile.UserName select profile;
            foreach (var item in result)
            {
                al.Add(new DataModel.User.UserInfo(item.ID, item.Name, item.Password, item.UserLevel, item.Txlog, item.Reclog, item.SysRight, item.UserLock, item.LastUser, item.CreateDate, item.Lang, item.UserName));
            }

            return al;
        }


        public static DataTable IntialSources(ArrayList al)
        {
            DataTable dt = InitalUserTable();
            DataRow dr;


            foreach (DataModel.User.UserInfo d in al)
            {
                dr = dt.NewRow();
                dr["UserName"] = d.Username;
                dr["loginName"] = d.Name;
                dr["CreateDate"] = Constant.Constants.ConvetTimestamp((double)d.CreateDate, Constant.Constants.TIME_ZONE).ToString(Constant.Constants.DATETIME_FORMAT);
                dr["ID"] = d.ID;
                dt.Rows.Add(dr);
            }
            return dt;
        }

    }
}

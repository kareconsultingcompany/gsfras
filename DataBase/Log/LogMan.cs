﻿using DataBase.User;
using DataModel.Type;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataBase.Log
{
    public class LogMan
    {
        private static DataTable InitalLog()
        {
            DataTable dt = new DataTable("Log");
            DataColumn dcID = new DataColumn("ID");
            dt.Columns.Add(dcID);
            DataColumn dcDate = new DataColumn("Date");
            dt.Columns.Add(dcDate);
            DataColumn dcName = new DataColumn("UserName");
            dt.Columns.Add(dcName);
            DataColumn dcMessage = new DataColumn("Message");
            dt.Columns.Add(dcMessage);
            DataColumn dcRemark = new DataColumn("Remark");
            dt.Columns.Add(dcRemark);
            return dt;
        }

          public static DataTable getLogSources(ArrayList al)
           {
               DataTable dt = InitalLog();
               DataRow dr;
               foreach (DataModel.System.LogInfo d in al)
               {
                   dr = dt.NewRow();
                   dr["ID"] = d.nid;
                   dr["Date"] = d.dtDate.ToString(Constant.Constants.DATETIME_FORMAT);
                   dr["UserName"] = d.name;
                   dr["Message"] = d.message;
                   dr["Remark"] = d.remark;
                   dt.Rows.Add(dr);

               }
               return dt;
           }

        public static ArrayList getLog(String uid, DateTime start, DateTime end,LogType type)
        {
            ArrayList al = new ArrayList();
            gsfrasDataContext mContext = new gsfrasDataContext();
            String[] filter = null;
            switch(type){
                case LogType.ALL:
                    filter = new String[]{LogType.DEBUG.ToString("G"),LogType.ERROR.ToString("G"),LogType.FATAL.ToString("G"),LogType.INFO.ToString("G") ,LogType.WARM.ToString("G")};
                    break;
                case LogType.DEBUG:
                    filter = new String[] { LogType.DEBUG.ToString("G")};
                    break;
                case LogType.ERROR:
                    filter = new String[] { LogType.ERROR.ToString("G")};
                    break;
                case LogType.FATAL:
                    filter = new String[] { LogType.FATAL.ToString("G")};
                    break;
                case LogType.INFO:
                    filter = new String[] { LogType.INFO.ToString("G")};
                    break;
                case LogType.WARM:
                    filter = new String[] { LogType.WARM.ToString("G") };
                    break;


            }
            var result = from log in mContext.SLogs where log.sUid == uid && start.CompareTo(log.dtDate)<=0 && end.CompareTo( log.dtDate)>=0 && filter.Contains( log.sLevel)  orderby log.dtDate ascending select log;
            foreach (var item in result)
            {
                DataModel.System.LogInfo lInfo = new DataModel.System.LogInfo(item.nId, item.dtDate, item.sMessage, item.sUid, item.sException);
                if (item.sUid!= null&&item.sUid.ToString() != "")
                {
                    lInfo.name = UserMan.getUser(item.sUid.ToString()).Username;
                }
                else { lInfo.name = ""; }
                al.Add(lInfo);
            }
            return al;
        }


    }
}

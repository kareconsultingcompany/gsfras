﻿using DataBase.User;
using DataModel.SDK;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataBase.DeviceMan
{
    public class DeviceMan
    {
        public static readonly int MODE_INCLUDE = 0;
        public static readonly int MODE_EXCLUDE = 1;

        private static DataTable InitalEmployeeDevice()
        {
            DataTable dt = new DataTable("DeviceEmployee");
            DataColumn dcImage = new DataColumn("image");
            dcImage.DataType = System.Type.GetType("System.Byte[]");
            dcImage.AllowDBNull = true;
            dt.Columns.Add(dcImage);
            DataColumn dcID = new DataColumn("EmployeeIDE");
            dt.Columns.Add(dcID);
            DataColumn dcEID = new DataColumn("EmployeeID");
            dt.Columns.Add(dcEID);
            DataColumn dcName = new DataColumn("UserName");
            dt.Columns.Add(dcName);
            DataColumn dcCardNo = new DataColumn("CardNo");
            dt.Columns.Add(dcCardNo);
            DataColumn dcVerifyMode = new DataColumn("VerifyMode");
            dt.Columns.Add(dcVerifyMode);
            return dt;
        }

        public static DataTable getEmpolyeeInDevice(ArrayList al)
        {
            DataTable dt = InitalEmployeeDevice();
            DataRow dr;
            foreach (DataModel.DeviceModel.UserDevice d in al)
            {
                dr = dt.NewRow();
                dr["image"] = Constant.Constants.ImageFromBase64String(d.Base64PhotoData);
                dr["EmployeeIDE"] = UserMan.eidToHKID(d.EmployeeID.ToString());
                dr["EmployeeID"] = d.EmployeeID.ToString();
                dr["UserName"] = d.UserName;
                dr["CardNo"] = d.CardNo;
                dr["VerifyMode"] = d.VerfyMode;
                dt.Rows.Add(dr);

            }
            return dt;
        }


        public static DataTable getEmpolyeeInDevice(String key)
        {
            DataTable dt = InitalEmployeeDevice();
            DataRow dr;
            ArrayList al = null;
            if (key != null && key != "")
            {
                al = getEmployeeDeviceDataByFilter(key);
            }
            else
            {
                al = getEmployeeDeviceData();
            }

            foreach (DataModel.DeviceModel.UserDevice d in al)
            {
                dr = dt.NewRow();
                dr["image"] = Constant.Constants.ImageFromBase64String(d.Base64PhotoData);
                dr["EmployeeIDE"] = UserMan.eidToHKID(d.EmployeeID.ToString());
                dr["EmployeeID"] = d.EmployeeID.ToString();
                dr["UserName"] = d.UserName;
                dr["CardNo"] = d.CardNo;
                dr["VerifyMode"] = d.VerfyMode;
                dt.Rows.Add(dr);

            }
            return dt;
        }

        public static byte[] imageToByteArray(System.Drawing.Image imageIn)
        {
            try
            {
                MemoryStream ms = new MemoryStream();
                imageIn.Save(ms, System.Drawing.Imaging.ImageFormat.Gif);
                return ms.ToArray();
            }
            catch (Exception) { return new byte[] { 0 }; }
        }

        public static ArrayList getEmployeeDeviceData()
        {
            ArrayList al = new ArrayList();

            gsfrasDataContext mContext = new gsfrasDataContext();
            var result = from device in mContext.DeviceEmployees orderby device.EmployeeID select device;
            foreach (var item in result)
            {
                al.Add(new DataModel.DeviceModel.UserDevice(item.EmployeeID, item.CardNo, item.UserName, item.VerifyMode, item.RegStatus, item.PhotoLen, item.Base64PhotoData, item.FeatureLen, item.FeatureBase64, item.userType));
            }


            return al;
        }

        public static ArrayList getDuplicateData()
        {
            ArrayList al = new ArrayList();

            gsfrasDataContext mContext = new gsfrasDataContext();
            var result = from device in mContext.DeviceEmployees group device by device.UserName into g where g.Count() > 1 select g;
            foreach (var items in result)
            {
                foreach (var item in items)
                    al.Add(new DataModel.DeviceModel.UserDevice(item.EmployeeID, item.CardNo, item.UserName, item.VerifyMode, item.RegStatus, item.PhotoLen, item.Base64PhotoData, item.FeatureLen, item.FeatureBase64, item.userType));
            }


            return al;
        }

        private static ArrayList getEmployeeDeviceDataByFilter(String key)
        {
            ArrayList al = new ArrayList();

            gsfrasDataContext mContext = new gsfrasDataContext();
            var result = from device in mContext.DeviceEmployees where device.EmployeeID.ToString().Contains(key) || device.UserName.Contains(key) orderby device.EmployeeID select device;
            foreach (var item in result)
            {
                al.Add(new DataModel.DeviceModel.UserDevice(item.EmployeeID, item.CardNo, item.UserName, item.VerifyMode, item.RegStatus, item.PhotoLen, item.Base64PhotoData, item.FeatureLen, item.FeatureBase64, item.userType));
            }


            return al;

        }

        public static ArrayList getEmployeeDeviceDataByFilter(int mode, Boolean isUserID, Boolean isUserName, String key)
        {
            ArrayList al = new ArrayList();

            gsfrasDataContext mContext = new gsfrasDataContext();

            if (mode == MODE_INCLUDE)
            {
                var result = from device in mContext.DeviceEmployees select device;
                if (isUserID == true&&isUserName ==false)
                {
                    result = result.Where(i => i.EmployeeID.ToString().Contains(UserMan.generateID(key)));
                }
                else if (isUserName == true && isUserID == false)
                {
                    result = result.Where(i => i.UserName.ToString().Contains(key));
                }
                else {
                    result = result.Where(i => i.EmployeeID.ToString().Contains(UserMan.generateID(key)) || i.UserName.ToString().Contains(key));
                }
                foreach (var item in result)
                {
                    al.Add(new DataModel.DeviceModel.UserDevice(item.EmployeeID, item.CardNo, item.UserName, item.VerifyMode, item.RegStatus, item.PhotoLen, item.Base64PhotoData, item.FeatureLen, item.FeatureBase64, item.userType));
                }
            }
            else if (mode == MODE_EXCLUDE)
            {
                var result = from device in mContext.DeviceEmployees select device;
                if (isUserID == true)
                {
                    result = result.Where(i => !i.EmployeeID.ToString().Contains(UserMan.generateID(key)));
                }
                if (isUserName == true)
                {
                    result = result.Where(i => !i.UserName.ToString().Contains(key));
                }
                foreach (var item in result)
                {
                    al.Add(new DataModel.DeviceModel.UserDevice(item.EmployeeID, item.CardNo, item.UserName, item.VerifyMode, item.RegStatus, item.PhotoLen, item.Base64PhotoData, item.FeatureLen, item.FeatureBase64, item.userType));
                }
            }

            /*var result = from device in mContext.DeviceEmployees where device.EmployeeID.ToString().Contains(key) || device.UserName.Contains(key) orderby device.EmployeeID select device;
            foreach (var item in result)
            {
                al.Add(new DataModel.DeviceModel.UserDevice(item.EmployeeID, item.CardNo, item.UserName, item.VerifyMode, item.RegStatus, item.PhotoLen, item.Base64PhotoData, item.FeatureLen, item.FeatureBase64, item.userType));
            }*/


            return al;

        }

        public static ArrayList getSelectedEmployeeData(ArrayList ids)
        {
            ArrayList al = new ArrayList();
            int[] idarray = new int[ids.Count];
            int i = 0;
            foreach (int index in ids)
            {
                idarray[i] = index;
                i++;
            }
            gsfrasDataContext mContext = new gsfrasDataContext();
            var result = from device in mContext.DeviceEmployees where idarray.Contains(device.EmployeeID) orderby device.EmployeeID select device;
            //  var result = from device in mContext.DeviceEmployees select device;
            foreach (var item in result)
            {
                al.Add(new DataModel.DeviceModel.UserDevice(item.EmployeeID, item.CardNo, item.UserName.Trim(), item.VerifyMode, item.RegStatus, item.PhotoLen, item.Base64PhotoData, item.FeatureLen, item.FeatureBase64, item.userType));
            }
            return al;
        }

        public static Boolean updateUser(DataModel.DeviceModel.UserDevice aUser, int employeeID)
        {
            Boolean isUpdate = true;
            gsfrasDataContext mContext = new gsfrasDataContext();
            try
            {

                var employee = from e in mContext.DeviceEmployees where e.EmployeeID == employeeID select e;
                //if (employee != null)
                //{
                /* employee.EmployeeID = aUser.EmployeeID;
                 employee.CardNo = aUser.CardNo;
                 employee.UserName = aUser.UserName;
                 employee.VerifyMode = aUser.VerfyMode;
                 employee.RegStatus = aUser.RegStatus;
                 employee.PhotoLen = aUser.PhotoLen;
                 employee.Base64PhotoData = aUser.Base64PhotoData;
                 employee.FeatureLen = aUser.FeatureLen;
                 employee.FeatureBase64 = aUser.FeatureBase64;
                 employee.userType = aUser.UserType;
                 mContext.SubmitChanges();*/

                mContext.DeviceEmployees.DeleteAllOnSubmit(employee);
                DeviceEmployee de = new DeviceEmployee()
                {
                    EmployeeID = aUser.EmployeeID,
                    CardNo = aUser.CardNo,
                    UserName = aUser.UserName.Trim(),
                    VerifyMode = aUser.VerfyMode,
                    RegStatus = aUser.RegStatus,
                    PhotoLen = aUser.PhotoLen,
                    Base64PhotoData = aUser.Base64PhotoData,
                    FeatureLen = aUser.FeatureLen,
                    FeatureBase64 = aUser.FeatureBase64,
                    userType = aUser.UserType
                };
                mContext.DeviceEmployees.InsertOnSubmit(de);
                mContext.SubmitChanges();

            }
            catch (Exception ex)
            {
                Constant.LogHelper.Warn(ex.Message,ex,"0");
                isUpdate = false;

            }
            return isUpdate;


        }

        public static Boolean updateData(ArrayList datas)
        {
            Boolean isUpdate = true;
            gsfrasDataContext mContext = new gsfrasDataContext();
            try
            {
                foreach (DataModel.DeviceModel.UserDevice d in datas)
                {
                    DeviceEmployee employee = (from e in mContext.DeviceEmployees where e.EmployeeID == d.EmployeeID select e).SingleOrDefault();
                    if (employee == null)
                    {

                        DeviceEmployee de = new DeviceEmployee()
                        {
                            EmployeeID = d.EmployeeID,
                            CardNo = d.CardNo,
                            UserName = d.UserName.Trim(),
                            VerifyMode = d.VerfyMode,
                            RegStatus = d.RegStatus,
                            PhotoLen = d.PhotoLen,
                            Base64PhotoData = d.Base64PhotoData,
                            FeatureLen = d.FeatureLen,
                            FeatureBase64 = d.FeatureBase64,
                            userType = d.UserType
                        };
                        mContext.DeviceEmployees.InsertOnSubmit(de);
                        mContext.SubmitChanges();
                    }
                    else
                    {
                        employee.EmployeeID = d.EmployeeID;
                        employee.CardNo = d.CardNo;
                        employee.UserName = d.UserName.Trim();
                        employee.VerifyMode = d.VerfyMode;
                        employee.RegStatus = d.RegStatus;
                        employee.PhotoLen = d.PhotoLen;
                        employee.Base64PhotoData = d.Base64PhotoData;
                        employee.FeatureLen = d.FeatureLen;
                        employee.FeatureBase64 = d.FeatureBase64;
                        employee.userType = d.UserType;
                        mContext.SubmitChanges();
                    }

                }

            }
            catch (Exception)
            {
                isUpdate = false;

            }
            return isUpdate;
        }

        public static ArrayList getCSV(String path, String fileName)
        {
            ArrayList al = new ArrayList();
            /* string strCon = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source="+path+";Extended Properties='Text;HDR=NO;IMEX=1'";
             OleDbConnection GetCSV = new OleDbConnection(strCon);
             DataTable Table = new DataTable();
             GetCSV.Open();
             OleDbDataAdapter daCSV = new OleDbDataAdapter("SELECT * FROM [" + fileName + "]", GetCSV);
             daCSV.Fill(Table);

 foreach (DataRow dr in Table.Rows)
 {
     foreach (DataColumn dc in Table.Columns)
     {
        // ListCSV .Add(dr[dc.ColumnName].ToString(););
         //al.Add(new DataModel.DeviceModel.UserDevice(item.EmployeeID, item.CardNo, item.UserName, item.VerifyMode, item.RegStatus, item.PhotoLen, item.Base64PhotoData, item.FeatureLen, item.FeatureBase64, item.userType));
     }
 }

             GetCSV.Close();*/

            var reader = new StreamReader(File.OpenRead(path));
            int i = 0;
            while (!reader.EndOfStream)
            {
                var line = reader.ReadLine();
                var values = line.Split(',');
                if (i > 0)
                {

                    int id = 0;
                    try
                    {
                        id = Convert.ToInt32(UserMan.generateID(values[0]));
                    }
                    catch (Exception) { }
                    int EmployeeID = id;
                    // double CardNo = Convert.ToDouble(values[2]);
                    String UserName = values[3].Trim();
                    //int VerifyMode = Convert.ToInt32(values[4]);
                    //int RegStatus = Convert.ToInt32(values[5]);
                    // int PhotoLen=Convert.ToInt32(values[6]);
                    //String Base64PhotoData=values[7];
                    //int FeatureLen=Convert.ToInt32( values[8]);
                    //String FeatureBase64=values[9];
                    //int userType = Convert.ToInt32(values[1]);

                    al.Add(new DataModel.DeviceModel.UserDevice(EmployeeID, null, UserName, 1, 1, 0, "", 0, "", 0));

                }
                i++;
                /* var line = reader.ReadLine();
                 var values = line.Split(';');


                 listA.Add(values[0]);
                 listB.Add(values[1]);*/
            }
            return al;

        }

        public static void exportCSV(ArrayList al, String path)
        {

            Stopwatch sWatch = new Stopwatch();
            sWatch.Start();
            StringBuilder sBuilder = new StringBuilder();
            sBuilder.AppendLine("EmployeeID,userType,CardNo,UserName,VerifyMode,ReqStatus");
            foreach (DataModel.DeviceModel.UserDevice ud in al)
            {
                String outp = UserMan.eidToHKID(ud.EmployeeID.ToString()) + "," + ud.UserType + "," + ud.CardNo + "," + ud.UserName.Trim() + "," + ud.VerfyMode + "," + ud.RegStatus;
                outp = outp.Replace("\"", "\"\"").Replace("\r\n", " ").Replace("\r", " ").Replace("\n", "");
                sBuilder.AppendLine(outp);
            }
            File.WriteAllText(path, sBuilder.ToString());
            sWatch.Stop();
        }

        public static Boolean deleteRecord(ArrayList ids)
        {
            Boolean isDel = true;
            try
            {
                int[] idarray = new int[ids.Count];
                int i = 0;
                foreach (int index in ids)
                {
                    idarray[i] = index;
                    i++;
                }
                gsfrasDataContext mContext = new gsfrasDataContext();
                var result = from device in mContext.DeviceEmployees where idarray.Contains(device.EmployeeID) orderby device.EmployeeID select device;
                mContext.DeviceEmployees.DeleteAllOnSubmit(result);
                mContext.SubmitChanges();
            }
            catch (Exception)
            {
                isDel = false;
            }
            return isDel;
        }


        public static DataModel.DeviceModel.UserDevice getUser(String uid)
        {
            DataModel.DeviceModel.UserDevice user = null;
            gsfrasDataContext mContext = new gsfrasDataContext();
            var result = from device in mContext.DeviceEmployees where device.EmployeeID == double.Parse(uid) orderby device.EmployeeID select device;
            foreach (var de in result)
            {
                user = new DataModel.DeviceModel.UserDevice(de.EmployeeID, de.CardNo, de.UserName.Trim(), de.VerifyMode, de.RegStatus, de.PhotoLen, de.Base64PhotoData, de.FeatureLen, de.FeatureBase64, de.userType);
            }

            return user;
        }


    }
}

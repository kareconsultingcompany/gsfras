﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataBase.DeviceMan
{
    public class HardwareMan
    {
        private static DataTable InitalDevice()
        {
            DataTable dt = new DataTable("Hardware");
            DataColumn dcID = new DataColumn("SerialNumber");
            dt.Columns.Add(dcID);
            DataColumn dcName = new DataColumn("Name");
            dt.Columns.Add(dcName);
            DataColumn dcIP = new DataColumn("IP");
            dt.Columns.Add(dcIP);
            return dt;
        }



        public static ArrayList getHardwareData()
        {
            ArrayList al = new ArrayList();

            gsfrasDataContext mContext = new gsfrasDataContext();
            var result = from device in mContext.Devices orderby device.DeviceSVN select device;
            foreach (var item in result)
            {
                al.Add(new DataModel.DeviceModel.HardwareInfo(item.Name, item.Location, item.IpAddress, item.AccessUserName, item.AccessUserName, item.LastSync, item.CreateDate, item.DeviceSVN));
            }
            return al;
        }

        public static DataTable intialDataSource(ArrayList al)
        {
            DataTable dt = InitalDevice();
            DataRow dr;

            foreach (DataModel.DeviceModel.HardwareInfo d in al)
            {
                dr = dt.NewRow();
                dr["SerialNumber"] = d.DeviceSVN;
                dr["Name"] = d.Name;
                dr["IP"] = d.IpAddress;
                dt.Rows.Add(dr);
            }
            return dt;

        }

        public static DataModel.DeviceModel.HardwareInfo getHardwareInfo(String sn)
        {
            DataModel.DeviceModel.HardwareInfo info = null;
            gsfrasDataContext mContext = new gsfrasDataContext();
            var result = from device in mContext.Devices where device.DeviceSVN == sn orderby device.DeviceSVN select device;
            foreach (var item in result)
            {
                info = new DataModel.DeviceModel.HardwareInfo(item.Name, item.Location, item.IpAddress, item.AccessUserName, item.AccessUserName, item.LastSync, item.CreateDate, item.DeviceSVN);
            }
            return info;
        
        }

        public static Boolean deletehardware(String sn) {
            Boolean isDel = true;
            try
            {
                gsfrasDataContext mContext = new gsfrasDataContext();
                var result = from device in mContext.Devices where device.DeviceSVN == sn orderby device.DeviceSVN select device;
                mContext.Devices.DeleteAllOnSubmit(result);
                mContext.SubmitChanges();
            }
            catch(Exception){
                isDel = false;
            }
            return isDel;
        }

        public static Boolean updateInfo(DataModel.DeviceModel.HardwareInfo info)
        {
            Boolean isUpdate = true;
            try
            {
                gsfrasDataContext mContext = new gsfrasDataContext();
                Device device = mContext.Devices.Single(p => p.DeviceSVN == info.DeviceSVN);
                device.AccessPassword = info.AccessPassword;
                device.AccessUserName = info.AccessUserName;
                device.IpAddress = info.IpAddress;
                device.LastSync = info.LastSync;
                device.Location = info.Location;
                device.Name = info.Name;
                mContext.SubmitChanges();
            }
            catch(Exception){
                isUpdate = false;
            }

            return isUpdate;
        }

        public static Boolean createInfo(DataModel.DeviceModel.HardwareInfo info)
        {
            Boolean isCreate=true;
            try
            {
                gsfrasDataContext mContext = new gsfrasDataContext();
                Device device = new Device();
                device.AccessPassword = info.AccessPassword;
                device.AccessUserName = info.AccessUserName;
                device.CreateDate = Constant.Constants.ConvertDateToTimestamp(DateTime.Now, Constant.Constants.TIME_ZONE);
                device.DeviceSVN = info.DeviceSVN;
                device.IpAddress = info.IpAddress;
                device.LastSync = info.LastSync;
                device.Location = info.Location;
                device.Name = info.Name;
                mContext.Devices.InsertOnSubmit(device);
                mContext.SubmitChanges();
            }
            catch(Exception){
                isCreate = false ;
            }
            return isCreate;
        }
    }
}

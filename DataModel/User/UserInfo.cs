﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.User
{
    public class UserInfo
    {
      

        public UserInfo() { }

        public UserInfo(int ID, string Name, string Password, int? UserLevel, int? Txlog, int? Reclog, int? SysRight, int? UserLock, int? LastUser, double? CreateDate, string Lang,string Username)
        {
            this.ID=ID;
            this.Name = Name;
            this.Password = Password;
            this.UserLevel = UserLevel;
            this.Txlog = Txlog;
            this.Reclog = Reclog;
            this.SysRight = SysRight;
            this.UserLock = UserLock;
            this.LastUser = LastUser;
            this.CreateDate = CreateDate;
            this.Lang = Lang;
            this.Username = Username;
        }


        public int ID { set; get; }
        public String Name { set; get; }
        public String Password { set; get; }
        public int? UserLevel { set; get; }
        public int? Txlog { set; get; }
        public int? Reclog { set; get; }
        public int? SysRight { set; get; }
        public int? UserLock { set; get; }
        public int? LastUser { set; get; }
        public double? CreateDate { set; get; }
        public String Lang { set; get; }
        public string Username { set; get; }

        public override string ToString()
        {
            return Username + "(" + Name + ")";
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.User
{
    public class AttendanceInfo
    {


        public AttendanceInfo() { }
        public AttendanceInfo(String ARID, double? TransactionDate, double? DeviceID, int? EmployeeID,int? RecType,int?Status,int?PhotoLen,int?Reason,String Base64PhotoData)
        {
            this.ARID = ARID;
            this.TransactionDate = TransactionDate;
            this.DeviceID = DeviceID;
            this.EmployeeID = EmployeeID;
            this.RecType = RecType;
            this.Status = Status;
            this.PhotoLen = PhotoLen;
            this.Reason = Reason;
            this.Base64PhotoData = Base64PhotoData;
            
        }


        public String ARID { get; set; }
        public double? TransactionDate { get; set; }
        public double? DeviceID { get; set; }
        public int? EmployeeID { get; set; }
        public int? RecType { get; set; }
        public int? Status { get; set; }
        public int? PhotoLen { get; set; }
        public int? Reason { get; set; }
        public String Base64PhotoData { get; set; }
        public DateTime getDateTime
        {
            get
            {
                double tran = 0;
                if (TransactionDate != null)
                {
                    tran = (Double)TransactionDate;
                }
                return Constant.Constants.ConvetTimestamp(tran, Constant.Constants.TIME_ZONE);
            }
        }
    }
}

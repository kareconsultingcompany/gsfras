﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.User
{
   public  class ExportAtendance
    {
       public String Code { get; set; }
       public String Name { get; set; }
       public String TransactionDate { get; set; }
       public String InTime { get; set; }
       public String OutTime { get; set; }
       public String ReaderID1 { get; set; }
       public String ReaderID2 { get; set; }
    }
}

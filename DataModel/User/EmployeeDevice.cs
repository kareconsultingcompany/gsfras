﻿using DataModel.DeviceModel;

namespace DataModel.User
{
    public class EmployeeDevice
    {

        public int EmployeeID { get; set; }
        public EmployeeInfo mInfo { get; set; }
        public UserDevice mDevice { get; set; }
        public EmployeeDevice() { }
        public EmployeeDevice(EmployeeInfo mInfo, UserDevice mDevice) {
            this.mDevice = mDevice ;
            this.mInfo = mInfo;

        }
    }
}

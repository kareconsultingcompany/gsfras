﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.User
{
    public class EmployeeInfo
    {
        public int EmployeeID { get; set; }
        public String Name { get; set; }
        public String HKID { get; set; }
        public double? DOB { get; set; }
        public int? RelationshipStatus { get; set; }
        public String Address { get; set; }
        public int? Tel { get; set; }
        public int? Mobile { get; set; }
        public String Email { get; set; }
        public String Type { get; set; }
        public int? Position { get; set; }
        public int? Department { get; set; }
        public int? Academic { get; set; }
        public double? JoinDate { get; set; }
        public double? QuitDate { get; set; }
        public decimal? BasicSalary { get; set; }
        public decimal? OT { get; set; }
        public double? AnnualLeave { get; set; }
        public double? ActualAnnualLeave { get; set; }
        public double? MPF { get; set; }
        public String Remark { get; set; }
        public int? lastUser { get; set; }
        public double? CreateDate { get; set; }

        public EmployeeInfo() { }
        public EmployeeInfo(int EmployeeID, String Name, String HKID, double? DOB, int? RelationshipStatus, String Address, int? Tel,
            int? Mobile, String Email, String Type, int? Position, int? Department, int? Academic, double? JoinDate, double? QuitDate, decimal? BasicSalary,
            decimal? OT, double? AnnualLeave, double? ActualAnnualLeave, double? MPF, String Remark, int? lastUser, double? CreateDate
            )
        {
            this.Academic = Academic;
            this.ActualAnnualLeave = ActualAnnualLeave;
            this.Address = Address;
            this.AnnualLeave = AnnualLeave;
            this.BasicSalary = BasicSalary;
            this.CreateDate = CreateDate;
            this.Department = Department;
            this.DOB = DOB;
            this.Email = Email;
            this.EmployeeID = EmployeeID;
            this.HKID = HKID;
            this.JoinDate = JoinDate; this.Remark = Remark;
            this.lastUser = lastUser;
            this.Mobile = Mobile;
            this.MPF = MPF;
            this.Name = Name;
            this.OT = OT;
            this.Position = Position;
            this.QuitDate = QuitDate;
            this.RelationshipStatus = RelationshipStatus;
            this.Tel = Tel;
            this.Type = Type;

        }

        public DateTime getBOD
        {
            get
            {
                if (DOB == null)
                {
                    return DateTime.Now;
                }
                else { return Constant.Constants.ConvetTimestamp((double)DOB, Constant.Constants.TIME_ZONE); }

            }
        }

        public DateTime getJoinDate {

            get
            {
                if (JoinDate == null)
                {
                    return DateTime.Now;
                }
                else { return Constant.Constants.ConvetTimestamp((double)JoinDate, Constant.Constants.TIME_ZONE); }

            }
        
        }

        public DateTime getQuitDate {
            get
            {
                if (QuitDate == null)
                {
                    return DateTime.Now;
                }
                else { return Constant.Constants.ConvetTimestamp((double)QuitDate, Constant.Constants.TIME_ZONE); }

            }
        
        }


        public DateTime getActualAnnualLeave {
            get
            {
                if (ActualAnnualLeave == null)
                {
                    return DateTime.Now;
                }
                else { return Constant.Constants.ConvetTimestamp((double)ActualAnnualLeave, Constant.Constants.TIME_ZONE); }

            }
        }

    }
}

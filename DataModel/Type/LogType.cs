﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.Type
{
    public enum LogType
    {
        DEBUG,INFO,WARM,ERROR,FATAL,ALL
    }
}

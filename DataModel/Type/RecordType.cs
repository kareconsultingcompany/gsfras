﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.Type
{
    public enum RecordType:int
    {
        //1－人脸识别 2－刷卡拍照 3－刷卡人脸识别 4－工号人脸识别
        FaceDetect=1,CardDetect=2,FaceCardDetect=3,NumDetect=4
    }
}

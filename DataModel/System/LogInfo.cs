﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.System
{
   public class LogInfo
    {
       public long nid { get; set; }
       public DateTime dtDate { get;set;}
       public String message{get;set;}
       public String uid { get; set; }
       public String name { get; set; }
       public String remark { get; set; }

       public LogInfo() { }
       public LogInfo(long nid,DateTime dtDate,String message,String uid,String remark) {
           this.nid = nid;
           this.dtDate = dtDate;
           this.message = message; 
           this.remark = remark;
           this.uid = uid;
       }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FaceKit.EventInterface
{
    public interface FaceEventWrapper
    {
         void OnEventConnect();
         void OnEventDisconnect();
         void OnTrapAccess(string strDevSn, int lOpCode, int lUserData, int lExtendParam, int lUserID, string strRecTime, int lRecType, int lScore, int lStatus, int lPhotoType, int lPhotoLen, int lReason, string strBase64PhotoData);
         void OnEventErrorNotify(string strDevSn, int lOpCode, int lUserData, int lExtendParam, int lErrorCode);
         void OnError(String error, int errorCod);

         void OnUploadStart(String message);
         void OnUploadStarting(int count);
         void OnUplading(int val,String message);
         void OnUpladEnd();

         void OnInsertDeviceProblem(int id,String name);
        
    }
}
;
﻿using DataModel.DeviceModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.SDK
{
    public class DeviceData
    {
        public DeviceData(String deviceIP, int devicePort, String deviceSn, int opCode, int userData, int extendPara)
        {
            this.deviceIP = deviceIP;
            this.devicePort = devicePort;
            this.deviceSn = deviceSn;
            this.opCode = opCode;
            this.userData = userData;
            this.extendParam = extendParam;
        }
        public String deviceIP { get; set; }
        public int devicePort { get; set; }
        public String deviceSn { get; set; }
        public int opCode { get; set; }
        public int userData { get; set; }
        public int extendParam { get; set; }

        public override string ToString()
        {
            if (hinfo == null)
            {
                return deviceSn + "(" + deviceIP + ")";
            }
            else {
                return hinfo.Name+" "+deviceSn + "(" + deviceIP + ")";
            }
        }


        public HardwareInfo hinfo { get; set; }

    }
}

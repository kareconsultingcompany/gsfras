﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.DeviceModel
{
    public class UserDevice
    {


        public UserDevice() { }
        public UserDevice(int EmployeeID, string CardNo, String UserName, int? VerfyMode, int? RegStatus, int? PhotoLen, String Base64PhotoData, int? FeatureLen, String FeatureBasse64, int? UserType)
        {

            this.EmployeeID = EmployeeID;
            this.CardNo = CardNo;
            this.UserName = UserName;
            this.VerfyMode = VerfyMode;
            this.RegStatus = RegStatus;
            this.PhotoLen = PhotoLen;
            this.Base64PhotoData = Base64PhotoData;
            this.FeatureLen = FeatureLen;
            this.FeatureBase64 = FeatureBasse64;
            this.UserType = UserType;
        }





        public int EmployeeID { get; set; }
        public string CardNo { get; set; }
        public String UserName { get; set; }
        public int? VerfyMode { set; get; }
        public int? RegStatus { set; get; }
        public int? PhotoLen { set; get; }
        public String Base64PhotoData { get; set; }
        public int? FeatureLen { get; set; }
        public String FeatureBase64 { get; set; }
        public int? UserType { get; set; }


    }
}

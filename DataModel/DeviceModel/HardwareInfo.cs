﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.DeviceModel
{
    public class HardwareInfo
    {
        public String Name { get; set; }
        public String Location { get; set; }
        public String IpAddress { get; set; }
        public String AccessUserName { get; set; }
        public String AccessPassword { get; set; }
        public double? LastSync { get; set; }
        public double? CreateDate { get; set; }
        public String DeviceSVN { get; set; }

        public HardwareInfo() { }
        public HardwareInfo(String Name, String Location, String IpAddress, String AccessUserName, String AccessPassword, double? LastSync, double? CreateDate, String DeviceSVN)
        {
            this.Name = Name;
            this.Location = Location;
            this.IpAddress = IpAddress;
            this.AccessUserName = AccessUserName;
            this.AccessPassword = AccessPassword;
            this.LastSync = LastSync;
            this.CreateDate = CreateDate;
            this.DeviceSVN = DeviceSVN;
        }

        public String getLastSync()
        {

            return Constant.Constants.ConvetTimestamp((double)LastSync, Constant.Constants.TIME_ZONE).ToString(Constant.Constants.DATETIME_FORMAT);
        }

        public String getCreateDate()
        {
            return Constant.Constants.ConvetTimestamp((double)CreateDate, Constant.Constants.TIME_ZONE).ToString(Constant.Constants.DATETIME_FORMAT);
        }
    }
}

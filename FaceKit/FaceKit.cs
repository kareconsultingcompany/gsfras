﻿using DataBase.DeviceMan;
using DataModel.DeviceModel;
using DataModel.SDK;
using DataModel.User;
using FaceKit.EventInterface;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FaceKit
{
    public class FaceKit
    {

        private Hashtable mDeviceList = new Hashtable();
        private static FaceKit instance;
        private FaceServerSdkLib.FaceServerSdkCtrl mSdk;
        private FaceEventWrapper mListener;

        private FaceKit()
        {
            mSdk = new FaceServerSdkLib.FaceServerSdkCtrl();
            mSdk.OnEventCConnect += new FaceServerSdkLib._IFaceServerSdkCtrlEvents_OnEventCConnectEventHandler(OnEventCConnect);
            mSdk.OnEventCDisconnect += new FaceServerSdkLib._IFaceServerSdkCtrlEvents_OnEventCDisconnectEventHandler(OnEventDisConnect);
            mSdk.OnEventCTrap += new FaceServerSdkLib._IFaceServerSdkCtrlEvents_OnEventCTrapEventHandler(OnTrapAccess);
            mSdk.OnEventCErrorNotify += new FaceServerSdkLib._IFaceServerSdkCtrlEvents_OnEventCErrorNotifyEventHandler(OnEventCErrorNotify);
        }
        public static FaceKit getInstance()
        {
            if (instance == null)
            {
                instance = new FaceKit();
            }
            return instance;
        }

        public Hashtable getDevices()
        {
            return mDeviceList;
        }


        public void setListener(FaceEventWrapper aListener)
        {
            this.mListener = aListener;
        }

        //*************************Face SDK function***************************************/
        public void serverStart(int port)
        {
            mSdk.C_ServerInit(port, 0);
        }

        public void serverClose()
        {
            mSdk.C_ServerUnit();
            mDeviceList.Clear();
        }

        public String getUserimageFeature(String sn, int userId, int userType)
        {
            string strFeature = "";
            int nRet = mSdk.C_GetSingleUserFeature(sn, userId, userType, ref strFeature);
            if (nRet != 0)
            {
                if (mListener != null)
                {
                    mListener.OnError("C_GetSingleUserFeature failed！Errcode：" + Convert.ToString(mSdk.C_SdkGetLastError(sn), 16), mSdk.C_SdkGetLastError(sn));
                }

            }
            return strFeature;

        }

        public FaceServerSdkLib.UserInfo getSingleUserInfo(String sn, int userId, int userType)
        {
            FaceServerSdkLib.UserInfo User = new FaceServerSdkLib.UserInfo();
            int nRet = mSdk.C_GetSingleUserInfo(sn, userId, userType, ref User);
            if (nRet != 0)
            {
                if (mListener != null)
                {
                    mListener.OnError("C_GetSingleUserInfo failed！Errcode：" + Convert.ToString(mSdk.C_SdkGetLastError(sn), 16), mSdk.C_SdkGetLastError(sn));
                }
            }
            return User;
        }

        public ArrayList getAllUser(String sn)
        {
            mListener.OnUploadStart("Initial the processing.");
            ArrayList userList = new ArrayList();
            int mUserCount = 0;
            int mRet = mSdk.C_GetAllUsersCount(sn, ref mUserCount);
            mListener.OnUploadStarting(mUserCount);
            if (mUserCount > 0)
            {
                for (int nIndex = 1; nIndex <= mUserCount; nIndex++)
                {
                    FaceServerSdkLib.UserInfo reUserInfo = new FaceServerSdkLib.UserInfo();
                    int nRet1 = mSdk.C_GetUsersInfo(sn, ref reUserInfo);
                    if (nRet1 == 0)
                    {
                        mListener.OnUplading(nIndex, reUserInfo.UserName);
                        userList.Add(reUserInfo);
                    }
                }
            }
            mListener.OnUpladEnd();
            return userList;
        }


        public ArrayList getAllUserWithFeature(String sn) {
            ArrayList al = new ArrayList();
            ArrayList userList = getAllUser(sn);
            foreach(FaceServerSdkLib.UserInfo u in userList){
                String feature = getUserimageFeature(sn, u.UserID, u.UserType);
                DataModel.DeviceModel.UserDevice de = new DataModel.DeviceModel.UserDevice(u.UserID, u.CardNo, u.UserName, u.VerifyMode, u.RegStatus, u.PhotoLen, u.PhotoBase64Data, feature.Length, feature, u.UserType);
                al.Add(de);
            }
            return al;
        }

        public ArrayList getUserSuccessAccess(String sn, int userId, String startTime, String endTime, Boolean withPhoto)
        {
            ArrayList userAccessList = new ArrayList();
            int nCount = mSdk.C_GetUserSuccessAccessCount(sn, userId, startTime, endTime, withPhoto);
            if (nCount > 0)
            {
                for (int nIndex = 1; nIndex <= nCount; nIndex++)
                {
                    FaceServerSdkLib.AccessInfo access = new FaceServerSdkLib.AccessInfo();
                    
                    int nRet = mSdk.C_GetAccess(sn, ref access);
                    if (nRet == 0)
                    {
                        DateTime dt = DateTime.ParseExact(access.strRecTime, "yyyy-MM-dd HH:mm:ss", null);
                        double timestamp = (dt.AddHours(-Constant.Constants.TIME_ZONE) - new DateTime(1970, 1, 1, 0, 0, 0)).TotalSeconds;
                        String arid = sn.ToString() + timestamp;
                        userAccessList.Add(new AttendanceInfo(arid, timestamp, Double.Parse(sn), access.UserID, access.lRecType, access.lStatus, access.lPhotoLen, access.lReason, access.strBase64PhotoData));
                    }
                }
            }

            return userAccessList;

        }


        public ArrayList getUserAccess(String sn, String startTime, String endTime, Boolean withPhoto)
        {
            mListener.OnUploadStart("Initial the processing.");
            ArrayList userAccessList = new ArrayList();
            int nCount = mSdk.C_GetAllUsersAccessCount(sn, startTime, endTime, withPhoto);
            mListener.OnUploadStarting(nCount);

            if (nCount > 0)
            {
                for (int nIndex = 1; nIndex <= nCount; nIndex++)
                {
                    FaceServerSdkLib.AccessInfo access = new FaceServerSdkLib.AccessInfo();
                    int nRet = mSdk.C_GetAccess(sn, ref access);
                    if (nRet == 0)
                    {
                        mListener.OnUplading(nIndex,"");
                       // userAccessList.Add(access);
                        DateTime dt = DateTime.ParseExact(access.strRecTime, "yyyy-MM-dd HH:mm:ss", null);
                        double timestamp = (dt.AddHours(-Constant.Constants.TIME_ZONE) - new DateTime(1970, 1, 1, 0, 0, 0)).TotalSeconds;
                        String arid =sn.ToString() + timestamp;
                       userAccessList.Add( new AttendanceInfo(arid,timestamp,Double.Parse(sn),access.UserID,access.lRecType,access.lStatus,access.lPhotoLen,access.lReason,access.strBase64PhotoData));
                    }
                }
            }
            mListener.OnUpladEnd();
            return userAccessList;
        }


        public ArrayList getUserFailList(String sn, String startTime, String endTime, Boolean withPhoto)
        {
            ArrayList userAccessList = new ArrayList();

            int nCount = mSdk.C_GetAllUsersFailAccessCount(sn, startTime, endTime, withPhoto);
            if (nCount > 0)
            {
                for (int nIndex = 1; nIndex <= nCount; nIndex++)
                {
                    FaceServerSdkLib.AccessInfo access = new FaceServerSdkLib.AccessInfo();
                    int nRet = mSdk.C_GetAccess(sn, ref access);
                    if (nRet == 0)
                    {
                        DateTime dt = DateTime.ParseExact(access.strRecTime, "yyyy-MM-dd HH:mm:ss", null);
                        double timestamp = (dt.AddHours(-Constant.Constants.TIME_ZONE) - new DateTime(1970, 1, 1, 0, 0, 0)).TotalSeconds;
                        String arid = sn.ToString() + timestamp;
                        userAccessList.Add(new AttendanceInfo(arid, timestamp, Double.Parse(sn), access.UserID, access.lRecType, access.lStatus, access.lPhotoLen, access.lReason, access.strBase64PhotoData));
                    }
                }
            }
            return userAccessList;
        }

        public Boolean addUser(String sn, int userType, int userId, String cardNo, String userName, int verifyMode, int status, int photLen, String base64Photo)
        {
            Boolean isAdd = false;
            FaceServerSdkLib.UserInfo user = new FaceServerSdkLib.UserInfo();
            user.UserType = userType;
            user.UserID = userId;
            user.CardNo = cardNo.PadLeft(10, '0');
            user.UserName = userName;
            user.VerifyMode = verifyMode;
            user.Status = status;
            user.PhotoLen = photLen;
            user.PhotoBase64Data = base64Photo;

            long lRet = mSdk.C_AddUser(sn, user);
            if (lRet != 0)
            {
                if (mListener != null)
                {
                    mListener.OnError("C_AddUser failed!ErrCode：" + Convert.ToString(mSdk.C_SdkGetLastError(sn), 16), mSdk.C_SdkGetLastError(sn));
                }
            }
            else
            {
                isAdd = true;
            }
            return isAdd;

        }

        public Boolean addUserWithFeature(String sn, int userType, int userId, String cardNo, String userName, int verifyMode, int status, int photLen, String base64Photo, String base64Featue)
        {
            Boolean isAdd = false;
            FaceServerSdkLib.UserInfoAndFeature user = new FaceServerSdkLib.UserInfoAndFeature();
            user.UserType = userType;
            user.UserID = userId;
            user.CardNo = cardNo.PadLeft(10, '0');
            user.UserName = userName;
            user.VerifyMode = verifyMode;
            user.Status = status;
            user.PhotoLen = photLen;
            user.PhotoBase64Data = base64Photo;
            user.FeatureBase64Data = base64Featue;


            long lRet = mSdk.C_AddUserAndFeature(sn, user);
            if (lRet != 0)
            {

                if (mListener != null)
                {
                    mListener.OnError("C_AddUserAndFeature failed!ErrCode：" + Convert.ToString(mSdk.C_SdkGetLastError(sn), 16), mSdk.C_SdkGetLastError(sn));
                    if (Convert.ToString(mSdk.C_SdkGetLastError(sn), 16) == "40000008")
                    {
                        mListener.OnInsertDeviceProblem(userId, userName);
                    }

                }
            }
            else
            {
                isAdd = true;
            }

            return isAdd;

        }

        public Boolean AddUsersWithFeature(ArrayList users,string sn) {
            Boolean isAdd = true;
            int mUserCount =users.Count ;
            mListener.OnUploadStarting(mUserCount);
            try
            {
                int nIndex = 0;
                foreach (UserDevice u in users)
                {
                    mListener.OnUplading(++nIndex, u.UserName);
                    addUserWithFeature(sn,(int)u.UserType,u.EmployeeID,Convert.ToString(u.CardNo),u.UserName,(int)u.VerfyMode,(int)u.RegStatus,(int)u.PhotoLen,u.Base64PhotoData,u.FeatureBase64);
                }
            }
            catch(Exception){
                isAdd = false;
            }
            mListener.OnUpladEnd();
            return isAdd;
        }


        public Boolean modifyUser(String sn, int userType, int userId, String cardNo, String userName, int verifyMode, int status, int photLen, String base64Photo)
        {
            Boolean isModify = false;
            FaceServerSdkLib.UserInfo user = new FaceServerSdkLib.UserInfo();
            user.UserType = userType;
            user.UserID = userId;
            user.CardNo = cardNo.PadLeft(10, '0');
            user.UserName = userName;
            user.VerifyMode = verifyMode;
            user.Status = status;
            user.PhotoLen = photLen;
            user.PhotoBase64Data = base64Photo;

            long lRet = mSdk.C_ModifyUser(sn, user);
            if (lRet != 0)
            {
                if (mListener != null)
                {
                    mListener.OnError("C_ModifyUser failed!ErrCode：" + Convert.ToString(mSdk.C_SdkGetLastError(sn), 16), mSdk.C_SdkGetLastError(sn));
                }
            }
            else
            {
                isModify = true;
            }

            return isModify;
        }

        public Boolean deleteSingleUser(String sn, int userId, int userType)
        {
            Boolean isDelete = false;
            long lRet = mSdk.C_DelSingleUser(sn, userId, userType);
            if (lRet != 0)
            {
                if (mListener != null)
                {
                    mListener.OnError("C_DelSingleUser failed!ErrCode：" + Convert.ToString(mSdk.C_SdkGetLastError(sn), 16), mSdk.C_SdkGetLastError(sn));
                }

            }
            else
            {
                isDelete = true;
            }


            return isDelete;
        }

        public Boolean deleteAllUser(String sn)
        {
            Boolean isDeleted = false;
            long lRet = mSdk.C_DelAllUsers(sn);
            if (lRet != 0)
            {
                if (mListener != null)
                {
                    mListener.OnError("C_DelAllUsers failed!ErrCode：" + Convert.ToString(mSdk.C_SdkGetLastError(sn), 16), mSdk.C_SdkGetLastError(sn));
                }
            }
            else
            {
                isDeleted = true;
            }
            return isDeleted;
        }


        public Boolean modifyUserFeature(String sn, int userId, int userType, String base64Feature)
        {
            Boolean isModify = false;
            long lRet = mSdk.C_ModifyUserFeature(sn, userId, userType, base64Feature);
            if (lRet != 0)
            {
                if (mListener != null)
                    mListener.OnError("C_ModifyUserFeature failed!ErrCode：" + Convert.ToString(this.mSdk.C_SdkGetLastError(sn), 16), mSdk.C_SdkGetLastError(sn));
            }
            else
            {
                isModify = true;
            }

            return isModify;

        }

        public void refreshDevice() {
            ICollection key = mDeviceList.Keys;
             Hashtable mDevice = new Hashtable();
            foreach (String k in key)
            {
                DeviceData aDevice = (DeviceData)mDeviceList[k];
                aDevice.hinfo = HardwareMan.getHardwareInfo(aDevice.deviceSn);
                mDevice.Add(aDevice.deviceSn, aDevice);

            }
            mDeviceList = mDevice;
        }
        /**************************Event Listener****************************/
        private void OnEventCConnect(string strDevSn, int lOpCode, int lUserData, int lExtendParam, string strIP, int lPort)
        {
            DeviceData dd=new DeviceData(strIP, lPort, strDevSn, lOpCode, lUserData, lExtendParam);
            dd.hinfo=HardwareMan.getHardwareInfo(strDevSn);
            mDeviceList.Add(strDevSn, dd);
            if (mListener != null)
            {
                mListener.OnEventConnect();
            }
        }

        private void OnEventDisConnect(string strDevSn, int lOpCode, int lUserData, int lExtendParam)
        {
            mDeviceList.Remove(strDevSn);
            if (mListener != null)
            {
                mListener.OnEventDisconnect();
            }
        }

        private void OnTrapAccess(string strDevSn, int lOpCode, int lUserData, int lExtendParam, int lUserID, string strRecTime, int lRecType, int lScore, int lStatus, int lPhotoType, int lPhotoLen, int lReason, string strBase64PhotoData)
        {
            if (mListener != null)
            {
                mListener.OnTrapAccess(strDevSn, lOpCode, lUserData, lExtendParam, lUserID, strRecTime, lRecType, lScore, lStatus, lPhotoType, lPhotoLen, lReason, strBase64PhotoData);
            }
        }


        private void OnEventCErrorNotify(string strDevSn, int lOpCode, int lUserData, int lExtendParam, int lErrorCode)
        {
            if (mListener != null)
            {
                mListener.OnEventErrorNotify(strDevSn, lOpCode, lUserData, lExtendParam, lErrorCode);
            }
        }
    }
}

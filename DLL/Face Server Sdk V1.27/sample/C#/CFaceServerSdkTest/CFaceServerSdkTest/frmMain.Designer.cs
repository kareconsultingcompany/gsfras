﻿namespace CFaceServerSdkTest
{
    partial class frmMain
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.panel1 = new System.Windows.Forms.Panel();
            this.listView1 = new System.Windows.Forms.ListView();
            this.colNo = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colTime = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colDev = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colStatus = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colDetail = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.clearToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.txtDesCardNo = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.button7 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.txtSourceCardNo = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.button5 = new System.Windows.Forms.Button();
            this.txtDvsSn = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.button4 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.txtSdkVer = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.button13 = new System.Windows.Forms.Button();
            this.button12 = new System.Windows.Forms.Button();
            this.button11 = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.txtDvSn = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtDvsMode = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtDvsName = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtDvsType = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtProVer = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.button15 = new System.Windows.Forms.Button();
            this.button14 = new System.Windows.Forms.Button();
            this.txtServerIP = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txtTimeType = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtTime = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtZoom = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.button17 = new System.Windows.Forms.Button();
            this.label17 = new System.Windows.Forms.Label();
            this.txtGetUserType = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.button16 = new System.Windows.Forms.Button();
            this.txtGetUserID = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.label18 = new System.Windows.Forms.Label();
            this.txtGetFeaUserType = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.button19 = new System.Windows.Forms.Button();
            this.txtGetFeaUserID = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.tabPage15 = new System.Windows.Forms.TabPage();
            this.button21 = new System.Windows.Forms.Button();
            this.button20 = new System.Windows.Forms.Button();
            this.button18 = new System.Windows.Forms.Button();
            this.chkSaveFile = new System.Windows.Forms.CheckBox();
            this.chkGetPhoto = new System.Windows.Forms.CheckBox();
            this.txtAccessEnd = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.txtAccessStart = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.txtAccessUserID = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.tabPage16 = new System.Windows.Forms.TabPage();
            this.label40 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.button28 = new System.Windows.Forms.Button();
            this.button27 = new System.Windows.Forms.Button();
            this.button26 = new System.Windows.Forms.Button();
            this.button25 = new System.Windows.Forms.Button();
            this.button24 = new System.Windows.Forms.Button();
            this.button23 = new System.Windows.Forms.Button();
            this.button22 = new System.Windows.Forms.Button();
            this.txtSetUserFea = new System.Windows.Forms.TextBox();
            this.label37 = new System.Windows.Forms.Label();
            this.txtSetPhoto = new System.Windows.Forms.TextBox();
            this.label36 = new System.Windows.Forms.Label();
            this.txtSetPhotoType = new System.Windows.Forms.TextBox();
            this.label35 = new System.Windows.Forms.Label();
            this.txtSetUserRegStatus = new System.Windows.Forms.TextBox();
            this.label34 = new System.Windows.Forms.Label();
            this.txtSetUserStatus = new System.Windows.Forms.TextBox();
            this.label33 = new System.Windows.Forms.Label();
            this.txtSetUserName = new System.Windows.Forms.TextBox();
            this.label32 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.txtSetUserVerify = new System.Windows.Forms.TextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.txtSetUserCardNo = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.txtSetUserId = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.txtSetUserType = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.tabPage17 = new System.Windows.Forms.TabPage();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.txtSetFeaFile = new System.Windows.Forms.TextBox();
            this.label41 = new System.Windows.Forms.Label();
            this.txtSetFeaUserID = new System.Windows.Forms.TextBox();
            this.label42 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.txtSetFeaUserType = new System.Windows.Forms.TextBox();
            this.label44 = new System.Windows.Forms.Label();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.button30 = new System.Windows.Forms.Button();
            this.button29 = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label45 = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.label47 = new System.Windows.Forms.Label();
            this.tabPage7 = new System.Windows.Forms.TabPage();
            this.button33 = new System.Windows.Forms.Button();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.label49 = new System.Windows.Forms.Label();
            this.button32 = new System.Windows.Forms.Button();
            this.button31 = new System.Windows.Forms.Button();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.label48 = new System.Windows.Forms.Label();
            this.tabPage8 = new System.Windows.Forms.TabPage();
            this.button35 = new System.Windows.Forms.Button();
            this.button34 = new System.Windows.Forms.Button();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.label50 = new System.Windows.Forms.Label();
            this.tabPage9 = new System.Windows.Forms.TabPage();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.label52 = new System.Windows.Forms.Label();
            this.button36 = new System.Windows.Forms.Button();
            this.button37 = new System.Windows.Forms.Button();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.label51 = new System.Windows.Forms.Label();
            this.tabPage10 = new System.Windows.Forms.TabPage();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.button52 = new System.Windows.Forms.Button();
            this.button53 = new System.Windows.Forms.Button();
            this.textBox27 = new System.Windows.Forms.TextBox();
            this.label84 = new System.Windows.Forms.Label();
            this.textBox28 = new System.Windows.Forms.TextBox();
            this.label85 = new System.Windows.Forms.Label();
            this.textBox29 = new System.Windows.Forms.TextBox();
            this.label86 = new System.Windows.Forms.Label();
            this.textBox30 = new System.Windows.Forms.TextBox();
            this.label87 = new System.Windows.Forms.Label();
            this.textBox23 = new System.Windows.Forms.TextBox();
            this.label80 = new System.Windows.Forms.Label();
            this.textBox24 = new System.Windows.Forms.TextBox();
            this.label81 = new System.Windows.Forms.Label();
            this.textBox25 = new System.Windows.Forms.TextBox();
            this.label82 = new System.Windows.Forms.Label();
            this.textBox26 = new System.Windows.Forms.TextBox();
            this.label83 = new System.Windows.Forms.Label();
            this.textBox19 = new System.Windows.Forms.TextBox();
            this.label76 = new System.Windows.Forms.Label();
            this.textBox20 = new System.Windows.Forms.TextBox();
            this.label77 = new System.Windows.Forms.Label();
            this.textBox21 = new System.Windows.Forms.TextBox();
            this.label78 = new System.Windows.Forms.Label();
            this.textBox22 = new System.Windows.Forms.TextBox();
            this.label79 = new System.Windows.Forms.Label();
            this.textBox17 = new System.Windows.Forms.TextBox();
            this.label75 = new System.Windows.Forms.Label();
            this.textBox18 = new System.Windows.Forms.TextBox();
            this.label74 = new System.Windows.Forms.Label();
            this.textBox16 = new System.Windows.Forms.TextBox();
            this.label73 = new System.Windows.Forms.Label();
            this.textBox15 = new System.Windows.Forms.TextBox();
            this.label72 = new System.Windows.Forms.Label();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.button50 = new System.Windows.Forms.Button();
            this.button51 = new System.Windows.Forms.Button();
            this.label71 = new System.Windows.Forms.Label();
            this.textBox14 = new System.Windows.Forms.TextBox();
            this.label70 = new System.Windows.Forms.Label();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.button48 = new System.Windows.Forms.Button();
            this.button49 = new System.Windows.Forms.Button();
            this.label69 = new System.Windows.Forms.Label();
            this.textBox13 = new System.Windows.Forms.TextBox();
            this.label68 = new System.Windows.Forms.Label();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.button47 = new System.Windows.Forms.Button();
            this.button46 = new System.Windows.Forms.Button();
            this.label67 = new System.Windows.Forms.Label();
            this.textBox12 = new System.Windows.Forms.TextBox();
            this.label66 = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.button45 = new System.Windows.Forms.Button();
            this.button44 = new System.Windows.Forms.Button();
            this.label65 = new System.Windows.Forms.Label();
            this.textBox11 = new System.Windows.Forms.TextBox();
            this.label64 = new System.Windows.Forms.Label();
            this.button43 = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.button42 = new System.Windows.Forms.Button();
            this.label63 = new System.Windows.Forms.Label();
            this.textBox10 = new System.Windows.Forms.TextBox();
            this.label62 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.button41 = new System.Windows.Forms.Button();
            this.button40 = new System.Windows.Forms.Button();
            this.label61 = new System.Windows.Forms.Label();
            this.textBox9 = new System.Windows.Forms.TextBox();
            this.label60 = new System.Windows.Forms.Label();
            this.label59 = new System.Windows.Forms.Label();
            this.textBox8 = new System.Windows.Forms.TextBox();
            this.label58 = new System.Windows.Forms.Label();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.label57 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.textBox91 = new System.Windows.Forms.TextBox();
            this.textBox90 = new System.Windows.Forms.TextBox();
            this.textBox89 = new System.Windows.Forms.TextBox();
            this.textBox88 = new System.Windows.Forms.TextBox();
            this.button39 = new System.Windows.Forms.Button();
            this.button38 = new System.Windows.Forms.Button();
            this.label56 = new System.Windows.Forms.Label();
            this.label55 = new System.Windows.Forms.Label();
            this.label54 = new System.Windows.Forms.Label();
            this.label53 = new System.Windows.Forms.Label();
            this.tabPage11 = new System.Windows.Forms.TabPage();
            this.groupBox20 = new System.Windows.Forms.GroupBox();
            this.button71 = new System.Windows.Forms.Button();
            this.button72 = new System.Windows.Forms.Button();
            this.textBox58 = new System.Windows.Forms.TextBox();
            this.textBox57 = new System.Windows.Forms.TextBox();
            this.textBox56 = new System.Windows.Forms.TextBox();
            this.textBox55 = new System.Windows.Forms.TextBox();
            this.label116 = new System.Windows.Forms.Label();
            this.label115 = new System.Windows.Forms.Label();
            this.label114 = new System.Windows.Forms.Label();
            this.label113 = new System.Windows.Forms.Label();
            this.groupBox19 = new System.Windows.Forms.GroupBox();
            this.button70 = new System.Windows.Forms.Button();
            this.groupBox18 = new System.Windows.Forms.GroupBox();
            this.button68 = new System.Windows.Forms.Button();
            this.button69 = new System.Windows.Forms.Button();
            this.label112 = new System.Windows.Forms.Label();
            this.textBox54 = new System.Windows.Forms.TextBox();
            this.label111 = new System.Windows.Forms.Label();
            this.groupBox17 = new System.Windows.Forms.GroupBox();
            this.button67 = new System.Windows.Forms.Button();
            this.button66 = new System.Windows.Forms.Button();
            this.textBox53 = new System.Windows.Forms.TextBox();
            this.label110 = new System.Windows.Forms.Label();
            this.textBox52 = new System.Windows.Forms.TextBox();
            this.label109 = new System.Windows.Forms.Label();
            this.textBox51 = new System.Windows.Forms.TextBox();
            this.label108 = new System.Windows.Forms.Label();
            this.groupBox16 = new System.Windows.Forms.GroupBox();
            this.button65 = new System.Windows.Forms.Button();
            this.button64 = new System.Windows.Forms.Button();
            this.button63 = new System.Windows.Forms.Button();
            this.textBox50 = new System.Windows.Forms.TextBox();
            this.textBox49 = new System.Windows.Forms.TextBox();
            this.label107 = new System.Windows.Forms.Label();
            this.label106 = new System.Windows.Forms.Label();
            this.groupBox15 = new System.Windows.Forms.GroupBox();
            this.button61 = new System.Windows.Forms.Button();
            this.button62 = new System.Windows.Forms.Button();
            this.checkBox4 = new System.Windows.Forms.CheckBox();
            this.checkBox3 = new System.Windows.Forms.CheckBox();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.groupBox14 = new System.Windows.Forms.GroupBox();
            this.button60 = new System.Windows.Forms.Button();
            this.groupBox13 = new System.Windows.Forms.GroupBox();
            this.button59 = new System.Windows.Forms.Button();
            this.button58 = new System.Windows.Forms.Button();
            this.textBox47 = new System.Windows.Forms.TextBox();
            this.label104 = new System.Windows.Forms.Label();
            this.textBox48 = new System.Windows.Forms.TextBox();
            this.label105 = new System.Windows.Forms.Label();
            this.textBox45 = new System.Windows.Forms.TextBox();
            this.label102 = new System.Windows.Forms.Label();
            this.textBox46 = new System.Windows.Forms.TextBox();
            this.label103 = new System.Windows.Forms.Label();
            this.textBox43 = new System.Windows.Forms.TextBox();
            this.label100 = new System.Windows.Forms.Label();
            this.textBox44 = new System.Windows.Forms.TextBox();
            this.label101 = new System.Windows.Forms.Label();
            this.textBox41 = new System.Windows.Forms.TextBox();
            this.label98 = new System.Windows.Forms.Label();
            this.textBox42 = new System.Windows.Forms.TextBox();
            this.label99 = new System.Windows.Forms.Label();
            this.textBox39 = new System.Windows.Forms.TextBox();
            this.label96 = new System.Windows.Forms.Label();
            this.textBox40 = new System.Windows.Forms.TextBox();
            this.label97 = new System.Windows.Forms.Label();
            this.textBox37 = new System.Windows.Forms.TextBox();
            this.label94 = new System.Windows.Forms.Label();
            this.textBox38 = new System.Windows.Forms.TextBox();
            this.label95 = new System.Windows.Forms.Label();
            this.textBox35 = new System.Windows.Forms.TextBox();
            this.label92 = new System.Windows.Forms.Label();
            this.textBox36 = new System.Windows.Forms.TextBox();
            this.label93 = new System.Windows.Forms.Label();
            this.textBox34 = new System.Windows.Forms.TextBox();
            this.label91 = new System.Windows.Forms.Label();
            this.textBox33 = new System.Windows.Forms.TextBox();
            this.label90 = new System.Windows.Forms.Label();
            this.groupBox12 = new System.Windows.Forms.GroupBox();
            this.button57 = new System.Windows.Forms.Button();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.button56 = new System.Windows.Forms.Button();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.button54 = new System.Windows.Forms.Button();
            this.button55 = new System.Windows.Forms.Button();
            this.textBox32 = new System.Windows.Forms.TextBox();
            this.textBox31 = new System.Windows.Forms.TextBox();
            this.label89 = new System.Windows.Forms.Label();
            this.label88 = new System.Windows.Forms.Label();
            this.tabPage12 = new System.Windows.Forms.TabPage();
            this.button81 = new System.Windows.Forms.Button();
            this.button82 = new System.Windows.Forms.Button();
            this.label161 = new System.Windows.Forms.Label();
            this.label160 = new System.Windows.Forms.Label();
            this.textBox86 = new System.Windows.Forms.TextBox();
            this.label159 = new System.Windows.Forms.Label();
            this.textBox85 = new System.Windows.Forms.TextBox();
            this.label158 = new System.Windows.Forms.Label();
            this.textBox84 = new System.Windows.Forms.TextBox();
            this.label157 = new System.Windows.Forms.Label();
            this.textBox83 = new System.Windows.Forms.TextBox();
            this.label156 = new System.Windows.Forms.Label();
            this.textBox82 = new System.Windows.Forms.TextBox();
            this.label155 = new System.Windows.Forms.Label();
            this.tabPage13 = new System.Windows.Forms.TabPage();
            this.groupBox23 = new System.Windows.Forms.GroupBox();
            this.button79 = new System.Windows.Forms.Button();
            this.button80 = new System.Windows.Forms.Button();
            this.label154 = new System.Windows.Forms.Label();
            this.textBox81 = new System.Windows.Forms.TextBox();
            this.label153 = new System.Windows.Forms.Label();
            this.groupBox22 = new System.Windows.Forms.GroupBox();
            this.groupBox29 = new System.Windows.Forms.GroupBox();
            this.radioButton5 = new System.Windows.Forms.RadioButton();
            this.radioButton6 = new System.Windows.Forms.RadioButton();
            this.groupBox28 = new System.Windows.Forms.GroupBox();
            this.radioButton4 = new System.Windows.Forms.RadioButton();
            this.radioButton3 = new System.Windows.Forms.RadioButton();
            this.button77 = new System.Windows.Forms.Button();
            this.button78 = new System.Windows.Forms.Button();
            this.label152 = new System.Windows.Forms.Label();
            this.textBox80 = new System.Windows.Forms.TextBox();
            this.label151 = new System.Windows.Forms.Label();
            this.label150 = new System.Windows.Forms.Label();
            this.label149 = new System.Windows.Forms.Label();
            this.groupBox21 = new System.Windows.Forms.GroupBox();
            this.button75 = new System.Windows.Forms.Button();
            this.button76 = new System.Windows.Forms.Button();
            this.label143 = new System.Windows.Forms.Label();
            this.label144 = new System.Windows.Forms.Label();
            this.textBox76 = new System.Windows.Forms.TextBox();
            this.label145 = new System.Windows.Forms.Label();
            this.textBox77 = new System.Windows.Forms.TextBox();
            this.label146 = new System.Windows.Forms.Label();
            this.textBox78 = new System.Windows.Forms.TextBox();
            this.label147 = new System.Windows.Forms.Label();
            this.textBox79 = new System.Windows.Forms.TextBox();
            this.label148 = new System.Windows.Forms.Label();
            this.label137 = new System.Windows.Forms.Label();
            this.label138 = new System.Windows.Forms.Label();
            this.textBox72 = new System.Windows.Forms.TextBox();
            this.label139 = new System.Windows.Forms.Label();
            this.textBox73 = new System.Windows.Forms.TextBox();
            this.label140 = new System.Windows.Forms.Label();
            this.textBox74 = new System.Windows.Forms.TextBox();
            this.label141 = new System.Windows.Forms.Label();
            this.textBox75 = new System.Windows.Forms.TextBox();
            this.label142 = new System.Windows.Forms.Label();
            this.label131 = new System.Windows.Forms.Label();
            this.label132 = new System.Windows.Forms.Label();
            this.textBox68 = new System.Windows.Forms.TextBox();
            this.label133 = new System.Windows.Forms.Label();
            this.textBox69 = new System.Windows.Forms.TextBox();
            this.label134 = new System.Windows.Forms.Label();
            this.textBox70 = new System.Windows.Forms.TextBox();
            this.label135 = new System.Windows.Forms.Label();
            this.textBox71 = new System.Windows.Forms.TextBox();
            this.label136 = new System.Windows.Forms.Label();
            this.label125 = new System.Windows.Forms.Label();
            this.label126 = new System.Windows.Forms.Label();
            this.textBox64 = new System.Windows.Forms.TextBox();
            this.label127 = new System.Windows.Forms.Label();
            this.textBox65 = new System.Windows.Forms.TextBox();
            this.label128 = new System.Windows.Forms.Label();
            this.textBox66 = new System.Windows.Forms.TextBox();
            this.label129 = new System.Windows.Forms.Label();
            this.textBox67 = new System.Windows.Forms.TextBox();
            this.label130 = new System.Windows.Forms.Label();
            this.label124 = new System.Windows.Forms.Label();
            this.label123 = new System.Windows.Forms.Label();
            this.textBox63 = new System.Windows.Forms.TextBox();
            this.label122 = new System.Windows.Forms.Label();
            this.textBox62 = new System.Windows.Forms.TextBox();
            this.label121 = new System.Windows.Forms.Label();
            this.textBox61 = new System.Windows.Forms.TextBox();
            this.label120 = new System.Windows.Forms.Label();
            this.textBox60 = new System.Windows.Forms.TextBox();
            this.label119 = new System.Windows.Forms.Label();
            this.button73 = new System.Windows.Forms.Button();
            this.button74 = new System.Windows.Forms.Button();
            this.label118 = new System.Windows.Forms.Label();
            this.textBox59 = new System.Windows.Forms.TextBox();
            this.label117 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.treeView1 = new System.Windows.Forms.TreeView();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.bu_Uninit = new System.Windows.Forms.Button();
            this.bu_Init = new System.Windows.Forms.Button();
            this.txtPort = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.contextMenuStrip1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel4.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.tabPage5.SuspendLayout();
            this.tabPage15.SuspendLayout();
            this.tabPage16.SuspendLayout();
            this.tabPage17.SuspendLayout();
            this.tabPage6.SuspendLayout();
            this.tabPage7.SuspendLayout();
            this.tabPage8.SuspendLayout();
            this.tabPage9.SuspendLayout();
            this.tabPage10.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.tabPage11.SuspendLayout();
            this.groupBox20.SuspendLayout();
            this.groupBox19.SuspendLayout();
            this.groupBox18.SuspendLayout();
            this.groupBox17.SuspendLayout();
            this.groupBox16.SuspendLayout();
            this.groupBox15.SuspendLayout();
            this.groupBox14.SuspendLayout();
            this.groupBox13.SuspendLayout();
            this.groupBox12.SuspendLayout();
            this.groupBox11.SuspendLayout();
            this.groupBox10.SuspendLayout();
            this.tabPage12.SuspendLayout();
            this.tabPage13.SuspendLayout();
            this.groupBox23.SuspendLayout();
            this.groupBox22.SuspendLayout();
            this.groupBox29.SuspendLayout();
            this.groupBox28.SuspendLayout();
            this.groupBox21.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.listView1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 495);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1078, 131);
            this.panel1.TabIndex = 0;
            // 
            // listView1
            // 
            this.listView1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colNo,
            this.colTime,
            this.colDev,
            this.colStatus,
            this.colDetail});
            this.listView1.ContextMenuStrip = this.contextMenuStrip1;
            this.listView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listView1.FullRowSelect = true;
            this.listView1.HideSelection = false;
            this.listView1.Location = new System.Drawing.Point(0, 0);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(1078, 131);
            this.listView1.TabIndex = 0;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.View = System.Windows.Forms.View.Details;
            // 
            // colNo
            // 
            this.colNo.Text = "NO";
            // 
            // colTime
            // 
            this.colTime.Text = "Time";
            this.colTime.Width = 150;
            // 
            // colDev
            // 
            this.colDev.Text = "Device";
            this.colDev.Width = 100;
            // 
            // colStatus
            // 
            this.colStatus.Text = "Status";
            this.colStatus.Width = 100;
            // 
            // colDetail
            // 
            this.colDetail.Text = "Detail";
            this.colDetail.Width = 500;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.clearToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(107, 26);
            // 
            // clearToolStripMenuItem
            // 
            this.clearToolStripMenuItem.Name = "clearToolStripMenuItem";
            this.clearToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.clearToolStripMenuItem.Text = "Clear";
            this.clearToolStripMenuItem.Click += new System.EventHandler(this.clearToolStripMenuItem_Click);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.panel4);
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1078, 495);
            this.panel2.TabIndex = 1;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.tabControl1);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(185, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(893, 495);
            this.panel4.TabIndex = 1;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Controls.Add(this.tabPage15);
            this.tabControl1.Controls.Add(this.tabPage16);
            this.tabControl1.Controls.Add(this.tabPage17);
            this.tabControl1.Controls.Add(this.tabPage6);
            this.tabControl1.Controls.Add(this.tabPage7);
            this.tabControl1.Controls.Add(this.tabPage8);
            this.tabControl1.Controls.Add(this.tabPage9);
            this.tabControl1.Controls.Add(this.tabPage10);
            this.tabControl1.Controls.Add(this.tabPage11);
            this.tabControl1.Controls.Add(this.tabPage12);
            this.tabControl1.Controls.Add(this.tabPage13);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(893, 495);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.txtDesCardNo);
            this.tabPage1.Controls.Add(this.label5);
            this.tabPage1.Controls.Add(this.button7);
            this.tabPage1.Controls.Add(this.button6);
            this.tabPage1.Controls.Add(this.txtSourceCardNo);
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Controls.Add(this.button5);
            this.tabPage1.Controls.Add(this.txtDvsSn);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.button4);
            this.tabPage1.Controls.Add(this.button3);
            this.tabPage1.Controls.Add(this.txtSdkVer);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(885, 469);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "SDK Information";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // txtDesCardNo
            // 
            this.txtDesCardNo.Location = new System.Drawing.Point(105, 160);
            this.txtDesCardNo.Name = "txtDesCardNo";
            this.txtDesCardNo.ReadOnly = true;
            this.txtDesCardNo.Size = new System.Drawing.Size(126, 21);
            this.txtDesCardNo.TabIndex = 12;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(27, 164);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(65, 12);
            this.label5.TabIndex = 11;
            this.label5.Text = "Des CardNo";
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(245, 164);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(151, 21);
            this.button7.TabIndex = 10;
            this.button7.Text = "ConvertCardNoToLong";
            this.button7.UseVisualStyleBackColor = true;
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(245, 133);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(151, 21);
            this.button6.TabIndex = 9;
            this.button6.Text = "ConvertCardNoToString";
            this.button6.UseVisualStyleBackColor = true;
            // 
            // txtSourceCardNo
            // 
            this.txtSourceCardNo.Location = new System.Drawing.Point(105, 133);
            this.txtSourceCardNo.Name = "txtSourceCardNo";
            this.txtSourceCardNo.Size = new System.Drawing.Size(126, 21);
            this.txtSourceCardNo.TabIndex = 8;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(27, 137);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(77, 12);
            this.label4.TabIndex = 7;
            this.label4.Text = "SourceCardNo";
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(277, 85);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(119, 20);
            this.button5.TabIndex = 6;
            this.button5.Text = "CheckOnline";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // txtDvsSn
            // 
            this.txtDvsSn.Location = new System.Drawing.Point(105, 84);
            this.txtDvsSn.Name = "txtDvsSn";
            this.txtDvsSn.Size = new System.Drawing.Size(162, 21);
            this.txtDvsSn.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(25, 91);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(23, 12);
            this.label3.TabIndex = 4;
            this.label3.Text = "SN:";
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(277, 52);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(96, 20);
            this.button4.TabIndex = 3;
            this.button4.Text = "GetDeviceList";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(277, 21);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(103, 20);
            this.button3.TabIndex = 2;
            this.button3.Text = "Get SdkVersion";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // txtSdkVer
            // 
            this.txtSdkVer.Location = new System.Drawing.Point(105, 21);
            this.txtSdkVer.Name = "txtSdkVer";
            this.txtSdkVer.Size = new System.Drawing.Size(162, 21);
            this.txtSdkVer.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(22, 24);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(77, 12);
            this.label2.TabIndex = 0;
            this.label2.Text = "Sdk Version:";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.button13);
            this.tabPage2.Controls.Add(this.button12);
            this.tabPage2.Controls.Add(this.button11);
            this.tabPage2.Controls.Add(this.button10);
            this.tabPage2.Controls.Add(this.button9);
            this.tabPage2.Controls.Add(this.button8);
            this.tabPage2.Controls.Add(this.txtDvSn);
            this.tabPage2.Controls.Add(this.label10);
            this.tabPage2.Controls.Add(this.txtDvsMode);
            this.tabPage2.Controls.Add(this.label9);
            this.tabPage2.Controls.Add(this.txtDvsName);
            this.tabPage2.Controls.Add(this.label8);
            this.tabPage2.Controls.Add(this.txtDvsType);
            this.tabPage2.Controls.Add(this.label7);
            this.tabPage2.Controls.Add(this.txtProVer);
            this.tabPage2.Controls.Add(this.label6);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(885, 469);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Device Info";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // button13
            // 
            this.button13.Location = new System.Drawing.Point(257, 161);
            this.button13.Name = "button13";
            this.button13.Size = new System.Drawing.Size(96, 20);
            this.button13.TabIndex = 15;
            this.button13.Text = "GetDeviceSn";
            this.button13.UseVisualStyleBackColor = true;
            this.button13.Click += new System.EventHandler(this.button13_Click);
            // 
            // button12
            // 
            this.button12.Location = new System.Drawing.Point(257, 128);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(96, 20);
            this.button12.TabIndex = 14;
            this.button12.Text = "GetDeviceModel";
            this.button12.UseVisualStyleBackColor = true;
            this.button12.Click += new System.EventHandler(this.button12_Click);
            // 
            // button11
            // 
            this.button11.Location = new System.Drawing.Point(257, 96);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(96, 20);
            this.button11.TabIndex = 13;
            this.button11.Text = "GetDeviceName";
            this.button11.UseVisualStyleBackColor = true;
            this.button11.Click += new System.EventHandler(this.button11_Click);
            // 
            // button10
            // 
            this.button10.Location = new System.Drawing.Point(257, 57);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(96, 20);
            this.button10.TabIndex = 12;
            this.button10.Text = "GetDeviceType";
            this.button10.UseVisualStyleBackColor = true;
            this.button10.Click += new System.EventHandler(this.button10_Click);
            // 
            // button9
            // 
            this.button9.Location = new System.Drawing.Point(398, 16);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(115, 20);
            this.button9.TabIndex = 11;
            this.button9.Text = "GetDeviceVersion";
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.button9_Click);
            // 
            // button8
            // 
            this.button8.Location = new System.Drawing.Point(257, 17);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(135, 20);
            this.button8.TabIndex = 10;
            this.button8.Text = "GetProtocolVersion";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // txtDvSn
            // 
            this.txtDvSn.Location = new System.Drawing.Point(112, 160);
            this.txtDvSn.Name = "txtDvSn";
            this.txtDvSn.Size = new System.Drawing.Size(139, 21);
            this.txtDvSn.TabIndex = 9;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(11, 164);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(59, 12);
            this.label10.TabIndex = 8;
            this.label10.Text = "Device SN";
            // 
            // txtDvsMode
            // 
            this.txtDvsMode.Location = new System.Drawing.Point(112, 127);
            this.txtDvsMode.Name = "txtDvsMode";
            this.txtDvsMode.Size = new System.Drawing.Size(139, 21);
            this.txtDvsMode.TabIndex = 7;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(11, 130);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(71, 12);
            this.label9.TabIndex = 6;
            this.label9.Text = "Device Mode";
            // 
            // txtDvsName
            // 
            this.txtDvsName.Location = new System.Drawing.Point(112, 93);
            this.txtDvsName.Name = "txtDvsName";
            this.txtDvsName.Size = new System.Drawing.Size(139, 21);
            this.txtDvsName.TabIndex = 5;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(11, 96);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(71, 12);
            this.label8.TabIndex = 4;
            this.label8.Text = "Device Name";
            // 
            // txtDvsType
            // 
            this.txtDvsType.Location = new System.Drawing.Point(112, 57);
            this.txtDvsType.Name = "txtDvsType";
            this.txtDvsType.Size = new System.Drawing.Size(139, 21);
            this.txtDvsType.TabIndex = 3;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(11, 60);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(71, 12);
            this.label7.TabIndex = 2;
            this.label7.Text = "Device Type";
            // 
            // txtProVer
            // 
            this.txtProVer.Location = new System.Drawing.Point(112, 17);
            this.txtProVer.Name = "txtProVer";
            this.txtProVer.Size = new System.Drawing.Size(139, 21);
            this.txtProVer.TabIndex = 1;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(11, 20);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(95, 12);
            this.label6.TabIndex = 0;
            this.label6.Text = "ProtocolVersion";
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.button15);
            this.tabPage3.Controls.Add(this.button14);
            this.tabPage3.Controls.Add(this.txtServerIP);
            this.tabPage3.Controls.Add(this.label14);
            this.tabPage3.Controls.Add(this.txtTimeType);
            this.tabPage3.Controls.Add(this.label13);
            this.tabPage3.Controls.Add(this.txtTime);
            this.tabPage3.Controls.Add(this.label12);
            this.tabPage3.Controls.Add(this.txtZoom);
            this.tabPage3.Controls.Add(this.label11);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(885, 469);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Time";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // button15
            // 
            this.button15.Location = new System.Drawing.Point(248, 62);
            this.button15.Name = "button15";
            this.button15.Size = new System.Drawing.Size(96, 20);
            this.button15.TabIndex = 12;
            this.button15.Text = "SetDeviceTime";
            this.button15.UseVisualStyleBackColor = true;
            this.button15.Click += new System.EventHandler(this.button15_Click);
            // 
            // button14
            // 
            this.button14.Location = new System.Drawing.Point(248, 31);
            this.button14.Name = "button14";
            this.button14.Size = new System.Drawing.Size(96, 20);
            this.button14.TabIndex = 11;
            this.button14.Text = "GetDeviceTime";
            this.button14.UseVisualStyleBackColor = true;
            this.button14.Click += new System.EventHandler(this.button14_Click);
            // 
            // txtServerIP
            // 
            this.txtServerIP.Location = new System.Drawing.Point(89, 113);
            this.txtServerIP.Name = "txtServerIP";
            this.txtServerIP.Size = new System.Drawing.Size(133, 21);
            this.txtServerIP.TabIndex = 9;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(11, 116);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(65, 12);
            this.label14.TabIndex = 8;
            this.label14.Text = "Ntp Server";
            // 
            // txtTimeType
            // 
            this.txtTimeType.Location = new System.Drawing.Point(89, 86);
            this.txtTimeType.Name = "txtTimeType";
            this.txtTimeType.Size = new System.Drawing.Size(133, 21);
            this.txtTimeType.TabIndex = 7;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(11, 89);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(53, 12);
            this.label13.TabIndex = 6;
            this.label13.Text = "Ntp Type";
            // 
            // txtTime
            // 
            this.txtTime.Location = new System.Drawing.Point(89, 59);
            this.txtTime.Name = "txtTime";
            this.txtTime.Size = new System.Drawing.Size(133, 21);
            this.txtTime.TabIndex = 5;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(11, 62);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(29, 12);
            this.label12.TabIndex = 4;
            this.label12.Text = "Time";
            // 
            // txtZoom
            // 
            this.txtZoom.Location = new System.Drawing.Point(89, 32);
            this.txtZoom.Name = "txtZoom";
            this.txtZoom.Size = new System.Drawing.Size(133, 21);
            this.txtZoom.TabIndex = 3;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(11, 35);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(59, 12);
            this.label11.TabIndex = 2;
            this.label11.Text = "Time Zone";
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.button17);
            this.tabPage4.Controls.Add(this.label17);
            this.tabPage4.Controls.Add(this.txtGetUserType);
            this.tabPage4.Controls.Add(this.label16);
            this.tabPage4.Controls.Add(this.button16);
            this.tabPage4.Controls.Add(this.txtGetUserID);
            this.tabPage4.Controls.Add(this.label15);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(885, 469);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "GetUserAndManager";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // button17
            // 
            this.button17.Location = new System.Drawing.Point(132, 93);
            this.button17.Name = "button17";
            this.button17.Size = new System.Drawing.Size(124, 20);
            this.button17.TabIndex = 16;
            this.button17.Text = "GetAllUsersInfo";
            this.button17.UseVisualStyleBackColor = true;
            this.button17.Click += new System.EventHandler(this.button17_Click);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(130, 55);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(299, 12);
            this.label17.TabIndex = 15;
            this.label17.Text = "(0-Normal User 1-Normal Manager 2-Supper Manager)";
            // 
            // txtGetUserType
            // 
            this.txtGetUserType.Location = new System.Drawing.Point(73, 51);
            this.txtGetUserType.Name = "txtGetUserType";
            this.txtGetUserType.Size = new System.Drawing.Size(48, 21);
            this.txtGetUserType.TabIndex = 14;
            this.txtGetUserType.Text = "0";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(17, 55);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(59, 12);
            this.label16.TabIndex = 13;
            this.label16.Text = "User Type";
            // 
            // button16
            // 
            this.button16.Location = new System.Drawing.Point(212, 18);
            this.button16.Name = "button16";
            this.button16.Size = new System.Drawing.Size(139, 20);
            this.button16.TabIndex = 12;
            this.button16.Text = "GetSingleUserInfo";
            this.button16.UseVisualStyleBackColor = true;
            this.button16.Click += new System.EventHandler(this.button16_Click);
            // 
            // txtGetUserID
            // 
            this.txtGetUserID.Location = new System.Drawing.Point(73, 17);
            this.txtGetUserID.Name = "txtGetUserID";
            this.txtGetUserID.Size = new System.Drawing.Size(133, 21);
            this.txtGetUserID.TabIndex = 5;
            this.txtGetUserID.Text = "1";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(17, 21);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(47, 12);
            this.label15.TabIndex = 4;
            this.label15.Text = "User ID";
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.label18);
            this.tabPage5.Controls.Add(this.txtGetFeaUserType);
            this.tabPage5.Controls.Add(this.label19);
            this.tabPage5.Controls.Add(this.button19);
            this.tabPage5.Controls.Add(this.txtGetFeaUserID);
            this.tabPage5.Controls.Add(this.label20);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Size = new System.Drawing.Size(885, 469);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "GetFeature";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(125, 53);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(299, 12);
            this.label18.TabIndex = 22;
            this.label18.Text = "(0-Normal User 1-Normal Manager 2-Supper manager)";
            // 
            // txtGetFeaUserType
            // 
            this.txtGetFeaUserType.Location = new System.Drawing.Point(68, 49);
            this.txtGetFeaUserType.Name = "txtGetFeaUserType";
            this.txtGetFeaUserType.Size = new System.Drawing.Size(48, 21);
            this.txtGetFeaUserType.TabIndex = 21;
            this.txtGetFeaUserType.Text = "0";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(12, 53);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(59, 12);
            this.label19.TabIndex = 20;
            this.label19.Text = "User Type";
            // 
            // button19
            // 
            this.button19.Location = new System.Drawing.Point(207, 16);
            this.button19.Name = "button19";
            this.button19.Size = new System.Drawing.Size(139, 20);
            this.button19.TabIndex = 19;
            this.button19.Text = "GetSingleUserFeature";
            this.button19.UseVisualStyleBackColor = true;
            this.button19.Click += new System.EventHandler(this.button19_Click);
            // 
            // txtGetFeaUserID
            // 
            this.txtGetFeaUserID.Location = new System.Drawing.Point(68, 15);
            this.txtGetFeaUserID.Name = "txtGetFeaUserID";
            this.txtGetFeaUserID.Size = new System.Drawing.Size(133, 21);
            this.txtGetFeaUserID.TabIndex = 18;
            this.txtGetFeaUserID.Text = "1";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(12, 19);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(47, 12);
            this.label20.TabIndex = 17;
            this.label20.Text = "User ID";
            // 
            // tabPage15
            // 
            this.tabPage15.Controls.Add(this.button21);
            this.tabPage15.Controls.Add(this.button20);
            this.tabPage15.Controls.Add(this.button18);
            this.tabPage15.Controls.Add(this.chkSaveFile);
            this.tabPage15.Controls.Add(this.chkGetPhoto);
            this.tabPage15.Controls.Add(this.txtAccessEnd);
            this.tabPage15.Controls.Add(this.label23);
            this.tabPage15.Controls.Add(this.txtAccessStart);
            this.tabPage15.Controls.Add(this.label21);
            this.tabPage15.Controls.Add(this.txtAccessUserID);
            this.tabPage15.Controls.Add(this.label22);
            this.tabPage15.Location = new System.Drawing.Point(4, 22);
            this.tabPage15.Name = "tabPage15";
            this.tabPage15.Size = new System.Drawing.Size(885, 469);
            this.tabPage15.TabIndex = 14;
            this.tabPage15.Text = "GetRecord";
            this.tabPage15.UseVisualStyleBackColor = true;
            // 
            // button21
            // 
            this.button21.Location = new System.Drawing.Point(225, 82);
            this.button21.Name = "button21";
            this.button21.Size = new System.Drawing.Size(139, 20);
            this.button21.TabIndex = 32;
            this.button21.Text = "GetAllUsersFailAccess";
            this.button21.UseVisualStyleBackColor = true;
            this.button21.Click += new System.EventHandler(this.button21_Click);
            // 
            // button20
            // 
            this.button20.Location = new System.Drawing.Point(225, 47);
            this.button20.Name = "button20";
            this.button20.Size = new System.Drawing.Size(139, 20);
            this.button20.TabIndex = 31;
            this.button20.Text = "GetAllUsersAccess";
            this.button20.UseVisualStyleBackColor = true;
            this.button20.Click += new System.EventHandler(this.button20_Click);
            // 
            // button18
            // 
            this.button18.Location = new System.Drawing.Point(225, 13);
            this.button18.Name = "button18";
            this.button18.Size = new System.Drawing.Size(176, 20);
            this.button18.TabIndex = 30;
            this.button18.Text = "GetSingleUserSuccessAccess";
            this.button18.UseVisualStyleBackColor = true;
            this.button18.Click += new System.EventHandler(this.button18_Click);
            // 
            // chkSaveFile
            // 
            this.chkSaveFile.AutoSize = true;
            this.chkSaveFile.Location = new System.Drawing.Point(122, 122);
            this.chkSaveFile.Name = "chkSaveFile";
            this.chkSaveFile.Size = new System.Drawing.Size(84, 16);
            this.chkSaveFile.TabIndex = 29;
            this.chkSaveFile.Text = "Save File?";
            this.chkSaveFile.UseVisualStyleBackColor = true;
            // 
            // chkGetPhoto
            // 
            this.chkGetPhoto.AutoSize = true;
            this.chkGetPhoto.Location = new System.Drawing.Point(28, 122);
            this.chkGetPhoto.Name = "chkGetPhoto";
            this.chkGetPhoto.Size = new System.Drawing.Size(84, 16);
            this.chkGetPhoto.TabIndex = 28;
            this.chkGetPhoto.Text = "With Photo";
            this.chkGetPhoto.UseVisualStyleBackColor = true;
            // 
            // txtAccessEnd
            // 
            this.txtAccessEnd.Location = new System.Drawing.Point(77, 82);
            this.txtAccessEnd.Name = "txtAccessEnd";
            this.txtAccessEnd.Size = new System.Drawing.Size(133, 21);
            this.txtAccessEnd.TabIndex = 27;
            this.txtAccessEnd.Text = "2014-09-01 00:00:00";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(9, 86);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(53, 12);
            this.label23.TabIndex = 26;
            this.label23.Text = "End Time";
            // 
            // txtAccessStart
            // 
            this.txtAccessStart.Location = new System.Drawing.Point(77, 48);
            this.txtAccessStart.Name = "txtAccessStart";
            this.txtAccessStart.Size = new System.Drawing.Size(133, 21);
            this.txtAccessStart.TabIndex = 25;
            this.txtAccessStart.Text = "2000-01-01 00:00:00";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(9, 52);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(65, 12);
            this.label21.TabIndex = 24;
            this.label21.Text = "Start Time";
            // 
            // txtAccessUserID
            // 
            this.txtAccessUserID.Location = new System.Drawing.Point(77, 14);
            this.txtAccessUserID.Name = "txtAccessUserID";
            this.txtAccessUserID.Size = new System.Drawing.Size(133, 21);
            this.txtAccessUserID.TabIndex = 23;
            this.txtAccessUserID.Text = "1";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(9, 18);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(47, 12);
            this.label22.TabIndex = 22;
            this.label22.Text = "User ID";
            // 
            // tabPage16
            // 
            this.tabPage16.Controls.Add(this.label40);
            this.tabPage16.Controls.Add(this.label39);
            this.tabPage16.Controls.Add(this.label38);
            this.tabPage16.Controls.Add(this.button28);
            this.tabPage16.Controls.Add(this.button27);
            this.tabPage16.Controls.Add(this.button26);
            this.tabPage16.Controls.Add(this.button25);
            this.tabPage16.Controls.Add(this.button24);
            this.tabPage16.Controls.Add(this.button23);
            this.tabPage16.Controls.Add(this.button22);
            this.tabPage16.Controls.Add(this.txtSetUserFea);
            this.tabPage16.Controls.Add(this.label37);
            this.tabPage16.Controls.Add(this.txtSetPhoto);
            this.tabPage16.Controls.Add(this.label36);
            this.tabPage16.Controls.Add(this.txtSetPhotoType);
            this.tabPage16.Controls.Add(this.label35);
            this.tabPage16.Controls.Add(this.txtSetUserRegStatus);
            this.tabPage16.Controls.Add(this.label34);
            this.tabPage16.Controls.Add(this.txtSetUserStatus);
            this.tabPage16.Controls.Add(this.label33);
            this.tabPage16.Controls.Add(this.txtSetUserName);
            this.tabPage16.Controls.Add(this.label32);
            this.tabPage16.Controls.Add(this.label31);
            this.tabPage16.Controls.Add(this.label30);
            this.tabPage16.Controls.Add(this.txtSetUserVerify);
            this.tabPage16.Controls.Add(this.label29);
            this.tabPage16.Controls.Add(this.label28);
            this.tabPage16.Controls.Add(this.txtSetUserCardNo);
            this.tabPage16.Controls.Add(this.label27);
            this.tabPage16.Controls.Add(this.txtSetUserId);
            this.tabPage16.Controls.Add(this.label26);
            this.tabPage16.Controls.Add(this.label25);
            this.tabPage16.Controls.Add(this.txtSetUserType);
            this.tabPage16.Controls.Add(this.label24);
            this.tabPage16.Location = new System.Drawing.Point(4, 22);
            this.tabPage16.Name = "tabPage16";
            this.tabPage16.Size = new System.Drawing.Size(885, 469);
            this.tabPage16.TabIndex = 15;
            this.tabPage16.Text = "SetUserAndManager";
            this.tabPage16.UseVisualStyleBackColor = true;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(143, 242);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(71, 12);
            this.label40.TabIndex = 57;
            this.label40.Text = "0-bmp 1-jpg";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(143, 218);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(161, 12);
            this.label39.TabIndex = 56;
            this.label39.Text = "0-Not Regsiter, 1-Register";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(143, 192);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(161, 12);
            this.label38.TabIndex = 55;
            this.label38.Text = "0-Normal 1-Disable 2-Local";
            // 
            // button28
            // 
            this.button28.Location = new System.Drawing.Point(310, 240);
            this.button28.Name = "button28";
            this.button28.Size = new System.Drawing.Size(92, 20);
            this.button28.TabIndex = 54;
            this.button28.Text = "DelAllUsers";
            this.button28.UseVisualStyleBackColor = true;
            this.button28.Click += new System.EventHandler(this.button28_Click);
            // 
            // button27
            // 
            this.button27.Location = new System.Drawing.Point(310, 214);
            this.button27.Name = "button27";
            this.button27.Size = new System.Drawing.Size(92, 20);
            this.button27.TabIndex = 53;
            this.button27.Text = "DelSingleUser";
            this.button27.UseVisualStyleBackColor = true;
            this.button27.Click += new System.EventHandler(this.button27_Click);
            // 
            // button26
            // 
            this.button26.Location = new System.Drawing.Point(310, 187);
            this.button26.Name = "button26";
            this.button26.Size = new System.Drawing.Size(49, 20);
            this.button26.TabIndex = 52;
            this.button26.Text = "ModifyUser";
            this.button26.UseVisualStyleBackColor = true;
            this.button26.Click += new System.EventHandler(this.button26_Click);
            // 
            // button25
            // 
            this.button25.Location = new System.Drawing.Point(366, 159);
            this.button25.Name = "button25";
            this.button25.Size = new System.Drawing.Size(139, 20);
            this.button25.TabIndex = 51;
            this.button25.Text = "AddUserAndFeature";
            this.button25.UseVisualStyleBackColor = true;
            this.button25.Click += new System.EventHandler(this.button25_Click);
            // 
            // button24
            // 
            this.button24.Location = new System.Drawing.Point(311, 159);
            this.button24.Name = "button24";
            this.button24.Size = new System.Drawing.Size(49, 20);
            this.button24.TabIndex = 50;
            this.button24.Text = "AddUser";
            this.button24.UseVisualStyleBackColor = true;
            this.button24.Click += new System.EventHandler(this.button24_Click);
            // 
            // button23
            // 
            this.button23.Location = new System.Drawing.Point(336, 300);
            this.button23.Name = "button23";
            this.button23.Size = new System.Drawing.Size(31, 20);
            this.button23.TabIndex = 49;
            this.button23.Text = "...";
            this.button23.UseVisualStyleBackColor = true;
            this.button23.Click += new System.EventHandler(this.button23_Click);
            // 
            // button22
            // 
            this.button22.Location = new System.Drawing.Point(336, 271);
            this.button22.Name = "button22";
            this.button22.Size = new System.Drawing.Size(31, 20);
            this.button22.TabIndex = 48;
            this.button22.Text = "...";
            this.button22.UseVisualStyleBackColor = true;
            this.button22.Click += new System.EventHandler(this.button22_Click);
            // 
            // txtSetUserFea
            // 
            this.txtSetUserFea.Location = new System.Drawing.Point(71, 299);
            this.txtSetUserFea.Name = "txtSetUserFea";
            this.txtSetUserFea.ReadOnly = true;
            this.txtSetUserFea.Size = new System.Drawing.Size(259, 21);
            this.txtSetUserFea.TabIndex = 47;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(15, 303);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(47, 12);
            this.label37.TabIndex = 46;
            this.label37.Text = "Feature";
            // 
            // txtSetPhoto
            // 
            this.txtSetPhoto.Location = new System.Drawing.Point(71, 272);
            this.txtSetPhoto.Name = "txtSetPhoto";
            this.txtSetPhoto.ReadOnly = true;
            this.txtSetPhoto.Size = new System.Drawing.Size(259, 21);
            this.txtSetPhoto.TabIndex = 45;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(15, 276);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(35, 12);
            this.label36.TabIndex = 44;
            this.label36.Text = "Photo";
            // 
            // txtSetPhotoType
            // 
            this.txtSetPhotoType.Location = new System.Drawing.Point(92, 240);
            this.txtSetPhotoType.Name = "txtSetPhotoType";
            this.txtSetPhotoType.Size = new System.Drawing.Size(43, 21);
            this.txtSetPhotoType.TabIndex = 43;
            this.txtSetPhotoType.Text = "1";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(15, 244);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(65, 12);
            this.label35.TabIndex = 42;
            this.label35.Text = "Photo Type";
            // 
            // txtSetUserRegStatus
            // 
            this.txtSetUserRegStatus.Location = new System.Drawing.Point(92, 213);
            this.txtSetUserRegStatus.Name = "txtSetUserRegStatus";
            this.txtSetUserRegStatus.ReadOnly = true;
            this.txtSetUserRegStatus.Size = new System.Drawing.Size(43, 21);
            this.txtSetUserRegStatus.TabIndex = 41;
            this.txtSetUserRegStatus.Text = "0";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(15, 217);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(59, 12);
            this.label34.TabIndex = 40;
            this.label34.Text = "RegStatus";
            // 
            // txtSetUserStatus
            // 
            this.txtSetUserStatus.Location = new System.Drawing.Point(92, 186);
            this.txtSetUserStatus.Name = "txtSetUserStatus";
            this.txtSetUserStatus.ReadOnly = true;
            this.txtSetUserStatus.Size = new System.Drawing.Size(43, 21);
            this.txtSetUserStatus.TabIndex = 39;
            this.txtSetUserStatus.Text = "0";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(15, 190);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(71, 12);
            this.label33.TabIndex = 38;
            this.label33.Text = "User Status";
            // 
            // txtSetUserName
            // 
            this.txtSetUserName.Location = new System.Drawing.Point(92, 159);
            this.txtSetUserName.Name = "txtSetUserName";
            this.txtSetUserName.Size = new System.Drawing.Size(108, 21);
            this.txtSetUserName.TabIndex = 37;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(15, 163);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(59, 12);
            this.label32.TabIndex = 36;
            this.label32.Text = "User Name";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(141, 135);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(275, 12);
            this.label31.TabIndex = 35;
            this.label31.Text = "(Manager:3-Face And Card 4-Face And Password)";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(141, 118);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(533, 12);
            this.label30.TabIndex = 34;
            this.label30.Text = "(Normal User:1-Face 2-Face Or Card 3-Face And Card 5-Face And ID 6-Card 7-Card an" +
                "d Face)";
            // 
            // txtSetUserVerify
            // 
            this.txtSetUserVerify.Location = new System.Drawing.Point(92, 118);
            this.txtSetUserVerify.Name = "txtSetUserVerify";
            this.txtSetUserVerify.Size = new System.Drawing.Size(43, 21);
            this.txtSetUserVerify.TabIndex = 33;
            this.txtSetUserVerify.Text = "1";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(15, 122);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(71, 12);
            this.label29.TabIndex = 32;
            this.label29.Text = "Verify Mode";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(206, 86);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(161, 12);
            this.label28.TabIndex = 31;
            this.label28.Text = "[00000~65535][00000~65535]";
            // 
            // txtSetUserCardNo
            // 
            this.txtSetUserCardNo.Location = new System.Drawing.Point(71, 82);
            this.txtSetUserCardNo.Name = "txtSetUserCardNo";
            this.txtSetUserCardNo.Size = new System.Drawing.Size(129, 21);
            this.txtSetUserCardNo.TabIndex = 30;
            this.txtSetUserCardNo.Text = "00000000";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(15, 86);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(47, 12);
            this.label27.TabIndex = 29;
            this.label27.Text = "Card NO";
            // 
            // txtSetUserId
            // 
            this.txtSetUserId.Location = new System.Drawing.Point(71, 51);
            this.txtSetUserId.Name = "txtSetUserId";
            this.txtSetUserId.Size = new System.Drawing.Size(64, 21);
            this.txtSetUserId.TabIndex = 28;
            this.txtSetUserId.Text = "1";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(15, 55);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(47, 12);
            this.label26.TabIndex = 27;
            this.label26.Text = "User ID";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(141, 22);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(299, 12);
            this.label25.TabIndex = 26;
            this.label25.Text = "(0-Normal User 1-Normal Manager 2-Supper Manager)";
            // 
            // txtSetUserType
            // 
            this.txtSetUserType.Location = new System.Drawing.Point(71, 18);
            this.txtSetUserType.Name = "txtSetUserType";
            this.txtSetUserType.Size = new System.Drawing.Size(64, 21);
            this.txtSetUserType.TabIndex = 25;
            this.txtSetUserType.Text = "0";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(15, 22);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(59, 12);
            this.label24.TabIndex = 24;
            this.label24.Text = "User Type";
            // 
            // tabPage17
            // 
            this.tabPage17.Controls.Add(this.button2);
            this.tabPage17.Controls.Add(this.button1);
            this.tabPage17.Controls.Add(this.txtSetFeaFile);
            this.tabPage17.Controls.Add(this.label41);
            this.tabPage17.Controls.Add(this.txtSetFeaUserID);
            this.tabPage17.Controls.Add(this.label42);
            this.tabPage17.Controls.Add(this.label43);
            this.tabPage17.Controls.Add(this.txtSetFeaUserType);
            this.tabPage17.Controls.Add(this.label44);
            this.tabPage17.Location = new System.Drawing.Point(4, 22);
            this.tabPage17.Name = "tabPage17";
            this.tabPage17.Size = new System.Drawing.Size(885, 469);
            this.tabPage17.TabIndex = 16;
            this.tabPage17.Text = "SetFeature";
            this.tabPage17.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(76, 121);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(139, 20);
            this.button2.TabIndex = 58;
            this.button2.Text = "ModifyUserFeature";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(341, 82);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(31, 20);
            this.button1.TabIndex = 57;
            this.button1.Text = "...";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // txtSetFeaFile
            // 
            this.txtSetFeaFile.Location = new System.Drawing.Point(76, 81);
            this.txtSetFeaFile.Name = "txtSetFeaFile";
            this.txtSetFeaFile.ReadOnly = true;
            this.txtSetFeaFile.Size = new System.Drawing.Size(259, 21);
            this.txtSetFeaFile.TabIndex = 56;
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(20, 85);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(47, 12);
            this.label41.TabIndex = 55;
            this.label41.Text = "Feature";
            // 
            // txtSetFeaUserID
            // 
            this.txtSetFeaUserID.Location = new System.Drawing.Point(77, 47);
            this.txtSetFeaUserID.Name = "txtSetFeaUserID";
            this.txtSetFeaUserID.Size = new System.Drawing.Size(64, 21);
            this.txtSetFeaUserID.TabIndex = 54;
            this.txtSetFeaUserID.Text = "1";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(21, 51);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(41, 12);
            this.label42.TabIndex = 53;
            this.label42.Text = "UserId";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(147, 18);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(299, 12);
            this.label43.TabIndex = 52;
            this.label43.Text = "(0-Normal User 1-Normal Manager 2-Supper Manager)";
            // 
            // txtSetFeaUserType
            // 
            this.txtSetFeaUserType.Location = new System.Drawing.Point(77, 14);
            this.txtSetFeaUserType.Name = "txtSetFeaUserType";
            this.txtSetFeaUserType.Size = new System.Drawing.Size(64, 21);
            this.txtSetFeaUserType.TabIndex = 51;
            this.txtSetFeaUserType.Text = "0";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(21, 18);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(53, 12);
            this.label44.TabIndex = 50;
            this.label44.Text = "UserType";
            // 
            // tabPage6
            // 
            this.tabPage6.Controls.Add(this.button30);
            this.tabPage6.Controls.Add(this.button29);
            this.tabPage6.Controls.Add(this.textBox1);
            this.tabPage6.Controls.Add(this.label45);
            this.tabPage6.Controls.Add(this.label46);
            this.tabPage6.Controls.Add(this.textBox2);
            this.tabPage6.Controls.Add(this.label47);
            this.tabPage6.Location = new System.Drawing.Point(4, 22);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Size = new System.Drawing.Size(885, 469);
            this.tabPage6.TabIndex = 5;
            this.tabPage6.Text = "Enroll";
            this.tabPage6.UseVisualStyleBackColor = true;
            // 
            // button30
            // 
            this.button30.Location = new System.Drawing.Point(43, 108);
            this.button30.Name = "button30";
            this.button30.Size = new System.Drawing.Size(169, 20);
            this.button30.TabIndex = 61;
            this.button30.Text = "EnrollUser";
            this.button30.UseVisualStyleBackColor = true;
            this.button30.Click += new System.EventHandler(this.button30_Click);
            // 
            // button29
            // 
            this.button29.Location = new System.Drawing.Point(43, 82);
            this.button29.Name = "button29";
            this.button29.Size = new System.Drawing.Size(169, 20);
            this.button29.TabIndex = 60;
            this.button29.Text = "GeneratePhotoAndFeature";
            this.button29.UseVisualStyleBackColor = true;
            this.button29.Click += new System.EventHandler(this.button29_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(67, 48);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(64, 21);
            this.textBox1.TabIndex = 59;
            this.textBox1.Text = "0";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(11, 52);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(41, 12);
            this.label45.TabIndex = 58;
            this.label45.Text = "UserId";
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(137, 19);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(299, 12);
            this.label46.TabIndex = 57;
            this.label46.Text = "(0-Normal User 1-Normal Manager 2-Supper Manager)";
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(67, 15);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(64, 21);
            this.textBox2.TabIndex = 56;
            this.textBox2.Text = "0";
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Location = new System.Drawing.Point(11, 19);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(53, 12);
            this.label47.TabIndex = 55;
            this.label47.Text = "UserType";
            // 
            // tabPage7
            // 
            this.tabPage7.Controls.Add(this.button33);
            this.tabPage7.Controls.Add(this.textBox4);
            this.tabPage7.Controls.Add(this.label49);
            this.tabPage7.Controls.Add(this.button32);
            this.tabPage7.Controls.Add(this.button31);
            this.tabPage7.Controls.Add(this.textBox3);
            this.tabPage7.Controls.Add(this.label48);
            this.tabPage7.Location = new System.Drawing.Point(4, 22);
            this.tabPage7.Name = "tabPage7";
            this.tabPage7.Size = new System.Drawing.Size(885, 469);
            this.tabPage7.TabIndex = 6;
            this.tabPage7.Text = "Verify";
            this.tabPage7.UseVisualStyleBackColor = true;
            // 
            // button33
            // 
            this.button33.Location = new System.Drawing.Point(138, 126);
            this.button33.Name = "button33";
            this.button33.Size = new System.Drawing.Size(102, 20);
            this.button33.TabIndex = 64;
            this.button33.Text = "VerifyUser";
            this.button33.UseVisualStyleBackColor = true;
            this.button33.Click += new System.EventHandler(this.button33_Click);
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(68, 125);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(64, 21);
            this.textBox4.TabIndex = 63;
            this.textBox4.Text = "0";
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Location = new System.Drawing.Point(13, 130);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(41, 12);
            this.label49.TabIndex = 62;
            this.label49.Text = "UserId";
            // 
            // button32
            // 
            this.button32.Location = new System.Drawing.Point(68, 52);
            this.button32.Name = "button32";
            this.button32.Size = new System.Drawing.Size(189, 20);
            this.button32.TabIndex = 61;
            this.button32.Text = "VerifyUserByDownloadFeature";
            this.button32.UseVisualStyleBackColor = true;
            this.button32.Click += new System.EventHandler(this.button32_Click);
            // 
            // button31
            // 
            this.button31.Location = new System.Drawing.Point(333, 19);
            this.button31.Name = "button31";
            this.button31.Size = new System.Drawing.Size(31, 20);
            this.button31.TabIndex = 60;
            this.button31.Text = "...";
            this.button31.UseVisualStyleBackColor = true;
            this.button31.Click += new System.EventHandler(this.button31_Click);
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(68, 18);
            this.textBox3.Name = "textBox3";
            this.textBox3.ReadOnly = true;
            this.textBox3.Size = new System.Drawing.Size(259, 21);
            this.textBox3.TabIndex = 59;
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Location = new System.Drawing.Point(12, 22);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(47, 12);
            this.label48.TabIndex = 58;
            this.label48.Text = "Feature";
            // 
            // tabPage8
            // 
            this.tabPage8.Controls.Add(this.button35);
            this.tabPage8.Controls.Add(this.button34);
            this.tabPage8.Controls.Add(this.textBox5);
            this.tabPage8.Controls.Add(this.label50);
            this.tabPage8.Location = new System.Drawing.Point(4, 22);
            this.tabPage8.Name = "tabPage8";
            this.tabPage8.Size = new System.Drawing.Size(885, 469);
            this.tabPage8.TabIndex = 17;
            this.tabPage8.Text = "Door";
            this.tabPage8.UseVisualStyleBackColor = true;
            // 
            // button35
            // 
            this.button35.Location = new System.Drawing.Point(239, 20);
            this.button35.Name = "button35";
            this.button35.Size = new System.Drawing.Size(90, 20);
            this.button35.TabIndex = 67;
            this.button35.Text = "GetDoorDelay";
            this.button35.UseVisualStyleBackColor = true;
            this.button35.Click += new System.EventHandler(this.button35_Click);
            // 
            // button34
            // 
            this.button34.Location = new System.Drawing.Point(141, 21);
            this.button34.Name = "button34";
            this.button34.Size = new System.Drawing.Size(92, 20);
            this.button34.TabIndex = 66;
            this.button34.Text = "SetDoorDelay";
            this.button34.UseVisualStyleBackColor = true;
            this.button34.Click += new System.EventHandler(this.button34_Click);
            // 
            // textBox5
            // 
            this.textBox5.Location = new System.Drawing.Point(87, 21);
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(48, 21);
            this.textBox5.TabIndex = 65;
            this.textBox5.Text = "0";
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Location = new System.Drawing.Point(16, 26);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(65, 12);
            this.label50.TabIndex = 64;
            this.label50.Text = "Door Delay";
            // 
            // tabPage9
            // 
            this.tabPage9.Controls.Add(this.textBox7);
            this.tabPage9.Controls.Add(this.label52);
            this.tabPage9.Controls.Add(this.button36);
            this.tabPage9.Controls.Add(this.button37);
            this.tabPage9.Controls.Add(this.textBox6);
            this.tabPage9.Controls.Add(this.label51);
            this.tabPage9.Location = new System.Drawing.Point(4, 22);
            this.tabPage9.Name = "tabPage9";
            this.tabPage9.Size = new System.Drawing.Size(885, 469);
            this.tabPage9.TabIndex = 18;
            this.tabPage9.Text = "SetRecord";
            this.tabPage9.UseVisualStyleBackColor = true;
            // 
            // textBox7
            // 
            this.textBox7.Location = new System.Drawing.Point(80, 57);
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new System.Drawing.Size(175, 21);
            this.textBox7.TabIndex = 73;
            this.textBox7.Text = "2012-08-29 00:00:00";
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Location = new System.Drawing.Point(25, 62);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(59, 12);
            this.label52.TabIndex = 72;
            this.label52.Text = "End Time:";
            // 
            // button36
            // 
            this.button36.Location = new System.Drawing.Point(275, 58);
            this.button36.Name = "button36";
            this.button36.Size = new System.Drawing.Size(164, 20);
            this.button36.TabIndex = 71;
            this.button36.Text = "DelAllUsersAccessRecord";
            this.button36.UseVisualStyleBackColor = true;
            this.button36.Click += new System.EventHandler(this.button36_Click);
            // 
            // button37
            // 
            this.button37.Location = new System.Drawing.Point(275, 24);
            this.button37.Name = "button37";
            this.button37.Size = new System.Drawing.Size(164, 20);
            this.button37.TabIndex = 70;
            this.button37.Text = "DelSingleUserAccessRecord";
            this.button37.UseVisualStyleBackColor = true;
            this.button37.Click += new System.EventHandler(this.button37_Click);
            // 
            // textBox6
            // 
            this.textBox6.Location = new System.Drawing.Point(80, 22);
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new System.Drawing.Size(64, 21);
            this.textBox6.TabIndex = 69;
            this.textBox6.Text = "0";
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Location = new System.Drawing.Point(25, 27);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(41, 12);
            this.label51.TabIndex = 68;
            this.label51.Text = "UserId";
            // 
            // tabPage10
            // 
            this.tabPage10.Controls.Add(this.groupBox9);
            this.tabPage10.Controls.Add(this.groupBox8);
            this.tabPage10.Controls.Add(this.groupBox7);
            this.tabPage10.Controls.Add(this.groupBox6);
            this.tabPage10.Controls.Add(this.groupBox5);
            this.tabPage10.Controls.Add(this.button43);
            this.tabPage10.Controls.Add(this.groupBox4);
            this.tabPage10.Controls.Add(this.groupBox3);
            this.tabPage10.Controls.Add(this.groupBox2);
            this.tabPage10.Location = new System.Drawing.Point(4, 22);
            this.tabPage10.Name = "tabPage10";
            this.tabPage10.Size = new System.Drawing.Size(885, 469);
            this.tabPage10.TabIndex = 19;
            this.tabPage10.Text = "System_1";
            this.tabPage10.UseVisualStyleBackColor = true;
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.button52);
            this.groupBox9.Controls.Add(this.button53);
            this.groupBox9.Controls.Add(this.textBox27);
            this.groupBox9.Controls.Add(this.label84);
            this.groupBox9.Controls.Add(this.textBox28);
            this.groupBox9.Controls.Add(this.label85);
            this.groupBox9.Controls.Add(this.textBox29);
            this.groupBox9.Controls.Add(this.label86);
            this.groupBox9.Controls.Add(this.textBox30);
            this.groupBox9.Controls.Add(this.label87);
            this.groupBox9.Controls.Add(this.textBox23);
            this.groupBox9.Controls.Add(this.label80);
            this.groupBox9.Controls.Add(this.textBox24);
            this.groupBox9.Controls.Add(this.label81);
            this.groupBox9.Controls.Add(this.textBox25);
            this.groupBox9.Controls.Add(this.label82);
            this.groupBox9.Controls.Add(this.textBox26);
            this.groupBox9.Controls.Add(this.label83);
            this.groupBox9.Controls.Add(this.textBox19);
            this.groupBox9.Controls.Add(this.label76);
            this.groupBox9.Controls.Add(this.textBox20);
            this.groupBox9.Controls.Add(this.label77);
            this.groupBox9.Controls.Add(this.textBox21);
            this.groupBox9.Controls.Add(this.label78);
            this.groupBox9.Controls.Add(this.textBox22);
            this.groupBox9.Controls.Add(this.label79);
            this.groupBox9.Controls.Add(this.textBox17);
            this.groupBox9.Controls.Add(this.label75);
            this.groupBox9.Controls.Add(this.textBox18);
            this.groupBox9.Controls.Add(this.label74);
            this.groupBox9.Controls.Add(this.textBox16);
            this.groupBox9.Controls.Add(this.label73);
            this.groupBox9.Controls.Add(this.textBox15);
            this.groupBox9.Controls.Add(this.label72);
            this.groupBox9.Location = new System.Drawing.Point(264, 298);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(290, 160);
            this.groupBox9.TabIndex = 19;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "ForbitCheckIn";
            // 
            // button52
            // 
            this.button52.Location = new System.Drawing.Point(138, 131);
            this.button52.Name = "button52";
            this.button52.Size = new System.Drawing.Size(60, 20);
            this.button52.TabIndex = 41;
            this.button52.Text = "Get";
            this.button52.UseVisualStyleBackColor = true;
            this.button52.Click += new System.EventHandler(this.button52_Click);
            // 
            // button53
            // 
            this.button53.Location = new System.Drawing.Point(61, 131);
            this.button53.Name = "button53";
            this.button53.Size = new System.Drawing.Size(60, 20);
            this.button53.TabIndex = 40;
            this.button53.Text = "Set";
            this.button53.UseVisualStyleBackColor = true;
            this.button53.Click += new System.EventHandler(this.button53_Click);
            // 
            // textBox27
            // 
            this.textBox27.Location = new System.Drawing.Point(212, 98);
            this.textBox27.Name = "textBox27";
            this.textBox27.Size = new System.Drawing.Size(31, 21);
            this.textBox27.TabIndex = 39;
            this.textBox27.Text = "0";
            // 
            // label84
            // 
            this.label84.AutoSize = true;
            this.label84.Location = new System.Drawing.Point(201, 103);
            this.label84.Name = "label84";
            this.label84.Size = new System.Drawing.Size(11, 12);
            this.label84.TabIndex = 38;
            this.label84.Text = ":";
            // 
            // textBox28
            // 
            this.textBox28.Location = new System.Drawing.Point(167, 98);
            this.textBox28.Name = "textBox28";
            this.textBox28.Size = new System.Drawing.Size(31, 21);
            this.textBox28.TabIndex = 37;
            this.textBox28.Text = "0";
            // 
            // label85
            // 
            this.label85.AutoSize = true;
            this.label85.Location = new System.Drawing.Point(148, 103);
            this.label85.Name = "label85";
            this.label85.Size = new System.Drawing.Size(11, 12);
            this.label85.TabIndex = 36;
            this.label85.Text = "-";
            // 
            // textBox29
            // 
            this.textBox29.Location = new System.Drawing.Point(111, 98);
            this.textBox29.Name = "textBox29";
            this.textBox29.Size = new System.Drawing.Size(31, 21);
            this.textBox29.TabIndex = 35;
            this.textBox29.Text = "0";
            // 
            // label86
            // 
            this.label86.AutoSize = true;
            this.label86.Location = new System.Drawing.Point(100, 103);
            this.label86.Name = "label86";
            this.label86.Size = new System.Drawing.Size(11, 12);
            this.label86.TabIndex = 34;
            this.label86.Text = ":";
            // 
            // textBox30
            // 
            this.textBox30.Location = new System.Drawing.Point(66, 98);
            this.textBox30.Name = "textBox30";
            this.textBox30.Size = new System.Drawing.Size(31, 21);
            this.textBox30.TabIndex = 33;
            this.textBox30.Text = "0";
            // 
            // label87
            // 
            this.label87.AutoSize = true;
            this.label87.Location = new System.Drawing.Point(10, 103);
            this.label87.Name = "label87";
            this.label87.Size = new System.Drawing.Size(59, 12);
            this.label87.TabIndex = 32;
            this.label87.Text = "StartTime";
            // 
            // textBox23
            // 
            this.textBox23.Location = new System.Drawing.Point(212, 74);
            this.textBox23.Name = "textBox23";
            this.textBox23.Size = new System.Drawing.Size(31, 21);
            this.textBox23.TabIndex = 31;
            this.textBox23.Text = "0";
            // 
            // label80
            // 
            this.label80.AutoSize = true;
            this.label80.Location = new System.Drawing.Point(201, 79);
            this.label80.Name = "label80";
            this.label80.Size = new System.Drawing.Size(11, 12);
            this.label80.TabIndex = 30;
            this.label80.Text = ":";
            // 
            // textBox24
            // 
            this.textBox24.Location = new System.Drawing.Point(167, 74);
            this.textBox24.Name = "textBox24";
            this.textBox24.Size = new System.Drawing.Size(31, 21);
            this.textBox24.TabIndex = 29;
            this.textBox24.Text = "0";
            // 
            // label81
            // 
            this.label81.AutoSize = true;
            this.label81.Location = new System.Drawing.Point(148, 79);
            this.label81.Name = "label81";
            this.label81.Size = new System.Drawing.Size(11, 12);
            this.label81.TabIndex = 28;
            this.label81.Text = "-";
            // 
            // textBox25
            // 
            this.textBox25.Location = new System.Drawing.Point(111, 74);
            this.textBox25.Name = "textBox25";
            this.textBox25.Size = new System.Drawing.Size(31, 21);
            this.textBox25.TabIndex = 27;
            this.textBox25.Text = "0";
            // 
            // label82
            // 
            this.label82.AutoSize = true;
            this.label82.Location = new System.Drawing.Point(100, 79);
            this.label82.Name = "label82";
            this.label82.Size = new System.Drawing.Size(11, 12);
            this.label82.TabIndex = 26;
            this.label82.Text = ":";
            // 
            // textBox26
            // 
            this.textBox26.Location = new System.Drawing.Point(66, 74);
            this.textBox26.Name = "textBox26";
            this.textBox26.Size = new System.Drawing.Size(31, 21);
            this.textBox26.TabIndex = 25;
            this.textBox26.Text = "0";
            // 
            // label83
            // 
            this.label83.AutoSize = true;
            this.label83.Location = new System.Drawing.Point(10, 79);
            this.label83.Name = "label83";
            this.label83.Size = new System.Drawing.Size(59, 12);
            this.label83.TabIndex = 24;
            this.label83.Text = "StartTime";
            // 
            // textBox19
            // 
            this.textBox19.Location = new System.Drawing.Point(212, 49);
            this.textBox19.Name = "textBox19";
            this.textBox19.Size = new System.Drawing.Size(31, 21);
            this.textBox19.TabIndex = 23;
            this.textBox19.Text = "0";
            // 
            // label76
            // 
            this.label76.AutoSize = true;
            this.label76.Location = new System.Drawing.Point(201, 54);
            this.label76.Name = "label76";
            this.label76.Size = new System.Drawing.Size(11, 12);
            this.label76.TabIndex = 22;
            this.label76.Text = ":";
            // 
            // textBox20
            // 
            this.textBox20.Location = new System.Drawing.Point(167, 49);
            this.textBox20.Name = "textBox20";
            this.textBox20.Size = new System.Drawing.Size(31, 21);
            this.textBox20.TabIndex = 21;
            this.textBox20.Text = "0";
            // 
            // label77
            // 
            this.label77.AutoSize = true;
            this.label77.Location = new System.Drawing.Point(148, 54);
            this.label77.Name = "label77";
            this.label77.Size = new System.Drawing.Size(11, 12);
            this.label77.TabIndex = 20;
            this.label77.Text = "-";
            // 
            // textBox21
            // 
            this.textBox21.Location = new System.Drawing.Point(111, 49);
            this.textBox21.Name = "textBox21";
            this.textBox21.Size = new System.Drawing.Size(31, 21);
            this.textBox21.TabIndex = 19;
            this.textBox21.Text = "0";
            // 
            // label78
            // 
            this.label78.AutoSize = true;
            this.label78.Location = new System.Drawing.Point(100, 54);
            this.label78.Name = "label78";
            this.label78.Size = new System.Drawing.Size(11, 12);
            this.label78.TabIndex = 18;
            this.label78.Text = ":";
            // 
            // textBox22
            // 
            this.textBox22.Location = new System.Drawing.Point(66, 49);
            this.textBox22.Name = "textBox22";
            this.textBox22.Size = new System.Drawing.Size(31, 21);
            this.textBox22.TabIndex = 17;
            this.textBox22.Text = "0";
            // 
            // label79
            // 
            this.label79.AutoSize = true;
            this.label79.Location = new System.Drawing.Point(10, 54);
            this.label79.Name = "label79";
            this.label79.Size = new System.Drawing.Size(59, 12);
            this.label79.TabIndex = 16;
            this.label79.Text = "StartTime";
            // 
            // textBox17
            // 
            this.textBox17.Location = new System.Drawing.Point(212, 25);
            this.textBox17.Name = "textBox17";
            this.textBox17.Size = new System.Drawing.Size(31, 21);
            this.textBox17.TabIndex = 15;
            this.textBox17.Text = "0";
            // 
            // label75
            // 
            this.label75.AutoSize = true;
            this.label75.Location = new System.Drawing.Point(201, 30);
            this.label75.Name = "label75";
            this.label75.Size = new System.Drawing.Size(11, 12);
            this.label75.TabIndex = 14;
            this.label75.Text = ":";
            // 
            // textBox18
            // 
            this.textBox18.Location = new System.Drawing.Point(167, 25);
            this.textBox18.Name = "textBox18";
            this.textBox18.Size = new System.Drawing.Size(31, 21);
            this.textBox18.TabIndex = 13;
            this.textBox18.Text = "0";
            // 
            // label74
            // 
            this.label74.AutoSize = true;
            this.label74.Location = new System.Drawing.Point(148, 30);
            this.label74.Name = "label74";
            this.label74.Size = new System.Drawing.Size(11, 12);
            this.label74.TabIndex = 12;
            this.label74.Text = "-";
            // 
            // textBox16
            // 
            this.textBox16.Location = new System.Drawing.Point(111, 25);
            this.textBox16.Name = "textBox16";
            this.textBox16.Size = new System.Drawing.Size(31, 21);
            this.textBox16.TabIndex = 11;
            this.textBox16.Text = "0";
            // 
            // label73
            // 
            this.label73.AutoSize = true;
            this.label73.Location = new System.Drawing.Point(100, 30);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(11, 12);
            this.label73.TabIndex = 10;
            this.label73.Text = ":";
            // 
            // textBox15
            // 
            this.textBox15.Location = new System.Drawing.Point(66, 25);
            this.textBox15.Name = "textBox15";
            this.textBox15.Size = new System.Drawing.Size(31, 21);
            this.textBox15.TabIndex = 9;
            this.textBox15.Text = "0";
            // 
            // label72
            // 
            this.label72.AutoSize = true;
            this.label72.Location = new System.Drawing.Point(10, 30);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(59, 12);
            this.label72.TabIndex = 0;
            this.label72.Text = "StartTime";
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.button50);
            this.groupBox8.Controls.Add(this.button51);
            this.groupBox8.Controls.Add(this.label71);
            this.groupBox8.Controls.Add(this.textBox14);
            this.groupBox8.Controls.Add(this.label70);
            this.groupBox8.Location = new System.Drawing.Point(263, 213);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(231, 70);
            this.groupBox8.TabIndex = 18;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Trap Switch";
            // 
            // button50
            // 
            this.button50.Location = new System.Drawing.Point(141, 38);
            this.button50.Name = "button50";
            this.button50.Size = new System.Drawing.Size(60, 20);
            this.button50.TabIndex = 18;
            this.button50.Text = "Get";
            this.button50.UseVisualStyleBackColor = true;
            this.button50.Click += new System.EventHandler(this.button50_Click);
            // 
            // button51
            // 
            this.button51.Location = new System.Drawing.Point(141, 12);
            this.button51.Name = "button51";
            this.button51.Size = new System.Drawing.Size(60, 20);
            this.button51.TabIndex = 17;
            this.button51.Text = "Set";
            this.button51.UseVisualStyleBackColor = true;
            this.button51.Click += new System.EventHandler(this.button51_Click);
            // 
            // label71
            // 
            this.label71.AutoSize = true;
            this.label71.Location = new System.Drawing.Point(17, 50);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(77, 12);
            this.label71.TabIndex = 10;
            this.label71.Text = "(0:Off 1:On)";
            // 
            // textBox14
            // 
            this.textBox14.Location = new System.Drawing.Point(59, 22);
            this.textBox14.Name = "textBox14";
            this.textBox14.Size = new System.Drawing.Size(38, 21);
            this.textBox14.TabIndex = 9;
            this.textBox14.Text = "1";
            // 
            // label70
            // 
            this.label70.AutoSize = true;
            this.label70.Location = new System.Drawing.Point(15, 28);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(41, 12);
            this.label70.TabIndex = 0;
            this.label70.Text = "Switch";
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.button48);
            this.groupBox7.Controls.Add(this.button49);
            this.groupBox7.Controls.Add(this.label69);
            this.groupBox7.Controls.Add(this.textBox13);
            this.groupBox7.Controls.Add(this.label68);
            this.groupBox7.Location = new System.Drawing.Point(262, 125);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(267, 85);
            this.groupBox7.TabIndex = 17;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "SyncMode";
            // 
            // button48
            // 
            this.button48.Location = new System.Drawing.Point(143, 45);
            this.button48.Name = "button48";
            this.button48.Size = new System.Drawing.Size(60, 20);
            this.button48.TabIndex = 16;
            this.button48.Text = "Get";
            this.button48.UseVisualStyleBackColor = true;
            this.button48.Click += new System.EventHandler(this.button48_Click);
            // 
            // button49
            // 
            this.button49.Location = new System.Drawing.Point(143, 19);
            this.button49.Name = "button49";
            this.button49.Size = new System.Drawing.Size(60, 20);
            this.button49.TabIndex = 15;
            this.button49.Text = "Set";
            this.button49.UseVisualStyleBackColor = true;
            this.button49.Click += new System.EventHandler(this.button49_Click);
            // 
            // label69
            // 
            this.label69.AutoSize = true;
            this.label69.Location = new System.Drawing.Point(21, 53);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(83, 12);
            this.label69.TabIndex = 9;
            this.label69.Text = "(0:Off  1:On)";
            // 
            // textBox13
            // 
            this.textBox13.Location = new System.Drawing.Point(69, 20);
            this.textBox13.Name = "textBox13";
            this.textBox13.Size = new System.Drawing.Size(38, 21);
            this.textBox13.TabIndex = 8;
            this.textBox13.Text = "0";
            // 
            // label68
            // 
            this.label68.AutoSize = true;
            this.label68.Location = new System.Drawing.Point(10, 28);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(53, 12);
            this.label68.TabIndex = 1;
            this.label68.Text = "SyncMode";
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.button47);
            this.groupBox6.Controls.Add(this.button46);
            this.groupBox6.Controls.Add(this.label67);
            this.groupBox6.Controls.Add(this.textBox12);
            this.groupBox6.Controls.Add(this.label66);
            this.groupBox6.Location = new System.Drawing.Point(15, 382);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(221, 84);
            this.groupBox6.TabIndex = 16;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Language";
            // 
            // button47
            // 
            this.button47.Location = new System.Drawing.Point(159, 24);
            this.button47.Name = "button47";
            this.button47.Size = new System.Drawing.Size(60, 20);
            this.button47.TabIndex = 17;
            this.button47.Text = "Get";
            this.button47.UseVisualStyleBackColor = true;
            this.button47.Click += new System.EventHandler(this.button47_Click);
            // 
            // button46
            // 
            this.button46.Location = new System.Drawing.Point(97, 25);
            this.button46.Name = "button46";
            this.button46.Size = new System.Drawing.Size(60, 20);
            this.button46.TabIndex = 16;
            this.button46.Text = "Set";
            this.button46.UseVisualStyleBackColor = true;
            this.button46.Click += new System.EventHandler(this.button46_Click);
            // 
            // label67
            // 
            this.label67.Location = new System.Drawing.Point(19, 55);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(196, 26);
            this.label67.TabIndex = 11;
            this.label67.Text = "(1:English 2:Simplified Chinese 3:Traditional Chinese 4:Korean) ";
            // 
            // textBox12
            // 
            this.textBox12.Location = new System.Drawing.Point(66, 25);
            this.textBox12.Name = "textBox12";
            this.textBox12.Size = new System.Drawing.Size(25, 21);
            this.textBox12.TabIndex = 10;
            this.textBox12.Text = "2";
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Location = new System.Drawing.Point(12, 28);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(53, 12);
            this.label66.TabIndex = 0;
            this.label66.Text = "Language";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.button45);
            this.groupBox5.Controls.Add(this.button44);
            this.groupBox5.Controls.Add(this.label65);
            this.groupBox5.Controls.Add(this.textBox11);
            this.groupBox5.Controls.Add(this.label64);
            this.groupBox5.Location = new System.Drawing.Point(14, 292);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(221, 85);
            this.groupBox5.TabIndex = 15;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Attend Interval";
            // 
            // button45
            // 
            this.button45.Location = new System.Drawing.Point(142, 46);
            this.button45.Name = "button45";
            this.button45.Size = new System.Drawing.Size(60, 20);
            this.button45.TabIndex = 16;
            this.button45.Text = "Get";
            this.button45.UseVisualStyleBackColor = true;
            this.button45.Click += new System.EventHandler(this.button45_Click);
            // 
            // button44
            // 
            this.button44.Location = new System.Drawing.Point(142, 20);
            this.button44.Name = "button44";
            this.button44.Size = new System.Drawing.Size(60, 20);
            this.button44.TabIndex = 15;
            this.button44.Text = "Set";
            this.button44.UseVisualStyleBackColor = true;
            this.button44.Click += new System.EventHandler(this.button44_Click);
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Location = new System.Drawing.Point(52, 62);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(59, 12);
            this.label65.TabIndex = 10;
            this.label65.Text = "(0-255 m)";
            // 
            // textBox11
            // 
            this.textBox11.Location = new System.Drawing.Point(98, 25);
            this.textBox11.Name = "textBox11";
            this.textBox11.Size = new System.Drawing.Size(38, 21);
            this.textBox11.TabIndex = 9;
            this.textBox11.Text = "0";
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.Location = new System.Drawing.Point(15, 28);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(53, 12);
            this.label64.TabIndex = 0;
            this.label64.Text = "Interval";
            // 
            // button43
            // 
            this.button43.Location = new System.Drawing.Point(152, 208);
            this.button43.Name = "button43";
            this.button43.Size = new System.Drawing.Size(60, 20);
            this.button43.TabIndex = 14;
            this.button43.Text = "Set";
            this.button43.UseVisualStyleBackColor = true;
            this.button43.Click += new System.EventHandler(this.button43_Click);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.button42);
            this.groupBox4.Controls.Add(this.label63);
            this.groupBox4.Controls.Add(this.textBox10);
            this.groupBox4.Controls.Add(this.label62);
            this.groupBox4.Location = new System.Drawing.Point(14, 189);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(221, 91);
            this.groupBox4.TabIndex = 2;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "LockShellAlarm";
            // 
            // button42
            // 
            this.button42.Location = new System.Drawing.Point(138, 52);
            this.button42.Name = "button42";
            this.button42.Size = new System.Drawing.Size(60, 20);
            this.button42.TabIndex = 15;
            this.button42.Text = "Get";
            this.button42.UseVisualStyleBackColor = true;
            this.button42.Click += new System.EventHandler(this.button42_Click);
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.Location = new System.Drawing.Point(59, 60);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(77, 12);
            this.label63.TabIndex = 9;
            this.label63.Text = "(0:Off 1:On)";
            // 
            // textBox10
            // 
            this.textBox10.Location = new System.Drawing.Point(93, 20);
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new System.Drawing.Size(38, 21);
            this.textBox10.TabIndex = 8;
            this.textBox10.Text = "1";
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Location = new System.Drawing.Point(14, 24);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(77, 12);
            this.label62.TabIndex = 0;
            this.label62.Text = "Alarm Switch";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.button41);
            this.groupBox3.Controls.Add(this.button40);
            this.groupBox3.Controls.Add(this.label61);
            this.groupBox3.Controls.Add(this.textBox9);
            this.groupBox3.Controls.Add(this.label60);
            this.groupBox3.Controls.Add(this.label59);
            this.groupBox3.Controls.Add(this.textBox8);
            this.groupBox3.Controls.Add(this.label58);
            this.groupBox3.Controls.Add(this.radioButton2);
            this.groupBox3.Controls.Add(this.radioButton1);
            this.groupBox3.Controls.Add(this.label57);
            this.groupBox3.Location = new System.Drawing.Point(260, 10);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(385, 112);
            this.groupBox3.TabIndex = 1;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Wiegand";
            // 
            // button41
            // 
            this.button41.Location = new System.Drawing.Point(317, 48);
            this.button41.Name = "button41";
            this.button41.Size = new System.Drawing.Size(60, 20);
            this.button41.TabIndex = 14;
            this.button41.Text = "Get";
            this.button41.UseVisualStyleBackColor = true;
            this.button41.Click += new System.EventHandler(this.button41_Click);
            // 
            // button40
            // 
            this.button40.Location = new System.Drawing.Point(319, 20);
            this.button40.Name = "button40";
            this.button40.Size = new System.Drawing.Size(60, 20);
            this.button40.TabIndex = 13;
            this.button40.Text = "Set";
            this.button40.UseVisualStyleBackColor = true;
            this.button40.Click += new System.EventHandler(this.button40_Click);
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Location = new System.Drawing.Point(145, 76);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(287, 12);
            this.label61.TabIndex = 8;
            this.label61.Text = "(Wiegand26：001 ~ 254 Wiegand34: 00001 ~ 65534)";
            // 
            // textBox9
            // 
            this.textBox9.Location = new System.Drawing.Point(87, 73);
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new System.Drawing.Size(38, 21);
            this.textBox9.TabIndex = 7;
            this.textBox9.Text = "1";
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Location = new System.Drawing.Point(14, 73);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(53, 12);
            this.label60.TabIndex = 6;
            this.label60.Text = "ODD Code";
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Location = new System.Drawing.Point(174, 48);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(137, 12);
            this.label59.TabIndex = 5;
            this.label59.Text = "(0: UserId  1: CardNo)";
            // 
            // textBox8
            // 
            this.textBox8.Location = new System.Drawing.Point(122, 43);
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new System.Drawing.Size(38, 21);
            this.textBox8.TabIndex = 4;
            this.textBox8.Text = "0";
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Location = new System.Drawing.Point(14, 46);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(41, 12);
            this.label58.TabIndex = 3;
            this.label58.Text = "Output";
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Location = new System.Drawing.Point(175, 21);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(77, 16);
            this.radioButton2.TabIndex = 2;
            this.radioButton2.Text = "Wiegand34";
            this.radioButton2.UseVisualStyleBackColor = true;
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Checked = true;
            this.radioButton1.Location = new System.Drawing.Point(87, 21);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(77, 16);
            this.radioButton1.TabIndex = 1;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "Wiegand26";
            this.radioButton1.UseVisualStyleBackColor = true;
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Location = new System.Drawing.Point(14, 22);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(71, 12);
            this.label57.TabIndex = 0;
            this.label57.Text = "Output Mode";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.textBox91);
            this.groupBox2.Controls.Add(this.textBox90);
            this.groupBox2.Controls.Add(this.textBox89);
            this.groupBox2.Controls.Add(this.textBox88);
            this.groupBox2.Controls.Add(this.button39);
            this.groupBox2.Controls.Add(this.button38);
            this.groupBox2.Controls.Add(this.label56);
            this.groupBox2.Controls.Add(this.label55);
            this.groupBox2.Controls.Add(this.label54);
            this.groupBox2.Controls.Add(this.label53);
            this.groupBox2.Location = new System.Drawing.Point(14, 10);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(222, 164);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Network";
            // 
            // textBox91
            // 
            this.textBox91.Location = new System.Drawing.Point(73, 100);
            this.textBox91.Name = "textBox91";
            this.textBox91.Size = new System.Drawing.Size(136, 21);
            this.textBox91.TabIndex = 17;
            this.textBox91.Text = "192.168.3.1";
            // 
            // textBox90
            // 
            this.textBox90.Location = new System.Drawing.Point(73, 72);
            this.textBox90.Name = "textBox90";
            this.textBox90.Size = new System.Drawing.Size(136, 21);
            this.textBox90.TabIndex = 16;
            this.textBox90.Text = "192.168.3.1";
            // 
            // textBox89
            // 
            this.textBox89.Location = new System.Drawing.Point(73, 46);
            this.textBox89.Name = "textBox89";
            this.textBox89.Size = new System.Drawing.Size(136, 21);
            this.textBox89.TabIndex = 15;
            this.textBox89.Text = "255.255.255.0";
            // 
            // textBox88
            // 
            this.textBox88.Location = new System.Drawing.Point(73, 20);
            this.textBox88.Name = "textBox88";
            this.textBox88.Size = new System.Drawing.Size(136, 21);
            this.textBox88.TabIndex = 14;
            this.textBox88.Text = "192.168.3.13";
            // 
            // button39
            // 
            this.button39.Location = new System.Drawing.Point(123, 138);
            this.button39.Name = "button39";
            this.button39.Size = new System.Drawing.Size(60, 20);
            this.button39.TabIndex = 13;
            this.button39.Text = "GetNet";
            this.button39.UseVisualStyleBackColor = true;
            this.button39.Click += new System.EventHandler(this.button39_Click);
            // 
            // button38
            // 
            this.button38.Location = new System.Drawing.Point(57, 138);
            this.button38.Name = "button38";
            this.button38.Size = new System.Drawing.Size(60, 20);
            this.button38.TabIndex = 12;
            this.button38.Text = "SetNet";
            this.button38.UseVisualStyleBackColor = true;
            this.button38.Click += new System.EventHandler(this.button38_Click);
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Location = new System.Drawing.Point(5, 104);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(23, 12);
            this.label56.TabIndex = 6;
            this.label56.Text = "DNS";
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Location = new System.Drawing.Point(5, 77);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(47, 12);
            this.label55.TabIndex = 4;
            this.label55.Text = "Gateway";
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Location = new System.Drawing.Point(5, 50);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(29, 12);
            this.label54.TabIndex = 2;
            this.label54.Text = "Mask";
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Location = new System.Drawing.Point(5, 23);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(65, 12);
            this.label53.TabIndex = 0;
            this.label53.Text = "IP address";
            // 
            // tabPage11
            // 
            this.tabPage11.Controls.Add(this.groupBox20);
            this.tabPage11.Controls.Add(this.groupBox19);
            this.tabPage11.Controls.Add(this.groupBox18);
            this.tabPage11.Controls.Add(this.groupBox17);
            this.tabPage11.Controls.Add(this.groupBox16);
            this.tabPage11.Controls.Add(this.groupBox15);
            this.tabPage11.Controls.Add(this.groupBox14);
            this.tabPage11.Controls.Add(this.groupBox13);
            this.tabPage11.Controls.Add(this.groupBox12);
            this.tabPage11.Controls.Add(this.groupBox11);
            this.tabPage11.Controls.Add(this.groupBox10);
            this.tabPage11.Location = new System.Drawing.Point(4, 22);
            this.tabPage11.Name = "tabPage11";
            this.tabPage11.Size = new System.Drawing.Size(885, 469);
            this.tabPage11.TabIndex = 20;
            this.tabPage11.Text = "System_2";
            this.tabPage11.UseVisualStyleBackColor = true;
            // 
            // groupBox20
            // 
            this.groupBox20.Controls.Add(this.button71);
            this.groupBox20.Controls.Add(this.button72);
            this.groupBox20.Controls.Add(this.textBox58);
            this.groupBox20.Controls.Add(this.textBox57);
            this.groupBox20.Controls.Add(this.textBox56);
            this.groupBox20.Controls.Add(this.textBox55);
            this.groupBox20.Controls.Add(this.label116);
            this.groupBox20.Controls.Add(this.label115);
            this.groupBox20.Controls.Add(this.label114);
            this.groupBox20.Controls.Add(this.label113);
            this.groupBox20.Location = new System.Drawing.Point(463, 323);
            this.groupBox20.Name = "groupBox20";
            this.groupBox20.Size = new System.Drawing.Size(237, 128);
            this.groupBox20.TabIndex = 10;
            this.groupBox20.TabStop = false;
            this.groupBox20.Text = "Connect Device";
            // 
            // button71
            // 
            this.button71.Location = new System.Drawing.Point(145, 72);
            this.button71.Name = "button71";
            this.button71.Size = new System.Drawing.Size(80, 20);
            this.button71.TabIndex = 40;
            this.button71.Text = "DisConnect";
            this.button71.UseVisualStyleBackColor = true;
            this.button71.Click += new System.EventHandler(this.button71_Click);
            // 
            // button72
            // 
            this.button72.Location = new System.Drawing.Point(145, 45);
            this.button72.Name = "button72";
            this.button72.Size = new System.Drawing.Size(80, 20);
            this.button72.TabIndex = 39;
            this.button72.Text = "Connect";
            this.button72.UseVisualStyleBackColor = true;
            this.button72.Click += new System.EventHandler(this.button72_Click);
            // 
            // textBox58
            // 
            this.textBox58.Location = new System.Drawing.Point(47, 96);
            this.textBox58.Name = "textBox58";
            this.textBox58.PasswordChar = '*';
            this.textBox58.Size = new System.Drawing.Size(90, 21);
            this.textBox58.TabIndex = 8;
            this.textBox58.Text = "8";
            // 
            // textBox57
            // 
            this.textBox57.Location = new System.Drawing.Point(47, 68);
            this.textBox57.Name = "textBox57";
            this.textBox57.Size = new System.Drawing.Size(90, 21);
            this.textBox57.TabIndex = 7;
            this.textBox57.Text = "admin";
            // 
            // textBox56
            // 
            this.textBox56.Location = new System.Drawing.Point(47, 42);
            this.textBox56.Name = "textBox56";
            this.textBox56.Size = new System.Drawing.Size(90, 21);
            this.textBox56.TabIndex = 6;
            this.textBox56.Text = "30001";
            // 
            // textBox55
            // 
            this.textBox55.Location = new System.Drawing.Point(47, 17);
            this.textBox55.Name = "textBox55";
            this.textBox55.Size = new System.Drawing.Size(90, 21);
            this.textBox55.TabIndex = 5;
            this.textBox55.Text = "192.168.3.252";
            // 
            // label116
            // 
            this.label116.AutoSize = true;
            this.label116.Location = new System.Drawing.Point(6, 100);
            this.label116.Name = "label116";
            this.label116.Size = new System.Drawing.Size(23, 12);
            this.label116.TabIndex = 4;
            this.label116.Text = "pwd";
            // 
            // label115
            // 
            this.label115.AutoSize = true;
            this.label115.Location = new System.Drawing.Point(6, 72);
            this.label115.Name = "label115";
            this.label115.Size = new System.Drawing.Size(29, 12);
            this.label115.TabIndex = 3;
            this.label115.Text = "User";
            // 
            // label114
            // 
            this.label114.AutoSize = true;
            this.label114.Location = new System.Drawing.Point(6, 46);
            this.label114.Name = "label114";
            this.label114.Size = new System.Drawing.Size(29, 12);
            this.label114.TabIndex = 2;
            this.label114.Text = "Port";
            // 
            // label113
            // 
            this.label113.AutoSize = true;
            this.label113.Location = new System.Drawing.Point(6, 21);
            this.label113.Name = "label113";
            this.label113.Size = new System.Drawing.Size(17, 12);
            this.label113.TabIndex = 1;
            this.label113.Text = "IP";
            // 
            // groupBox19
            // 
            this.groupBox19.Controls.Add(this.button70);
            this.groupBox19.Location = new System.Drawing.Point(464, 257);
            this.groupBox19.Name = "groupBox19";
            this.groupBox19.Size = new System.Drawing.Size(218, 60);
            this.groupBox19.TabIndex = 9;
            this.groupBox19.TabStop = false;
            this.groupBox19.Text = "Storage Information";
            // 
            // button70
            // 
            this.button70.Location = new System.Drawing.Point(74, 20);
            this.button70.Name = "button70";
            this.button70.Size = new System.Drawing.Size(103, 20);
            this.button70.TabIndex = 39;
            this.button70.Text = "GetStorage";
            this.button70.UseVisualStyleBackColor = true;
            this.button70.Click += new System.EventHandler(this.button70_Click);
            // 
            // groupBox18
            // 
            this.groupBox18.Controls.Add(this.button68);
            this.groupBox18.Controls.Add(this.button69);
            this.groupBox18.Controls.Add(this.label112);
            this.groupBox18.Controls.Add(this.textBox54);
            this.groupBox18.Controls.Add(this.label111);
            this.groupBox18.Location = new System.Drawing.Point(465, 160);
            this.groupBox18.Name = "groupBox18";
            this.groupBox18.Size = new System.Drawing.Size(217, 89);
            this.groupBox18.TabIndex = 8;
            this.groupBox18.TabStop = false;
            this.groupBox18.Text = "HumanSense";
            // 
            // button68
            // 
            this.button68.Location = new System.Drawing.Point(146, 48);
            this.button68.Name = "button68";
            this.button68.Size = new System.Drawing.Size(49, 20);
            this.button68.TabIndex = 38;
            this.button68.Text = "Set";
            this.button68.UseVisualStyleBackColor = true;
            this.button68.Click += new System.EventHandler(this.button68_Click);
            // 
            // button69
            // 
            this.button69.Location = new System.Drawing.Point(146, 21);
            this.button69.Name = "button69";
            this.button69.Size = new System.Drawing.Size(49, 20);
            this.button69.TabIndex = 37;
            this.button69.Text = "Get";
            this.button69.UseVisualStyleBackColor = true;
            this.button69.Click += new System.EventHandler(this.button69_Click);
            // 
            // label112
            // 
            this.label112.AutoSize = true;
            this.label112.Location = new System.Drawing.Point(80, 53);
            this.label112.Name = "label112";
            this.label112.Size = new System.Drawing.Size(35, 12);
            this.label112.TabIndex = 35;
            this.label112.Text = "(1-6)";
            // 
            // textBox54
            // 
            this.textBox54.Location = new System.Drawing.Point(82, 27);
            this.textBox54.Name = "textBox54";
            this.textBox54.Size = new System.Drawing.Size(36, 21);
            this.textBox54.TabIndex = 34;
            this.textBox54.Text = "1";
            // 
            // label111
            // 
            this.label111.AutoSize = true;
            this.label111.Location = new System.Drawing.Point(14, 33);
            this.label111.Name = "label111";
            this.label111.Size = new System.Drawing.Size(101, 12);
            this.label111.TabIndex = 0;
            this.label111.Text = "HumanSense Level";
            // 
            // groupBox17
            // 
            this.groupBox17.Controls.Add(this.button67);
            this.groupBox17.Controls.Add(this.button66);
            this.groupBox17.Controls.Add(this.textBox53);
            this.groupBox17.Controls.Add(this.label110);
            this.groupBox17.Controls.Add(this.textBox52);
            this.groupBox17.Controls.Add(this.label109);
            this.groupBox17.Controls.Add(this.textBox51);
            this.groupBox17.Controls.Add(this.label108);
            this.groupBox17.Location = new System.Drawing.Point(463, 11);
            this.groupBox17.Name = "groupBox17";
            this.groupBox17.Size = new System.Drawing.Size(219, 143);
            this.groupBox17.TabIndex = 7;
            this.groupBox17.TabStop = false;
            this.groupBox17.Text = "Safe Config";
            // 
            // button67
            // 
            this.button67.Location = new System.Drawing.Point(102, 116);
            this.button67.Name = "button67";
            this.button67.Size = new System.Drawing.Size(49, 20);
            this.button67.TabIndex = 36;
            this.button67.Text = "Set";
            this.button67.UseVisualStyleBackColor = true;
            this.button67.Click += new System.EventHandler(this.button67_Click);
            // 
            // button66
            // 
            this.button66.Location = new System.Drawing.Point(30, 116);
            this.button66.Name = "button66";
            this.button66.Size = new System.Drawing.Size(49, 20);
            this.button66.TabIndex = 35;
            this.button66.Text = "Get";
            this.button66.UseVisualStyleBackColor = true;
            this.button66.Click += new System.EventHandler(this.button66_Click);
            // 
            // textBox53
            // 
            this.textBox53.Location = new System.Drawing.Point(94, 87);
            this.textBox53.Name = "textBox53";
            this.textBox53.PasswordChar = '*';
            this.textBox53.Size = new System.Drawing.Size(113, 21);
            this.textBox53.TabIndex = 5;
            this.textBox53.Text = "88888888";
            // 
            // label110
            // 
            this.label110.AutoSize = true;
            this.label110.Location = new System.Drawing.Point(12, 90);
            this.label110.Name = "label110";
            this.label110.Size = new System.Drawing.Size(77, 12);
            this.label110.TabIndex = 4;
            this.label110.Text = "OpenDoor Pwd";
            // 
            // textBox52
            // 
            this.textBox52.Location = new System.Drawing.Point(94, 57);
            this.textBox52.Name = "textBox52";
            this.textBox52.PasswordChar = '*';
            this.textBox52.Size = new System.Drawing.Size(113, 21);
            this.textBox52.TabIndex = 3;
            this.textBox52.Text = "88888888";
            // 
            // label109
            // 
            this.label109.AutoSize = true;
            this.label109.Location = new System.Drawing.Point(12, 60);
            this.label109.Name = "label109";
            this.label109.Size = new System.Drawing.Size(65, 12);
            this.label109.TabIndex = 2;
            this.label109.Text = "SDCard Pwd";
            // 
            // textBox51
            // 
            this.textBox51.Location = new System.Drawing.Point(94, 30);
            this.textBox51.Name = "textBox51";
            this.textBox51.PasswordChar = '*';
            this.textBox51.Size = new System.Drawing.Size(113, 21);
            this.textBox51.TabIndex = 1;
            this.textBox51.Text = "88888888";
            // 
            // label108
            // 
            this.label108.AutoSize = true;
            this.label108.Location = new System.Drawing.Point(12, 33);
            this.label108.Name = "label108";
            this.label108.Size = new System.Drawing.Size(47, 12);
            this.label108.TabIndex = 0;
            this.label108.Text = "WEB Pwd";
            // 
            // groupBox16
            // 
            this.groupBox16.Controls.Add(this.button65);
            this.groupBox16.Controls.Add(this.button64);
            this.groupBox16.Controls.Add(this.button63);
            this.groupBox16.Controls.Add(this.textBox50);
            this.groupBox16.Controls.Add(this.textBox49);
            this.groupBox16.Controls.Add(this.label107);
            this.groupBox16.Controls.Add(this.label106);
            this.groupBox16.Location = new System.Drawing.Point(16, 356);
            this.groupBox16.Name = "groupBox16";
            this.groupBox16.Size = new System.Drawing.Size(404, 88);
            this.groupBox16.TabIndex = 6;
            this.groupBox16.TabStop = false;
            this.groupBox16.Text = "DecryptFile";
            this.groupBox16.Visible = false;
            // 
            // button65
            // 
            this.button65.Location = new System.Drawing.Point(291, 35);
            this.button65.Name = "button65";
            this.button65.Size = new System.Drawing.Size(91, 20);
            this.button65.TabIndex = 22;
            this.button65.Text = "DecryptFile";
            this.button65.UseVisualStyleBackColor = true;
            // 
            // button64
            // 
            this.button64.Location = new System.Drawing.Point(249, 57);
            this.button64.Name = "button64";
            this.button64.Size = new System.Drawing.Size(26, 20);
            this.button64.TabIndex = 21;
            this.button64.Text = "...";
            this.button64.UseVisualStyleBackColor = true;
            // 
            // button63
            // 
            this.button63.Location = new System.Drawing.Point(249, 20);
            this.button63.Name = "button63";
            this.button63.Size = new System.Drawing.Size(26, 20);
            this.button63.TabIndex = 20;
            this.button63.Text = "...";
            this.button63.UseVisualStyleBackColor = true;
            // 
            // textBox50
            // 
            this.textBox50.Location = new System.Drawing.Point(82, 57);
            this.textBox50.Name = "textBox50";
            this.textBox50.ReadOnly = true;
            this.textBox50.Size = new System.Drawing.Size(161, 21);
            this.textBox50.TabIndex = 3;
            // 
            // textBox49
            // 
            this.textBox49.Location = new System.Drawing.Point(82, 18);
            this.textBox49.Name = "textBox49";
            this.textBox49.ReadOnly = true;
            this.textBox49.Size = new System.Drawing.Size(161, 21);
            this.textBox49.TabIndex = 2;
            // 
            // label107
            // 
            this.label107.AutoSize = true;
            this.label107.Location = new System.Drawing.Point(11, 60);
            this.label107.Name = "label107";
            this.label107.Size = new System.Drawing.Size(53, 12);
            this.label107.TabIndex = 1;
            this.label107.Text = "Des File";
            // 
            // label106
            // 
            this.label106.AutoSize = true;
            this.label106.Location = new System.Drawing.Point(11, 22);
            this.label106.Name = "label106";
            this.label106.Size = new System.Drawing.Size(71, 12);
            this.label106.TabIndex = 0;
            this.label106.Text = "Source File";
            // 
            // groupBox15
            // 
            this.groupBox15.Controls.Add(this.button61);
            this.groupBox15.Controls.Add(this.button62);
            this.groupBox15.Controls.Add(this.checkBox4);
            this.groupBox15.Controls.Add(this.checkBox3);
            this.groupBox15.Controls.Add(this.checkBox2);
            this.groupBox15.Controls.Add(this.checkBox1);
            this.groupBox15.Location = new System.Drawing.Point(208, 267);
            this.groupBox15.Name = "groupBox15";
            this.groupBox15.Size = new System.Drawing.Size(234, 83);
            this.groupBox15.TabIndex = 5;
            this.groupBox15.TabStop = false;
            this.groupBox15.Text = "Auto Sleep Switch";
            // 
            // button61
            // 
            this.button61.Location = new System.Drawing.Point(181, 51);
            this.button61.Name = "button61";
            this.button61.Size = new System.Drawing.Size(49, 20);
            this.button61.TabIndex = 37;
            this.button61.Text = "Set";
            this.button61.UseVisualStyleBackColor = true;
            this.button61.Click += new System.EventHandler(this.button61_Click);
            // 
            // button62
            // 
            this.button62.Location = new System.Drawing.Point(182, 16);
            this.button62.Name = "button62";
            this.button62.Size = new System.Drawing.Size(49, 20);
            this.button62.TabIndex = 36;
            this.button62.Text = "Get";
            this.button62.UseVisualStyleBackColor = true;
            this.button62.Click += new System.EventHandler(this.button62_Click);
            // 
            // checkBox4
            // 
            this.checkBox4.AutoSize = true;
            this.checkBox4.Location = new System.Drawing.Point(99, 52);
            this.checkBox4.Name = "checkBox4";
            this.checkBox4.Size = new System.Drawing.Size(60, 16);
            this.checkBox4.TabIndex = 3;
            this.checkBox4.Text = "Field4";
            this.checkBox4.UseVisualStyleBackColor = true;
            // 
            // checkBox3
            // 
            this.checkBox3.AutoSize = true;
            this.checkBox3.Location = new System.Drawing.Point(12, 52);
            this.checkBox3.Name = "checkBox3";
            this.checkBox3.Size = new System.Drawing.Size(60, 16);
            this.checkBox3.TabIndex = 2;
            this.checkBox3.Text = "Field3";
            this.checkBox3.UseVisualStyleBackColor = true;
            // 
            // checkBox2
            // 
            this.checkBox2.AutoSize = true;
            this.checkBox2.Location = new System.Drawing.Point(99, 20);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(60, 16);
            this.checkBox2.TabIndex = 1;
            this.checkBox2.Text = "Filed2";
            this.checkBox2.UseVisualStyleBackColor = true;
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(12, 20);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(60, 16);
            this.checkBox1.TabIndex = 0;
            this.checkBox1.Text = "Filed1";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // groupBox14
            // 
            this.groupBox14.Controls.Add(this.button60);
            this.groupBox14.Location = new System.Drawing.Point(16, 260);
            this.groupBox14.Name = "groupBox14";
            this.groupBox14.Size = new System.Drawing.Size(172, 57);
            this.groupBox14.TabIndex = 4;
            this.groupBox14.TabStop = false;
            this.groupBox14.Text = "ReBoot";
            // 
            // button60
            // 
            this.button60.Location = new System.Drawing.Point(56, 24);
            this.button60.Name = "button60";
            this.button60.Size = new System.Drawing.Size(93, 20);
            this.button60.TabIndex = 19;
            this.button60.Text = "Reboot";
            this.button60.UseVisualStyleBackColor = true;
            this.button60.Click += new System.EventHandler(this.button60_Click);
            // 
            // groupBox13
            // 
            this.groupBox13.Controls.Add(this.button59);
            this.groupBox13.Controls.Add(this.button58);
            this.groupBox13.Controls.Add(this.textBox47);
            this.groupBox13.Controls.Add(this.label104);
            this.groupBox13.Controls.Add(this.textBox48);
            this.groupBox13.Controls.Add(this.label105);
            this.groupBox13.Controls.Add(this.textBox45);
            this.groupBox13.Controls.Add(this.label102);
            this.groupBox13.Controls.Add(this.textBox46);
            this.groupBox13.Controls.Add(this.label103);
            this.groupBox13.Controls.Add(this.textBox43);
            this.groupBox13.Controls.Add(this.label100);
            this.groupBox13.Controls.Add(this.textBox44);
            this.groupBox13.Controls.Add(this.label101);
            this.groupBox13.Controls.Add(this.textBox41);
            this.groupBox13.Controls.Add(this.label98);
            this.groupBox13.Controls.Add(this.textBox42);
            this.groupBox13.Controls.Add(this.label99);
            this.groupBox13.Controls.Add(this.textBox39);
            this.groupBox13.Controls.Add(this.label96);
            this.groupBox13.Controls.Add(this.textBox40);
            this.groupBox13.Controls.Add(this.label97);
            this.groupBox13.Controls.Add(this.textBox37);
            this.groupBox13.Controls.Add(this.label94);
            this.groupBox13.Controls.Add(this.textBox38);
            this.groupBox13.Controls.Add(this.label95);
            this.groupBox13.Controls.Add(this.textBox35);
            this.groupBox13.Controls.Add(this.label92);
            this.groupBox13.Controls.Add(this.textBox36);
            this.groupBox13.Controls.Add(this.label93);
            this.groupBox13.Controls.Add(this.textBox34);
            this.groupBox13.Controls.Add(this.label91);
            this.groupBox13.Controls.Add(this.textBox33);
            this.groupBox13.Controls.Add(this.label90);
            this.groupBox13.Location = new System.Drawing.Point(209, 12);
            this.groupBox13.Name = "groupBox13";
            this.groupBox13.Size = new System.Drawing.Size(233, 241);
            this.groupBox13.TabIndex = 3;
            this.groupBox13.TabStop = false;
            this.groupBox13.Text = "Auto Sleep";
            // 
            // button59
            // 
            this.button59.Location = new System.Drawing.Point(177, 156);
            this.button59.Name = "button59";
            this.button59.Size = new System.Drawing.Size(49, 20);
            this.button59.TabIndex = 35;
            this.button59.Text = "Set";
            this.button59.UseVisualStyleBackColor = true;
            this.button59.Click += new System.EventHandler(this.button59_Click);
            // 
            // button58
            // 
            this.button58.Location = new System.Drawing.Point(178, 121);
            this.button58.Name = "button58";
            this.button58.Size = new System.Drawing.Size(49, 20);
            this.button58.TabIndex = 34;
            this.button58.Text = "Get";
            this.button58.UseVisualStyleBackColor = true;
            this.button58.Click += new System.EventHandler(this.button58_Click);
            // 
            // textBox47
            // 
            this.textBox47.Location = new System.Drawing.Point(130, 202);
            this.textBox47.Name = "textBox47";
            this.textBox47.Size = new System.Drawing.Size(36, 21);
            this.textBox47.TabIndex = 33;
            this.textBox47.Text = "0";
            // 
            // label104
            // 
            this.label104.AutoSize = true;
            this.label104.Location = new System.Drawing.Point(113, 206);
            this.label104.Name = "label104";
            this.label104.Size = new System.Drawing.Size(11, 12);
            this.label104.TabIndex = 32;
            this.label104.Text = ":";
            // 
            // textBox48
            // 
            this.textBox48.Location = new System.Drawing.Point(73, 201);
            this.textBox48.Name = "textBox48";
            this.textBox48.Size = new System.Drawing.Size(36, 21);
            this.textBox48.TabIndex = 31;
            this.textBox48.Text = "0";
            // 
            // label105
            // 
            this.label105.AutoSize = true;
            this.label105.Location = new System.Drawing.Point(8, 205);
            this.label105.Name = "label105";
            this.label105.Size = new System.Drawing.Size(53, 12);
            this.label105.TabIndex = 30;
            this.label105.Text = "End Time";
            // 
            // textBox45
            // 
            this.textBox45.Location = new System.Drawing.Point(130, 175);
            this.textBox45.Name = "textBox45";
            this.textBox45.Size = new System.Drawing.Size(36, 21);
            this.textBox45.TabIndex = 29;
            this.textBox45.Text = "0";
            // 
            // label102
            // 
            this.label102.AutoSize = true;
            this.label102.Location = new System.Drawing.Point(113, 179);
            this.label102.Name = "label102";
            this.label102.Size = new System.Drawing.Size(11, 12);
            this.label102.TabIndex = 28;
            this.label102.Text = ":";
            // 
            // textBox46
            // 
            this.textBox46.Location = new System.Drawing.Point(73, 174);
            this.textBox46.Name = "textBox46";
            this.textBox46.Size = new System.Drawing.Size(36, 21);
            this.textBox46.TabIndex = 27;
            this.textBox46.Text = "0";
            // 
            // label103
            // 
            this.label103.AutoSize = true;
            this.label103.Location = new System.Drawing.Point(8, 178);
            this.label103.Name = "label103";
            this.label103.Size = new System.Drawing.Size(65, 12);
            this.label103.TabIndex = 26;
            this.label103.Text = "Start Time";
            // 
            // textBox43
            // 
            this.textBox43.Location = new System.Drawing.Point(130, 148);
            this.textBox43.Name = "textBox43";
            this.textBox43.Size = new System.Drawing.Size(36, 21);
            this.textBox43.TabIndex = 25;
            this.textBox43.Text = "0";
            // 
            // label100
            // 
            this.label100.AutoSize = true;
            this.label100.Location = new System.Drawing.Point(113, 152);
            this.label100.Name = "label100";
            this.label100.Size = new System.Drawing.Size(11, 12);
            this.label100.TabIndex = 24;
            this.label100.Text = ":";
            // 
            // textBox44
            // 
            this.textBox44.Location = new System.Drawing.Point(73, 147);
            this.textBox44.Name = "textBox44";
            this.textBox44.Size = new System.Drawing.Size(36, 21);
            this.textBox44.TabIndex = 23;
            this.textBox44.Text = "0";
            // 
            // label101
            // 
            this.label101.AutoSize = true;
            this.label101.Location = new System.Drawing.Point(8, 151);
            this.label101.Name = "label101";
            this.label101.Size = new System.Drawing.Size(53, 12);
            this.label101.TabIndex = 22;
            this.label101.Text = "End Time";
            // 
            // textBox41
            // 
            this.textBox41.Location = new System.Drawing.Point(130, 122);
            this.textBox41.Name = "textBox41";
            this.textBox41.Size = new System.Drawing.Size(36, 21);
            this.textBox41.TabIndex = 21;
            this.textBox41.Text = "0";
            // 
            // label98
            // 
            this.label98.AutoSize = true;
            this.label98.Location = new System.Drawing.Point(113, 126);
            this.label98.Name = "label98";
            this.label98.Size = new System.Drawing.Size(11, 12);
            this.label98.TabIndex = 20;
            this.label98.Text = ":";
            // 
            // textBox42
            // 
            this.textBox42.Location = new System.Drawing.Point(73, 121);
            this.textBox42.Name = "textBox42";
            this.textBox42.Size = new System.Drawing.Size(36, 21);
            this.textBox42.TabIndex = 19;
            this.textBox42.Text = "0";
            // 
            // label99
            // 
            this.label99.AutoSize = true;
            this.label99.Location = new System.Drawing.Point(8, 125);
            this.label99.Name = "label99";
            this.label99.Size = new System.Drawing.Size(65, 12);
            this.label99.TabIndex = 18;
            this.label99.Text = "Start Time";
            // 
            // textBox39
            // 
            this.textBox39.Location = new System.Drawing.Point(130, 97);
            this.textBox39.Name = "textBox39";
            this.textBox39.Size = new System.Drawing.Size(36, 21);
            this.textBox39.TabIndex = 17;
            this.textBox39.Text = "0";
            // 
            // label96
            // 
            this.label96.AutoSize = true;
            this.label96.Location = new System.Drawing.Point(113, 101);
            this.label96.Name = "label96";
            this.label96.Size = new System.Drawing.Size(11, 12);
            this.label96.TabIndex = 16;
            this.label96.Text = ":";
            // 
            // textBox40
            // 
            this.textBox40.Location = new System.Drawing.Point(73, 96);
            this.textBox40.Name = "textBox40";
            this.textBox40.Size = new System.Drawing.Size(36, 21);
            this.textBox40.TabIndex = 15;
            this.textBox40.Text = "0";
            // 
            // label97
            // 
            this.label97.AutoSize = true;
            this.label97.Location = new System.Drawing.Point(8, 100);
            this.label97.Name = "label97";
            this.label97.Size = new System.Drawing.Size(53, 12);
            this.label97.TabIndex = 14;
            this.label97.Text = "End Time";
            // 
            // textBox37
            // 
            this.textBox37.Location = new System.Drawing.Point(130, 71);
            this.textBox37.Name = "textBox37";
            this.textBox37.Size = new System.Drawing.Size(36, 21);
            this.textBox37.TabIndex = 13;
            this.textBox37.Text = "0";
            // 
            // label94
            // 
            this.label94.AutoSize = true;
            this.label94.Location = new System.Drawing.Point(113, 75);
            this.label94.Name = "label94";
            this.label94.Size = new System.Drawing.Size(11, 12);
            this.label94.TabIndex = 12;
            this.label94.Text = ":";
            // 
            // textBox38
            // 
            this.textBox38.Location = new System.Drawing.Point(73, 70);
            this.textBox38.Name = "textBox38";
            this.textBox38.Size = new System.Drawing.Size(36, 21);
            this.textBox38.TabIndex = 11;
            this.textBox38.Text = "0";
            // 
            // label95
            // 
            this.label95.AutoSize = true;
            this.label95.Location = new System.Drawing.Point(8, 74);
            this.label95.Name = "label95";
            this.label95.Size = new System.Drawing.Size(65, 12);
            this.label95.TabIndex = 10;
            this.label95.Text = "Start Time";
            // 
            // textBox35
            // 
            this.textBox35.Location = new System.Drawing.Point(130, 46);
            this.textBox35.Name = "textBox35";
            this.textBox35.Size = new System.Drawing.Size(36, 21);
            this.textBox35.TabIndex = 9;
            this.textBox35.Text = "0";
            // 
            // label92
            // 
            this.label92.AutoSize = true;
            this.label92.Location = new System.Drawing.Point(113, 50);
            this.label92.Name = "label92";
            this.label92.Size = new System.Drawing.Size(11, 12);
            this.label92.TabIndex = 8;
            this.label92.Text = ":";
            // 
            // textBox36
            // 
            this.textBox36.Location = new System.Drawing.Point(73, 45);
            this.textBox36.Name = "textBox36";
            this.textBox36.Size = new System.Drawing.Size(36, 21);
            this.textBox36.TabIndex = 7;
            this.textBox36.Text = "0";
            // 
            // label93
            // 
            this.label93.AutoSize = true;
            this.label93.Location = new System.Drawing.Point(8, 49);
            this.label93.Name = "label93";
            this.label93.Size = new System.Drawing.Size(53, 12);
            this.label93.TabIndex = 6;
            this.label93.Text = "End Time";
            // 
            // textBox34
            // 
            this.textBox34.Location = new System.Drawing.Point(130, 22);
            this.textBox34.Name = "textBox34";
            this.textBox34.Size = new System.Drawing.Size(36, 21);
            this.textBox34.TabIndex = 5;
            this.textBox34.Text = "0";
            // 
            // label91
            // 
            this.label91.AutoSize = true;
            this.label91.Location = new System.Drawing.Point(113, 26);
            this.label91.Name = "label91";
            this.label91.Size = new System.Drawing.Size(11, 12);
            this.label91.TabIndex = 4;
            this.label91.Text = ":";
            // 
            // textBox33
            // 
            this.textBox33.Location = new System.Drawing.Point(73, 21);
            this.textBox33.Name = "textBox33";
            this.textBox33.Size = new System.Drawing.Size(36, 21);
            this.textBox33.TabIndex = 3;
            this.textBox33.Text = "0";
            // 
            // label90
            // 
            this.label90.AutoSize = true;
            this.label90.Location = new System.Drawing.Point(8, 25);
            this.label90.Name = "label90";
            this.label90.Size = new System.Drawing.Size(65, 12);
            this.label90.TabIndex = 0;
            this.label90.Text = "Start Time";
            // 
            // groupBox12
            // 
            this.groupBox12.Controls.Add(this.button57);
            this.groupBox12.Location = new System.Drawing.Point(15, 197);
            this.groupBox12.Name = "groupBox12";
            this.groupBox12.Size = new System.Drawing.Size(172, 57);
            this.groupBox12.TabIndex = 2;
            this.groupBox12.TabStop = false;
            this.groupBox12.Text = "System ReNew";
            // 
            // button57
            // 
            this.button57.Location = new System.Drawing.Point(56, 24);
            this.button57.Name = "button57";
            this.button57.Size = new System.Drawing.Size(93, 20);
            this.button57.TabIndex = 19;
            this.button57.Text = "SystemReNew";
            this.button57.UseVisualStyleBackColor = true;
            this.button57.Click += new System.EventHandler(this.button57_Click);
            // 
            // groupBox11
            // 
            this.groupBox11.Controls.Add(this.button56);
            this.groupBox11.Location = new System.Drawing.Point(15, 131);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(172, 57);
            this.groupBox11.TabIndex = 1;
            this.groupBox11.TabStop = false;
            this.groupBox11.Text = "ScreenCalib";
            // 
            // button56
            // 
            this.button56.Location = new System.Drawing.Point(14, 27);
            this.button56.Name = "button56";
            this.button56.Size = new System.Drawing.Size(135, 20);
            this.button56.TabIndex = 19;
            this.button56.Text = "SetScreenCalib";
            this.button56.UseVisualStyleBackColor = true;
            this.button56.Click += new System.EventHandler(this.button56_Click);
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this.button54);
            this.groupBox10.Controls.Add(this.button55);
            this.groupBox10.Controls.Add(this.textBox32);
            this.groupBox10.Controls.Add(this.textBox31);
            this.groupBox10.Controls.Add(this.label89);
            this.groupBox10.Controls.Add(this.label88);
            this.groupBox10.Location = new System.Drawing.Point(12, 11);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(176, 118);
            this.groupBox10.TabIndex = 0;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "RecogThreshold";
            // 
            // button54
            // 
            this.button54.Location = new System.Drawing.Point(92, 84);
            this.button54.Name = "button54";
            this.button54.Size = new System.Drawing.Size(60, 20);
            this.button54.TabIndex = 18;
            this.button54.Text = "Get";
            this.button54.UseVisualStyleBackColor = true;
            this.button54.Click += new System.EventHandler(this.button54_Click);
            // 
            // button55
            // 
            this.button55.Location = new System.Drawing.Point(13, 84);
            this.button55.Name = "button55";
            this.button55.Size = new System.Drawing.Size(60, 20);
            this.button55.TabIndex = 17;
            this.button55.Text = "Set";
            this.button55.UseVisualStyleBackColor = true;
            this.button55.Click += new System.EventHandler(this.button55_Click);
            // 
            // textBox32
            // 
            this.textBox32.Location = new System.Drawing.Point(93, 48);
            this.textBox32.Name = "textBox32";
            this.textBox32.Size = new System.Drawing.Size(36, 21);
            this.textBox32.TabIndex = 3;
            this.textBox32.Text = "0";
            // 
            // textBox31
            // 
            this.textBox31.Location = new System.Drawing.Point(92, 23);
            this.textBox31.Name = "textBox31";
            this.textBox31.Size = new System.Drawing.Size(36, 21);
            this.textBox31.TabIndex = 2;
            this.textBox31.Text = "0";
            // 
            // label89
            // 
            this.label89.AutoSize = true;
            this.label89.Location = new System.Drawing.Point(11, 53);
            this.label89.Name = "label89";
            this.label89.Size = new System.Drawing.Size(71, 12);
            this.label89.TabIndex = 1;
            this.label89.Text = "UpdateValue";
            // 
            // label88
            // 
            this.label88.AutoSize = true;
            this.label88.Location = new System.Drawing.Point(11, 26);
            this.label88.Name = "label88";
            this.label88.Size = new System.Drawing.Size(83, 12);
            this.label88.TabIndex = 0;
            this.label88.Text = "IdentifyValue";
            // 
            // tabPage12
            // 
            this.tabPage12.Controls.Add(this.button81);
            this.tabPage12.Controls.Add(this.button82);
            this.tabPage12.Controls.Add(this.label161);
            this.tabPage12.Controls.Add(this.label160);
            this.tabPage12.Controls.Add(this.textBox86);
            this.tabPage12.Controls.Add(this.label159);
            this.tabPage12.Controls.Add(this.textBox85);
            this.tabPage12.Controls.Add(this.label158);
            this.tabPage12.Controls.Add(this.textBox84);
            this.tabPage12.Controls.Add(this.label157);
            this.tabPage12.Controls.Add(this.textBox83);
            this.tabPage12.Controls.Add(this.label156);
            this.tabPage12.Controls.Add(this.textBox82);
            this.tabPage12.Controls.Add(this.label155);
            this.tabPage12.Location = new System.Drawing.Point(4, 22);
            this.tabPage12.Name = "tabPage12";
            this.tabPage12.Size = new System.Drawing.Size(885, 469);
            this.tabPage12.TabIndex = 21;
            this.tabPage12.Text = "IO Config";
            this.tabPage12.UseVisualStyleBackColor = true;
            // 
            // button81
            // 
            this.button81.Location = new System.Drawing.Point(166, 178);
            this.button81.Name = "button81";
            this.button81.Size = new System.Drawing.Size(49, 20);
            this.button81.TabIndex = 42;
            this.button81.Text = "Set";
            this.button81.UseVisualStyleBackColor = true;
            this.button81.Click += new System.EventHandler(this.button81_Click);
            // 
            // button82
            // 
            this.button82.Location = new System.Drawing.Point(75, 178);
            this.button82.Name = "button82";
            this.button82.Size = new System.Drawing.Size(49, 20);
            this.button82.TabIndex = 41;
            this.button82.Text = "Get";
            this.button82.UseVisualStyleBackColor = true;
            this.button82.Click += new System.EventHandler(this.button82_Click);
            // 
            // label161
            // 
            this.label161.AutoSize = true;
            this.label161.Location = new System.Drawing.Point(221, 85);
            this.label161.Name = "label161";
            this.label161.Size = new System.Drawing.Size(77, 12);
            this.label161.TabIndex = 11;
            this.label161.Text = "(0:Off 1:On)";
            // 
            // label160
            // 
            this.label160.AutoSize = true;
            this.label160.Location = new System.Drawing.Point(223, 61);
            this.label160.Name = "label160";
            this.label160.Size = new System.Drawing.Size(47, 12);
            this.label160.TabIndex = 10;
            this.label160.Text = "(1-99)s";
            // 
            // textBox86
            // 
            this.textBox86.Location = new System.Drawing.Point(115, 136);
            this.textBox86.Name = "textBox86";
            this.textBox86.Size = new System.Drawing.Size(100, 21);
            this.textBox86.TabIndex = 9;
            this.textBox86.Text = "0";
            // 
            // label159
            // 
            this.label159.AutoSize = true;
            this.label159.Location = new System.Drawing.Point(23, 139);
            this.label159.Name = "label159";
            this.label159.Size = new System.Drawing.Size(71, 12);
            this.label159.TabIndex = 8;
            this.label159.Text = "AudioStatus";
            // 
            // textBox85
            // 
            this.textBox85.Location = new System.Drawing.Point(115, 109);
            this.textBox85.Name = "textBox85";
            this.textBox85.Size = new System.Drawing.Size(100, 21);
            this.textBox85.TabIndex = 7;
            this.textBox85.Text = "0";
            // 
            // label158
            // 
            this.label158.AutoSize = true;
            this.label158.Location = new System.Drawing.Point(23, 112);
            this.label158.Name = "label158";
            this.label158.Size = new System.Drawing.Size(77, 12);
            this.label158.TabIndex = 6;
            this.label158.Text = "SwitchStatus";
            // 
            // textBox84
            // 
            this.textBox84.Location = new System.Drawing.Point(115, 82);
            this.textBox84.Name = "textBox84";
            this.textBox84.Size = new System.Drawing.Size(100, 21);
            this.textBox84.TabIndex = 5;
            this.textBox84.Text = "0";
            // 
            // label157
            // 
            this.label157.AutoSize = true;
            this.label157.Location = new System.Drawing.Point(23, 85);
            this.label157.Name = "label157";
            this.label157.Size = new System.Drawing.Size(65, 12);
            this.label157.TabIndex = 4;
            this.label157.Text = "FireStatus";
            // 
            // textBox83
            // 
            this.textBox83.Location = new System.Drawing.Point(115, 52);
            this.textBox83.Name = "textBox83";
            this.textBox83.Size = new System.Drawing.Size(100, 21);
            this.textBox83.TabIndex = 3;
            this.textBox83.Text = "3";
            // 
            // label156
            // 
            this.label156.AutoSize = true;
            this.label156.Location = new System.Drawing.Point(23, 55);
            this.label156.Name = "label156";
            this.label156.Size = new System.Drawing.Size(59, 12);
            this.label156.TabIndex = 2;
            this.label156.Text = "GateDelay";
            // 
            // textBox82
            // 
            this.textBox82.Location = new System.Drawing.Point(115, 25);
            this.textBox82.Name = "textBox82";
            this.textBox82.Size = new System.Drawing.Size(100, 21);
            this.textBox82.TabIndex = 1;
            this.textBox82.Text = "0";
            // 
            // label155
            // 
            this.label155.AutoSize = true;
            this.label155.Location = new System.Drawing.Point(23, 28);
            this.label155.Name = "label155";
            this.label155.Size = new System.Drawing.Size(95, 12);
            this.label155.TabIndex = 0;
            this.label155.Text = "GateInputStatus";
            // 
            // tabPage13
            // 
            this.tabPage13.Controls.Add(this.groupBox23);
            this.tabPage13.Controls.Add(this.groupBox22);
            this.tabPage13.Controls.Add(this.groupBox21);
            this.tabPage13.Controls.Add(this.button73);
            this.tabPage13.Controls.Add(this.button74);
            this.tabPage13.Controls.Add(this.label118);
            this.tabPage13.Controls.Add(this.textBox59);
            this.tabPage13.Controls.Add(this.label117);
            this.tabPage13.Location = new System.Drawing.Point(4, 22);
            this.tabPage13.Name = "tabPage13";
            this.tabPage13.Size = new System.Drawing.Size(885, 469);
            this.tabPage13.TabIndex = 22;
            this.tabPage13.Text = "Sound Config";
            this.tabPage13.UseVisualStyleBackColor = true;
            // 
            // groupBox23
            // 
            this.groupBox23.Controls.Add(this.button79);
            this.groupBox23.Controls.Add(this.button80);
            this.groupBox23.Controls.Add(this.label154);
            this.groupBox23.Controls.Add(this.textBox81);
            this.groupBox23.Controls.Add(this.label153);
            this.groupBox23.Location = new System.Drawing.Point(397, 172);
            this.groupBox23.Name = "groupBox23";
            this.groupBox23.Size = new System.Drawing.Size(235, 94);
            this.groupBox23.TabIndex = 31;
            this.groupBox23.TabStop = false;
            this.groupBox23.Text = "Ring Duration";
            // 
            // button79
            // 
            this.button79.Location = new System.Drawing.Point(116, 61);
            this.button79.Name = "button79";
            this.button79.Size = new System.Drawing.Size(60, 20);
            this.button79.TabIndex = 24;
            this.button79.Text = "Get";
            this.button79.UseVisualStyleBackColor = true;
            this.button79.Click += new System.EventHandler(this.button79_Click);
            // 
            // button80
            // 
            this.button80.Location = new System.Drawing.Point(37, 61);
            this.button80.Name = "button80";
            this.button80.Size = new System.Drawing.Size(60, 20);
            this.button80.TabIndex = 23;
            this.button80.Text = "Set";
            this.button80.UseVisualStyleBackColor = true;
            this.button80.Click += new System.EventHandler(this.button80_Click);
            // 
            // label154
            // 
            this.label154.AutoSize = true;
            this.label154.Location = new System.Drawing.Point(139, 28);
            this.label154.Name = "label154";
            this.label154.Size = new System.Drawing.Size(41, 12);
            this.label154.TabIndex = 18;
            this.label154.Text = "(1-25)";
            // 
            // textBox81
            // 
            this.textBox81.Location = new System.Drawing.Point(98, 25);
            this.textBox81.Name = "textBox81";
            this.textBox81.Size = new System.Drawing.Size(35, 21);
            this.textBox81.TabIndex = 17;
            this.textBox81.Text = "1";
            // 
            // label153
            // 
            this.label153.AutoSize = true;
            this.label153.Location = new System.Drawing.Point(15, 28);
            this.label153.Name = "label153";
            this.label153.Size = new System.Drawing.Size(83, 12);
            this.label153.TabIndex = 0;
            this.label153.Text = "Ring Duration";
            // 
            // groupBox22
            // 
            this.groupBox22.Controls.Add(this.groupBox29);
            this.groupBox22.Controls.Add(this.groupBox28);
            this.groupBox22.Controls.Add(this.button77);
            this.groupBox22.Controls.Add(this.button78);
            this.groupBox22.Controls.Add(this.label152);
            this.groupBox22.Controls.Add(this.textBox80);
            this.groupBox22.Controls.Add(this.label151);
            this.groupBox22.Controls.Add(this.label150);
            this.groupBox22.Controls.Add(this.label149);
            this.groupBox22.Location = new System.Drawing.Point(394, 24);
            this.groupBox22.Name = "groupBox22";
            this.groupBox22.Size = new System.Drawing.Size(370, 139);
            this.groupBox22.TabIndex = 30;
            this.groupBox22.TabStop = false;
            this.groupBox22.Text = "Volume Setting";
            // 
            // groupBox29
            // 
            this.groupBox29.Controls.Add(this.radioButton5);
            this.groupBox29.Controls.Add(this.radioButton6);
            this.groupBox29.Location = new System.Drawing.Point(128, 47);
            this.groupBox29.Name = "groupBox29";
            this.groupBox29.Size = new System.Drawing.Size(102, 33);
            this.groupBox29.TabIndex = 24;
            this.groupBox29.TabStop = false;
            // 
            // radioButton5
            // 
            this.radioButton5.AutoSize = true;
            this.radioButton5.Location = new System.Drawing.Point(56, 12);
            this.radioButton5.Name = "radioButton5";
            this.radioButton5.Size = new System.Drawing.Size(41, 16);
            this.radioButton5.TabIndex = 7;
            this.radioButton5.TabStop = true;
            this.radioButton5.Text = "Off";
            this.radioButton5.UseVisualStyleBackColor = true;
            // 
            // radioButton6
            // 
            this.radioButton6.AutoSize = true;
            this.radioButton6.Location = new System.Drawing.Point(5, 12);
            this.radioButton6.Name = "radioButton6";
            this.radioButton6.Size = new System.Drawing.Size(35, 16);
            this.radioButton6.TabIndex = 6;
            this.radioButton6.TabStop = true;
            this.radioButton6.Text = "On";
            this.radioButton6.UseVisualStyleBackColor = true;
            // 
            // groupBox28
            // 
            this.groupBox28.Controls.Add(this.radioButton4);
            this.groupBox28.Controls.Add(this.radioButton3);
            this.groupBox28.Location = new System.Drawing.Point(128, 8);
            this.groupBox28.Name = "groupBox28";
            this.groupBox28.Size = new System.Drawing.Size(102, 33);
            this.groupBox28.TabIndex = 23;
            this.groupBox28.TabStop = false;
            // 
            // radioButton4
            // 
            this.radioButton4.AutoSize = true;
            this.radioButton4.Location = new System.Drawing.Point(55, 11);
            this.radioButton4.Name = "radioButton4";
            this.radioButton4.Size = new System.Drawing.Size(41, 16);
            this.radioButton4.TabIndex = 4;
            this.radioButton4.TabStop = true;
            this.radioButton4.Text = "Off";
            this.radioButton4.UseVisualStyleBackColor = true;
            // 
            // radioButton3
            // 
            this.radioButton3.AutoSize = true;
            this.radioButton3.Location = new System.Drawing.Point(10, 11);
            this.radioButton3.Name = "radioButton3";
            this.radioButton3.Size = new System.Drawing.Size(35, 16);
            this.radioButton3.TabIndex = 3;
            this.radioButton3.TabStop = true;
            this.radioButton3.Text = "On";
            this.radioButton3.UseVisualStyleBackColor = true;
            // 
            // button77
            // 
            this.button77.Location = new System.Drawing.Point(119, 113);
            this.button77.Name = "button77";
            this.button77.Size = new System.Drawing.Size(60, 20);
            this.button77.TabIndex = 22;
            this.button77.Text = "Get";
            this.button77.UseVisualStyleBackColor = true;
            this.button77.Click += new System.EventHandler(this.button77_Click);
            // 
            // button78
            // 
            this.button78.Location = new System.Drawing.Point(40, 113);
            this.button78.Name = "button78";
            this.button78.Size = new System.Drawing.Size(60, 20);
            this.button78.TabIndex = 21;
            this.button78.Text = "Set";
            this.button78.UseVisualStyleBackColor = true;
            this.button78.Click += new System.EventHandler(this.button78_Click);
            // 
            // label152
            // 
            this.label152.AutoSize = true;
            this.label152.Location = new System.Drawing.Point(168, 88);
            this.label152.Name = "label152";
            this.label152.Size = new System.Drawing.Size(35, 12);
            this.label152.TabIndex = 17;
            this.label152.Text = "(0-3)";
            // 
            // textBox80
            // 
            this.textBox80.Location = new System.Drawing.Point(127, 85);
            this.textBox80.Name = "textBox80";
            this.textBox80.Size = new System.Drawing.Size(35, 21);
            this.textBox80.TabIndex = 16;
            this.textBox80.Text = "2";
            // 
            // label151
            // 
            this.label151.AutoSize = true;
            this.label151.Location = new System.Drawing.Point(13, 88);
            this.label151.Name = "label151";
            this.label151.Size = new System.Drawing.Size(77, 12);
            this.label151.TabIndex = 6;
            this.label151.Text = "Volume Level";
            // 
            // label150
            // 
            this.label150.AutoSize = true;
            this.label150.Location = new System.Drawing.Point(13, 60);
            this.label150.Name = "label150";
            this.label150.Size = new System.Drawing.Size(83, 12);
            this.label150.TabIndex = 3;
            this.label150.Text = "Volume Switch";
            // 
            // label149
            // 
            this.label149.AutoSize = true;
            this.label149.Location = new System.Drawing.Point(13, 29);
            this.label149.Name = "label149";
            this.label149.Size = new System.Drawing.Size(107, 12);
            this.label149.TabIndex = 0;
            this.label149.Text = "Key Volume Switch";
            // 
            // groupBox21
            // 
            this.groupBox21.Controls.Add(this.button75);
            this.groupBox21.Controls.Add(this.button76);
            this.groupBox21.Controls.Add(this.label143);
            this.groupBox21.Controls.Add(this.label144);
            this.groupBox21.Controls.Add(this.textBox76);
            this.groupBox21.Controls.Add(this.label145);
            this.groupBox21.Controls.Add(this.textBox77);
            this.groupBox21.Controls.Add(this.label146);
            this.groupBox21.Controls.Add(this.textBox78);
            this.groupBox21.Controls.Add(this.label147);
            this.groupBox21.Controls.Add(this.textBox79);
            this.groupBox21.Controls.Add(this.label148);
            this.groupBox21.Controls.Add(this.label137);
            this.groupBox21.Controls.Add(this.label138);
            this.groupBox21.Controls.Add(this.textBox72);
            this.groupBox21.Controls.Add(this.label139);
            this.groupBox21.Controls.Add(this.textBox73);
            this.groupBox21.Controls.Add(this.label140);
            this.groupBox21.Controls.Add(this.textBox74);
            this.groupBox21.Controls.Add(this.label141);
            this.groupBox21.Controls.Add(this.textBox75);
            this.groupBox21.Controls.Add(this.label142);
            this.groupBox21.Controls.Add(this.label131);
            this.groupBox21.Controls.Add(this.label132);
            this.groupBox21.Controls.Add(this.textBox68);
            this.groupBox21.Controls.Add(this.label133);
            this.groupBox21.Controls.Add(this.textBox69);
            this.groupBox21.Controls.Add(this.label134);
            this.groupBox21.Controls.Add(this.textBox70);
            this.groupBox21.Controls.Add(this.label135);
            this.groupBox21.Controls.Add(this.textBox71);
            this.groupBox21.Controls.Add(this.label136);
            this.groupBox21.Controls.Add(this.label125);
            this.groupBox21.Controls.Add(this.label126);
            this.groupBox21.Controls.Add(this.textBox64);
            this.groupBox21.Controls.Add(this.label127);
            this.groupBox21.Controls.Add(this.textBox65);
            this.groupBox21.Controls.Add(this.label128);
            this.groupBox21.Controls.Add(this.textBox66);
            this.groupBox21.Controls.Add(this.label129);
            this.groupBox21.Controls.Add(this.textBox67);
            this.groupBox21.Controls.Add(this.label130);
            this.groupBox21.Controls.Add(this.label124);
            this.groupBox21.Controls.Add(this.label123);
            this.groupBox21.Controls.Add(this.textBox63);
            this.groupBox21.Controls.Add(this.label122);
            this.groupBox21.Controls.Add(this.textBox62);
            this.groupBox21.Controls.Add(this.label121);
            this.groupBox21.Controls.Add(this.textBox61);
            this.groupBox21.Controls.Add(this.label120);
            this.groupBox21.Controls.Add(this.textBox60);
            this.groupBox21.Controls.Add(this.label119);
            this.groupBox21.Location = new System.Drawing.Point(22, 61);
            this.groupBox21.Name = "groupBox21";
            this.groupBox21.Size = new System.Drawing.Size(355, 317);
            this.groupBox21.TabIndex = 29;
            this.groupBox21.TabStop = false;
            this.groupBox21.Text = "Ring Setting";
            // 
            // button75
            // 
            this.button75.Location = new System.Drawing.Point(274, 255);
            this.button75.Name = "button75";
            this.button75.Size = new System.Drawing.Size(60, 20);
            this.button75.TabIndex = 53;
            this.button75.Text = "Get";
            this.button75.UseVisualStyleBackColor = true;
            this.button75.Click += new System.EventHandler(this.button75_Click);
            // 
            // button76
            // 
            this.button76.Location = new System.Drawing.Point(195, 255);
            this.button76.Name = "button76";
            this.button76.Size = new System.Drawing.Size(60, 20);
            this.button76.TabIndex = 52;
            this.button76.Text = "Set";
            this.button76.UseVisualStyleBackColor = true;
            this.button76.Click += new System.EventHandler(this.button76_Click);
            // 
            // label143
            // 
            this.label143.AutoSize = true;
            this.label143.Location = new System.Drawing.Point(106, 282);
            this.label143.Name = "label143";
            this.label143.Size = new System.Drawing.Size(77, 12);
            this.label143.TabIndex = 51;
            this.label143.Text = "(0:Off 1:On)";
            // 
            // label144
            // 
            this.label144.AutoSize = true;
            this.label144.Location = new System.Drawing.Point(106, 255);
            this.label144.Name = "label144";
            this.label144.Size = new System.Drawing.Size(35, 12);
            this.label144.TabIndex = 50;
            this.label144.Text = "(1-5)";
            // 
            // textBox76
            // 
            this.textBox76.Location = new System.Drawing.Point(65, 276);
            this.textBox76.Name = "textBox76";
            this.textBox76.Size = new System.Drawing.Size(35, 21);
            this.textBox76.TabIndex = 49;
            this.textBox76.Text = "0";
            // 
            // label145
            // 
            this.label145.AutoSize = true;
            this.label145.Location = new System.Drawing.Point(10, 282);
            this.label145.Name = "label145";
            this.label145.Size = new System.Drawing.Size(41, 12);
            this.label145.TabIndex = 48;
            this.label145.Text = "Switch";
            // 
            // textBox77
            // 
            this.textBox77.Location = new System.Drawing.Point(65, 249);
            this.textBox77.Name = "textBox77";
            this.textBox77.Size = new System.Drawing.Size(35, 21);
            this.textBox77.TabIndex = 47;
            this.textBox77.Text = "1";
            // 
            // label146
            // 
            this.label146.AutoSize = true;
            this.label146.Location = new System.Drawing.Point(10, 255);
            this.label146.Name = "label146";
            this.label146.Size = new System.Drawing.Size(65, 12);
            this.label146.TabIndex = 46;
            this.label146.Text = "SoundIndex";
            // 
            // textBox78
            // 
            this.textBox78.Location = new System.Drawing.Point(118, 222);
            this.textBox78.Name = "textBox78";
            this.textBox78.Size = new System.Drawing.Size(35, 21);
            this.textBox78.TabIndex = 45;
            this.textBox78.Text = "0";
            // 
            // label147
            // 
            this.label147.AutoSize = true;
            this.label147.Location = new System.Drawing.Point(106, 228);
            this.label147.Name = "label147";
            this.label147.Size = new System.Drawing.Size(11, 12);
            this.label147.TabIndex = 44;
            this.label147.Text = ":";
            // 
            // textBox79
            // 
            this.textBox79.Location = new System.Drawing.Point(65, 222);
            this.textBox79.Name = "textBox79";
            this.textBox79.Size = new System.Drawing.Size(35, 21);
            this.textBox79.TabIndex = 43;
            this.textBox79.Text = "0";
            // 
            // label148
            // 
            this.label148.AutoSize = true;
            this.label148.Location = new System.Drawing.Point(10, 228);
            this.label148.Name = "label148";
            this.label148.Size = new System.Drawing.Size(59, 12);
            this.label148.TabIndex = 42;
            this.label148.Text = "Ring time";
            // 
            // label137
            // 
            this.label137.AutoSize = true;
            this.label137.Location = new System.Drawing.Point(275, 183);
            this.label137.Name = "label137";
            this.label137.Size = new System.Drawing.Size(77, 12);
            this.label137.TabIndex = 41;
            this.label137.Text = "(0:Off 1:On)";
            // 
            // label138
            // 
            this.label138.AutoSize = true;
            this.label138.Location = new System.Drawing.Point(275, 156);
            this.label138.Name = "label138";
            this.label138.Size = new System.Drawing.Size(35, 12);
            this.label138.TabIndex = 40;
            this.label138.Text = "(1-5)";
            // 
            // textBox72
            // 
            this.textBox72.Location = new System.Drawing.Point(234, 177);
            this.textBox72.Name = "textBox72";
            this.textBox72.Size = new System.Drawing.Size(35, 21);
            this.textBox72.TabIndex = 39;
            this.textBox72.Text = "0";
            // 
            // label139
            // 
            this.label139.AutoSize = true;
            this.label139.Location = new System.Drawing.Point(179, 183);
            this.label139.Name = "label139";
            this.label139.Size = new System.Drawing.Size(41, 12);
            this.label139.TabIndex = 38;
            this.label139.Text = "Switch";
            // 
            // textBox73
            // 
            this.textBox73.Location = new System.Drawing.Point(234, 150);
            this.textBox73.Name = "textBox73";
            this.textBox73.Size = new System.Drawing.Size(35, 21);
            this.textBox73.TabIndex = 37;
            this.textBox73.Text = "1";
            // 
            // label140
            // 
            this.label140.AutoSize = true;
            this.label140.Location = new System.Drawing.Point(179, 156);
            this.label140.Name = "label140";
            this.label140.Size = new System.Drawing.Size(65, 12);
            this.label140.TabIndex = 36;
            this.label140.Text = "SoundIndex";
            // 
            // textBox74
            // 
            this.textBox74.Location = new System.Drawing.Point(287, 123);
            this.textBox74.Name = "textBox74";
            this.textBox74.Size = new System.Drawing.Size(35, 21);
            this.textBox74.TabIndex = 35;
            this.textBox74.Text = "0";
            // 
            // label141
            // 
            this.label141.AutoSize = true;
            this.label141.Location = new System.Drawing.Point(275, 129);
            this.label141.Name = "label141";
            this.label141.Size = new System.Drawing.Size(11, 12);
            this.label141.TabIndex = 34;
            this.label141.Text = ":";
            // 
            // textBox75
            // 
            this.textBox75.Location = new System.Drawing.Point(234, 123);
            this.textBox75.Name = "textBox75";
            this.textBox75.Size = new System.Drawing.Size(35, 21);
            this.textBox75.TabIndex = 33;
            this.textBox75.Text = "0";
            // 
            // label142
            // 
            this.label142.AutoSize = true;
            this.label142.Location = new System.Drawing.Point(179, 129);
            this.label142.Name = "label142";
            this.label142.Size = new System.Drawing.Size(59, 12);
            this.label142.TabIndex = 32;
            this.label142.Text = "Ring time";
            // 
            // label131
            // 
            this.label131.AutoSize = true;
            this.label131.Location = new System.Drawing.Point(106, 186);
            this.label131.Name = "label131";
            this.label131.Size = new System.Drawing.Size(77, 12);
            this.label131.TabIndex = 31;
            this.label131.Text = "(0:Off 1:On)";
            // 
            // label132
            // 
            this.label132.AutoSize = true;
            this.label132.Location = new System.Drawing.Point(106, 159);
            this.label132.Name = "label132";
            this.label132.Size = new System.Drawing.Size(35, 12);
            this.label132.TabIndex = 30;
            this.label132.Text = "(1-5)";
            // 
            // textBox68
            // 
            this.textBox68.Location = new System.Drawing.Point(65, 180);
            this.textBox68.Name = "textBox68";
            this.textBox68.Size = new System.Drawing.Size(35, 21);
            this.textBox68.TabIndex = 29;
            this.textBox68.Text = "0";
            // 
            // label133
            // 
            this.label133.AutoSize = true;
            this.label133.Location = new System.Drawing.Point(10, 186);
            this.label133.Name = "label133";
            this.label133.Size = new System.Drawing.Size(41, 12);
            this.label133.TabIndex = 28;
            this.label133.Text = "Switch";
            // 
            // textBox69
            // 
            this.textBox69.Location = new System.Drawing.Point(65, 153);
            this.textBox69.Name = "textBox69";
            this.textBox69.Size = new System.Drawing.Size(35, 21);
            this.textBox69.TabIndex = 27;
            this.textBox69.Text = "1";
            // 
            // label134
            // 
            this.label134.AutoSize = true;
            this.label134.Location = new System.Drawing.Point(10, 159);
            this.label134.Name = "label134";
            this.label134.Size = new System.Drawing.Size(65, 12);
            this.label134.TabIndex = 26;
            this.label134.Text = "SoundIndex";
            // 
            // textBox70
            // 
            this.textBox70.Location = new System.Drawing.Point(118, 126);
            this.textBox70.Name = "textBox70";
            this.textBox70.Size = new System.Drawing.Size(35, 21);
            this.textBox70.TabIndex = 25;
            this.textBox70.Text = "0";
            // 
            // label135
            // 
            this.label135.AutoSize = true;
            this.label135.Location = new System.Drawing.Point(106, 132);
            this.label135.Name = "label135";
            this.label135.Size = new System.Drawing.Size(11, 12);
            this.label135.TabIndex = 24;
            this.label135.Text = ":";
            // 
            // textBox71
            // 
            this.textBox71.Location = new System.Drawing.Point(65, 126);
            this.textBox71.Name = "textBox71";
            this.textBox71.Size = new System.Drawing.Size(35, 21);
            this.textBox71.TabIndex = 23;
            this.textBox71.Text = "0";
            // 
            // label136
            // 
            this.label136.AutoSize = true;
            this.label136.Location = new System.Drawing.Point(10, 132);
            this.label136.Name = "label136";
            this.label136.Size = new System.Drawing.Size(59, 12);
            this.label136.TabIndex = 22;
            this.label136.Text = "Ring time";
            // 
            // label125
            // 
            this.label125.AutoSize = true;
            this.label125.Location = new System.Drawing.Point(275, 87);
            this.label125.Name = "label125";
            this.label125.Size = new System.Drawing.Size(77, 12);
            this.label125.TabIndex = 21;
            this.label125.Text = "(0:Off 1:On)";
            // 
            // label126
            // 
            this.label126.AutoSize = true;
            this.label126.Location = new System.Drawing.Point(275, 60);
            this.label126.Name = "label126";
            this.label126.Size = new System.Drawing.Size(35, 12);
            this.label126.TabIndex = 20;
            this.label126.Text = "(1-5)";
            // 
            // textBox64
            // 
            this.textBox64.Location = new System.Drawing.Point(234, 81);
            this.textBox64.Name = "textBox64";
            this.textBox64.Size = new System.Drawing.Size(35, 21);
            this.textBox64.TabIndex = 19;
            this.textBox64.Text = "0";
            // 
            // label127
            // 
            this.label127.AutoSize = true;
            this.label127.Location = new System.Drawing.Point(179, 87);
            this.label127.Name = "label127";
            this.label127.Size = new System.Drawing.Size(41, 12);
            this.label127.TabIndex = 18;
            this.label127.Text = "Switch";
            // 
            // textBox65
            // 
            this.textBox65.Location = new System.Drawing.Point(234, 54);
            this.textBox65.Name = "textBox65";
            this.textBox65.Size = new System.Drawing.Size(35, 21);
            this.textBox65.TabIndex = 17;
            this.textBox65.Text = "1";
            // 
            // label128
            // 
            this.label128.AutoSize = true;
            this.label128.Location = new System.Drawing.Point(179, 60);
            this.label128.Name = "label128";
            this.label128.Size = new System.Drawing.Size(65, 12);
            this.label128.TabIndex = 16;
            this.label128.Text = "SoundIndex";
            // 
            // textBox66
            // 
            this.textBox66.Location = new System.Drawing.Point(287, 27);
            this.textBox66.Name = "textBox66";
            this.textBox66.Size = new System.Drawing.Size(35, 21);
            this.textBox66.TabIndex = 15;
            this.textBox66.Text = "0";
            // 
            // label129
            // 
            this.label129.AutoSize = true;
            this.label129.Location = new System.Drawing.Point(275, 33);
            this.label129.Name = "label129";
            this.label129.Size = new System.Drawing.Size(11, 12);
            this.label129.TabIndex = 14;
            this.label129.Text = ":";
            // 
            // textBox67
            // 
            this.textBox67.Location = new System.Drawing.Point(234, 27);
            this.textBox67.Name = "textBox67";
            this.textBox67.Size = new System.Drawing.Size(35, 21);
            this.textBox67.TabIndex = 13;
            this.textBox67.Text = "0";
            // 
            // label130
            // 
            this.label130.AutoSize = true;
            this.label130.Location = new System.Drawing.Point(179, 33);
            this.label130.Name = "label130";
            this.label130.Size = new System.Drawing.Size(59, 12);
            this.label130.TabIndex = 12;
            this.label130.Text = "Ring time";
            // 
            // label124
            // 
            this.label124.AutoSize = true;
            this.label124.Location = new System.Drawing.Point(106, 87);
            this.label124.Name = "label124";
            this.label124.Size = new System.Drawing.Size(77, 12);
            this.label124.TabIndex = 11;
            this.label124.Text = "(0:Off 1:On)";
            // 
            // label123
            // 
            this.label123.AutoSize = true;
            this.label123.Location = new System.Drawing.Point(106, 60);
            this.label123.Name = "label123";
            this.label123.Size = new System.Drawing.Size(35, 12);
            this.label123.TabIndex = 10;
            this.label123.Text = "(1-5)";
            // 
            // textBox63
            // 
            this.textBox63.Location = new System.Drawing.Point(65, 81);
            this.textBox63.Name = "textBox63";
            this.textBox63.Size = new System.Drawing.Size(35, 21);
            this.textBox63.TabIndex = 9;
            this.textBox63.Text = "0";
            // 
            // label122
            // 
            this.label122.AutoSize = true;
            this.label122.Location = new System.Drawing.Point(10, 87);
            this.label122.Name = "label122";
            this.label122.Size = new System.Drawing.Size(41, 12);
            this.label122.TabIndex = 8;
            this.label122.Text = "Switch";
            // 
            // textBox62
            // 
            this.textBox62.Location = new System.Drawing.Point(65, 54);
            this.textBox62.Name = "textBox62";
            this.textBox62.Size = new System.Drawing.Size(35, 21);
            this.textBox62.TabIndex = 7;
            this.textBox62.Text = "1";
            // 
            // label121
            // 
            this.label121.AutoSize = true;
            this.label121.Location = new System.Drawing.Point(10, 60);
            this.label121.Name = "label121";
            this.label121.Size = new System.Drawing.Size(65, 12);
            this.label121.TabIndex = 6;
            this.label121.Text = "SoundIndex";
            // 
            // textBox61
            // 
            this.textBox61.Location = new System.Drawing.Point(118, 27);
            this.textBox61.Name = "textBox61";
            this.textBox61.Size = new System.Drawing.Size(35, 21);
            this.textBox61.TabIndex = 5;
            this.textBox61.Text = "0";
            // 
            // label120
            // 
            this.label120.AutoSize = true;
            this.label120.Location = new System.Drawing.Point(106, 33);
            this.label120.Name = "label120";
            this.label120.Size = new System.Drawing.Size(11, 12);
            this.label120.TabIndex = 4;
            this.label120.Text = ":";
            // 
            // textBox60
            // 
            this.textBox60.Location = new System.Drawing.Point(65, 27);
            this.textBox60.Name = "textBox60";
            this.textBox60.Size = new System.Drawing.Size(35, 21);
            this.textBox60.TabIndex = 3;
            this.textBox60.Text = "0";
            // 
            // label119
            // 
            this.label119.AutoSize = true;
            this.label119.Location = new System.Drawing.Point(10, 33);
            this.label119.Name = "label119";
            this.label119.Size = new System.Drawing.Size(59, 12);
            this.label119.TabIndex = 2;
            this.label119.Text = "Ring time";
            // 
            // button73
            // 
            this.button73.Location = new System.Drawing.Point(277, 20);
            this.button73.Name = "button73";
            this.button73.Size = new System.Drawing.Size(60, 20);
            this.button73.TabIndex = 28;
            this.button73.Text = "Get";
            this.button73.UseVisualStyleBackColor = true;
            this.button73.Click += new System.EventHandler(this.button73_Click);
            // 
            // button74
            // 
            this.button74.Location = new System.Drawing.Point(208, 20);
            this.button74.Name = "button74";
            this.button74.Size = new System.Drawing.Size(60, 20);
            this.button74.TabIndex = 27;
            this.button74.Text = "Set";
            this.button74.UseVisualStyleBackColor = true;
            this.button74.Click += new System.EventHandler(this.button74_Click);
            // 
            // label118
            // 
            this.label118.AutoSize = true;
            this.label118.Location = new System.Drawing.Point(123, 25);
            this.label118.Name = "label118";
            this.label118.Size = new System.Drawing.Size(77, 12);
            this.label118.TabIndex = 26;
            this.label118.Text = "(0:Off 1:On)";
            // 
            // textBox59
            // 
            this.textBox59.Location = new System.Drawing.Point(87, 19);
            this.textBox59.Name = "textBox59";
            this.textBox59.Size = new System.Drawing.Size(27, 21);
            this.textBox59.TabIndex = 25;
            this.textBox59.Text = "0";
            // 
            // label117
            // 
            this.label117.AutoSize = true;
            this.label117.Location = new System.Drawing.Point(18, 25);
            this.label117.Name = "label117";
            this.label117.Size = new System.Drawing.Size(65, 12);
            this.label117.TabIndex = 24;
            this.label117.Text = "BellStatus";
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.pictureBox1);
            this.panel3.Controls.Add(this.treeView1);
            this.panel3.Controls.Add(this.groupBox1);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(185, 495);
            this.panel3.TabIndex = 0;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox1.Location = new System.Drawing.Point(21, 288);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(113, 121);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 2;
            this.pictureBox1.TabStop = false;
            // 
            // treeView1
            // 
            this.treeView1.HideSelection = false;
            this.treeView1.Location = new System.Drawing.Point(5, 103);
            this.treeView1.Name = "treeView1";
            this.treeView1.Size = new System.Drawing.Size(176, 172);
            this.treeView1.TabIndex = 1;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.bu_Uninit);
            this.groupBox1.Controls.Add(this.bu_Init);
            this.groupBox1.Controls.Add(this.txtPort);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(4, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(178, 90);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Server Port";
            // 
            // bu_Uninit
            // 
            this.bu_Uninit.Enabled = false;
            this.bu_Uninit.Location = new System.Drawing.Point(96, 50);
            this.bu_Uninit.Name = "bu_Uninit";
            this.bu_Uninit.Size = new System.Drawing.Size(47, 22);
            this.bu_Uninit.TabIndex = 3;
            this.bu_Uninit.Text = "Stop";
            this.bu_Uninit.UseVisualStyleBackColor = true;
            this.bu_Uninit.Click += new System.EventHandler(this.bu_Uninit_Click);
            // 
            // bu_Init
            // 
            this.bu_Init.Location = new System.Drawing.Point(31, 50);
            this.bu_Init.Name = "bu_Init";
            this.bu_Init.Size = new System.Drawing.Size(47, 22);
            this.bu_Init.TabIndex = 2;
            this.bu_Init.Text = "Start";
            this.bu_Init.UseVisualStyleBackColor = true;
            this.bu_Init.Click += new System.EventHandler(this.bu_Init_Click);
            // 
            // txtPort
            // 
            this.txtPort.Location = new System.Drawing.Point(47, 17);
            this.txtPort.Name = "txtPort";
            this.txtPort.Size = new System.Drawing.Size(96, 21);
            this.txtPort.TabIndex = 1;
            this.txtPort.Text = "30000";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "Port";
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1078, 626);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Name = "frmMain";
            this.Text = "FaceServerTest";
            this.Load += new System.EventHandler(this.frmMain_Load);
            this.panel1.ResumeLayout(false);
            this.contextMenuStrip1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            this.tabPage5.ResumeLayout(false);
            this.tabPage5.PerformLayout();
            this.tabPage15.ResumeLayout(false);
            this.tabPage15.PerformLayout();
            this.tabPage16.ResumeLayout(false);
            this.tabPage16.PerformLayout();
            this.tabPage17.ResumeLayout(false);
            this.tabPage17.PerformLayout();
            this.tabPage6.ResumeLayout(false);
            this.tabPage6.PerformLayout();
            this.tabPage7.ResumeLayout(false);
            this.tabPage7.PerformLayout();
            this.tabPage8.ResumeLayout(false);
            this.tabPage8.PerformLayout();
            this.tabPage9.ResumeLayout(false);
            this.tabPage9.PerformLayout();
            this.tabPage10.ResumeLayout(false);
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.tabPage11.ResumeLayout(false);
            this.groupBox20.ResumeLayout(false);
            this.groupBox20.PerformLayout();
            this.groupBox19.ResumeLayout(false);
            this.groupBox18.ResumeLayout(false);
            this.groupBox18.PerformLayout();
            this.groupBox17.ResumeLayout(false);
            this.groupBox17.PerformLayout();
            this.groupBox16.ResumeLayout(false);
            this.groupBox16.PerformLayout();
            this.groupBox15.ResumeLayout(false);
            this.groupBox15.PerformLayout();
            this.groupBox14.ResumeLayout(false);
            this.groupBox13.ResumeLayout(false);
            this.groupBox13.PerformLayout();
            this.groupBox12.ResumeLayout(false);
            this.groupBox11.ResumeLayout(false);
            this.groupBox10.ResumeLayout(false);
            this.groupBox10.PerformLayout();
            this.tabPage12.ResumeLayout(false);
            this.tabPage12.PerformLayout();
            this.tabPage13.ResumeLayout(false);
            this.tabPage13.PerformLayout();
            this.groupBox23.ResumeLayout(false);
            this.groupBox23.PerformLayout();
            this.groupBox22.ResumeLayout(false);
            this.groupBox22.PerformLayout();
            this.groupBox29.ResumeLayout(false);
            this.groupBox29.PerformLayout();
            this.groupBox28.ResumeLayout(false);
            this.groupBox28.PerformLayout();
            this.groupBox21.ResumeLayout(false);
            this.groupBox21.PerformLayout();
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtPort;
        private System.Windows.Forms.Button bu_Uninit;
        private System.Windows.Forms.Button bu_Init;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.TreeView treeView1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.TabPage tabPage6;
        private System.Windows.Forms.TabPage tabPage7;
        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.ColumnHeader colNo;
        private System.Windows.Forms.ColumnHeader colTime;
        private System.Windows.Forms.ColumnHeader colDev;
        private System.Windows.Forms.ColumnHeader colStatus;
        private System.Windows.Forms.ColumnHeader colDetail;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.TextBox txtSdkVer;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.TextBox txtDvsSn;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtSourceCardNo;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.TextBox txtDesCardNo;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtProVer;
        private System.Windows.Forms.TextBox txtDvSn;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtDvsMode;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtDvsName;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtDvsType;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button13;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.TextBox txtServerIP;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtTimeType;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtTime;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtZoom;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button button15;
        private System.Windows.Forms.Button button14;
        private System.Windows.Forms.Button button16;
        private System.Windows.Forms.TextBox txtGetUserID;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txtGetUserType;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Button button17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox txtGetFeaUserType;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Button button19;
        private System.Windows.Forms.TextBox txtGetFeaUserID;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TabPage tabPage15;
        private System.Windows.Forms.TextBox txtAccessEnd;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox txtAccessStart;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox txtAccessUserID;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TabPage tabPage16;
        private System.Windows.Forms.TabPage tabPage17;
        private System.Windows.Forms.CheckBox chkSaveFile;
        private System.Windows.Forms.CheckBox chkGetPhoto;
        private System.Windows.Forms.Button button21;
        private System.Windows.Forms.Button button20;
        private System.Windows.Forms.Button button18;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.TextBox txtSetUserCardNo;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox txtSetUserId;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox txtSetUserType;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.TextBox txtSetUserVerify;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.TextBox txtSetUserName;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.TextBox txtSetUserStatus;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.TextBox txtSetUserRegStatus;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.TextBox txtSetPhotoType;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.TextBox txtSetUserFea;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.TextBox txtSetPhoto;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Button button23;
        private System.Windows.Forms.Button button22;
        private System.Windows.Forms.Button button28;
        private System.Windows.Forms.Button button27;
        private System.Windows.Forms.Button button26;
        private System.Windows.Forms.Button button25;
        private System.Windows.Forms.Button button24;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem clearToolStripMenuItem;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox txtSetFeaFile;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.TextBox txtSetFeaUserID;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.TextBox txtSetFeaUserType;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button30;
        private System.Windows.Forms.Button button29;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Button button32;
        private System.Windows.Forms.Button button31;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Button button33;
        private System.Windows.Forms.TabPage tabPage8;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.TabPage tabPage9;
        private System.Windows.Forms.TabPage tabPage10;
        private System.Windows.Forms.TabPage tabPage11;
        private System.Windows.Forms.TabPage tabPage12;
        private System.Windows.Forms.TabPage tabPage13;
        private System.Windows.Forms.Button button35;
        private System.Windows.Forms.Button button34;
        private System.Windows.Forms.TextBox textBox7;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Button button36;
        private System.Windows.Forms.Button button37;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button button41;
        private System.Windows.Forms.Button button40;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.TextBox textBox9;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.TextBox textBox8;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.Button button39;
        private System.Windows.Forms.Button button38;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.Button button43;
        private System.Windows.Forms.Button button42;
        private System.Windows.Forms.Label label63;
        private System.Windows.Forms.TextBox textBox10;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label label64;
        private System.Windows.Forms.Button button45;
        private System.Windows.Forms.Button button44;
        private System.Windows.Forms.Label label65;
        private System.Windows.Forms.TextBox textBox11;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Button button47;
        private System.Windows.Forms.Button button46;
        private System.Windows.Forms.Label label67;
        private System.Windows.Forms.TextBox textBox12;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.Button button48;
        private System.Windows.Forms.Button button49;
        private System.Windows.Forms.Label label69;
        private System.Windows.Forms.TextBox textBox13;
        private System.Windows.Forms.Label label68;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.Button button50;
        private System.Windows.Forms.Button button51;
        private System.Windows.Forms.Label label71;
        private System.Windows.Forms.TextBox textBox14;
        private System.Windows.Forms.Label label70;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.Label label72;
        private System.Windows.Forms.Button button52;
        private System.Windows.Forms.Button button53;
        private System.Windows.Forms.TextBox textBox27;
        private System.Windows.Forms.Label label84;
        private System.Windows.Forms.TextBox textBox28;
        private System.Windows.Forms.Label label85;
        private System.Windows.Forms.TextBox textBox29;
        private System.Windows.Forms.Label label86;
        private System.Windows.Forms.TextBox textBox30;
        private System.Windows.Forms.Label label87;
        private System.Windows.Forms.TextBox textBox23;
        private System.Windows.Forms.Label label80;
        private System.Windows.Forms.TextBox textBox24;
        private System.Windows.Forms.Label label81;
        private System.Windows.Forms.TextBox textBox25;
        private System.Windows.Forms.Label label82;
        private System.Windows.Forms.TextBox textBox26;
        private System.Windows.Forms.Label label83;
        private System.Windows.Forms.TextBox textBox19;
        private System.Windows.Forms.Label label76;
        private System.Windows.Forms.TextBox textBox20;
        private System.Windows.Forms.Label label77;
        private System.Windows.Forms.TextBox textBox21;
        private System.Windows.Forms.Label label78;
        private System.Windows.Forms.TextBox textBox22;
        private System.Windows.Forms.Label label79;
        private System.Windows.Forms.TextBox textBox17;
        private System.Windows.Forms.Label label75;
        private System.Windows.Forms.TextBox textBox18;
        private System.Windows.Forms.Label label74;
        private System.Windows.Forms.TextBox textBox16;
        private System.Windows.Forms.Label label73;
        private System.Windows.Forms.TextBox textBox15;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.Button button54;
        private System.Windows.Forms.Button button55;
        private System.Windows.Forms.TextBox textBox32;
        private System.Windows.Forms.TextBox textBox31;
        private System.Windows.Forms.Label label89;
        private System.Windows.Forms.Label label88;
        private System.Windows.Forms.GroupBox groupBox12;
        private System.Windows.Forms.Button button57;
        private System.Windows.Forms.GroupBox groupBox11;
        private System.Windows.Forms.Button button56;
        private System.Windows.Forms.GroupBox groupBox13;
        private System.Windows.Forms.Label label91;
        private System.Windows.Forms.TextBox textBox33;
        private System.Windows.Forms.Label label90;
        private System.Windows.Forms.TextBox textBox47;
        private System.Windows.Forms.Label label104;
        private System.Windows.Forms.TextBox textBox48;
        private System.Windows.Forms.Label label105;
        private System.Windows.Forms.TextBox textBox45;
        private System.Windows.Forms.Label label102;
        private System.Windows.Forms.TextBox textBox46;
        private System.Windows.Forms.Label label103;
        private System.Windows.Forms.TextBox textBox43;
        private System.Windows.Forms.Label label100;
        private System.Windows.Forms.TextBox textBox44;
        private System.Windows.Forms.Label label101;
        private System.Windows.Forms.TextBox textBox41;
        private System.Windows.Forms.Label label98;
        private System.Windows.Forms.TextBox textBox42;
        private System.Windows.Forms.Label label99;
        private System.Windows.Forms.TextBox textBox39;
        private System.Windows.Forms.Label label96;
        private System.Windows.Forms.TextBox textBox40;
        private System.Windows.Forms.Label label97;
        private System.Windows.Forms.TextBox textBox37;
        private System.Windows.Forms.Label label94;
        private System.Windows.Forms.TextBox textBox38;
        private System.Windows.Forms.Label label95;
        private System.Windows.Forms.TextBox textBox35;
        private System.Windows.Forms.Label label92;
        private System.Windows.Forms.TextBox textBox36;
        private System.Windows.Forms.Label label93;
        private System.Windows.Forms.TextBox textBox34;
        private System.Windows.Forms.Button button59;
        private System.Windows.Forms.Button button58;
        private System.Windows.Forms.GroupBox groupBox14;
        private System.Windows.Forms.Button button60;
        private System.Windows.Forms.GroupBox groupBox15;
        private System.Windows.Forms.Button button61;
        private System.Windows.Forms.Button button62;
        private System.Windows.Forms.CheckBox checkBox4;
        private System.Windows.Forms.CheckBox checkBox3;
        private System.Windows.Forms.CheckBox checkBox2;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.GroupBox groupBox16;
        private System.Windows.Forms.Label label107;
        private System.Windows.Forms.Label label106;
        private System.Windows.Forms.TextBox textBox50;
        private System.Windows.Forms.TextBox textBox49;
        private System.Windows.Forms.Button button65;
        private System.Windows.Forms.Button button64;
        private System.Windows.Forms.Button button63;
        private System.Windows.Forms.GroupBox groupBox17;
        private System.Windows.Forms.Label label108;
        private System.Windows.Forms.Button button67;
        private System.Windows.Forms.Button button66;
        private System.Windows.Forms.TextBox textBox53;
        private System.Windows.Forms.Label label110;
        private System.Windows.Forms.TextBox textBox52;
        private System.Windows.Forms.Label label109;
        private System.Windows.Forms.TextBox textBox51;
        private System.Windows.Forms.GroupBox groupBox18;
        private System.Windows.Forms.Label label112;
        private System.Windows.Forms.TextBox textBox54;
        private System.Windows.Forms.Label label111;
        private System.Windows.Forms.Button button68;
        private System.Windows.Forms.Button button69;
        private System.Windows.Forms.GroupBox groupBox19;
        private System.Windows.Forms.Button button70;
        private System.Windows.Forms.GroupBox groupBox20;
        private System.Windows.Forms.Button button71;
        private System.Windows.Forms.Button button72;
        private System.Windows.Forms.TextBox textBox58;
        private System.Windows.Forms.TextBox textBox57;
        private System.Windows.Forms.TextBox textBox56;
        private System.Windows.Forms.TextBox textBox55;
        private System.Windows.Forms.Label label116;
        private System.Windows.Forms.Label label115;
        private System.Windows.Forms.Label label114;
        private System.Windows.Forms.Label label113;
        private System.Windows.Forms.GroupBox groupBox23;
        private System.Windows.Forms.Button button79;
        private System.Windows.Forms.Button button80;
        private System.Windows.Forms.Label label154;
        private System.Windows.Forms.TextBox textBox81;
        private System.Windows.Forms.Label label153;
        private System.Windows.Forms.GroupBox groupBox22;
        private System.Windows.Forms.Button button77;
        private System.Windows.Forms.Button button78;
        private System.Windows.Forms.Label label152;
        private System.Windows.Forms.TextBox textBox80;
        private System.Windows.Forms.Label label151;
        private System.Windows.Forms.Label label150;
        private System.Windows.Forms.Label label149;
        private System.Windows.Forms.GroupBox groupBox21;
        private System.Windows.Forms.Button button75;
        private System.Windows.Forms.Button button76;
        private System.Windows.Forms.Label label143;
        private System.Windows.Forms.Label label144;
        private System.Windows.Forms.TextBox textBox76;
        private System.Windows.Forms.Label label145;
        private System.Windows.Forms.TextBox textBox77;
        private System.Windows.Forms.Label label146;
        private System.Windows.Forms.TextBox textBox78;
        private System.Windows.Forms.Label label147;
        private System.Windows.Forms.TextBox textBox79;
        private System.Windows.Forms.Label label148;
        private System.Windows.Forms.Label label137;
        private System.Windows.Forms.Label label138;
        private System.Windows.Forms.TextBox textBox72;
        private System.Windows.Forms.Label label139;
        private System.Windows.Forms.TextBox textBox73;
        private System.Windows.Forms.Label label140;
        private System.Windows.Forms.TextBox textBox74;
        private System.Windows.Forms.Label label141;
        private System.Windows.Forms.TextBox textBox75;
        private System.Windows.Forms.Label label142;
        private System.Windows.Forms.Label label131;
        private System.Windows.Forms.Label label132;
        private System.Windows.Forms.TextBox textBox68;
        private System.Windows.Forms.Label label133;
        private System.Windows.Forms.TextBox textBox69;
        private System.Windows.Forms.Label label134;
        private System.Windows.Forms.TextBox textBox70;
        private System.Windows.Forms.Label label135;
        private System.Windows.Forms.TextBox textBox71;
        private System.Windows.Forms.Label label136;
        private System.Windows.Forms.Label label125;
        private System.Windows.Forms.Label label126;
        private System.Windows.Forms.TextBox textBox64;
        private System.Windows.Forms.Label label127;
        private System.Windows.Forms.TextBox textBox65;
        private System.Windows.Forms.Label label128;
        private System.Windows.Forms.TextBox textBox66;
        private System.Windows.Forms.Label label129;
        private System.Windows.Forms.TextBox textBox67;
        private System.Windows.Forms.Label label130;
        private System.Windows.Forms.Label label124;
        private System.Windows.Forms.Label label123;
        private System.Windows.Forms.TextBox textBox63;
        private System.Windows.Forms.Label label122;
        private System.Windows.Forms.TextBox textBox62;
        private System.Windows.Forms.Label label121;
        private System.Windows.Forms.TextBox textBox61;
        private System.Windows.Forms.Label label120;
        private System.Windows.Forms.TextBox textBox60;
        private System.Windows.Forms.Label label119;
        private System.Windows.Forms.Button button73;
        private System.Windows.Forms.Button button74;
        private System.Windows.Forms.Label label118;
        private System.Windows.Forms.TextBox textBox59;
        private System.Windows.Forms.Label label117;
        private System.Windows.Forms.TextBox textBox86;
        private System.Windows.Forms.Label label159;
        private System.Windows.Forms.TextBox textBox85;
        private System.Windows.Forms.Label label158;
        private System.Windows.Forms.TextBox textBox84;
        private System.Windows.Forms.Label label157;
        private System.Windows.Forms.TextBox textBox83;
        private System.Windows.Forms.Label label156;
        private System.Windows.Forms.TextBox textBox82;
        private System.Windows.Forms.Label label155;
        private System.Windows.Forms.Label label161;
        private System.Windows.Forms.Label label160;
        private System.Windows.Forms.Button button81;
        private System.Windows.Forms.Button button82;
        private System.Windows.Forms.TextBox textBox91;
        private System.Windows.Forms.TextBox textBox90;
        private System.Windows.Forms.TextBox textBox89;
        private System.Windows.Forms.TextBox textBox88;
        private System.Windows.Forms.GroupBox groupBox29;
        private System.Windows.Forms.RadioButton radioButton5;
        private System.Windows.Forms.RadioButton radioButton6;
        private System.Windows.Forms.GroupBox groupBox28;
        private System.Windows.Forms.RadioButton radioButton4;
        private System.Windows.Forms.RadioButton radioButton3;
    }
}


﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Diagnostics;

///////////////////////////////////////////////
//
//这个Demo用来演示接口的使用方法，供参考
//
///////////////////////////////////////////////
namespace CFaceServerSdkTest
{
    public partial class frmMain : Form
    {
        TreeNode m_MainNode = null;
        FaceServerSdkLib.FaceServerSdkCtrl objSdk = null;

        public delegate void deleAddDvs(string sSn, string sIP);
        public delegate void deleRemoveDvs(string sn);
        public delegate void deleAddInfo(string sInfo);


        public frmMain()
        {
            InitializeComponent();
            

            objSdk = new FaceServerSdkLib.FaceServerSdkCtrl();
            objSdk.OnEventCConnect += new FaceServerSdkLib._IFaceServerSdkCtrlEvents_OnEventCConnectEventHandler(OnEventCConnect);
            objSdk.OnEventCDisconnect += new FaceServerSdkLib._IFaceServerSdkCtrlEvents_OnEventCDisconnectEventHandler(OnEventDisConnect);
            objSdk.OnEventCTrap += new FaceServerSdkLib._IFaceServerSdkCtrlEvents_OnEventCTrapEventHandler(OnTrapAccess);
            objSdk.OnEventUsbDeviceConnect += new FaceServerSdkLib._IFaceServerSdkCtrlEvents_OnEventUsbDeviceConnectEventHandler(OnEventUsbDeviceConnect);
            objSdk.OnEventCErrorNotify += new FaceServerSdkLib._IFaceServerSdkCtrlEvents_OnEventCErrorNotifyEventHandler(OnEventCErrorNotify);
        }

        /// <summary>
        /// 保存特征
        /// </summary>
        /// <param name="sFix"></param>
        /// <param name="lUserid"></param>
        /// <param name="sBase64Fea"></param>
        private void SaveFea(string sFix, long lUserid, string sBase64Fea)
        {
            try
            {
                if (sBase64Fea.Length <= 0) return;
                if (Directory.Exists(Environment.CurrentDirectory + "\\BinData\\") == false) Directory.CreateDirectory(Environment.CurrentDirectory + "\\BinData\\");
                string sFile = Environment.CurrentDirectory + "\\BinData\\" + sFix + "_Fea_" + lUserid.ToString() + ".dat";
                if (File.Exists(sFile) == true) File.Delete(sFile);

                FileStream fs = new FileStream(sFile, FileMode.Create);
                BinaryWriter bw = new BinaryWriter(fs);
                byte[] bData = Convert.FromBase64String(sBase64Fea);
                bw.Write(bData);
                bw.Close();
                fs.Close();
                bw = null;
                fs = null;
            }
            catch
            { }
        }

        /// <summary>
        /// 显示相片
        /// </summary>
        /// <param name="sBase64Photo"></param>
        private void ShowImg(string sFix,long lUserid,string sBase64Photo)
        {
            try
            {
                if (sBase64Photo.Length <= 0) return;
                if (Directory.Exists(Environment.CurrentDirectory + "\\BinData\\") == false) Directory.CreateDirectory(Environment.CurrentDirectory + "\\BinData\\");
                string sFile = Environment.CurrentDirectory + "\\BinData\\" + sFix + "_" + lUserid.ToString() + ".jpg";
                if (File.Exists(sFile) == true) File.Delete(sFile);

                FileStream fs = new FileStream(sFile, FileMode.Create);
                BinaryWriter bw = new BinaryWriter(fs);
                byte[] bData = Convert.FromBase64String(sBase64Photo);
                bw.Write(bData);
                bw.Close();
                fs.Close();
                bw = null;
                fs = null;

                pictureBox1.Image = Image.FromFile(sFile);
            }
            catch
            { }
        }

        /// <summary>
        /// 添加信息
        /// </summary>
        /// <param name="sInfo"></param>
        private void AddInfo(string sSn,long lOpCode,string sInfo)
        {
            int nOrder = listView1.Items.Count+1;
            ListViewItem tem = listView1.Items.Add(nOrder.ToString());
            tem.SubItems.Add(DateTime.Now.ToString());
            tem.SubItems.Add(sSn);
            tem.SubItems.Add(lOpCode == 0 ? "Succ" : "Failed");
            tem.SubItems.Add(sInfo);
            tem.EnsureVisible();
        }

        /// <summary>
        /// 添加信息
        /// </summary>
        /// <param name="sInfo"></param>
        private void AddInfoFun(string sInfo)
        {
            int nOrder = listView1.Items.Count + 1;
            ListViewItem tem = listView1.Items.Add(nOrder.ToString());
            tem.SubItems.Add(DateTime.Now.ToString());
            tem.SubItems.Add(GetCurrentSn());
            tem.SubItems.Add("Success");
            tem.SubItems.Add(sInfo);
            tem.EnsureVisible();
        }

        private void AddInfo(string sInfo)
        {
            deleAddInfo AddInfoMothod = new deleAddInfo(AddInfoFun);
            this.Invoke(AddInfoMothod, sInfo);
        }

        /// <summary>
        /// 添加设备到列表
        /// </summary>
        /// <param name="sSn"></param>
        /// <param name="sIP"></param>
        private void AddDvsFun(string sSn, string sIP)
        {
            if (m_MainNode != null)
            {
                TreeNode temNod=m_MainNode.Nodes.Add(sIP+"["+sSn+"]");
                temNod.Tag = sSn;
                treeView1.ExpandAll();
            }
        }

        private void AddDvs(string sSn, string sIP)
        {
            deleAddDvs AddDvsMothod = new deleAddDvs(AddDvsFun);
            this.Invoke(AddDvsMothod, sSn, sIP);
        }

        private void RemoveDvs(string sSn)
        {
            deleRemoveDvs RemoveDvsMothod = new deleRemoveDvs(RemoveDvsFun);
            this.Invoke(RemoveDvsMothod,sSn);
        }
        private void RemoveDvsFun(string sSn)
        {
            foreach (TreeNode td in m_MainNode.Nodes)
            {
                if (td.Tag.ToString() == sSn)
                {
                    m_MainNode.Nodes.Remove(td);
                }
            }
        }

        /// <summary>
        /// 获取当前选中的设备SN号
        /// </summary>
        /// <returns></returns>
        private string GetCurrentSn()
        {
            try
            {
                if (treeView1.SelectedNode == null)
                {
                    return "";
                }
                if (treeView1.SelectedNode.Tag.ToString() == "") return "";
                return treeView1.SelectedNode.Tag.ToString();
            }
            catch
            {
                return "";
            }
        }

        /// <summary>
        /// OnEventCErrorNotify
        /// </summary>
        /// <param name="strDevSn"></param>
        /// <param name="lOpCode"></param>
        /// <param name="lUserData"></param>
        /// <param name="lExtendParam"></param>
        /// <param name="lErrorCode"></param>
        void OnEventCErrorNotify(string strDevSn, int lOpCode, int lUserData, int lExtendParam, int lErrorCode)
        {
            AddInfo("OnEventCErrorNotify  ErrorCode=" + lErrorCode.ToString());
        }

        /// <summary>
        /// OnEventUsbDeviceConnect
        /// </summary>
        /// <param name="strDevSn"></param>
        /// <param name="lOpCode"></param>
        /// <param name="lUserData"></param>
        /// <param name="lExtendParam"></param>
        private void OnEventUsbDeviceConnect(string strDevSn, int lOpCode, int lUserData, int lExtendParam)
        {
            AddDvs(strDevSn, "Usb Device");
        }

        /// <summary>
        /// OnEventCConnect
        /// </summary>
        /// <param name="strDevSn"></param>
        /// <param name="lOpCode"></param>
        /// <param name="lUserData"></param>
        /// <param name="lExtendParam"></param>
        /// <param name="strIP"></param>
        /// <param name="lPort"></param>
        private void OnEventCConnect(string strDevSn, int lOpCode, int lUserData, int lExtendParam, string strIP, int lPort)
        {
            AddDvs(strDevSn, strIP);
        }

        /// <summary>
        /// OnEventDisConnect
        /// </summary>
        /// <param name="strDevSn"></param>
        /// <param name="lOpCode"></param>
        /// <param name="lUserData"></param>
        /// <param name="lExtendParam"></param>
        private void OnEventDisConnect(string strDevSn, int lOpCode, int lUserData, int lExtendParam)
        {
            RemoveDvs(strDevSn);
        }

        /// <summary>
        /// OnTrapAccess
        /// </summary>
        /// <param name="strDevSn"></param>
        /// <param name="lOpCode"></param>
        /// <param name="lUserData"></param>
        /// <param name="lExtendParam"></param>
        /// <param name="lUserID"></param>
        /// <param name="strRecTime"></param>
        /// <param name="lRecType"></param>
        /// <param name="lScore"></param>
        /// <param name="lStatus"></param>
        /// <param name="lPhotoType"></param>
        /// <param name="lPhotoLen"></param>
        /// <param name="lReason"></param>
        /// <param name="strBase64PhotoData"></param>
        private void OnTrapAccess(string strDevSn, int lOpCode, int lUserData, int lExtendParam, int lUserID, string strRecTime, int lRecType, int lScore, int lStatus, int lPhotoType, int lPhotoLen, int lReason, string strBase64PhotoData)
        {
            AddInfo("OnTrapAccess  UserID=" + lUserID.ToString()+"  Time="+strRecTime);
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            treeView1.Nodes.Clear();
            m_MainNode = treeView1.Nodes.Add("Connected Device");
        }

        /// <summary>
        /// C_ServerInit
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void bu_Init_Click(object sender, EventArgs e)
        {
           int nRet= objSdk.C_ServerInit(int.Parse(txtPort.Text), 0);
           if (nRet == 0)
           {
               bu_Init.Enabled = false;
               bu_Uninit.Enabled = true;
               txtPort.ReadOnly = true;
            }
           else
           {
               MessageBox.Show("C_ServerInit failed！Errcode：" + Convert.ToString(objSdk.C_SdkGetLastError(GetCurrentSn()), 16));
           }
        }

        /// <summary>
        /// C_ServerUnit
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void bu_Uninit_Click(object sender, EventArgs e)
        {
            int nRet=objSdk.C_ServerUnit();
            if (nRet == 0)
            {
                bu_Init.Enabled = true;
                bu_Uninit.Enabled = false;
                txtPort.ReadOnly = false;
                if (m_MainNode != null)
                {
                    m_MainNode.Nodes.Clear();
                }
            }
            else
            {
                MessageBox.Show("C_ServerUnit failed！Errcode：" + Convert.ToString(objSdk.C_SdkGetLastError(GetCurrentSn()), 16));
            }
           
        }

        /// <summary>
        /// C_GetSdkVersion
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button3_Click(object sender, EventArgs e)
        {
            
            string strVersion = "";
            objSdk.C_GetSdkVersion(ref strVersion);
            txtSdkVer.Text = strVersion;
            
        }

        /// <summary>
        /// C_GetDevicesList
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button4_Click(object sender, EventArgs e)
        {
            
            string strList="";
            objSdk.C_GetDevicesList(ref strList);
            MessageBox.Show(strList);

        }

        /// <summary>
        /// C_IsDeviceConnected
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button5_Click(object sender, EventArgs e)
        {
            if (txtDvsSn.Text == "")
            {
                MessageBox.Show("Please input sn!");
                return;
            }

            bool IsConnect=false;
            objSdk.C_IsDeviceConnected(txtDvsSn.Text, ref IsConnect);

            if (IsConnect == true)
            {
                MessageBox.Show("Device " + txtDvsSn.Text + " is connected！");
            }
            else
            {
                MessageBox.Show("Device" + txtDvsSn.Text + " is not connected！");
            }
        }


        /// <summary>
        /// C_GetSingleUserInfo
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button16_Click(object sender, EventArgs e)
        {
            FaceServerSdkLib.UserInfo User=new FaceServerSdkLib.UserInfo();
            int nRet=objSdk.C_GetSingleUserInfo(GetCurrentSn(), int.Parse(txtGetUserID.Text), int.Parse(txtGetUserType.Text), ref User);
            if (nRet == 0)
            {
                ShowImg("GetSingleUserInfo", User.UserID, User.PhotoBase64Data);
                AddInfo(GetCurrentSn(), 0, "C_GetSingleUserInfo success!UserID=" + User.UserID.ToString() + " UserType=" + User.UserType.ToString() + " UserName=" + User.UserName + " UserStatus=" + User.Status.ToString() + " Verifymode=" + User.VerifyMode.ToString());
            }
            else
            {
                MessageBox.Show("C_GetSingleUserInfo failed！Errcode：" + Convert.ToString(objSdk.C_SdkGetLastError(GetCurrentSn()), 16));
            }
        }

        private void clearToolStripMenuItem_Click(object sender, EventArgs e)
        {
            listView1.Items.Clear();
        }

        /// <summary>
        /// C_GetDeviceTime
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button14_Click(object sender, EventArgs e)
        {
            FaceServerSdkLib.TimeInfo time=new FaceServerSdkLib.TimeInfo();
            int nRet=objSdk.C_GetDeviceTime(GetCurrentSn(), ref time);
            if (nRet == 0)
            {
                txtZoom.Text = time.lTimeZone.ToString();
                txtTime.Text = time.strTime;
                txtTimeType.Text = time.lNtpType.ToString();
                txtServerIP.Text = time.strNtpServer;
            }
            else
            {
                MessageBox.Show("C_GetDeviceTime failed！Errcode：" + Convert.ToString(objSdk.C_SdkGetLastError(GetCurrentSn()), 16));
            }
            
        }

        /// <summary>
        /// C_GetSingleUserFeature
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button19_Click(object sender, EventArgs e)
        {
            if (txtGetFeaUserID.Text == "")
            {
                MessageBox.Show("Please input userid!");
                return;
            }

            if (txtGetFeaUserType.Text == "")
            {
                MessageBox.Show("Please input usertype！");
                return;
            }

            string strFeature = "";
            int nRet=objSdk.C_GetSingleUserFeature(GetCurrentSn(), int.Parse(txtGetFeaUserID.Text), int.Parse(txtGetFeaUserType.Text),ref strFeature);
            if (nRet == 0)
            {
                SaveFea("GetSingleUserFeature", int.Parse(txtGetFeaUserID.Text), strFeature);
                AddInfo(GetCurrentSn(), 0, "C_GetSingleUserFeature success!");
            }
            else
            {
                MessageBox.Show("C_GetSingleUserFeature failed！Errcode：" + Convert.ToString(objSdk.C_SdkGetLastError(GetCurrentSn()), 16));
            } 
        }

        /// <summary>
        /// C_SetDeviceTime
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button15_Click(object sender, EventArgs e)
        {
            if (txtZoom.Text == "")
            {
                MessageBox.Show("Please input time zoom！");
                return;
            }

            if (txtTime.Text == "")
            {
                MessageBox.Show("Please input time!");
                return;
            }

            if (txtTimeType.Text == "")
            {
                MessageBox.Show("Please input time type!");
                return;
            }

            FaceServerSdkLib.TimeInfo timeInfo = new FaceServerSdkLib.TimeInfo();
            timeInfo.lNtpType = int.Parse(txtTimeType.Text);
            timeInfo.lTimeZone = int.Parse(txtZoom.Text);
            timeInfo.strTime = txtTime.Text;
            timeInfo.strNtpServer = txtServerIP.Text;

            int nRet = objSdk.C_SetDeviceTime(GetCurrentSn(), timeInfo);
            if(nRet==0)
            {
                MessageBox.Show("C_SetDeviceTime success！");
            }
            else
            {
                MessageBox.Show("C_SetDeviceTime failed！Errcode：" + Convert.ToString(objSdk.C_SdkGetLastError(GetCurrentSn()), 16));
            }
        }

        /// <summary>
        /// C_GetProtocolVersion
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button8_Click(object sender, EventArgs e)
        {
            txtProVer.Text = objSdk.C_GetProtocolVersion(GetCurrentSn()).ToString();
        }

        private void button9_Click(object sender, EventArgs e)
        {
            txtProVer.Text = this.objSdk.C_GetDeviceVersion(GetCurrentSn()).ToString();
        }

        private void button10_Click(object sender, EventArgs e)
        {
            txtDvsType.Text = objSdk.C_GetDeviceType(GetCurrentSn()).ToString();
        }

        private void button11_Click(object sender, EventArgs e)
        {
            txtDvsName.Text = objSdk.C_GetDeviceName(GetCurrentSn());
        }

        /// <summary>
        /// C_GetUsersInfo
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button17_Click(object sender, EventArgs e)
        {
            int nUserCount = 0;
            int nRet = objSdk.C_GetAllUsersCount(GetCurrentSn(),ref nUserCount);
            if (nUserCount > 0)
            {
                for(int nIndex=1;nIndex<=nUserCount;nIndex++)
                {
                    FaceServerSdkLib.UserInfo reUserInfo=new FaceServerSdkLib.UserInfo();
                    int nRet1 = objSdk.C_GetUsersInfo(GetCurrentSn(), ref reUserInfo);
                    if (nRet1 == 0)
                    {
                        ShowImg("GetSingleUserInfo", reUserInfo.UserID, reUserInfo.PhotoBase64Data);
                        AddInfo(GetCurrentSn(), 0, "C_GetUsersInfo(" + nIndex.ToString() + "/" + nUserCount.ToString() + ")：UserID=" + reUserInfo.UserID.ToString() + " UserType=" + reUserInfo.UserType.ToString() + " UserName=" + reUserInfo.UserName + " UserStatus=" + reUserInfo.Status.ToString() + " Verifymode=" + reUserInfo.VerifyMode.ToString());
                    }
                }              
            }

        }

        /// <summary>
        /// C_GetUserSuccessAccessCount
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button18_Click(object sender, EventArgs e)
        {
            int nCount = objSdk.C_GetUserSuccessAccessCount(GetCurrentSn(), int.Parse(txtAccessUserID.Text), txtAccessStart.Text, txtAccessEnd.Text, chkGetPhoto.Checked);
            if (nCount > 0)
            {
                for (int nIndex = 1; nIndex <= nCount; nIndex++)
                {
                    FaceServerSdkLib.AccessInfo access = new FaceServerSdkLib.AccessInfo();
                    int nRet = objSdk.C_GetAccess(GetCurrentSn(),ref access);
                    if (nRet == 0)
                    {
                        ShowImg("C_GetUserSuccessAccess", access.UserID, access.strBase64PhotoData);
                        AddInfo(GetCurrentSn(), 0, "C_GetUserSuccessAccess success!Total:" + nCount.ToString() + " Current=" + nIndex.ToString() + " UserID=" + access.UserID.ToString() + " Time=" + access.strRecTime + " RecType=" + access.lRecType.ToString() + " Reason=" + access.lReason.ToString());
                    }
                }
            }
            else
            {
                MessageBox.Show("Have no record!");
            }
        }

        /// <summary>
        /// C_GetAccess
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button20_Click(object sender, EventArgs e)
        {
            int nCount = objSdk.C_GetAllUsersAccessCount(GetCurrentSn(), txtAccessStart.Text, txtAccessEnd.Text, chkGetPhoto.Checked);
            if (nCount > 0)
            {
                for (int nIndex = 1; nIndex <= nCount; nIndex++)
                {
                    FaceServerSdkLib.AccessInfo access = new FaceServerSdkLib.AccessInfo();
                    int nRet = objSdk.C_GetAccess(GetCurrentSn(), ref access);
                    if (nRet == 0)
                    {
                        ShowImg("C_GetAllUsersAccess", access.UserID, access.strBase64PhotoData);
                        AddInfo(GetCurrentSn(), 0, "C_GetAllUsersAccess success!Total:" + nCount.ToString() + " Current=" + nIndex.ToString() + " UserID=" + access.UserID.ToString() + " Time=" + access.strRecTime + " RecType=" + access.lRecType.ToString() + " Reason=" + access.lReason.ToString());
                    }
                }
            }
            else
            {
                MessageBox.Show("Have no record!");
            }
        }

        /// <summary>
        /// C_GetAllUsersAccess
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button21_Click(object sender, EventArgs e)
        {
            int nCount = objSdk.C_GetAllUsersFailAccessCount(GetCurrentSn(), txtAccessStart.Text, txtAccessEnd.Text, chkGetPhoto.Checked);
            if (nCount > 0)
            {
                for (int nIndex = 1; nIndex <= nCount; nIndex++)
                {
                    FaceServerSdkLib.AccessInfo access = new FaceServerSdkLib.AccessInfo();
                    int nRet = objSdk.C_GetAccess(GetCurrentSn(), ref access);
                    if (nRet == 0)
                    {
                        ShowImg("C_GetAllUsersAccess", access.UserID, access.strBase64PhotoData);
                        AddInfo(GetCurrentSn(), 0, "C_GetAllUsersFailAccess success!Total:" + nCount.ToString() + " Current=" + nIndex.ToString() + " UserID=" + access.UserID.ToString() + " Time=" + access.strRecTime + " RecType=" + access.lRecType.ToString() + " Reason=" + access.lReason.ToString());
                    }
                }
            }
            else
            {
                MessageBox.Show("Have no record!");
            }
        }

        /// <summary>
        /// C_AddUser
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button24_Click(object sender, EventArgs e)
        {
            if (txtSetUserType.Text == "")
            {
                MessageBox.Show("Please input usertype!");
                return;
            }
            if (txtSetUserId.Text == "")
            {
                MessageBox.Show("Please input userid!");
                return;
            }

            if (txtSetUserCardNo.Text == "")
            {
                MessageBox.Show("Please input cardno!");
                return;
            }
            if (txtSetUserVerify.Text == "")
            {
                MessageBox.Show("Please input verify mode!");
                return;
            }
            if (txtSetUserName.Text == "")
            {
                MessageBox.Show("Please input user name!");
                return;
            }
            string sBase64Photo = "";
            if (txtSetPhoto.Text != "")
            {
                FileStream fs = new FileStream(txtSetPhoto.Text, System.IO.FileMode.Open);
                BinaryReader br = new BinaryReader(fs);
                byte[] bData = br.ReadBytes((int)fs.Length);
                sBase64Photo = Convert.ToBase64String(bData);

                br.Close();
                fs.Close();
                br = null;
                fs = null;
            }

            FaceServerSdkLib.UserInfo user = new FaceServerSdkLib.UserInfo();
            user.UserType = int.Parse(txtSetUserType.Text);
            user.UserID = int.Parse(txtSetUserId.Text);
            user.CardNo = txtSetUserCardNo.Text.PadLeft(10, '0');
            user.UserName = txtSetUserName.Text;
            user.VerifyMode = int.Parse(txtSetUserVerify.Text);
            user.Status = int.Parse(txtSetUserStatus.Text);
            user.PhotoLen = sBase64Photo.Length;
            user.PhotoBase64Data = sBase64Photo;

            long lRet = objSdk.C_AddUser(GetCurrentSn(), user);
            if (lRet != 0)
            {
                MessageBox.Show("C_AddUser failed!ErrCode：" + Convert.ToString(this.objSdk.C_SdkGetLastError(GetCurrentSn()), 16));
            }
            else
            {
                MessageBox.Show("C_AddUser Success!");
            }
        }

        /// <summary>
        /// C_ModifyUser
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button26_Click(object sender, EventArgs e)
        {
            if (txtSetUserType.Text == "")
            {
                MessageBox.Show("Please input usertype!");
                return;
            }
            if (txtSetUserId.Text == "")
            {
                MessageBox.Show("Please input userid!");
                return;
            }

            if (txtSetUserCardNo.Text == "")
            {
                MessageBox.Show("Please input card no!");
                return;
            }
            if (txtSetUserVerify.Text == "")
            {
                MessageBox.Show("Please input verify mode!");
                return;
            }
            if (txtSetUserName.Text == "")
            {
                MessageBox.Show("Please input username!");
                return;
            }
            string sBase64Photo = "";
            if (txtSetPhoto.Text != "")
            {
                FileStream fs = new FileStream(txtSetPhoto.Text, System.IO.FileMode.Open);
                BinaryReader br = new BinaryReader(fs);
                byte[] bData = br.ReadBytes((int)fs.Length);
                sBase64Photo = Convert.ToBase64String(bData);
                br.Close();
                fs.Close();
                br = null;
                fs = null;
            }

            FaceServerSdkLib.UserInfo user = new FaceServerSdkLib.UserInfo();
            user.UserType = int.Parse(txtSetUserType.Text);
            user.UserID = int.Parse(txtSetUserId.Text);
            user.CardNo = txtSetUserCardNo.Text.PadLeft(10, '0');
            user.UserName = txtSetUserName.Text;
            user.VerifyMode = int.Parse(txtSetUserVerify.Text);
            user.Status = int.Parse(txtSetUserStatus.Text);
            user.PhotoLen = sBase64Photo.Length;
            user.PhotoBase64Data = sBase64Photo;
   

            long lRet = objSdk.C_ModifyUser(GetCurrentSn(), user);
            if (lRet != 0)
            {
                MessageBox.Show("C_ModifyUser failed!ErrCode：" + Convert.ToString(objSdk.C_SdkGetLastError(GetCurrentSn()), 16));
            }
            else
            {
                MessageBox.Show("C_ModifyUser Success!");
            }
        }

        private void button22_Click(object sender, EventArgs e)
        {
            OpenFileDialog fo = new OpenFileDialog();
            fo.Filter = "jpg file|*.jpg";
            fo.ShowDialog();
            if (fo.FileName != "")
            {
                txtSetPhoto.Text = fo.FileName;
            }
        }

        private void button23_Click(object sender, EventArgs e)
        {
            OpenFileDialog fo = new OpenFileDialog();
            fo.Filter = "Fea file|*.dat";
            fo.ShowDialog();
            if (fo.FileName != "")
            {
                txtSetUserFea.Text = fo.FileName;
            }
        }

        /// <summary>
        /// C_DelSingleUser
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button27_Click(object sender, EventArgs e)
        {
            if (txtSetUserType.Text == "")
            {
                MessageBox.Show("Please input usertype!");
                return;
            }
            if (txtSetUserId.Text == "")
            {
                MessageBox.Show("Please input user id!");
                return;
            }

            long lRet = objSdk.C_DelSingleUser(GetCurrentSn(), int.Parse(txtSetUserId.Text), int.Parse(txtSetUserType.Text));
            if (lRet != 0)
            {
                MessageBox.Show("C_DelSingleUser failed!ErrCode：" + Convert.ToString(objSdk.C_SdkGetLastError(GetCurrentSn()), 16));
            }
            else
            {
                MessageBox.Show("C_DelSingleUser Success!");
            }
        }

        /// <summary>
        /// C_DelAllUsers
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button28_Click(object sender, EventArgs e)
        {
            long lRet = objSdk.C_DelAllUsers(GetCurrentSn());
            if (lRet != 0)
            {
                MessageBox.Show("C_DelAllUsers failed!ErrCode：" + Convert.ToString(objSdk.C_SdkGetLastError(GetCurrentSn()), 16));
            }
            else
            {
                MessageBox.Show("C_DelAllUsers Success!");
            }
        }

        /// <summary>
        /// C_ModifyUserFeature
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button2_Click(object sender, EventArgs e)
        {
            if (txtSetFeaUserID.Text == "")
            {
                MessageBox.Show("Please input userid!");
                return;
            }

            if (txtSetFeaUserType.Text == "")
            {
                MessageBox.Show("Please input usertype!");
                return;
            }

            string sBase64Fea = "";
            if (txtSetFeaFile.Text != "")
            {
                FileStream fs = new FileStream(txtSetFeaFile.Text, System.IO.FileMode.Open);
                BinaryReader br = new BinaryReader(fs);
                byte[] bData = br.ReadBytes((int)fs.Length);
                sBase64Fea = Convert.ToBase64String(bData);
                br.Close();
                fs.Close();
                br = null;
                fs = null;
            }

            long lRet = objSdk.C_ModifyUserFeature(GetCurrentSn(), int.Parse(txtSetFeaUserID.Text), int.Parse(txtSetFeaUserType.Text), sBase64Fea);
            if (lRet != 0)
            {
                MessageBox.Show("C_ModifyUserFeature failed!ErrCode：" + Convert.ToString(this.objSdk.C_SdkGetLastError(GetCurrentSn()), 16));
            }
            else
            {
                MessageBox.Show("C_ModifyUserFeature Success!");
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            OpenFileDialog fo = new OpenFileDialog();
            fo.Filter = "Fea file|*.dat";
            fo.ShowDialog();
            if (fo.FileName != "")
            {
                txtSetFeaFile.Text = fo.FileName;
            }
        }

        /// <summary>
        /// C_GeneratePhotoAndFeature
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button29_Click(object sender, EventArgs e)
        {
            if (textBox1.Text == "")
            {
                MessageBox.Show("Please input user type！");
                return;
            }

            FaceServerSdkLib.EnrollInfo enroll = new FaceServerSdkLib.EnrollInfo();
            long lRet = objSdk.C_GeneratePhotoAndFeature(GetCurrentSn(), int.Parse(textBox2.Text), ref enroll);
            if (lRet != 0)
            {
                MessageBox.Show("C_GeneratePhotoAndFeature failed!ErrCode：" + Convert.ToString(objSdk.C_SdkGetLastError(GetCurrentSn()), 16));
            }
            else
            {
                AddInfo("C_GeneratePhotoAndFeature success! UserID=" + enroll.UserID.ToString()+" PhotoLen="+enroll.lPhotoLen.ToString()+" FeaLen="+enroll.lFeatureLen.ToString());
                SaveFea("GenPhotoAndFeature_Fea", enroll.UserID, enroll.strBase64FeatureData);
                ShowImg("GenPhotoAndFeature_Photo", enroll.UserID, enroll.strBase64PhotoData);
                MessageBox.Show("C_GeneratePhotoAndFeature Success!");
            }
        }

        /// <summary>
        /// C_AddUserAndFeature
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button25_Click(object sender, EventArgs e)
        {
            if (txtSetUserType.Text == "")
            {
                MessageBox.Show("Please input user type!");
                return;
            }
            if (txtSetUserId.Text == "")
            {
                MessageBox.Show("Please input userid!");
                return;
            }

            if (txtSetUserCardNo.Text == "")
            {
                MessageBox.Show("Please input card no!");
                return;
            }
            if (txtSetUserVerify.Text == "")
            {
                MessageBox.Show("Please input verify mode!");
                return;
            }
            if (txtSetUserName.Text == "")
            {
                MessageBox.Show("Please input username!");
                return;
            }
            string sBase64Photo = "";
            if (txtSetPhoto.Text != "")
            {
                FileStream fs = new FileStream(txtSetPhoto.Text, System.IO.FileMode.Open);
                BinaryReader br = new BinaryReader(fs);
                byte[] bData = br.ReadBytes((int)fs.Length);
                sBase64Photo = Convert.ToBase64String(bData);
            }

            string sBase64Fea = "";
            if (txtSetUserFea.Text != "")
            {
                FileStream fs = new FileStream(txtSetUserFea.Text, System.IO.FileMode.Open);
                BinaryReader br = new BinaryReader(fs);
                byte[] bData = br.ReadBytes((int)fs.Length);
                sBase64Fea = Convert.ToBase64String(bData);
            }

            FaceServerSdkLib.UserInfoAndFeature user = new FaceServerSdkLib.UserInfoAndFeature();
            user.UserType=int.Parse(txtSetUserType.Text);
            user.UserID=int.Parse(txtSetUserId.Text);
            user.CardNo=txtSetUserCardNo.Text.PadLeft(10, '0');
            user.UserName=txtSetUserName.Text;
            user.VerifyMode=int.Parse(txtSetUserVerify.Text);
            user.Status=int.Parse(txtSetUserStatus.Text);
            user.PhotoLen=sBase64Photo.Length;
            user.PhotoBase64Data=sBase64Photo;
            user.FeatureBase64Data=sBase64Fea;


            long lRet = objSdk.C_AddUserAndFeature(GetCurrentSn(), user);
            if (lRet != 0)
            {
                MessageBox.Show("C_AddUserAndFeature failed!ErrCode：" + Convert.ToString(objSdk.C_SdkGetLastError(GetCurrentSn()), 16));
            }
            else
            {
                MessageBox.Show("C_AddUserAndFeature Success!");
            }
        }

        /// <summary>
        /// C_EnrollUser
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button30_Click(object sender, EventArgs e)
        {
            if (textBox1.Text == "")
            {
                MessageBox.Show("Please input user type！");
                return;
            }

            FaceServerSdkLib.EnrollInfo enroll = new FaceServerSdkLib.EnrollInfo();
            long lRet = objSdk.C_EnrollUser(GetCurrentSn(), int.Parse(textBox1.Text), int.Parse(textBox2.Text), ref enroll);
            if (lRet != 0)
            {
                MessageBox.Show("C_EnrollUser failed!ErrCode：" + Convert.ToString(objSdk.C_SdkGetLastError(GetCurrentSn()), 16));
            }
            else
            {
                AddInfo("C_EnrollUser success! UserID=" + enroll.UserID.ToString() + " PhotoLen=" + enroll.lPhotoLen.ToString() + " FeaLen=" + enroll.lFeatureLen.ToString());
                SaveFea("C_EnrollUser_Fea", enroll.UserID, enroll.strBase64FeatureData);
                ShowImg("C_EnrollUser_Photo", enroll.UserID, enroll.strBase64PhotoData);
                MessageBox.Show("C_EnrollUser Success!");
            }
        }

        /// <summary>
        /// C_VerifyUserByDownloadFeature
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button32_Click(object sender, EventArgs e)
        {
            if (textBox3.Text == "")
            {
                MessageBox.Show("Please select a feature file！");
                return;
            }

            string sBase64Fea = "";
            if (textBox3.Text != "")
            {
                FileStream fs = new FileStream(textBox3.Text, System.IO.FileMode.Open);
                BinaryReader br = new BinaryReader(fs);
                byte[] bData = br.ReadBytes((int)fs.Length);
                sBase64Fea = Convert.ToBase64String(bData);
                br.Close();
                fs.Close();
                br = null;
                fs = null;
            }

            FaceServerSdkLib.VerifyResult vResult = new FaceServerSdkLib.VerifyResult();
            long lRet = objSdk.C_VerifyUserByDownloadFeature(GetCurrentSn(), sBase64Fea,ref vResult);
            if (lRet != 0)
            {
                MessageBox.Show("C_VerifyUserByDownloadFeature failed!ErrCode：" + Convert.ToString(objSdk.C_SdkGetLastError(GetCurrentSn()), 16));
            }
            else
            {
                ShowImg("OnEventCVerifyUserDownload", vResult.UserID, vResult.strBase64PhotoData);
                AddInfo(GetCurrentSn(), 0, "VerifyUserDownload success! UserID=" + vResult.UserID.ToString() + " VerifyResult=" + vResult.lVerifyResult.ToString() + " Score=" + vResult.lScore.ToString());
            }
        }

        /// <summary>
        /// C_SetDoorDelay
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button34_Click(object sender, EventArgs e)
        {
            if (textBox5.Text == "")
            {
                MessageBox.Show("Please input delay count!");
                return;
            }
            try
            {
                int nDelay = int.Parse(textBox5.Text);
                long lRet = objSdk.C_SetDoorDelay(GetCurrentSn(), nDelay);
                if (lRet != 0)
                {
                    MessageBox.Show("C_SetDoorDelay failed!ErrCode：" + Convert.ToString(objSdk.C_SdkGetLastError(GetCurrentSn()), 16));
                }
                else
                {
                    MessageBox.Show("C_SetDoorDelay Success!");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        /// <summary>
        /// Open Fea File
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button31_Click(object sender, EventArgs e)
        {
            OpenFileDialog fo = new OpenFileDialog();
            fo.Filter = "Fea file|*.dat";
            fo.ShowDialog();
            if (fo.FileName != "")
            {
                textBox3.Text = fo.FileName;
            }
        }

        /// <summary>
        /// C_VerifyUser
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button33_Click(object sender, EventArgs e)
        {
            FaceServerSdkLib.VerifyResult vResult = new FaceServerSdkLib.VerifyResult();
            long lRet = this.objSdk.C_VerifyUser(GetCurrentSn(), int.Parse(textBox4.Text),ref vResult);
            if (lRet != 0)
            {
                MessageBox.Show("C_VerifyUser failed!ErrCode：" + Convert.ToString(this.objSdk.C_SdkGetLastError(GetCurrentSn()), 16));
            }
            else
            {
                ShowImg("OnEventCVerifyUserDownload", vResult.UserID, vResult.strBase64PhotoData);
                AddInfo(GetCurrentSn(), 0, "C_VerifyUser success! UserID=" + vResult.UserID.ToString() + " VerifyResult=" + vResult.lVerifyResult.ToString() + " Score=" + vResult.lScore.ToString());

            }
        }

        /// <summary>
        /// C_GetDoorDelay
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button35_Click(object sender, EventArgs e)
        {
            int lValue = 0;
            long lRet = objSdk.C_GetDoorDelay(GetCurrentSn(),ref lValue);
            if (lRet != 0)
            {
                MessageBox.Show("C_GetDoorDelay failed!ErrCode：" + Convert.ToString(this.objSdk.C_SdkGetLastError(GetCurrentSn()), 16));
            }
            else
            {
                AddInfo("GetDoorDelay success!Value=" +lValue.ToString());
            }
        }

        private void button37_Click(object sender, EventArgs e)
        {
            if (textBox6.Text == "")
            {
                MessageBox.Show("Please input user id!");
                return;
            }

            int nOut;
            if (int.TryParse(textBox6.Text, out nOut) == false)
            {
                MessageBox.Show("Userid is invalidate!");
                return;
            }

            if (textBox7.Text == "")
            {
                MessageBox.Show("Please input end time!");
                return;
            }

            long lRet = this.objSdk.C_DelSingleUserAccessRecord(GetCurrentSn(), int.Parse(textBox6.Text), textBox7.Text);
            if (lRet != 0)
            {
                MessageBox.Show("C_DelSingleUserAccessRecord failed!ErrCode：" + Convert.ToString(this.objSdk.C_SdkGetLastError(GetCurrentSn()), 16));
            }
            else
            {
                MessageBox.Show("C_DelSingleUserAccessRecord Success!");
            }
        }

        private void button36_Click(object sender, EventArgs e)
        {
            if (textBox7.Text == "")
            {
                MessageBox.Show("Please input end time!");
                return;
            }

            long lRet = this.objSdk.C_DelAllUsersAccessRecord(GetCurrentSn(), textBox7.Text);
            if (lRet != 0)
            {
                MessageBox.Show("C_DelAllUsersAccessRecord failed!ErrCode：" + Convert.ToString(this.objSdk.C_SdkGetLastError(GetCurrentSn()), 16));
            }
            else
            {
                MessageBox.Show("C_DelAllUsersAccessRecord Success!");
            }
        }

        private void button38_Click(object sender, EventArgs e)
        {
            FaceServerSdkLib.NetInfo net = new FaceServerSdkLib.NetInfo();
            net.strIP = textBox88.Text;
            net.strMask = textBox89.Text;
            net.strGateWay = textBox90.Text;
            net.strDNS = textBox91.Text;
            long lRet = this.objSdk.C_SetNet(GetCurrentSn(), net);
            if (lRet != 0)
            {
                MessageBox.Show("C_SetNet failed!ErrCode：" + Convert.ToString(this.objSdk.C_SdkGetLastError(GetCurrentSn()), 16));
            }
            else
            {
                MessageBox.Show("C_SetNet Success!");
            }
        }

        private void button39_Click(object sender, EventArgs e)
        {
            FaceServerSdkLib.NetInfo net = new FaceServerSdkLib.NetInfo();
            long lRet = this.objSdk.C_GetNet(GetCurrentSn(),ref net);
            if (lRet != 0)
            {
                MessageBox.Show("C_GetNet failed!ErrCode：" + Convert.ToString(this.objSdk.C_SdkGetLastError(GetCurrentSn()), 16));
            }
            else
            {
                AddInfo("GetNet success!IP=" + net.strIP + " Mask=" + net.strMask + " Gate=" + net.strGateWay + " Dns=" + net.strDNS);
            }
        }

        private void button43_Click(object sender, EventArgs e)
        {
            if (int.Parse(textBox10.Text) > 1)
            {
                MessageBox.Show("Please input the switch value(0-1)!");
                return;
            }
            if (textBox10.Text == "")
            {
                MessageBox.Show("Please input the switch value!");
                return;
            }

            long lRet = this.objSdk.C_SetLockShellAlarm(GetCurrentSn(), textBox10.Text == "1" ? true : false);
            if (lRet != 0)
            {
                MessageBox.Show("C_SetLockShellAlarm failed!ErrCode：" + Convert.ToString(this.objSdk.C_SdkGetLastError(GetCurrentSn()), 16));
            }
            else
            {
                MessageBox.Show("C_SetLockShellAlarm success!");
            }
        }

        private void button42_Click(object sender, EventArgs e)
        {
            int lValue = 0;
            long lRet = this.objSdk.C_GetLockShellAlarm(GetCurrentSn(),ref lValue);
            if (lRet != 0)
            {
                MessageBox.Show("C_GetLockShellAlarm failed!ErrCode：" + Convert.ToString(this.objSdk.C_SdkGetLastError(GetCurrentSn()), 16));
            }
            else
            {
                AddInfo( "GetLockShellAlarm success! Value=" + lValue.ToString());
            }
        }

        private void button44_Click(object sender, EventArgs e)
        {
            if (textBox11.Text == "")
            {
                MessageBox.Show("Please input interval!");
                return;
            }

            long lRet = this.objSdk.C_SetAttendInterval(GetCurrentSn(), int.Parse(textBox11.Text));
            if (lRet != 0)
            {
                MessageBox.Show("C_SetAttendInterval failed!ErrCode：" + Convert.ToString(this.objSdk.C_SdkGetLastError(GetCurrentSn()), 16));
            }
            else
            {
                MessageBox.Show("C_SetAttendInterval Success!");
            }
        }

        private void button45_Click(object sender, EventArgs e)
        {
            int lValue = 0;
            long lRet = this.objSdk.C_GetAttendInterval(GetCurrentSn(),ref lValue);
            if (lRet != 0)
            {
                MessageBox.Show("C_GetAttendInterval failed!ErrCode：" + Convert.ToString(this.objSdk.C_SdkGetLastError(GetCurrentSn()), 16));
            }
            else
            {
                AddInfo("GetAttendInterval success! value=" + lValue.ToString());
            }
        }

        private void button46_Click(object sender, EventArgs e)
        {
            if (textBox12.Text == "")
            {
                MessageBox.Show("Please input language!");
                return;
            }

            long lRet = this.objSdk.C_SetLanguage(GetCurrentSn(), int.Parse(textBox12.Text));
            if (lRet != 0)
            {
                MessageBox.Show("C_SetLanguage failed!ErrCode：" + Convert.ToString(this.objSdk.C_SdkGetLastError(GetCurrentSn()), 16));
            }
            else
            {
                MessageBox.Show("C_SetLanguage Success!");
            }
        }

        private void button47_Click(object sender, EventArgs e)
        {
            int lValue = 0;
            long lRet = this.objSdk.C_GetLanguage(GetCurrentSn(), ref lValue);
            if (lRet != 0)
            {
                MessageBox.Show("C_GetLanguage failed!ErrCode：" + Convert.ToString(this.objSdk.C_SdkGetLastError(GetCurrentSn()), 16));
            }
            else
            {
                AddInfo("GetLanguage success!Language=" + lValue.ToString());
            }
        }

        private void button40_Click(object sender, EventArgs e)
        {
            FaceServerSdkLib.WiegandOutInfo wOut = new FaceServerSdkLib.WiegandOutInfo();
            wOut.WiegandOutMod = (radioButton1.Checked == true ? 0 : 1);
            wOut.CardNoOrID = int.Parse(textBox8.Text);
            wOut.Section = int.Parse(textBox9.Text);

            long lRet = this.objSdk.C_SetWiegandOut(GetCurrentSn(), wOut);
            if (lRet != 0)
            {
                MessageBox.Show("C_SetWiegandOut failed!ErrCode：" + Convert.ToString(this.objSdk.C_SdkGetLastError(GetCurrentSn()), 16));
            }
            else
            {
                MessageBox.Show("C_SetWiegandOut Success!");
            }
        }

        private void button41_Click(object sender, EventArgs e)
        {
            FaceServerSdkLib.WiegandOutInfo wOut = new FaceServerSdkLib.WiegandOutInfo();
            long lRet = this.objSdk.C_GetWiegandOut(GetCurrentSn(),ref wOut);
            if (lRet != 0)
            {
                MessageBox.Show("C_GetWiegandOut failed!ErrCode：" + Convert.ToString(this.objSdk.C_SdkGetLastError(GetCurrentSn()), 16));
            }
            else
            {
                AddInfo("GetWiegandOut success!WiegandoutMod=" + wOut.WiegandOutMod.ToString() + " Section=" + wOut.Section.ToString()+" OutputCardNoOrID="+wOut.CardNoOrID.ToString());
            }
        }

        private void button49_Click(object sender, EventArgs e)
        {
            long lRet = this.objSdk.C_SetDeviceSyncMode(GetCurrentSn(), int.Parse(textBox13.Text));
            if (lRet != 0)
            {
                MessageBox.Show("C_SetDeviceSyncMode failed!ErrCode：" + Convert.ToString(this.objSdk.C_SdkGetLastError(GetCurrentSn()), 16));
            }
            else
            {
                MessageBox.Show("C_SetDeviceSyncMode Success!");
            }
        }

        private void button48_Click(object sender, EventArgs e)
        {
            int lValue = 0;
            long lRet = this.objSdk.C_GetDeviceSync(GetCurrentSn(),ref lValue);
            if (lRet != 0)
            {
                MessageBox.Show("C_GetDeviceSync failed!ErrCode：" + Convert.ToString(this.objSdk.C_SdkGetLastError(GetCurrentSn()), 16));
            }
            else
            {
                AddInfo("GetDeviceSync success!Mode=" + lValue.ToString());
            }
        }

        private void button51_Click(object sender, EventArgs e)
        {
            long lRet = this.objSdk.C_SetUpLoadSwitch(GetCurrentSn(), int.Parse(textBox14.Text));
            if (lRet != 0)
            {
                MessageBox.Show("C_SetUpLoadSwitch failed!ErrCode：" + Convert.ToString(this.objSdk.C_SdkGetLastError(GetCurrentSn()), 16));
            }
            else
            {
                MessageBox.Show("C_SetUpLoadSwitch Success!");
            }
        }

        private void button50_Click(object sender, EventArgs e)
        {
            int lValue = 0;
            long lRet = this.objSdk.C_GetUpLoadSwitch(GetCurrentSn(),ref lValue);
            if (lRet != 0)
            {
                MessageBox.Show("C_GetUpLoadSwitch failed!ErrCode：" + Convert.ToString(this.objSdk.C_SdkGetLastError(GetCurrentSn()), 16));
            }
            else
            {
                AddInfo("GetUpLoadSwitch success!Switch=" + lValue.ToString());
            }
        }

        private void button53_Click(object sender, EventArgs e)
        {
            FaceServerSdkLib.AutoSleep fbcInfo = new FaceServerSdkLib.AutoSleep();
            fbcInfo.startHour0=int.Parse(textBox15.Text);
            fbcInfo.startMinute0=int.Parse(textBox16.Text);
            fbcInfo.endHour0=int.Parse(textBox18.Text);
            fbcInfo.endMinute0=int.Parse(textBox17.Text);
            fbcInfo.startHour1=int.Parse(textBox22.Text);
            fbcInfo.startMinute1=int.Parse(textBox21.Text);
            fbcInfo.endHour1=int.Parse(textBox20.Text);
            fbcInfo.endMinute1=int.Parse(textBox19.Text);
            fbcInfo.startHour2=int.Parse(textBox26.Text);
            fbcInfo.startMinute2=int.Parse(textBox25.Text);
            fbcInfo.endHour2=int.Parse(textBox24.Text);
            fbcInfo.endMinute2=int.Parse(textBox23.Text);
            fbcInfo.startHour3=int.Parse(textBox30.Text);
            fbcInfo.startMinute3=int.Parse(textBox29.Text);
            fbcInfo.endHour3=int.Parse(textBox28.Text);
            fbcInfo.endMinute3=int.Parse(textBox27.Text);

            long lRet = this.objSdk.C_SetForbitCheckIn(GetCurrentSn(),fbcInfo);
            if (lRet != 0)
            {
                MessageBox.Show("C_SetForbitCheckIn failed!ErrCode：" + Convert.ToString(this.objSdk.C_SdkGetLastError(GetCurrentSn()), 16));
            }
            else
            {
                MessageBox.Show("C_SetForbitCheckIn Success!");
            }
        }

        private void button52_Click(object sender, EventArgs e)
        {
            FaceServerSdkLib.AutoSleep fbcInfo = new FaceServerSdkLib.AutoSleep();
            long lRet = this.objSdk.C_GetForbitCheckIn(GetCurrentSn(),ref fbcInfo);
            if (lRet != 0)
            {
                MessageBox.Show("C_GetForbitCheckIn failed!ErrCode：" + Convert.ToString(this.objSdk.C_SdkGetLastError(GetCurrentSn()), 16));
            }
            else
            {
                AddInfo("GetForbitCheckIn success!TimeFrom1=" + fbcInfo.startHour0.ToString() + ":" + fbcInfo.startMinute0.ToString() + "-" + fbcInfo.endHour0.ToString() + ":" + fbcInfo.endMinute0.ToString() +
                                                                     " TimeFrom2=" + fbcInfo.startHour1.ToString() + ":" + fbcInfo.startMinute1.ToString() + "-" + fbcInfo.endHour1.ToString() + ":" + fbcInfo.endMinute1.ToString() +
                                                                     " TimeFrom3=" + fbcInfo.startHour2.ToString() + ":" + fbcInfo.startMinute2.ToString() + "-" + fbcInfo.endHour2.ToString() + ":" + fbcInfo.endMinute2.ToString() +
                                                                     " TimeFrom4=" + fbcInfo.startHour3.ToString() + ":" + fbcInfo.startMinute3.ToString() + "-" + fbcInfo.endHour3.ToString() + ":" + fbcInfo.endMinute3.ToString());
            }
        }

        private void button55_Click(object sender, EventArgs e)
        {
            FaceServerSdkLib.RecogInfo rgInfo = new FaceServerSdkLib.RecogInfo();
            rgInfo.IdentifyValue = int.Parse(textBox31.Text);
            rgInfo.UpdateValue = int.Parse(textBox32.Text);

            long lRet = this.objSdk.C_SetRecogThreshold(GetCurrentSn(), rgInfo);
            if (lRet != 0)
            {
                MessageBox.Show("C_SetRecogThreshold failed!ErrCode：" + Convert.ToString(this.objSdk.C_SdkGetLastError(GetCurrentSn()), 16));
            }
            else
            {
                MessageBox.Show("C_SetRecogThreshold Success!");
            }
        }

        private void button54_Click(object sender, EventArgs e)
        {
            FaceServerSdkLib.RecogInfo rgInfo = new FaceServerSdkLib.RecogInfo();
            long lRet = this.objSdk.C_GetRecogThreshold(GetCurrentSn(),ref rgInfo);
            if (lRet != 0)
            {
                MessageBox.Show("C_GetRecogThreshold failed!ErrCode：" + Convert.ToString(this.objSdk.C_SdkGetLastError(GetCurrentSn()), 16));
            }
            else
            {
                AddInfo("GetRecogThreshold success!lIdentifyValue=" + rgInfo.IdentifyValue.ToString() + " lUpdateValue=" + rgInfo.UpdateValue.ToString());
            }
        }

        private void button56_Click(object sender, EventArgs e)
        {
            long lRet = this.objSdk.C_SetScreenCalib(GetCurrentSn());
            if (lRet != 0)
            {
                MessageBox.Show("C_SetScreenCalib failed!ErrCode：" + Convert.ToString(this.objSdk.C_SdkGetLastError(GetCurrentSn()), 16));
            }
            else
            {
                MessageBox.Show("C_SetScreenCalib Success!");
            }
        }

        private void button57_Click(object sender, EventArgs e)
        {
            long lRet = this.objSdk.C_SystemReNew(GetCurrentSn());
            if (lRet != 0)
            {
                MessageBox.Show("C_SystemReNew failed!ErrCode：" + Convert.ToString(this.objSdk.C_SdkGetLastError(GetCurrentSn()), 16));
            }
        }

        private void button60_Click(object sender, EventArgs e)
        {
            long lRet = this.objSdk.C_Reboot(GetCurrentSn());
            if (lRet != 0)
            {
                MessageBox.Show("C_Reboot failed!ErrCode：" + Convert.ToString(this.objSdk.C_SdkGetLastError(GetCurrentSn()), 16));
            }
        }

        private void button58_Click(object sender, EventArgs e)
        {
            FaceServerSdkLib.AutoSleep auSleep = new FaceServerSdkLib.AutoSleep();

            long lRet = this.objSdk.C_GetAutoSleep(GetCurrentSn(),ref auSleep);
            if (lRet != 0)
            {
                MessageBox.Show("C_GetAutoSleep failed!ErrCode：" + Convert.ToString(this.objSdk.C_SdkGetLastError(GetCurrentSn()), 16));
            }
            else
            {
                AddInfo("GetAutoSleep success! Time1:" + auSleep.startHour0.ToString() + ":" + auSleep.startMinute0.ToString() + "-" + auSleep.endHour0.ToString() + ":" + auSleep.endMinute0.ToString() +
                                                                     " TimeFrom2=" + auSleep.startHour1.ToString() + ":" + auSleep.startMinute1.ToString() + "-" + auSleep.endHour1.ToString() + ":" + auSleep.endMinute1.ToString() +
                                                                     " TimeFrom3=" + auSleep.startHour2.ToString() + ":" + auSleep.startMinute2.ToString() + "-" + auSleep.endHour2.ToString() + ":" + auSleep.endMinute2.ToString() +
                                                                     " TimeFrom4=" + auSleep.startHour3.ToString() + ":" + auSleep.startMinute3.ToString() + "-" + auSleep.endHour3.ToString() + ":" + auSleep.endMinute3.ToString());
            }
        }

        private void button59_Click(object sender, EventArgs e)
        {
            FaceServerSdkLib.AutoSleep auSleep = new FaceServerSdkLib.AutoSleep();
            auSleep.startHour0 = int.Parse(textBox33.Text);
            auSleep.startMinute0 =  int.Parse(textBox34.Text);
            auSleep.endHour0 = int.Parse(textBox36.Text);
            auSleep.endMinute0 = int.Parse(textBox35.Text);
            auSleep.startHour1 = int.Parse(textBox38.Text);
            auSleep.startMinute1 = int.Parse(textBox37.Text);
            auSleep.endHour1 = int.Parse(textBox40.Text);
            auSleep.endMinute1 = int.Parse(textBox39.Text);
            auSleep.startHour2 = int.Parse(textBox42.Text);
            auSleep.startMinute2 =int.Parse(textBox41.Text);
            auSleep.endHour2 = int.Parse(textBox44.Text);
            auSleep.endMinute2 = int.Parse(textBox43.Text);
            auSleep.startHour3 = int.Parse(textBox46.Text);
            auSleep.startMinute3 = int.Parse(textBox45.Text);
            auSleep.endHour3 = int.Parse(textBox48.Text);
            auSleep.endMinute3 = int.Parse(textBox47.Text);
            long lRet = this.objSdk.C_SetAutoSleep(GetCurrentSn(),auSleep);
            if (lRet != 0)
            {
                MessageBox.Show("C_SetAutoSleep failed!ErrCode：" + Convert.ToString(this.objSdk.C_SdkGetLastError(GetCurrentSn()), 16));
            }
            else
            {
                MessageBox.Show("C_SetAutoSleep success!");
            }
        }

        private void button62_Click(object sender, EventArgs e)
        {
            FaceServerSdkLib.AutoSleepSwitch auSwitch = new FaceServerSdkLib.AutoSleepSwitch();
            long lRet = this.objSdk.C_GetAutoSleepSwitch(GetCurrentSn(),ref auSwitch);
            if (lRet != 0)
            {
                MessageBox.Show("C_GetAutoSleepSwitch failed!ErrCode：" + Convert.ToString(this.objSdk.C_SdkGetLastError(GetCurrentSn()), 16));
            }
            else
            {
                AddInfo("C_GetAutoSleepSwitch!Key0="+auSwitch.KeyOn0.ToString()+" Key1="+auSwitch.KeyOn1.ToString()+" Key2="+auSwitch.KeyOn2.ToString()+" Key3="+auSwitch.KeyOn3.ToString());
                MessageBox.Show("C_GetAutoSleepSwitch Success!");
            }
        }

        private void button61_Click(object sender, EventArgs e)
        {
            FaceServerSdkLib.AutoSleepSwitch auSwitch = new FaceServerSdkLib.AutoSleepSwitch();
            auSwitch.KeyOn0=(checkBox1.Checked?1:0);
            auSwitch.KeyOn1=(checkBox2.Checked?1:0);
            auSwitch.KeyOn2=(checkBox3.Checked?1:0);
            auSwitch.KeyOn3=(checkBox4.Checked?1:0);
            long lRet = this.objSdk.C_SetAutoSleepSwitch(GetCurrentSn(),auSwitch);
            if (lRet != 0)
            {
                MessageBox.Show("C_SetAutoSleepSwitch failed!ErrCode：" + Convert.ToString(this.objSdk.C_SdkGetLastError(GetCurrentSn()), 16));
            }
            else
            {
                MessageBox.Show("C_SetAutoSleepSwitch success!");
            }
        }

        private void button66_Click(object sender, EventArgs e)
        {
            FaceServerSdkLib.SafeConfig safecfg = new FaceServerSdkLib.SafeConfig();
            long lRet = this.objSdk.C_GetSafeConfig(GetCurrentSn(),ref safecfg);
            if (lRet != 0)
            {
                MessageBox.Show("C_GetSafeConfig failed!ErrCode：" + Convert.ToString(this.objSdk.C_SdkGetLastError(GetCurrentSn()), 16));
            }
            else
            {
                AddInfo("GetSafeConfig success!OpdPwd=" + safecfg.opPwd + " SdCardPwd=" + safecfg.sdPwd + " WebPwd=" + safecfg.webPwd);
            }
        }

        private void button67_Click(object sender, EventArgs e)
        {
            FaceServerSdkLib.SafeConfig safecfg = new FaceServerSdkLib.SafeConfig();
            safecfg.webPwd = textBox51.Text;
            safecfg.sdPwd = textBox52.Text;
            safecfg.opPwd = textBox53.Text;
            long lRet = this.objSdk.C_SetSafeConfig(GetCurrentSn(),safecfg);
            if (lRet != 0)
            {
                MessageBox.Show("C_SetSafeConfig failed!ErrCode：" + Convert.ToString(this.objSdk.C_SdkGetLastError(GetCurrentSn()), 16));
            }
            else
            {
                MessageBox.Show("C_SetSafeConfig Success!");
            }
        }

        private void button69_Click(object sender, EventArgs e)
        {
            int lValue = 0;
            long lRet = this.objSdk.C_GetHumanSense(GetCurrentSn(),ref lValue);
            if (lRet != 0)
            {
                MessageBox.Show("C_GetHumanSense failed!ErrCode：" + Convert.ToString(this.objSdk.C_SdkGetLastError(GetCurrentSn()), 16));
            }
            else
            {
                AddInfo("GetHumanSense success!Value=" + lValue.ToString());
            }
        }

        private void button68_Click(object sender, EventArgs e)
        {
            long lRet = this.objSdk.C_SetHumanSense(GetCurrentSn(), int.Parse(textBox54.Text));
            if (lRet != 0)
            {
                MessageBox.Show("C_SetHumanSense failed!ErrCode：" + Convert.ToString(this.objSdk.C_SdkGetLastError(GetCurrentSn()), 16));
            }
            else
            {
                MessageBox.Show("C_SetHumanSense success!");
            }
        }

        private void button70_Click(object sender, EventArgs e)
        {
            FaceServerSdkLib.StorageInfo sgCfg = new FaceServerSdkLib.StorageInfo();
            long lRet = this.objSdk.C_GetStorage(GetCurrentSn(),ref sgCfg);
            if (lRet != 0)
            {
                MessageBox.Show("C_GetStorage failed!ErrCode：" + Convert.ToString(this.objSdk.C_SdkGetLastError(GetCurrentSn()), 16));
            }
            else
            {
                AddInfo("GetStorage success!LogCount=" + sgCfg.LogCount.ToString() + " MaxLogCount=" + sgCfg.MaxLogCount.ToString() + " MaxRecordCount=" + sgCfg.MaxRecordCount.ToString() + " MaxTMPLCount=" + sgCfg.MaxTMPLCount.ToString() + " MaxUserCount=" + sgCfg.MaxUserCount.ToString() + " RecordCount=" + sgCfg.RecordCount.ToString() + " TMPLCount=" + sgCfg.TMPLCount.ToString() + " UserCount=" + sgCfg.UserCount.ToString());
            }
        }

        private void button72_Click(object sender, EventArgs e)
        {
            string strDevSn = "";
            long nRet=this.objSdk.C_Connect(textBox55.Text, int.Parse(textBox56.Text), textBox57.Text, textBox58.Text,ref strDevSn);
            if (nRet==0)
            {
                MessageBox.Show("Connect Success!");
            }
            else
            {
                MessageBox.Show("Connect Failed!");
            }
        }

        private void button71_Click(object sender, EventArgs e)
        {
            long nRet = this.objSdk.C_DisConnect(GetCurrentSn());
            if (nRet == 0)
            {
                MessageBox.Show("Disconnect Success!");
            }
            else
            {
                MessageBox.Show("Disconnect failed!");
            }
        }

        private void button82_Click(object sender, EventArgs e)
        {
            FaceServerSdkLib.IOConfig io = new FaceServerSdkLib.IOConfig();
            long lRet = this.objSdk.C_GetIOConfig(GetCurrentSn(),ref io);
            if (lRet != 0)
            {
                MessageBox.Show("C_GetIOConfig failed!ErrCode：" + Convert.ToString(this.objSdk.C_SdkGetLastError(GetCurrentSn()), 16));
            }
            else
            {
                AddInfo("GetIoConfig success!AlarmOn=" + io.AudioStatus.ToString() + " FireOn=" + io.FireStatus.ToString() + " GateDelay=" + io.DoorStatus.ToString() + " GateInputOn=" + io.InputStatus.ToString() + " SwitchOn=" + io.SwitchStatus.ToString());
            }
        }

        private void button81_Click(object sender, EventArgs e)
        {
            FaceServerSdkLib.IOConfig io = new FaceServerSdkLib.IOConfig();
            io.InputStatus=int.Parse(textBox82.Text);
            io.DoorStatus=int.Parse(textBox83.Text);
            io.FireStatus=int.Parse(textBox84.Text);
            io.SwitchStatus=int.Parse(textBox85.Text);
            io.AudioStatus=int.Parse(textBox86.Text);
            long lRet = this.objSdk.C_SetIOConfig(GetCurrentSn(),io);
            if (lRet != 0)
            {
                MessageBox.Show("C_SetIOConfig failed!ErrCode：" + Convert.ToString(this.objSdk.C_SdkGetLastError(GetCurrentSn()), 16));
            }
            else
            {
                MessageBox.Show("C_SetIOConfig success!");
            }
        }

        private void button74_Click(object sender, EventArgs e)
        {
            long lRet = this.objSdk.C_SetBell(GetCurrentSn(), int.Parse(textBox59.Text));
            if (lRet != 0)
            {
                MessageBox.Show("C_SetBell failed!ErrCode：" + Convert.ToString(this.objSdk.C_SdkGetLastError(GetCurrentSn()), 16));
            }
            else
            {
                MessageBox.Show("C_SetBell success!");
            }
        }

        private void button73_Click(object sender, EventArgs e)
        {
            int lValue = 0;
            long lRet = this.objSdk.C_GetBell(GetCurrentSn(),ref lValue);
            if (lRet != 0)
            {
                MessageBox.Show("C_GetBell failed!ErrCode：" + Convert.ToString(this.objSdk.C_SdkGetLastError(GetCurrentSn()), 16));
            }
            else
            {
                AddInfo("GetBell success!lBell=" + lValue.ToString());
            }
        }

        private void button76_Click(object sender, EventArgs e)
        {
            FaceServerSdkLib.RingInfo rgInfo = new FaceServerSdkLib.RingInfo();
            rgInfo.Hour_0=int.Parse(textBox60.Text);
            rgInfo.Minute_0=int.Parse(textBox61.Text);
            rgInfo.RingIndex_0=int.Parse(textBox62.Text);
            rgInfo.RingOn_0=int.Parse(textBox63.Text);

            rgInfo.Hour_1=int.Parse(textBox67.Text);
            rgInfo.Minute_1=int.Parse(textBox66.Text);
            rgInfo.RingIndex_1=int.Parse(textBox65.Text);
            rgInfo.RingOn_1=int.Parse(textBox64.Text);

            rgInfo.Hour_2=int.Parse(textBox71.Text);
            rgInfo.Minute_2=int.Parse(textBox70.Text);
            rgInfo.RingIndex_2=int.Parse(textBox69.Text);
            rgInfo.RingOn_2=int.Parse(textBox68.Text);

            rgInfo.Hour_3=int.Parse(textBox75.Text);
            rgInfo.Minute_3=int.Parse(textBox74.Text);
            rgInfo.RingIndex_3=int.Parse(textBox73.Text);
            rgInfo.RingOn_3=int.Parse(textBox72.Text);

            rgInfo.Hour_4=int.Parse(textBox79.Text);
            rgInfo.Minute_4=int.Parse(textBox78.Text);
            rgInfo.RingIndex_4=int.Parse(textBox77.Text);
            rgInfo.RingOn_4=int.Parse(textBox76.Text);

            long lRet = this.objSdk.C_SetRing(GetCurrentSn(), rgInfo);
            if (lRet != 0)
            {
                MessageBox.Show("C_SetRing failed!ErrCode：" + Convert.ToString(this.objSdk.C_SdkGetLastError(GetCurrentSn()), 16));
            }
            else
            {
                MessageBox.Show("C_SetRing success!");
            }
        }

        private void button75_Click(object sender, EventArgs e)
        {
            FaceServerSdkLib.RingInfo rgInfo = new FaceServerSdkLib.RingInfo();
            long lRet = this.objSdk.C_GetRing(GetCurrentSn(),ref rgInfo);
            if (lRet != 0)
            {
                MessageBox.Show("C_GetRing failed!ErrCode：" + Convert.ToString(this.objSdk.C_SdkGetLastError(GetCurrentSn()), 16));
            }
            else
            {
                AddInfo("GetRing success!Hour0=" + rgInfo.Hour_0.ToString() + " Minute0=" + rgInfo.Minute_0.ToString() + " SoundIndex0=" + rgInfo.RingIndex_0.ToString() + " RingOn0=" + rgInfo.RingOn_0.ToString()
                                      + "Hour1=" + rgInfo.Hour_1.ToString() + " Minute1=" + rgInfo.Minute_1.ToString() + " SoundIndex1=" + rgInfo.RingIndex_1.ToString() + " RingOn1=" + rgInfo.RingOn_1.ToString()
                                      + "Hour2=" + rgInfo.Hour_2.ToString() + " Minute2=" + rgInfo.Minute_2.ToString() + " SoundIndex2=" + rgInfo.RingIndex_2.ToString() + " RingOn2=" + rgInfo.RingOn_2.ToString()
                                      + "Hour3=" + rgInfo.Hour_3.ToString() + " Minute3=" + rgInfo.Minute_3.ToString() + " SoundIndex3=" + rgInfo.RingIndex_3.ToString() + " RingOn3=" + rgInfo.RingOn_3.ToString()
                                      + "Hour4=" + rgInfo.Hour_4.ToString() + " Minute4=" + rgInfo.Minute_4.ToString() + " SoundIndex4=" + rgInfo.RingIndex_4.ToString() + " RingOn4=" + rgInfo.RingOn_4.ToString());
            }
        }

        private void button78_Click(object sender, EventArgs e)
        {
            FaceServerSdkLib.AudioInfo au = new FaceServerSdkLib.AudioInfo();
            au.VolumeStatus=(radioButton3.Checked?1:0);
            au.SoundStatus=(radioButton6.Checked?1:0);
            au.VolumeLevel=int.Parse(textBox80.Text);
            long lRet = this.objSdk.C_SetAudio(GetCurrentSn(),au);
            if (lRet != 0)
            {
                MessageBox.Show("C_SetAudio failed!ErrCode：" + Convert.ToString(this.objSdk.C_SdkGetLastError(GetCurrentSn()), 16));
            }
            else
            {
                MessageBox.Show("C_SetAudio success!");
            }
        }

        private void button77_Click(object sender, EventArgs e)
        {
            FaceServerSdkLib.AudioInfo au = new FaceServerSdkLib.AudioInfo();
            long lRet = this.objSdk.C_GetAudio(GetCurrentSn(),ref au);
            if (lRet != 0)
            {
                MessageBox.Show("C_GetAudio failed!ErrCode：" + Convert.ToString(this.objSdk.C_SdkGetLastError(GetCurrentSn()), 16));
            }
            else
            {
                AddInfo("GetAudio success!StatusKeySound=" + au.VolumeStatus.ToString() + " StatusVolume=" + au.SoundStatus.ToString() + " VolumeLevel=" + au.VolumeLevel.ToString());
            }
        }

        private void button80_Click(object sender, EventArgs e)
        {
            long lRet = this.objSdk.C_SetRingDuration(GetCurrentSn(), int.Parse(textBox81.Text));
            if (lRet != 0)
            {
                MessageBox.Show("C_SetRingDuration failed!ErrCode：" + Convert.ToString(this.objSdk.C_SdkGetLastError(GetCurrentSn()), 16));
            }
            else
            {
                MessageBox.Show("C_SetRingDuration success!");
            }
        }

        private void button79_Click(object sender, EventArgs e)
        {
            int lValue = 0;
            long lRet = this.objSdk.C_GetRingDuration(GetCurrentSn(),ref lValue);
            if (lRet != 0)
            {
                MessageBox.Show("C_GetRingDuration failed!ErrCode：" + Convert.ToString(this.objSdk.C_SdkGetLastError(GetCurrentSn()), 16));
            }
            else
            {
                AddInfo("GetRingDuration success!Value=" + lValue.ToString());
            }
        }

        private void button12_Click(object sender, EventArgs e)
        {
            string sMode = "";
            long lRet= objSdk.C_GetDeviceModel(GetCurrentSn(),ref sMode);

            txtDvsMode.Text = sMode;
        }

        private void button13_Click(object sender, EventArgs e)
        {
            string sSn="";
            long lRet = objSdk.C_GetDeviceSn(GetCurrentSn(), ref sSn);
            txtDvSn.Text = sSn;
        }

    }
}

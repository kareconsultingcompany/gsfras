USE [GSFRAS]
GO
/****** Object:  Table [dbo].[Academic]    Script Date: 11/10/2014 10:47:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Academic](
	[AID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NULL,
 CONSTRAINT [PK_Academic] PRIMARY KEY CLUSTERED 
(
	[AID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Department]    Script Date: 11/10/2014 10:47:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Department](
	[DID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NULL,
 CONSTRAINT [PK_Department] PRIMARY KEY CLUSTERED 
(
	[DID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Device]    Script Date: 11/10/2014 10:47:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Device](
	[Name] [varchar](50) NULL,
	[Location] [varchar](200) NULL,
	[IpAddress] [varchar](20) NULL,
	[AccessUserName] [varchar](50) NULL,
	[AccessPassword] [varchar](20) NULL,
	[LastSync] [float] NULL,
	[CreateDate] [float] NULL,
	[DeviceSVN] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Device] PRIMARY KEY CLUSTERED 
(
	[DeviceSVN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DeviceEmployee]    Script Date: 11/10/2014 10:47:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DeviceEmployee](
	[EmployeeID] [int] NOT NULL,
	[userType] [int] NULL,
	[CardNo] [varchar](20) NOT NULL,
	[UserName] [varchar](50) NULL,
	[VerifyMode] [int] NOT NULL CONSTRAINT [DF_DeviceEmployee_VerifyMode]  DEFAULT ((1)),
	[RegStatus] [int] NOT NULL CONSTRAINT [DF_DeviceEmployee_RegStatus]  DEFAULT ((1)),
	[PhotoLen] [int] NOT NULL CONSTRAINT [DF_DeviceEmployee_PhotoLen]  DEFAULT ((0)),
	[Base64PhotoData] [varchar](max) NOT NULL,
	[FeatureLen] [int] NOT NULL CONSTRAINT [DF_DeviceEmployee_FeatureLen]  DEFAULT ((0)),
	[FeatureBase64] [varchar](max) NOT NULL,
 CONSTRAINT [PK_DeviceEmployee] PRIMARY KEY CLUSTERED 
(
	[EmployeeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Employee]    Script Date: 11/10/2014 10:47:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Employee](
	[EmployeeID] [int] NOT NULL,
	[Name] [varchar](20) NULL,
	[HKID] [nchar](20) NULL,
	[DOB] [float] NULL,
	[RelationshipStatus] [int] NULL,
	[Address] [varchar](200) NULL,
	[Tel] [int] NULL,
	[Mobile] [int] NULL,
	[Email] [varchar](50) NULL,
	[Type] [varchar](20) NULL,
	[Postion] [int] NULL,
	[Department] [int] NULL,
	[Academic] [int] NULL,
	[JoinDate] [float] NULL,
	[QuitDate] [float] NULL,
	[BasicSalary] [decimal](18, 0) NULL,
	[OT] [decimal](18, 0) NULL,
	[AnnualLeave] [float] NULL,
	[ActualAnnualLeave] [float] NULL,
	[MPF] [float] NULL,
	[Remark] [varchar](200) NULL,
	[LastUser] [int] NULL,
	[CreateDate] [float] NULL,
 CONSTRAINT [PK_Employee] PRIMARY KEY CLUSTERED 
(
	[EmployeeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[EmployeeAttendanceRecord]    Script Date: 11/10/2014 10:47:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[EmployeeAttendanceRecord](
	[ARID] [nvarchar](50) NOT NULL,
	[TransactionDate] [float] NULL,
	[DeviceID] [float] NULL,
	[EmployeeID] [int] NULL,
	[RecType] [int] NULL,
	[Status] [int] NULL,
	[PhotoLen] [int] NULL,
	[Reason] [int] NULL,
	[Base64PhotoData] [varchar](max) NULL,
 CONSTRAINT [PK_EmployeeAttendanceRecord] PRIMARY KEY CLUSTERED 
(
	[ARID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Position]    Script Date: 11/10/2014 10:47:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Position](
	[PID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NULL,
 CONSTRAINT [PK_Position] PRIMARY KEY CLUSTERED 
(
	[PID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[RelationshipStatus]    Script Date: 11/10/2014 10:47:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[RelationshipStatus](
	[RSID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NULL,
 CONSTRAINT [PK_RelationshipStatus] PRIMARY KEY CLUSTERED 
(
	[RSID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SLogs]    Script Date: 11/10/2014 10:47:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SLogs](
	[nId] [bigint] IDENTITY(1,1) NOT NULL,
	[dtDate] [datetime] NOT NULL,
	[sThread] [varchar](100) NOT NULL,
	[sLevel] [varchar](200) NOT NULL,
	[sLogger] [varchar](500) NOT NULL,
	[sMessage] [varchar](3000) NOT NULL,
	[sException] [varchar](4000) NULL,
	[sUid] [varchar](50) NULL,
 CONSTRAINT [PK_ErrorLog] PRIMARY KEY CLUSTERED 
(
	[nId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SysPara]    Script Date: 11/10/2014 10:47:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SysPara](
	[ID] [int] NOT NULL,
	[Company] [varchar](200) NULL,
	[Logo] [varchar](100) NULL,
	[Address] [varchar](200) NULL,
	[Lang] [varchar](20) NULL,
	[Curr] [varchar](50) NULL,
	[Tel] [int] NULL,
	[Fax] [int] NULL,
	[Email] [varchar](50) NULL,
	[Url] [varchar](50) NULL,
 CONSTRAINT [PK_SysPara] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[UserLog]    Script Date: 11/10/2014 10:47:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[UserLog](
	[UserID] [int] NOT NULL,
	[Pgm] [varchar](50) NULL,
	[Detail] [varchar](50) NULL,
	[CreateTime] [float] NULL,
 CONSTRAINT [PK_UserLog] PRIMARY KEY CLUSTERED 
(
	[UserID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[UserProfile]    Script Date: 11/10/2014 10:47:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[UserProfile](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Password] [varchar](50) NULL,
	[Name] [varchar](50) NOT NULL,
	[UserLevel] [int] NULL,
	[Txlog] [int] NULL,
	[Reclog] [int] NULL,
	[SysRight] [int] NULL,
	[UserLock] [int] NULL,
	[LastUser] [int] NULL,
	[CreateDate] [float] NULL,
	[Lang] [varchar](50) NULL,
	[UserName] [nvarchar](50) NULL,
 CONSTRAINT [PK_UserProfile] PRIMARY KEY CLUSTERED 
(
	[ID] ASC,
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO

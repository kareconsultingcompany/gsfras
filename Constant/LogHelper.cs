﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Constant
{
  public  class LogHelper
    {
      private static readonly String  UID="sUid";
        private static log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public static void Inital(){
            System.IO.FileInfo fileInfo = new System.IO.FileInfo("Log.config");
            log4net.Config.XmlConfigurator.Configure(fileInfo);
        }

        public static void SetReportClass(Type type) {
            log = log4net.LogManager.GetLogger(type);
        }

        public static void Debug(String message,string uid) {
            log4net.LogicalThreadContext.Properties[UID] = uid;
            log.Debug(message);
        }

        public static void Debug(String message, Exception ex,string uid) {
            log4net.LogicalThreadContext.Properties[UID] = uid;
            log.Debug(message,ex);
        }

        public static void Info(String message,String uid) {
            log4net.LogicalThreadContext.Properties[UID] = uid;
            log.Info(message);
        }
        public static void Info(String message,Exception ex,string uid) {
            log4net.LogicalThreadContext.Properties[UID] = uid;
            log.Info(message,ex);
        }

        public static void Warn(String message,string uid) {
            log4net.LogicalThreadContext.Properties[UID] = uid;
            log.Warn(message);
        }

        public static void Warn(String message,Exception ex,string uid) {
            log4net.LogicalThreadContext.Properties[UID] = uid;
            log.Warn(message,ex);
        }

        public static void Error(String message,string uid) {
            log4net.LogicalThreadContext.Properties[UID] = uid;
            log.Error(message);
        }

        public static void Error(String message, Exception ex,string uid) {
            log4net.LogicalThreadContext.Properties[UID] = uid;
            log.Error(message,ex);
        }

        public static void Fatal(String message,string uid) {
            log4net.LogicalThreadContext.Properties[UID] = uid;
            log.Fatal(message);
        }

        public static void Fatal(String message,Exception ex,string uid){
            log4net.LogicalThreadContext.Properties[UID] = uid;
            log.Fatal(message,ex);
        }
    }
}

﻿using System;
using System.Drawing;
using System.IO;

namespace Constant
{
    public static class Constants
    {
        public static readonly String VERSION = "Version: 1.0";
        public static readonly String PRODUCT_ID = "Product ID: KARE-99-FRAS-001";
        public static readonly int SERVER_PORT=30000;
        public static readonly String DEFAULT_NULL_64="R0lGODlhBwAGAPcAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACH5BAEAAP8ALAAAAAAHAAYAAAgNAP8JHEiwoMGDCA0GBAA7";
        public static readonly int TIME_ZONE = 8;
        public static readonly String DATETIME_FORMAT = "yyyy-MM-dd HH:mm:ss";
        public static readonly String DATE_FORMAT = "yyyy-MM-dd";
        public static readonly String TIME_FORMAT = "HH:mm:ss";

        public static byte[] ImageFromBase64String(string base64String)
        {
            try
            {
                String base64 = base64String;
                if(base64String==null||base64String==""){
                    base64 = DEFAULT_NULL_64;
                }
                byte[] imageBytes = Convert.FromBase64String(base64);
                /*MemoryStream ms = new MemoryStream(imageBytes, 0,
                  imageBytes.Length);
                ms.Write(imageBytes, 0, imageBytes.Length);
                Image image = Image.FromStream(ms, true);*/
                return imageBytes;
            }
            catch (Exception) { return null; }
        }

        public static Image Base64toImage(string base64String)
        {
            try
            {
                String base64 = base64String;
                if (base64String == null || base64String == "")
                {
                    base64 = DEFAULT_NULL_64;
                }
                byte[] imageBytes = Convert.FromBase64String(base64);
                MemoryStream ms = new MemoryStream(imageBytes, 0,
                  imageBytes.Length);
                ms.Write(imageBytes, 0, imageBytes.Length);
                Image image = Image.FromStream(ms, true);
                return image;
            }
            catch (Exception) { return null; }
        }

        public static DateTime ConvetTimestamp(double timestamp,int timezone) {
            DateTime dt = (new DateTime(1970, 1, 1, 0, 0, 0)).AddHours(timezone).AddSeconds(timestamp);
            return dt;
        }

        public static double ConvertDateToTimestamp(DateTime dt,int timezone) {
            dt = DateTime.Parse(dt.ToString(DATETIME_FORMAT));
            return (dt.AddHours(-timezone) - new DateTime(1970, 1, 1, 0, 0, 0)).TotalSeconds;
        }

        public static DateTime ConvertStandardDate(String t) {
            DateTime c = DateTime.Parse(t);
            return c;
        }

    }
}
